# pyTG Package

This is a python package *pyTG* develop for simualte vascular pathologies
development in ODEs model as for Jansen, Donadoni.
The documentation of the package is in *docs/*.

## Class made for general analytical and CFD hemodynamic

1. Artery
2. Fluid
3. Hemodynamic
4. Mesh
5. CFD_OpenFOAM
6. ModelTG_1D
7. ModelTG_2D
## Class for tissus growth modeling

List of subpackages

1. Jansen
- In monodimensional artery with self-consistency hypothesis
- In monodimensional artery with remodeling sensitivity hypothesis (*jansen_1D.py* and *couplingHemoTG_1D.py*)
- In 2D axisymetric geometry with remodeling sensitivity hypothesis (*jansen_2D.py* and *couplingHemoTG_2D.py*)
3. Donadoni ()
- *Donadoni_1D.py*
- *Donadoni_2D.py*

## Functions 
1. *utilities.py*, a collection of function used for example in postProcessing

## How to run simulation

The *scripts/* at the root of the package contains a collection of executable python scripts.
Please read the doc or execute (*runPyTG_Jansen_2D_fi -h*). Those scripts need a file *____.ini*

# Installation & test

Recommadation is to create a python env as :
```console
cd ~
mkdir test_pyTG
cd test_pyTG/
git clone https://gitlab.com/jrme.jansen/pytg.git
# create your python env
python3 -m venv my_env_path
source my_env_path/bin/activate
# test, pip3 from my_env_path/bin/pip3 ?
which pip3
cd pytg/
# install required packages
pip3 install -r requirements.txt
# install the package
python3 setup.py develop
# test the install my running the test-case
cd ../
# copy 1D test-case
cp -r pytg/tutorials/jansen_1D run_jansen_1D
cd run_jansen_1D/
# run and enjoy ! 
../my_env_py_tg/bin/runPyTG_Jansen_1D_fi -file run.ini -savedObj cpJansen
../my_env_py_tg/bin/postProcessBetween -cpObj cpJansen -normTime day -dir ppAllSimu
```

During execution of ***pyTG***, as only 1D configuration codes are available now, it is possible that import errors arise as :
```python
ModuleNotFoundError: No module named 'pyTG.cfd_OpenFoam'
```
If it happens, please go into source code and comment the problematic line.

