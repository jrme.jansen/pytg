#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from icecream import ic
import os
import pickle
import numpy.ma as ma
import numpy as np
import glob
from warnings import warn
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib.colors as colors
plt.rc('font', family='serif', size='20')
plt.rcParams["figure.figsize"] = (20,16)
from mpl_toolkits.mplot3d import Axes3D

from pyTG.utilities import find_last_gene, findIndex_t0tf, message
# shading parameter for pcolormesh 2D post processing
shading_2D = "gouraud" # "flat" # "gouraud"
AZIM = -60
DIST = 10
ELEV = 5

dictCMAP = {"jet": cm.jet,
            "grey": cm.Greys,
            "viridis": cm.viridis}


def plottingGFs_3D(iniF, path_abs, set_params, t0=None, tf=None, setting_t=('day',1.0),
                   folder='', r1=None, r2=None, points=[],
                   pcm=True, noAxis=False, colormap='jet', dpi=100, scalingFile=''):
    """plot GFs in 3D cas

    Parameters
    ----------
    path_abs : str
        The path
    set_params : dict
        A big dictionnary with lots of data
    t0 : float
        Initial time
    tf : float
        Final time
    setting_t : tuple

    folder : str

    r1 : float
        First position normalized by init radius
    r2 : float
        Last position normalized by init radius
    points : list
        list of specifics points where data will be plotted as function of time.
        Give x and theta coordinates
    colormap : str
        The type of colormap, avail : ``jet``, ``grey``, ``viridis`` 

    Returns
    -------

    """
    myCMAP = dictCMAP[colormap]

    message('GF 3D plotting')
    if scalingFile!='':
        try:
            scalingGFs = np.loadtxt('{}/{}'.format(path_abs, scalingFile))
        except OSError:
            print("{} not found in {}".format(scalingFile, path_abs))
            scaling = False
        else:
            print("Successfully loaded {}".format(scalingFile))
            scaling = True
            [NOi_nd_min, NOi_nd_max, NOm_nd_min, NOm_nd_max,
             PDGFi_nd_min, PDGFi_nd_max, PDGFm_nd_min, PDGFm_nd_max,
             FGFi_nd_min, FGFi_nd_max, FGFm_nd_min, FGFm_nd_max,
             Agi_nd_min, Agi_nd_max, Agm_nd_min, Agm_nd_max,
             TGFi_nd_min, TGFi_nd_max, TGFm_nd_min, TGFm_nd_max,
             TNFi_nd_min, TNFi_nd_max, TNFm_nd_min, TNFm_nd_max,
             MMPi_nd_min, MMPi_nd_max, MMPm_nd_min, MMPm_nd_max] = scalingGFs
            scal_l = [[NOi_nd_min, NOi_nd_max], [NOm_nd_min, NOm_nd_max],
                      [PDGFi_nd_min, PDGFi_nd_max], [PDGFm_nd_min, PDGFm_nd_max],
                      [FGFi_nd_min, FGFi_nd_max], [FGFm_nd_min, FGFm_nd_max],
                      [Agi_nd_min, Agi_nd_max], [Agm_nd_min, Agm_nd_max],
                      [TGFi_nd_min, TGFi_nd_max], [TGFm_nd_min, TGFm_nd_max],
                      [TNFi_nd_min, TNFi_nd_max], [TNFm_nd_min, TNFm_nd_max],
                      [MMPi_nd_min, MMPi_nd_max], [MMPm_nd_min, MMPm_nd_max]]
    else:
        # if not scaling creation of it
        scalingGFs = []
        scalingGFs_com = []

    print('GF 2D r1 {} r2 {}, colormap {}'.format(r1,r2, colormap))
    sPath = '%s/%s/GFs' % (path_abs, folder)
    try:
        os.mkdir(sPath)
    except OSError:
        print ("Creation of the directory \n %s \nfailed" % sPath )
    else:
        print ("Successfully created the directory \n %s " % sPath)
    if points:
        if len(points) % 2 != 0: raise ValueError('len(points) % 2 != 0')
        sPath_spe = '%s/specificPoints' % (sPath)
        try:
            os.mkdir(sPath_spe)
        except OSError:
            print ("Creation of the directory \n %s \nfailed" % sPath_spe )
        else:
            print ("Successfully created the directory \n %s " % sPath_spe)
        # tranform to table of x, theta coord
        points_arr =  np.array(points).reshape(int(len(points)*.5), 2)
        # ic(points_arr)

    xlab, normTime = setting_t
    paths = glob.glob('gene*')
    path_last_gene = find_last_gene(paths)

    path_data = \
        '%s/%s/tissueGrowth/' % (path_abs, path_last_gene)

    t_interp = np.load('%s/t_interp.npy' % path_data)
    t_gene = np.load('%s/t_gene.npy' % path_data)
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    # load mesh data
    xE = np.load('%s/xE_TG.npy' % path_data)
    thE = np.load('%s/thE_TG.npy' % path_data)
    xP = np.load('%s/xP_TG.npy' % path_data)
    
    X_grid, T_grid = np.meshgrid(xE, thE)
    xfla = X_grid.flatten('F')
    thfla = T_grid.flatten('F')
    xth_TG = np.vstack((xfla, thfla)).T

    Np_th = len(thE)
    Ni_x = len(xE)
    xE_star = xE / R0
    # check if r1,r2 are well defined
    default = r1==None and r2==None
    if default:
        # if None for the both, then default values, we plot for all along the
        # artery !
        print('\n default values for r1 r2')
        r1 = 0.0
        r2 = xE_star[-1]
    else:
        notGood = r1 < 0.0 or r2 > xE_star[-1]
        if notGood:
            raise ValueError('r1 < 0.0 or r2 > xE_star[-1]')
        # mask_xE_star = ma.masked_outside(xE_star, r1, r2)
        # xE_star = mask_xE_star[~mask_xE_star.mask].data

    X_star, T = np.meshgrid(xE_star, thE)
    Z = np.zeros_like(T)
    print('X_start.shape {} T.shape {}'.format(X_star.shape, T.shape))
    
    (i_t0, i_tf, ie_t0, ie_tf) = findIndex_t0tf(t_interp, t_gene, t0, tf)
    t_interp = t_interp[i_t0:i_tf] / normTime
    t_gene = t_gene[ie_t0:ie_tf] / normTime
    N_gene = len(t_gene)
    print('t_gene.shape', t_gene.shape)

    ######################################################################
    # locate index where event occurs
    ######################################################################
    print('t_gene', t_gene)
    idxEvents = []
    t_plots = t_gene
    # t_plots = t_gene[1:-1]
    for ti in t_plots:
        idx_i = np.argwhere(np.isclose(t_interp, ti))[:,0]
        if idx_i.size > 1:
            raise ValueError('More than 1 time event found')
        idxEvents.append(idx_i[0])
    print('len(idxEvents)', len(idxEvents))
    if len(idxEvents) == 0:
        raise ValueError('not event found')

    ######################################################################
    # Load data
    ######################################################################
    Y_interp_nd = np.load('%s/Y_interp_nd.npy' % path_data)[:,i_t0:i_tf,:]
    Y_interp_nd_ev = Y_interp_nd[:, idxEvents, :]
    # load GFs
    if set_params['model'][:6] == 'Jansen':
        NGFs = len(set_params['GFs_nd'])
        for j in range(NGFs):
            # create folder for each species 
            sGFs = '%s/%s' % (sPath, set_params['GFs_nd'][j])
            try: os.mkdir(sGFs)
            except OSError: print ("Creation of %s failed" % sGFs)
            else: print("Successfully created of %s " % sGFs)
            GFs_nd_ev_j = Y_interp_nd_ev[j, :, :]
            # create normalized map for colormap      
            # if default:                             
            if scalingFile!='':
                vmin = scal_l[j][0]
                vmax = scal_l[j][1]
            else:
                vmin = GFs_nd_ev_j.min()                  
                vmax = GFs_nd_ev_j.max()                  
                scalingGFs += [vmin, vmax]
                scalingGFs_com += [set_params['GFs_nd'][j]+"_min ",
                        set_params['GFs_nd'][j]+"_max "]
            norm = colors.Normalize(vmin=vmin, vmax=vmax)
            print('{} : vmin = {}, vmax = {}'.format(set_params['GFs_nd'][j],
                    vmin, vmax))

            fig = plt.figure()
            ax = plt.axes(projection='3d')
            ax.grid(False)
            print('GFs_nd_ev_j.shape', GFs_nd_ev_j.shape)
            # The azimuth is the rotation around the z axis
            ax.azim = AZIM
            # dist seems to be the distance from the center visible point in data coordinates.
            ax.dist = DIST
            # From this we understand that elev is the angle between the eye and the xy plane.
            ax.elev = ELEV
            for g in range(len(idxEvents)):
                gene_arr = Z + g
                GFs_ev_j_g_resh = GFs_nd_ev_j[g,:].reshape(Np_th, Ni_x, order='F')
                ### plot each gene
                fig_g, ax_g = plt.subplots()
                levels = 80
                # cs = ax.contourf(T,X,Y_interp_nd[j, :, :].T,levels,cmap=myCMAP)
                cs = ax_g.pcolormesh(X_star, T, GFs_ev_j_g_resh,
                        shading=shading_2D, vmin=vmin, vmax=vmax,
                            cmap=myCMAP,edgecolors='k')
                ax_g.set_ylabel(r'$\theta$')
                ax_g.set_xlabel('x/R0')
                ax_g.set_xlim(r1,r2)
                ax_g.set_title('{} t={}, vmin={:.4} vmax={:.4}'.format(
                    set_params['GFs_nd'][j], t_plots[g], GFs_ev_j_g_resh.min(),
                        GFs_ev_j_g_resh.max()))
                cb_g = fig_g.colorbar(cs)
                fig_g.savefig('%s/%s_g%s' % (sGFs, set_params['GFs_nd'][j], g))
                if noAxis:
                    ax_g.set_title('')
                    plt.axis('off')
                    cb_g.remove()
                    fig_g.savefig('{}/{}_g{}_noAxis'.format(
                        sGFs, set_params['GFs_nd'][j], g),
                        bbox_inches='tight', pad_inches=0, dpi=dpi)
                plt.close()


                if not default:
                    # fill on nan value
                    GFs_ev_j_g_resh[X_star > r2] = np.nan
                    GFs_ev_j_g_resh[X_star < r1] = np.nan
                    gene_arr[X_star > r2] = np.nan
                    gene_arr[X_star < r1] = np.nan
                ax.plot_surface(X_star, T, gene_arr,
                        rstride=1, cstride=1,
                        facecolors=myCMAP(norm(GFs_ev_j_g_resh)))
            ax.set_zticks([g for g in range(len(idxEvents))])
            ax.set_zticklabels(np.around(t_plots, 1))
            ax.set_xlabel(r'x/R0')
            ax.set_ylabel(r'$\theta$')
            ax.set_zlabel(r't ({})'.format(xlab))
            if not default:
                ax.set_xlim(r1, r2)
            ax.set_title('%s' % set_params['GFs_nd'][j])
            # m = cm.ScalarMappable(cmap=cm.rainbow)
            m = cm.ScalarMappable(cmap=myCMAP)
            m.set_array(np.linspace(vmin,vmax))
            cbar = fig.colorbar(m)
            cbar.ax.set_ylabel(r'{}'.format(set_params['GFs_nd'][j]))
            fig.tight_layout()
            fig.subplots_adjust()  # adjust fig, remove white space
            ax.set_title('vmin={:.4}, vmax={:.4}'.format(vmin, vmax))
            plt.savefig('%s/%s' % (sPath, set_params['GFs_nd'][j]))
            if noAxis:
                ax.set_title('')
                plt.axis('off')
                cbar.remove()
                plt.savefig('%s/%s_noAxis' % (sPath, set_params['GFs_nd'][j]),
                        bbox_inches='tight', pad_inches=0, dpi=dpi)
            plt.close()

            # fig, ax = plt.subplots()
            # levels = 80
            # cs = ax.contourf(T,X,Y_interp_nd[j, :, :].T,levels,cmap=myCMAP)
            # cs = ax.pcolormesh(T, X, Y_interp_nd[j, :, :].T,
                                # shading = shading_2D,
                                # vmin = Y_interp_nd[j, :, :].min(),
                                # vmax = Y_interp_nd[j, :, :].max(),
                                # cmap=myCMAP, edgecolors='k')
            # ax.set_xlabel('%s' % xlab)
            # ax.set_ylim(r1,r2)
            # ax.set_ylabel('x/R0')
            # ax.set_title('%s' % set_params['GFs_nd'][j])
            # plt.colorbar(cs)
            # plt.savefig('%s/%s' % (sPath, set_params['GFs_nd'][j]))
            # plt.close()

        if scalingFile == '':
            np.savetxt("scalingGFs.dat", np.array(scalingGFs).reshape(1,len(scalingGFs)),
                    header=' '.join(scalingGFs_com), fmt='%1.4E')

        if points:
            # specific points plot
            idxs = []
            print('specific points plot are at :')
            for k in range(points_arr.shape[0]):
                idxs.append(
                        np.linalg.norm(np.abs(xth_TG - points_arr[k]),
                            axis=1).argmin()
                        )
                print('points_arr[k]', points_arr[k],
                      'correspond to xE_stat:', xth_TG[idxs[k]])
            np.savetxt('%s/specificPoints.dat' % (sPath_spe), xth_TG[idxs],
                    header='points %s' % xth_TG[idxs])

            for j in range(NGFs):
                plt.figure()
                for k in range(points_arr.shape[0]):
                    plt.plot(t_interp, Y_interp_nd[j, :, idxs[k]],
                                        label=r'coord = {}'.format(xth_TG[idxs[k]]))
                plt.xlabel('%s' % xlab)
                plt.ylabel('%s' % set_params['GFs_nd'][j])
                plt.legend()
                plt.savefig('%s/%s_specificPoints' % (sPath_spe,
                                                set_params['GFs_nd'][j]))
                plt.close()

                np.savetxt('%s/%s_specificPoints.dat' % (sPath_spe,
                    # set_params['GFs_nd'][j]), np.transpose(t_interp, Y_interp_nd[j, :,idxs]))
                    set_params['GFs_nd'][j]),
                    np.vstack((t_interp, Y_interp_nd[j, :,idxs])).T,
                    header="t_d Y[i=%s,:] at x/R0 = %s" % (j, xth_TG[idxs]))
    else:
        # je n'ai pas encore ajouter ces valeurs au event pour donadoni
        raise NotImplementedError

def plottingCst_3D(iniF, path_abs, set_params, t0=None, tf=None, setting_t=('day',1.0),
                   folder='', r1=None, r2=None, points=[],
                   pcm=True, noAxis=False, colormap='jet', dpi=100, scalingFile=''):
    """plot Cst in 3D cas

    Parameters
    ----------
    path_abs : str
        The path
    set_params : dict
        A big dictionnary with lots of data
    t0 : float
        Initial time
    tf : float
        Final time
    setting_t : tuple

    folder : str

    r1 : float
        First position normalized by init radius
    r2 : float
        Last position normalized by init radius
    points : list
        list of specifics points where data will be plotted as function of time.
        Give x and theta coordinates
    colormap : str
        The type of colormap, avail : ``jet``, ``grey``, ``viridis`` 

    Returns
    -------

    """
    myCMAP = dictCMAP[colormap]

    message('Cst 3D plotting')
    if scalingFile != '':
        try:
            scalingCsts = np.loadtxt('{}/{}'.format(path_abs, scalingFile))
        except OSError:
            print("{} not found in {}".format(scalingFile, path_abs))
            scaling = False
        else:
            print("Successfully loaded {}".format(scalingFile))
            scaling = True
            [p_i_min, p_i_max, p_m_min, p_m_max, a_i_min, a_i_max,
                a_m_min, a_m_max, m_i_min, m_i_max, m_m_min, m_m_max,
                l_i_min, li_max, l_m_min, l_m_max,
                chi_i_min, chi_i_max, chi_m_min, chi_m_max,
                 c_i_min, c_i_max, c_m_min, c_m_max,
                 r_i_min, r_i_max, r_m_min, r_m_max] = scalingCsts
            scal_l = [[p_i_min, p_i_max], [p_m_min, p_m_max],
                    [a_i_min, a_i_max], [a_m_min, a_m_max],
                    [m_i_min, m_i_max], [m_m_min, m_m_max],
                    [l_i_min, li_max], [l_m_min, l_m_max],
                    [chi_i_min, chi_i_max],[chi_m_min, chi_m_max],
                    [c_i_min, c_i_max], [c_m_min, c_m_max],
                    [r_i_min, r_i_max], [r_m_min, r_m_max]]
    else:
        # if not scaling creation of it
        scalingCsts = []
        scalingCsts_com = []

    sPath = '{}/{}/Cst'.format(path_abs, folder)
    try:
        os.mkdir(sPath)
    except OSError:
        print ("Creation of the directory \n {} \nfailed".format(sPath))
    else:
        print ("Successfully created the directory \n {} ".format(sPath))

    if points:
        if len(points) % 2 != 0: raise ValueError('len(points) % 2 != 0')
        sPath_spe = '%s/specificPoints' % (sPath)
        try:
            os.mkdir(sPath_spe)
        except OSError:
            print ("Creation of the directory \n %s \nfailed" % sPath_spe )
        else:
            print ("Successfully created the directory \n %s " % sPath_spe)
        # tranform to table of x, theta coord
        points_arr =  np.array(points).reshape(int(len(points)*.5), 2)
        # ic(points_arr)

    xlab, normTime = setting_t
    paths = glob.glob('gene*')
    path_last_gene = find_last_gene(paths)

    path_data = '{}/{}/tissueGrowth/'.format(path_abs, path_last_gene)

    t_interp = np.load('{}/t_interp.npy'.format(path_data))
    t_gene = np.load('{}/t_gene.npy'.format(path_data))
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    # load mesh data
    xE = np.load('{}/xE_TG.npy'.format(path_data))
    thE = np.load('{}/thE_TG.npy'.format(path_data))
    xP = np.load('{}/xP_TG.npy'.format(path_data))
    
    X_grid, T_grid = np.meshgrid(xE, thE)
    xfla = X_grid.flatten('F')
    thfla = T_grid.flatten('F')
    xth_TG = np.vstack((xfla, thfla)).T

    Np_th = len(thE)
    Ni_x = len(xE)
    xE_star = xE / R0

    # check if r1,r2 are well defined
    default = r1==None and r2==None
    if default:
        # if None for the both, then default values, we plot for all along the
        # artery !
        print('\n default values for r1 r2')
        r1 = 0.0
        r2 = xE_star[-1]
    else:
        notGood = r1 < 0.0 or r2 > xE_star[-1]
        if notGood:
            raise ValueError('r1 < 0.0 or r2 > xE_star[-1]')
        # mask_xE_star = ma.masked_outside(xE_star, r1, r2)
        # xE_star = mask_xE_star[~mask_xE_star.mask].data

    X_star, T = np.meshgrid(xE_star, thE)
    Z = np.zeros_like(T)
    print('X_start.shape {} T.shape {}'.format(X_star.shape, T.shape))
    
    (i_t0, i_tf, ie_t0, ie_tf) = findIndex_t0tf(t_interp, t_gene, t0, tf)
    t_interp = t_interp[i_t0:i_tf] / normTime
    t_gene = t_gene[ie_t0:ie_tf] / normTime
    N_gene = len(t_gene)
    print('t_gene.shape', t_gene.shape)

    ######################################################################
    # locate index where event occurs
    ######################################################################
    print('t_gene', t_gene)
    idxEvents = []
    t_plots = t_gene
    # t_plots = t_gene[1:-1]
    for ti in t_plots:
        idx_i = np.argwhere(np.isclose(t_interp, ti))[:,0]
        if idx_i.size > 1:
            raise ValueError('More than 1 time event found')
        idxEvents.append(idx_i[0])
    print('len(idxEvents)', len(idxEvents))
    if len(idxEvents) == 0:
        raise ValueError('not event found')

    ######################################################################
    # Load data
    ######################################################################
    Cst_interp = np.load('{}/Cst_interp.npy'.format(path_data))[:,i_t0:i_tf,:]
    Cst_interp_ev = Cst_interp[:, idxEvents, :]

    # load GFs
    if set_params['model'][:6] == 'Jansen':
        r_i = Cst_interp_ev[0, :, :] - Cst_interp_ev[2, :, :]
        r_m = Cst_interp_ev[1, :, :] - Cst_interp_ev[3, :, :]
        Cst_interp_ev = np.concatenate((Cst_interp_ev,
            r_i[np.newaxis,:,:], r_m[np.newaxis,:,:]), axis=0)
        lgd = ['p_i', 'p_m',
                'a_i', 'a_m',
                'm_i', 'm_m',
                'l_i', 'l_m',
                'chi_i', 'chi_m',
                'c_i', 'c_m', 'r_i', 'r_m']
        for k in range(len(lgd)):
            # create folder for each species 
            sCst = '{}/{}'.format(sPath, lgd[k])
            try: os.mkdir(sCst)
            except OSError: print ("Creation of %s failed" % sCst)
            else: print("Successfully created of %s " % sCst)
            Cst_interp_ev_k = Cst_interp_ev[k]
            # create normalized map for colormap      
            # if default:                             
            if scalingFile!='':
                vmin = scal_l[j][0]
                vmax = scal_l[j][1]
            else:
                vmin = Cst_interp_ev_k.min()                  
                vmax = Cst_interp_ev_k.max()                  
                scalingCsts += [vmin, vmax]
                scalingCsts_com += [lgd[k]+"_min ", lgd[k]+"_max "]
            norm = colors.Normalize(vmin=vmin, vmax=vmax)
            print('{} : vmin = {}, vmax = {}'.format(lgd[k], vmin, vmax))

            fig = plt.figure()
            ax = plt.axes(projection='3d')
            ax.grid(False)
            print('Cst_interp_ev_k.shape', Cst_interp_ev_k.shape)
            # The azimuth is the rotation around the z axis
            ax.azim = AZIM
            # dist seems to be the distance from the center visible point in data coordinates.
            ax.dist = DIST
            # From this we understand that elev is the angle between the eye and the xy plane.
            ax.elev = ELEV
            for g in range(len(idxEvents)):
                gene_arr = Z + g
                Cst_interp_ev_k_resh = Cst_interp_ev_k[g,:].reshape(Np_th, Ni_x, order='F')
                ### plot each gene
                fig_g, ax_g = plt.subplots()
                levels = 80
                # cs = ax.contourf(T,X,Y_interp_nd[j, :, :].T,levels,cmap=myCMAP)
                cs = ax_g.pcolormesh(X_star, T, Cst_interp_ev_k_resh,
                        shading=shading_2D, vmin=vmin, vmax=vmax,
                            cmap=myCMAP, edgecolors='k')
                ax_g.set_ylabel(r'$\theta$')
                ax_g.set_xlabel('x/R0')
                ax_g.set_xlim(r1,r2)
                ax_g.set_title('{} t={}, vmin={:.4} vmax={:.4}'.format(
                    lgd[k], t_plots[g], vmin, vmax))
                cb_g = fig_g.colorbar(cs)
                fig_g.savefig('%s/%s_g%s' % (sCst, lgd[k], g))
                if noAxis:
                    ax_g.set_title('')
                    plt.axis('off')
                    cb_g.remove()
                    fig_g.savefig('{}/{}_g{}_noAxis'.format(
                        sCst, lgd[k], g),
                        bbox_inches='tight', pad_inches=0, dpi=dpi)
                plt.close()


                if not default:
                    # fill on nan value
                    Cst_interp_ev_k_resh[X_star > r2] = np.nan
                    Cst_interp_ev_k_resh[X_star < r1] = np.nan
                    gene_arr[X_star > r2] = np.nan
                    gene_arr[X_star < r1] = np.nan
                ax.plot_surface(X_star, T, gene_arr,
                        rstride=1, cstride=1,
                        facecolors=myCMAP(norm(Cst_interp_ev_k_resh)))
            ax.set_zticks([g for g in range(len(idxEvents))])
            ax.set_zticklabels(np.around(t_plots, 1))
            ax.set_xlabel(r'x/R0')
            ax.set_ylabel(r'$\theta$')
            ax.set_zlabel(r't ({})'.format(xlab))
            if not default:
                ax.set_xlim(r1, r2)
            ax.set_title('{}'.format(lgd[k]))
            # m = cm.ScalarMappable(cmap=cm.rainbow)
            m = cm.ScalarMappable(cmap=myCMAP)
            m.set_array(np.linspace(vmin,vmax))
            cbar = fig.colorbar(m)
            cbar.ax.set_ylabel(r'{}'.format(lgd[k]))
            fig.tight_layout()
            fig.subplots_adjust()  # adjust fig, remove white space
            ax.set_title('vmin={:.4}, vmax={:.4}'.format(vmin, vmax))
            plt.savefig('{}/{}'.format(sPath, lgd[k]))
            if noAxis:
                ax.set_title('')
                plt.axis('off')
                cbar.remove()
                plt.savefig('{}/{}_noAxis'.format(sPath, lgd[k]),
                        bbox_inches='tight', pad_inches=0, dpi=dpi)
            plt.close()

            # fig, ax = plt.subplots()
            # levels = 80
            # cs = ax.contourf(T,X,Y_interp_nd[j, :, :].T,levels,cmap=myCMAP)
            # cs = ax.pcolormesh(T, X, Y_interp_nd[j, :, :].T,
                                # shading = shading_2D,
                                # vmin = Y_interp_nd[j, :, :].min(),
                                # vmax = Y_interp_nd[j, :, :].max(),
                                # cmap=myCMAP, edgecolors='k')
            # ax.set_xlabel('%s' % xlab)
            # ax.set_ylim(r1,r2)
            # ax.set_ylabel('x/R0')
            # ax.set_title('%s' % set_params['GFs_nd'][j])
            # plt.colorbar(cs)
            # plt.savefig('%s/%s' % (sPath, set_params['GFs_nd'][j]))
            # plt.close()

        if scalingFile == '':
            np.savetxt("scalingCsts.dat", np.array(scalingCsts).reshape(1,len(scalingCsts)),
                    header=' '.join(scalingCsts_com), fmt='%1.4E')

        if points:
            # specific points plot
            idxs = []
            print('specific points plot are at :')
            for k in range(points_arr.shape[0]):
                idxs.append(
                        np.linalg.norm(np.abs(xth_TG - points_arr[k]),
                            axis=1).argmin()
                        )
                print('points_arr[k]', points_arr[k],
                      'correspond to xE_stat:', xth_TG[idxs[k]])
            np.savetxt('%s/specificPoints.dat' % (sPath_spe), xth_TG[idxs],
                    header='points %s' % xth_TG[idxs])

            for j in range(len(lgd)):
                plt.figure()
                for k in range(points_arr.shape[0]):
                    plt.plot(t_interp, Cst_interp[j, :, idxs[k]],
                                    label=r'coord = {}'.format(xth_TG[idxs[k]]))
                plt.xlabel('{}'.format(xlab))
                plt.ylabel('{}'.format(lgd[j]))
                plt.legend()
                plt.savefig('{}/{}_specificPoints'.format(sPath_spe, lgd[j]))
                plt.close()

                np.savetxt('{}/{}_specificPoints.dat'.format(sPath_spe,
                    # set_params['GFs_nd'][j]), np.transpose(t_interp, Y_interp_nd[j, :,idxs]))
                    lgd[j]), np.vstack((t_interp, Cst_interp[j, :,idxs])).T,
                    header="t_d Y[i={},:] at x/R0 = {}".format(j, xth_TG[idxs]))
    else:
        # je n'ai pas encore ajouter ces valeurs au event pour donadoni
        raise NotImplementedError

def plottingPop_3D(iniF, path_abs, set_params, t0=None, tf=None, setting_t=('day',1.0),
                   folder='', r1=None, r2=None, points=[],
                   pcm=True, noAxis=False, colormap='jet', dpi=100, scalingFile=''):
    """
    plot pop in 3D cas

    Parameters
    ----------
    path_abs : str
        The path
    set_params : dict
        A big dictionnary with lots of data
    t0 : float
        Initial time
    tf : float
        Final time
    setting_t : tuple

    folder : str

    r1 : float
        First position normalized by init radius
    r2 : float
        Last position normalized by init radius
    points : list
        list of specifics points where data will be plotted as function of time.
        Give x and theta coordinates
    colormap : str
        The type of colormap, avail : ``jet``, ``grey``, ``viridis`` 

    Returns
    -------

    """
    myCMAP = dictCMAP[colormap]

    message('Pop 3D plotting')
    if scalingFile != '':
        try:
            scalingPop = np.loadtxt('{}/{}'.format(path_abs, scalingFile))
        except OSError:
            print("{} not found in {}".format(scalingFile, path_abs))
            scaling = False
        else:
            print("Successfully loaded {}".format(scalingFile))
            scaling = True
            [Qi_nd_min, Qi_nd_max, Qm_nd_min, Qm_nd_max,
             Si_nd_min, Si_nd_max, Sm_nd_min, Sm_nd_max, Cji_nd_min, Cji_nd_max,
             Cjm_nd_min, Cjm_nd_max, Cvi_nd_min, Cvi_nd_max, Cvm_nd_min, Cvm_nd_max,
             E_nd_min, E_nd_max,
             ddteTot_min, ddteTot_max, eTot_min, eTot_max,
             e_i_min, e_i_max, e_m_min, e_m_max,
             R_gr_min, R_gr_max] = scalingPop
            scal_l = [[Qi_nd_min, Qi_nd_max], [Qm_nd_min, Qm_nd_max],
                    [Si_nd_min, Si_nd_max], [Sm_nd_min, Sm_nd_max],
                    [Cji_nd_min, Cji_nd_max], [Cjm_nd_min, Cjm_nd_max],
                    [Cvi_nd_min, Cvi_nd_max], [Cvm_nd_min, Cvm_nd_max],
                    [E_nd_min, E_nd_max], [ddteTot_min, ddteTot_max],
                    [eTot_min, eTot_max], [e_i_min, e_i_max],
                    [e_m_min, e_m_max], [R_gr_min, R_gr_max]
                    ]
    else:
        # if not scaling creation of it
        scalingPop = []
        scalingPop_com = []

    sPath = '%s/%s/Pop' % (path_abs, folder)
    try:
        os.mkdir(sPath)
    except OSError:
        print ("Creation of the directory \n %s \nfailed" % sPath )
    else:
        print ("Successfully created the directory \n %s " % sPath)
    if points:
        if len(points) % 2 != 0: raise ValueError('len(points) % 2 != 0')
        sPath_spe = '%s/specificPoints' % (sPath)
        try:
            os.mkdir(sPath_spe)
        except OSError:
            print ("Creation of the directory \n %s \nfailed" % sPath_spe )
        else:
            print ("Successfully created the directory \n %s " % sPath_spe)
        points_arr =  np.array(points).reshape(int(len(points)*.5), 2)
        # ic(points_arr)

    xlab, normTime = setting_t
    paths = glob.glob('gene*')
    path_last_gene = find_last_gene(paths)

    path_data = \
        '%s/%s/tissueGrowth/' % (path_abs, path_last_gene)

    t_interp = np.load('%s/t_interp.npy' % path_data)
    t_gene = np.load('%s/t_gene.npy' % path_data)
    (Rext, R_LEE, R0, L) = set_params['geom_dim']

    # load mesh data
    xE = np.load('%s/xE_TG.npy' % path_data)
    thE = np.load('%s/thE_TG.npy' % path_data)
    xP = np.load('%s/xP_TG.npy' % path_data)

    X_grid, T_grid = np.meshgrid(xE, thE)
    xfla = X_grid.flatten('F')
    thfla = T_grid.flatten('F')
    xth_TG = np.vstack((xfla, thfla)).T

    Np_th = len(thE)
    Ni_x = len(xE)
    xE_star = xE / R0
    # check if r1,r2 are well defined
    default = r1==None and r2==None
    if default:
        # if None for the both, then default values, we plot for all along the
        # artery !
        print('\n default values for r1 r2')
        r1 = 0.0
        r2 = xE_star[-1]
    else:
        notGood = r1 < 0.0 or r2 > xE_star[-1]
        if notGood:
            raise ValueError('r1 < 0.0 or r2 > xE_star[-1]')
        # mask_xE_star = ma.masked_outside(xE_star, r1, r2)
        # xE_star = mask_xE_star[~mask_xE_star.mask].data

    X_star, T = np.meshgrid(xE_star, thE)
    Z = np.zeros_like(T)
    print('X_start.shape {} T.shape {}'.format(X_star.shape, T.shape))
    
    (i_t0, i_tf, ie_t0, ie_tf) = findIndex_t0tf(t_interp, t_gene, t0, tf)
    t_interp = t_interp[i_t0:i_tf] / normTime
    t_gene = t_gene[ie_t0:ie_tf] / normTime
    N_gene = len(t_gene)
    print('t_gene.shape', t_gene.shape)


    ######################################################################
    # locate index where event occurs
    ######################################################################
    print('t_gene', t_gene)
    idxEvents = []
    t_plots = t_gene
    # t_plots = t_gene[1:-1]
    for ti in t_plots:
        idx_i = np.argwhere(np.isclose(t_interp, ti))[:,0]
        if idx_i.size > 1:
            raise ValueError('More than 1 time event found')
        idxEvents.append(idx_i[0])
    print('len(idxEvents)', len(idxEvents))
    if len(idxEvents) == 0:
        raise ValueError('not event found')
    ######################################################################
    # Load data
    ######################################################################
    # print('t_interp after modif', t_interp)
    Y_interp_nd = np.load('%s/Y_interp_nd.npy' % path_data)[:,i_t0:i_tf,:]
    var_1D = np.load('%s/var_1D.npy' % path_data)[:,i_t0:i_tf,:]

    Y_interp_nd_ev = Y_interp_nd[:, idxEvents, :]
    var_1D_ev = var_1D[:, idxEvents, :]

    if set_params['model'][:6] == 'Jansen':
        N_pop = len(set_params['species_nd'])
        NGFs = len(set_params['GFs_nd'])

        species_nd = Y_interp_nd[NGFs:]

        scal = np.array([1e6, 1e6, 1e6, 1e3])
        var_1D_scaled = var_1D * scal[:,np.newaxis, np.newaxis]
        eTot = var_1D_scaled[0, :, :] # in um
        ddteTot = np.gradient(eTot, t_interp, axis=0, edge_order=2)

        PopDatas = np.concatenate((species_nd, ddteTot[np.newaxis,:,:],
            var_1D_scaled), axis=0)
        PopNames = list(set_params['species_nd']) + ['ddeTot_umPerDays',
                'eTot_um', 'e_i_um', 'e_m_um', 'R_gr_mm']
        N_pop = len(PopNames)

        for k in range(N_pop):
            PopDatas_k_events = PopDatas[k, idxEvents, :]
            # create folder for each species 
            sSpe = '%s/%s' % (sPath, PopNames[k])
            try: os.mkdir(sSpe)
            except OSError: print ("Creation of %s failed" % sSpe )
            else: print("Successfully created of %s " % sSpe)
            # create normalized map for colormap
            # if default:
            if scalingFile != '':
               vmin = scal_l[k][0]
               vmax = scal_l[k][1]
            else:
                vmin = PopDatas_k_events.min()
                vmax = PopDatas_k_events.max()
                scalingPop += [vmin, vmax]
                scalingPop_com += [ PopNames[k] + "_min ",
                                    PopNames[k]  + "_max "]

            norm = colors.Normalize(vmin=vmin,vmax=vmax)
            # else:
                # species_ev_j[X_star > r2] = np.nan
                # species_ev_j[X_star < r1] = np.nan
                # vmin = np.nanmin(species_ev_j)
                # vmax = np.nanmax(species_ev_j)
            print('{} : vmin = {}, vmax = {}'.format(PopNames[k],
                    vmin, vmax))

            fig = plt.figure()
            ax = plt.axes(projection='3d')
            ax.grid(False)
            # ic(PopDatas_k.shape)
            # The azimuth is the rotation around the z axis
            ax.azim = AZIM
            # dist seems to be the distance from the center visible point in data coordinates.
            ax.dist = DIST
            # From this we understand that elev is the angle between the eye and the xy plane.
            ax.elev = ELEV
            for g in range(len(idxEvents)):
                gene_arr = Z + g
                PopDatas_k_events_g_resh = PopDatas_k_events[g,:].reshape(Np_th, Ni_x, order='F')

                ### plot each gene
                fig_g, ax_g = plt.subplots()
                levels = 80
                # cs = ax.contourf(T,X,Y_interp_nd[j, :, :].T,levels,cmap=myCMAP)
                cs = ax_g.pcolormesh(X_star, T, PopDatas_k_events_g_resh,
                    shading=shading_2D,
                    vmin=vmin, vmax=vmax, cmap=myCMAP)
                ax_g.set_ylabel(r'$\theta$')
                ax_g.set_xlabel('x/R0')
                ax_g.set_xlim(r1,r2)
                ax_g.set_title('{} t={}, vmin={:.4} vmax={:.4}'.format(
                    PopNames[k], t_plots[g],
                    vmin, vmax))
                cb_g = fig_g.colorbar(cs)
                fig_g.savefig('{}/{}_g{}'.format(sSpe, PopNames[k], g))
                if noAxis:
                    ax.set_title('')
                    cb_g.remove()
                    plt.axis('off')
                    fig_g.savefig('{}/{}_g{}_noAxis'.format(sSpe, PopNames[k], g),
                            bbox_inches='tight', pad_inches=0, dpi=dpi)
                plt.close()
                
                
                if not default:
                    # fill on nan value
                    PopDatas_k_events_g_resh[X_star > r2] = np.nan
                    PopDatas_k_events_g_resh[X_star < r1] = np.nan
                    gene_arr[X_star > r2] = np.nan
                    gene_arr[X_star < r1] = np.nan
                ax.plot_surface(X_star, T, gene_arr,
                        rstride=1, cstride=1,
                        facecolors=myCMAP(norm(PopDatas_k_events_g_resh)))
            ax.set_zticks([g for g in range(len(idxEvents))])
            ax.set_zticklabels(np.around(t_plots, 1))
            ax.set_xlabel(r'x/R0')
            ax.set_ylabel(r'$\theta$')
            ax.set_zlabel(r't ({})'.format(xlab))
            if not default:
                ax.set_xlim(r1, r2)
            ax.set_title('%s' % PopNames[k])
            # m = cm.ScalarMappable(cmap=cm.rainbow)
            m = cm.ScalarMappable(cmap=myCMAP)
            m.set_array(np.linspace(vmin, vmax))
            cbar = fig.colorbar(m)
            cbar.ax.set_ylabel(r'{}'.format(PopNames[k]))
            ax.set_title('vmin={:.4}, vmax={:.4}'.format(vmin, vmax))
            fig.tight_layout()
            fig.subplots_adjust()  # adjust fig, remove white space

            plt.savefig('%s/%s' % (sPath, PopNames[k]))
            if noAxis:                                                
                ax.set_title('')                                      
                plt.axis('off')                                       
                cbar.remove()                                           
                plt.savefig('%s/%s_noAxis.png' % (sPath, PopNames[k]),
                        bbox_inches='tight', pad_inches=0, dpi=dpi)   

            plt.close()

        if scalingFile == '':
            np.savetxt("scalingPop.dat", np.array(scalingPop).reshape(1,len(scalingPop)),
                    header=' '.join(scalingPop_com), fmt='%1.4E')

        if points:
            # specific points plot
            idxs = []
            print('specific points plot are at :')
            for k in range(points_arr.shape[0]):
                idxs.append(
                        np.linalg.norm(np.abs(xth_TG - points_arr[k]),
                            axis=1).argmin()
                        )
            np.savetxt('%s/specificPoints.dat' % (sPath_spe), xth_TG[idxs],
                    header='points %s' % xth_TG[idxs])

            for l in range(N_pop):
                plt.figure()
                for k in range(points_arr.shape[0]):
                    # ic(PopDatas[l, :, idxs[k]].shape)
                    # ic(xth_TG.shape)
                    plt.plot(t_interp, PopDatas[l, :, idxs[k]],'o',
                                        label=r'coord = {}'.format(xth_TG[idxs[k]]))
                plt.xlabel('%s' % xlab)
                plt.ylabel('%s' % PopNames[l])
                plt.legend()
                plt.savefig('%s/%s_specificPoints' % (sPath_spe, PopNames[l]))
                plt.close()
                np.savetxt('%s/%s_specificPoints.dat' % (sPath_spe, PopNames[l]),
                            np.vstack((t_interp, PopDatas[l,:,idxs])).T,
                            header='t_d xth=%s' % xth_TG[idxs])

    else:
        # je n'ai pas encore ajouter ces valeurs au event pour donadoni
        raise NotImplementedError

    return

def plottingVf_3D(iniF, path_abs, set_params, t0=None, tf=None, setting_t=('day',1.0),
                   folder='', r1=None, r2=None, points=[],
                   pcm=True, noAxis=False, colormap='jet', dpi=100, scalingFile=''):
    """
    plot Vf in 3D cas

    Parameters
    ----------
    path_abs : str
        The path
    set_params : dict
        A big dictionnary with lots of data
    t0 : float
        Initial time
    tf : float
        Final time
    setting_t : tuple

    folder : str

    r1 : float
        First position normalized by init radius
    r2 : float
        Last position normalized by init radius
    points : list
        list of specifics points where data will be plotted as function of time.
        Give x and theta coordinates
    colormap : str
        The type of colormap, avail : ``jet``, ``grey``, ``viridis`` 

    Returns
    -------

    """
    myCMAP = dictCMAP[colormap]

    message('Vf 3D plotting')
    if scalingFile != '':
        try:
            scalingPop = np.loadtxt('{}/{}'.format(path_abs, scalingFile))
        except OSError:
            print("{} not found in {}".format(scalingFile, path_abs))
            scaling = False
        else:
            print("Successfully loaded {}".format(scalingFile))
            scaling = True
            [Vf_Ei_min, Vf_Ei_max, Vf_Qi_min, Vf_Qi_max, Vf_Qm_min, Vf_Qm_max,
                    Vf_Si_min, Vf_Si_max, Vf_Sm_min, Vf_Sm_max,
                    Vf_Ci_min, Vf_Ci_max, Vf_Cm_min, Vf_Cm_max,
                    Vf_Oi_min, Vf_Oi_max, Vf_Om_min, Vf_Om_max] = scalingVf
            scal_l = [[Vf_Ei_min, Vf_Ei_max], [Vf_Qi_min, Vf_Qi_max],
                    [Vf_Qm_min, Vf_Qm_max], [Vf_Si_min, Vf_Si_max],
                    [Vf_Sm_min, Vf_Sm_max], [Vf_Ci_min, Vf_Ci_max],
                    [Vf_Cm_min, Vf_Cm_max],
                    [Vf_Oi_min, Vf_Oi_max], [Vf_Om_min, Vf_Om_max]]
    else:
        # if not scaling creation of it
        scalingVf = []               
        scalingVf_com = []           

    sPath = '{}/{}/Vf'.format(path_abs, folder)
    try:
        os.mkdir(sPath)
    except OSError:
        print ("Creation of the directory \n %s \nfailed" % sPath )
    else:
        print ("Successfully created the directory \n %s " % sPath)
    if points:
        if len(points) % 2 != 0: raise ValueError('len(points) % 2 != 0')
        sPath_spe = '%s/specificPoints' % (sPath)
        try:
            os.mkdir(sPath_spe)
        except OSError:
            print ("Creation of the directory \n %s \nfailed" % sPath_spe )
        else:
            print ("Successfully created the directory \n %s " % sPath_spe)
        points_arr =  np.array(points).reshape(int(len(points)*.5), 2)
        # ic(points_arr)

    xlab, normTime = setting_t
    paths = glob.glob('gene*')
    path_last_gene = find_last_gene(paths)

    path_data = '{}/{}/tissueGrowth/'.format(path_abs, path_last_gene)

    t_interp = np.load('%s/t_interp.npy' % path_data)
    t_gene = np.load('%s/t_gene.npy' % path_data)
    (Rext, R_LEE, R0, L) = set_params['geom_dim']

    # load mesh data
    xE = np.load('%s/xE_TG.npy' % path_data)
    thE = np.load('%s/thE_TG.npy' % path_data)
    xP = np.load('%s/xP_TG.npy' % path_data)

    X_grid, T_grid = np.meshgrid(xE, thE)
    xfla = X_grid.flatten('F')
    thfla = T_grid.flatten('F')
    xth_TG = np.vstack((xfla, thfla)).T

    Np_th = len(thE)
    Ni_x = len(xE)
    xE_star = xE / R0
    # check if r1,r2 are well defined
    default = r1==None and r2==None
    if default:
        # if None for the both, then default values, we plot for all along the
        # artery !
        print('\n default values for r1 r2')
        r1 = 0.0
        r2 = xE_star[-1]
    else:
        notGood = r1 < 0.0 or r2 > xE_star[-1]
        if notGood:
            raise ValueError('r1 < 0.0 or r2 > xE_star[-1]')
        # mask_xE_star = ma.masked_outside(xE_star, r1, r2)
        # xE_star = mask_xE_star[~mask_xE_star.mask].data

    X_star, T = np.meshgrid(xE_star, thE)
    Z = np.zeros_like(T)
    print('X_start.shape {} T.shape {}'.format(X_star.shape, T.shape))
    
    (i_t0, i_tf, ie_t0, ie_tf) = findIndex_t0tf(t_interp, t_gene, t0, tf)
    t_interp = t_interp[i_t0:i_tf] / normTime
    t_gene = t_gene[ie_t0:ie_tf] / normTime
    N_gene = len(t_gene)
    print('t_gene.shape', t_gene.shape)


    ######################################################################
    # locate index where event occurs
    ######################################################################
    print('t_gene', t_gene)
    idxEvents = []
    t_plots = t_gene
    # t_plots = t_gene[1:-1]
    for ti in t_plots:
        idx_i = np.argwhere(np.isclose(t_interp, ti))[:,0]
        if idx_i.size > 1:
            raise ValueError('More than 1 time event found')
        idxEvents.append(idx_i[0])
    print('len(idxEvents)', len(idxEvents))
    if len(idxEvents) == 0:
        raise ValueError('not event found')
    ######################################################################
    # Load data
    ######################################################################
    # print('t_interp after modif', t_interp)
    Vf = np.load('%s/Vf_stack.npy' % path_data)[:,i_t0:i_tf,:]

    Vf_ev = Vf[:, idxEvents, :]

    if set_params['model'][:6] == 'Jansen':
        N_pop = len(set_params['species_nd'])
        N_Vfs = len(set_params['Vf'])

        for k in range(N_Vfs):
            Vf_ev_k = Vf_ev[k]
            # create folder for each species 
            sSpe = '%s/%s' % (sPath, set_params['Vf'][k])
            try: os.mkdir(sSpe)
            except OSError: print ("Creation of %s failed" % sSpe )
            else: print("Successfully created of %s " % sSpe)
            # create normalized map for colormap
            # if default:
            if scalingFile != '':
               vmin = scal_l[k][0]
               vmax = scal_l[k][1]
            else:
                vmin = Vf_ev_k.min()
                vmax = Vf_ev_k.max()
                scalingVf += [vmin, vmax]
                scalingVf_com += [ set_params['Vf'][k] + "_min ",
                                    set_params['Vf'][k]  + "_max "]

            norm = colors.Normalize(vmin=vmin,vmax=vmax)
            # else:
                # species_ev_j[X_star > r2] = np.nan
                # species_ev_j[X_star < r1] = np.nan
                # vmin = np.nanmin(species_ev_j)
                # vmax = np.nanmax(species_ev_j)
            print('{} : vmin = {}, vmax = {}'.format(set_params['Vf'][k],
                    vmin, vmax))

            fig = plt.figure()
            ax = plt.axes(projection='3d')
            ax.grid(False)
            # ic(PopDatas_k.shape)
            # The azimuth is the rotation around the z axis
            ax.azim = AZIM
            # dist seems to be the distance from the center visible point in data coordinates.
            ax.dist = DIST
            # From this we understand that elev is the angle between the eye and the xy plane.
            ax.elev = ELEV
            for g in range(len(idxEvents)):
                gene_arr = Z + g
                Vf_ev_k_g_resh = Vf_ev_k[g,:].reshape(Np_th, Ni_x, order='F')

                ### plot each gene
                fig_g, ax_g = plt.subplots()
                levels = 80
                # cs = ax.contourf(T,X,Y_interp_nd[j, :, :].T,levels,cmap=myCMAP)
                cs = ax_g.pcolormesh(X_star, T, Vf_ev_k_g_resh,
                    shading=shading_2D, vmin=vmin, vmax=vmax, cmap=myCMAP)
                ax_g.set_ylabel(r'$\theta$')
                ax_g.set_xlabel('x/R0')
                ax_g.set_xlim(r1,r2)
                ax_g.set_title('{} t={}, vmin={:.4} vmax={:.4}'.format(
                    set_params['Vf'][k], t_plots[g],
                    vmin, vmax))
                cb_g = fig_g.colorbar(cs)
                fig_g.savefig('{}/{}_g{}'.format(sSpe, set_params['Vf'][k], g))
                if noAxis:
                    ax.set_title('')
                    cb_g.remove()
                    plt.axis('off')
                    fig_g.savefig('{}/{}_g{}_noAxis'.format(sSpe, set_params['Vf'][k], g),
                            bbox_inches='tight', pad_inches=0, dpi=dpi)
                plt.close()
                
                
                if not default:
                    # fill on nan value
                    Vf_ev_k_g_resh[X_star > r2] = np.nan
                    Vf_ev_k_g_resh[X_star < r1] = np.nan
                    gene_arr[X_star > r2] = np.nan
                    gene_arr[X_star < r1] = np.nan
                ax.plot_surface(X_star, T, gene_arr,
                        rstride=1, cstride=1,
                        facecolors=myCMAP(norm(Vf_ev_k_g_resh)))
            ax.set_zticks([g for g in range(len(idxEvents))])
            ax.set_zticklabels(np.around(t_plots, 1))
            ax.set_xlabel(r'x/R0')
            ax.set_ylabel(r'$\theta$')
            ax.set_zlabel(r't ({})'.format(xlab))
            if not default:
                ax.set_xlim(r1, r2)
            ax.set_title('%s' % set_params['Vf'][k])
            # m = cm.ScalarMappable(cmap=cm.rainbow)
            m = cm.ScalarMappable(cmap=myCMAP)
            m.set_array(np.linspace(vmin, vmax))
            cbar = fig.colorbar(m)
            cbar.ax.set_ylabel(r'{}'.format(set_params['Vf'][k]))
            ax.set_title('vmin={:.4}, vmax={:.4}'.format(vmin, vmax))
            fig.tight_layout()
            fig.subplots_adjust()  # adjust fig, remove white space

            plt.savefig('%s/%s' % (sPath, set_params['Vf'][k]))
            if noAxis:                                                
                ax.set_title('')                                      
                plt.axis('off')                                       
                cbar.remove()                                           
                plt.savefig('%s/%s_noAxis.png' % (sPath, set_params['Vf'][k]),
                        bbox_inches='tight', pad_inches=0, dpi=dpi)   

            plt.close()

        if scalingFile == '':
            np.savetxt("scalingVf.dat", np.array(scalingVf).reshape(1,len(scalingVf)),
                    header=' '.join(scalingVf_com), fmt='%1.4E')

        if points:
            # specific points plot
            idxs = []
            print('specific points plot are at :')
            for k in range(points_arr.shape[0]):
                idxs.append(
                        np.linalg.norm(np.abs(xth_TG - points_arr[k]),
                            axis=1).argmin()
                        )
            np.savetxt('%s/specificPoints.dat' % (sPath_spe), xth_TG[idxs],
                    header='points %s' % xth_TG[idxs])

            for l in range(N_Vfs):
                plt.figure()
                for k in range(points_arr.shape[0]):
                    # ic(PopDatas[l, :, idxs[k]].shape)
                    # ic(xth_TG.shape)
                    plt.plot(t_interp, Vf[l, :, idxs[k]],'o',
                                        label=r'coord = {}'.format(xth_TG[idxs[k]]))
                plt.xlabel('%s' % xlab)
                plt.ylabel('%s' % set_params['Vf'][l])
                plt.legend()
                plt.savefig('%s/%s_specificPoints' % (sPath_spe, set_params['Vf'][l]))
                plt.close()
                np.savetxt('%s/%s_specificPoints.dat' % (sPath_spe, set_params['Vf'][l]),
                            np.vstack((t_interp, Vf[l,:,idxs])).T,
                            header='t_d xth=%s' % xth_TG[idxs])
    else:
        # je n'ai pas encore ajouter ces valeurs au event pour donadoni
        raise NotImplementedError

    return

def plottingWSS_3D(iniF, path_abs, set_params, t0=None, tf=None,
                   setting_t=('day',1.0), folder='',
                   r1=None, r2=None, points=[],
                   pcm=True, noAxis=False, colormap='jet', dpi=100, scalingFile=''):
    """
    plot wss in 3D case
    """
    myCMAP = dictCMAP[colormap]
    message('WSS 3D plotting')

    if scalingFile!='':
        try:
            scalingGFs = np.loadtxt('{}/{}'.format(path_abs, scalingFile))
        except OSError:
            print("{} not found in {}".format(scalingFile, path_abs))
            scaling = False
        else:
            print("Successfully loaded {}".format(scalingFile))
            scaling = True
            [WSS_min, WSS_max] = scalingGFs
            scal_l = [WSS_min, WSS_max]
    else:
        # if not scaling creation of it
        scalingWSS = []
        scalingWSS_com = []

    sPath = '%s/%s/WSS' % (path_abs, folder)
    try:
        os.mkdir(sPath)
    except OSError:
        print ("Creation of the directory \n %s \nfailed" % sPath )
    else:
        print ("Successfully created the directory \n %s " % sPath)
    if points:
        if len(points) % 2 != 0: raise ValueError('len(points) % 2 != 0')
        sPath_spe = '%s/specificPoints' % (sPath)
        try:
            os.mkdir(sPath_spe)
        except OSError:
            print ("Creation of the directory \n %s \nfailed" % sPath_spe )
        else:
            print ("Successfully created the directory \n %s " % sPath_spe)
        points_arr =  np.array(points).reshape(int(len(points)*.5), 2)
    xlab, normTime = setting_t
    paths = glob.glob('{}/gene*'.format(path_abs))
    # print('paths', paths)
    l = glob.glob('{}/gene*'.format(path_abs))         
    for i in range(len(l)):                         
        #l[i] = l[i].replace(self.path_abs + '/','') 
        # print('l[i]', l[i])                       
        l[i] = l[i].replace(path_abs + '/gene', '')             
        # print('l[i]', l[i])                       
        l[i] = int(l[i][:l[i].find('_')])           
    # print('l', l)                                 
    prev_gene = np.asarray(l)
    paths = np.asarray(paths)[prev_gene.argsort()].tolist()
    # print('paths', paths)
    path_last_gene = paths[-1]

    path_data = path_last_gene + '/tissueGrowth/'
    t_events = np.load('%s/t_gene.npy' % path_data)

    (Rext, R_LEE, R0, L) = set_params['geom_dim']

    # load mesh data
    xE = np.load('%s/xE_TG.npy' % path_data)
    thE = np.load('%s/thE_TG.npy' % path_data)
    xP = np.load('%s/xP_TG.npy' % path_data)

    X_grid, T_grid = np.meshgrid(xE, thE)
    xfla = X_grid.flatten('F')
    thfla = T_grid.flatten('F')
    xth_TG = np.vstack((xfla, thfla)).T

    Np_th = len(thE)
    Ni_x = len(xE)
    xE_star = xE / R0
    # check if r1,r2 are well defined
    default = r1==None and r2==None
    if default:
        # if None for the both, then default values, we plot for all along the
        # artery !
        print('\n default values for r1 r2')
        r1 = 0.0
        r2 = xE_star[-1]
    else:
        notGood = r1 < 0.0 or r2 > xE_star[-1]
        if notGood:
            raise ValueError('r1 < 0.0 or r2 > xE_star[-1]')
    # mask_xE_star = ma.masked_outside(xE_star, r1, r2)
    # xE_star = mask_xE_star[~mask_xE_star.mask].data

    X_star, T = np.meshgrid(xE_star, thE)
    print('X_start.shape {} T.shape {}'.format(X_star.shape, T.shape))

    t_wss = [-1] + t_events[:-1].tolist()
    # the interpolated WSS on tissue growth nodes
    wssR = []
    for path_i in paths:
        # print('path_i', path_i)
        wss_i = np.load('%s/tissueGrowth/WSS_TG.npy' % path_i)#[ie_t0:ie_tf]
        wssR.append(wss_i)
    if len(wssR) != len(t_wss):
        print('t_events', t_wss)
        raise ValueError("len(wssR) {} != len(t_ws) {}".format(len(wssR),
            len(t_wss)))
    wssR = np.asarray(wssR)
    # ic(wssR.shape)
    # source for stacked contour plot :
    # https://stackoverflow.com/questions/20428402/3d-stacked-2d-histograms-
    # in-matplotlib-pyplot
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    # view camero
    # The azimuth is the rotation around the z axis
    ax.azim = AZIM
    # dist seems to be the distance from the center visible point in data coordinates.
    ax.dist = DIST
    # From this we understand that elev is the angle between the eye and the xy plane.
    ax.elev =ELEV
    Z = np.zeros_like(T)
    # create normalized map for colormap
    if scalingFile !='':
        vmin = scal_l[0]
        vmax = scal_l[1]
    else:
        vmin = np.min(wssR)
        vmax = np.max(wssR)
        scalingWSS += [vmin, vmax]
        scalingWSS_com += ['WSS_min', 'WSS_max']
    
    # print('WSS : vmin', vmin, 'vmax', vmax)
    norm = colors.Normalize(vmin=vmin,vmax=vmax)

    sWSS = '%s/WSS' % (sPath)
    try: os.mkdir(sWSS)
    except OSError: print ("Creation of %s failed" % sWSS)
    else: print("Successfully created of %s " % sWSS)
    for k in range(len(wssR)):
        gene_arr = Z + k
        wssR_k_reshape = wssR[k].reshape(Np_th, Ni_x, order='F')

        fig_g, ax_g = plt.subplots()
        # ax.plot(xth_TG[:,0], xth_TG[:,1], 'k.', ms=1)
        cs = ax_g.pcolormesh(X_star, T, wssR_k_reshape, shading = shading_2D,
                        vmin = vmin, vmax = vmax, cmap=myCMAP, edgecolors='k')
        ax_g.set_xlabel('x/R0')
        ax_g.set_ylabel(r'$\theta$')
        ax_g.set_xlim(r1, r2)
        ax_g.set_xlim(r1,r2)
        ax_g.set_title('WSS at t = {}, gene {}, vmin={:.4} vmax={:.4}'.format(
            t_wss[k], k, vmin, vmax))
        cb_g = fig_g.colorbar(cs)
        fig_g.savefig('%s/wss_gene_%s' % (sWSS, k))
        if noAxis:
            ax.set_title('')
            plt.axis('off')
            cb_g.remove()
            fig_g.savefig('%s/wss_gene_%s_noAxis' % (sWSS, k),
                    bbox_inches='tight', pad_inches=0, dpi=dpi)

        plt.close()
        # head += " WSS_{} (Pa)".format(k)
        if not default:
            wssR_k_reshape[X_star > r2] = np.nan
            wssR_k_reshape[X_star < r1] = np.nan
            gene_arr[X_star > r2] = np.nan
            gene_arr[X_star < r1] = np.nan
        # print('k', k , 'wssR[k].shape', wssR[k].shape, wssR_k_reshape.shape,
                # T.shape, Z.shape, X_star.shape, gene_arr.shape)
        ax.plot_surface(X_star, T, gene_arr,
                rstride=1, cstride=1,
                facecolors = myCMAP(norm(wssR_k_reshape)))

    plt.legend()
    ax.set_xlabel(r'$x/R_0$')
    ax.set_ylabel(r'$\theta$')
    ax.set_zlabel(r'generation')
    # plt.xlim(r1,r2)
    # plt.ylabel("$\tau_p$ $[Pa]$")
    if not default:        
        ax.set_xlim(r1, r2)
    m = cm.ScalarMappable(cmap=myCMAP)                
    m.set_array(np.linspace(vmin,vmax))                      
    cbar = fig.colorbar(m)
    cbar.ax.set_ylabel(r'$\tau_w$ (Pa)$')
    ax.set_title('vmin={:.4}, vmax={:.4}'.format(vmin, vmax))
    fig.tight_layout()                                     
    fig.subplots_adjust()  # adjust fig, remove white space
    ax.set_title('vmin={:.4}, vmax={:.4}'.format(vmin, vmax))
    plt.savefig('%s/wssGeneStacked' % sPath)
    if noAxis:
        ax.set_title('')                                      
        plt.axis('off')                                       
        cb.remove()                                           
        plt.savefig('{}/wssGeneStacked_noAxis.png'.format(sPath),
                bbox_inches='tight', pad_inches=0, dpi=dpi)   
    plt.close()

    if scalingFile == '':
        np.savetxt("scalingWSS.dat", np.array(scalingWSS).reshape(1,len(scalingWSS)),
                    header=' '.join(scalingWSS_com), fmt='%1.4E')


    if points:
        # specific points plot
        wssR_arr = np.asarray(wssR)
        idxs = []
        print('specific points plot are at :')
        for k in range(points_arr.shape[0]):
            idxs.append(
                    np.linalg.norm(np.abs(xth_TG - points_arr[k]),
                        axis=1).argmin()
                    )
        np.savetxt('%s/specificPoints.dat' % (sPath_spe), xth_TG[idxs],
                header='points %s' % xth_TG[idxs])

        plt.figure()
        for k in range(points_arr.shape[0]):
            # ic(len(t_wss), len(wssR), wssR_arr.shape)
            # ic(idxs[k])
            # ic(wssR_arr[:,idxs[k]].shape)
            plt.plot(t_wss, wssR_arr[:,idxs[k]],
                                label=r'coord = {}'.format(xth_TG[idxs[k]]))
        plt.xlabel('%s' % xlab)
        plt.ylabel('WSS (Pa)')
        plt.legend()
        plt.savefig('%s/WSS_specificPoints' % (sPath_spe))
        plt.close()
        np.savetxt('%s/wss_specificPoints.dat' % (sPath_spe),
                            np.vstack((t_wss, wssR_arr[:,idxs].T)).T,
                            header='t_d xE=%s' % xth_TG[idxs])
