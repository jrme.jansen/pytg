#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from warnings import warn
import numpy as np

AVAIL_HEMORHEO_MODELS = ['Newtonian', 'Quemada', 'WalburnScheck', 'Casson']

class Fluid(object):
    """ The Fluid class
    """
    muN = 3.45e-3

    def __init__(self, iterable=(), **kwargs):
        """Fast constructor of the object. As the class must be able to
        evolve quickly, the parameters are implicitly defined in the dictionary
        during the initialization. This is a method that allows flexibility in
        development but may cause a lot of errors.
        """
        self.__dict__.update(iterable, **kwargs)

class Newtonian(Fluid):
    """ Newtonian fluid class

    Parameters
    ----------
    mu : float
        Dynamic viscosity in [Pa.s]
    rho : float
        Fluid density [kg/m3]

    Attributes
    ----------
    mu : float
        Dynamic viscosity [Pa.s]
    rho : float
        Fluid density [kg/m3]
    nu : float
        Kinematic viscosity [m2/s]
    sR_eq : float
        The
    argsF : tuple
        All the constant of the fluid gather in a generic tuple
    """
    def __init__(self, iterable=(), **kwargs):
        self.__dict__.update(iterable, **kwargs)
        self.nu = self.mu / self.rho
        self.sR_eq = 1.0
        self.nuEq = self.dynamicViscosity(self.sR_eq) / self.rho
        self.argsF = (self.mu, self.nu)

    def dynamicViscosity(self, sR=np.linspace(1e-2,1e3,101)):
        """
        Parameters
        ---------
        sr : np.array or float
            shear rate variable
        Returns
        -------
        mu : np.array or float
            dynamic viscosity
        """
        return self.mu * sR/sR

class Quemada(Fluid):
    """ A Quemada fluid, implemented as it exist an analytical solution in
    Poiseuille flow conditions _[1].

    Parameters
    ----------
    mu : float
        Dynamic viscosity
    rho : float
        Fluid density
    nu : float
        Kinematic viscosity

    Attributes
    ----------
    rho : float
        Fluid density
    muN : float
        Dynamic viscosity of blood as Newtonian fluid
    muF :
        Dynamic viscosity
    k0 : float

    kInf : float

    Ht : float
        Hematocrite level
    sRC : float
        Critical shear rate
    tau0 : float
        Quemada paramter defined in _[1] as the bulk stress
    muInf : float
        Quemada paramter defined in _[1] as the dynamic viscosity when shear
        rate goes to infinity
    lmd : float
        Quemada paramter defined in _[1]
    q : float
        Quemada paramter defined in _[1]
    P1 : float
        Quemada paramter defined in _[1] as a polynom of q
    P2 : float
        Quemada paramter defined in _[1] as a polynom of q
    P3 : float
        Quemada paramter defined in _[1] as a polynom of q
    P4 : float
        Quemada paramter defined in _[1] as a polynom of q
    P5 : float
        Quemada paramter defined in _[1] as a polynom of q
    P6 : float
        Quemada paramter defined in _[1] as a polynom of q
    P7 : float
        Quemada paramter defined in _[1] as a polynom of q
    P8 : float
        Quemada paramter defined in _[1] as a polynom of q
    Psum : np.array of shape (8,)
        The array of [P1, P2, P3, P4, P5, P6, P7, P8]
    argsF : tuple
        All the constant of the fluid gather in a generic tuple

    References
    ----------
    .. [1] A.S. Popel, G. Enden, "An analytical solution for steady flow of
            a Quemada fluid in a circular tube", Vol. 32, pp. 422-426, 1993.
    """
    def __init__(self, iterable=(), **kwargs):
        """Fast constructor of the object. As the class must be able to
        evolve quickly, the parameters are implicitly defined in the dictionary
        during the initialization. This is a method that allows flexibility in
        development but may cause a lot of errors.
        """
        self.__dict__.update(iterable, **kwargs)
        # nu Newt
        self.nuN = self.muN / self.rho
        # eq (5) Popel
        self.nuF = self.muF / self.rho
        self.tau0 = self.muF * self.sRC * (((0.5 * self.Ht * \
                (self.k0 - self.kInf))**2.0) / \
                ((1.0 - 0.5 * self.kInf * self.Ht)**4.0))
        # eq (6) Popel
        self.muInf = self.muF / (1.0 - 0.5 * self.kInf * self.Ht)**2.0
        # print('self.muInf', self.muInf)
        # eq (7) Popel
        self.lmd = self.sRC * ((1.0 - 0.5 * self.k0 * self.Ht) /\
                (1.0 - 0.5 * self.kInf * self.Ht))**2.0
        # eq (10) Popel
        self.q = (np.sqrt(self.tau0) - np.sqrt(self.muInf * self.lmd)) /\
                (np.sqrt(self.tau0) + np.sqrt(self.muInf * self.lmd))
        self.P1 = -(1.0 / 7.0) * (self.q + 8.0)
        self.P2 = -(1.0 / 42.0) * (13.0 * self.q**2.0 - 8.0 * self.q - 7.0)
        self.P3 = -(1.0 / 210.0) * (143.0 * self.q**3.0 - 88.0 * self.q**2.0 - \
                113.0*self.q + 48.0)
        self.P4 = -(1.0 / 840.0) * (1287.0 * self.q**4.0 - 792.0 * self.q**3.0-\
                1342.0 * self.q**2.0 + 632.0 * self.q + 175.0)
        self.P5 = -(1. / 840.) * (3003. * self.q**5. - 1848. * self.q**4. - \
                3894. * self.q**3. + 1944. * self.q**2. + 1011. * self.q - 256.)
        self.P6 = -(1. / 1680.) * (15015. * self.q**6. - 9240. * self.q**5. - \
                23331. * self.q**4. + 12096. * self.q**3. + 9081. * self.q**2.-\
                3176.0 * self.q - 525.0)
        # weard power 27720.0 * self.q**2.0 ?? or 27720.0 * self.q**6.0
        # written as in the article self.q**2.
        self.P7 = -(1. / 1680.) * (45045. * self.q**7. - 27720. * self.q**2. - \
                82005. * self.q**5. + 43680. * self.q**4. + 42819. *self.q**3.-\
                17304. * self.q**2.0 - 5619.0 * self.q + 1024.0)
        self.P8 = -(1.0 / 16.0) * (1.0 - self.q)**2.0 * (1.0 + self.q) * \
                (429. * self.q**5. + 165. * self.q**4. - 330. * self.q**3. - \
                90.0 * self.q**2.0 + 45.0 * self.q + 5.0)
        self.Psum = np.array([self.P1, self.P2, self.P3, self.P4, \
                self.P5, self.P6, self.P7, self.P8])
        self.argsF = (self.tau0, self.muInf, self.lmd, self.q, self.Psum)

    def dynamicViscosity(self, sR=np.linspace(1e-2,1e3,101)):
        """
        Parameters
        ---------
        sr : np.array or float
            shear rate variable
        Returns
        -------
        mu : np.array or float
            dynamic viscosity
        """
        k = ((self.k0 + self.kInf * np.sqrt(sR/self.sRC)) / \
                (1. + np.sqrt(sR/self.sRC)))
        return self.muF * (1.0 - 0.5 * k * self.Ht)**-2.0

class Casson(Fluid):
    """ A Casson fluid

    Parameters
    ----------
    rho : float
        Fluid density
    muInf : float
        Dynamic viscosity when shear rate goes to infinity
    tau0 : float
        Bulk stress

    Attributes
    ----------
    rho : float
        Fluid density
    nuN : float
        Dynamic viscosity of Newtonian fluid
    muInf : float
        Dynamic viscosity when shear rate goes to infinity
    tau0 : float
        Bulk stress
    argsF : tuple
        All the constant of the fluid gather in 1 tuple

    """
    def __init__(self, iterable=(), **kwargs):
        """Fast constructor of the object. As the class must be able to
        evolve quickly, the parameters are implicitly defined in the dictionary
        during the initialization. This is a method that allows flexibility in
        development but may cause a lot of errors.
        """
        self.__dict__.update(iterable, **kwargs)
        # nu Newt
        self.nuInf = self.muInf / self.rho
        self.nuN = self.muN / self.rho
        self.sR_eq = 1.0
        self.nuEq = self.dynamicViscosity(self.sR_eq) / self.rho
        self.argsF = (self.tau0, self.muInf)
    def dynamicViscosity(self, sR=np.linspace(1e-2,1e3,101)):
        """
        Parameters
        ---------
        sr : np.array or float
            shear rate variable
        Returns
        -------
        mu : np.array or float
            dynamic viscosity
        """
        return self.muInf + (self.tau0 * self.muInf)**0.5 * sR**-0.5 + \
                self.tau0 * sR**-1.0

class WalburnScheck(Fluid):
    """ A Walburn--Scheck fluid _[1]

    Parameters
    ----------
    rho : float
        Fluid density [kg/m3]
    D1 : float
        Dynamic viscosity [Pa.s^n]
    D2 : float
        in [-]
    D3 : float
        in [-]
    D4 : float
        in [-]
    TPMA : float
        Total protein minus albumin concentration
    Ht : float
        Hematocrit in [-]
    
    Attributes
    ----------
    rho : float
        Fluid density
    D1 : float
        Dynamic viscosity [Pa.s^n]
    D2 : float
        in [-]
    D3 : float
        in [-]
    D4 : float
        in [-]
    TPMA : float
        Total protein minus albumin concentration
    Ht : float
        Hematocrit in [-]
    argsF : tuple
        All the constant of the fluid gather in 1 tuple

    References
    ----------
    .. [1] F.J. Walburn, D.J. Scheck, "A CONSTITUTIVE EQUATION FOR WHOLE HUMAN
            BLOOD", Vol. 13, pp. 201-210, 1976.

    """

    def __init__(self, iterable=(), **kwargs):
        """Fast constructor of the object. As the class must be able to
        evolve quickly, the parameters are implicitly defined in the dictionary
        during the initialization. This is a method that allows flexibility in
        development but may cause a lot of errors.
        """
        self.__dict__.update(iterable, **kwargs)
        # nu Newt
        self.nuN = self.muN / self.rho

        self.n = 1.0 - self.D3 * self.Ht
        self.K = self.D1 * np.exp(self.D2*self.Ht) * \
                np.exp(self.D4*self.TPMA/self.Ht**2)
        self.k = self.K / self.rho  # kinematic viscosity
        self.sR_eq = 1.0
        self.nuEq = self.dynamicViscosity(self.sR_eq) / self.rho

        self.argsF = (self.n, self.K)

    def dynamicViscosity(self, sR=np.linspace(1e-2,1e3,101)):
        """
        Parameters
        ---------
        sr : np.array or float
            shear rate variable
        Returns
        -------
        mu : np.array or float
            dynamic viscosity
        """
        return self.K * (sR)**(self.n - 1.0)

class CarreauYasuda(Fluid):
    """ A Carreau--Yasuda fluid _[1]

    Parameters
    ----------
    rho : float
        Fluid density [kg/m3]
    lmd : float
        [1/s]
    n : float
        Power coefficiant
    a : float
        Power coefficiant
    muInf : float
        Dynamic viscosity when shear rate goes to infinity [pa.s]
    mu0 : float
        Dynamic viscosity when shear rate goes to 0 in [Pa]

    Attributes
    ----------
    rho : float
        Fluid density [kg/m3]
    lmd : float
        [1/s]
    n : float
        Power coefficiant
    a : float
        Power coefficiant
    muInf : float
        Dynamic viscosity when shear rate goes to infinity [pa.s]
    mu0 : float
        Dynamic viscosity when shear rate goes to 0 in [Pa]
    nuN : float
        Dynamic viscosity of Newtonian fluid
    argsF : tuple
        All the constant of the fluid gather in 1 tuple

    References
    ----------
    .. [1] Carreau


    """
    def __init__(self, iterable=(), **kwargs):
        """Fast constructor of the object. As the class must be able to
        evolve quickly, the parameters are implicitly defined in the dictionary
        during the initialization. This is a method that allows flexibility in
        development but may cause a lot of errors.
        """
        self.__dict__.update(iterable, **kwargs)

        self.sR_eq = 1.0
        self.nuEq = self.dynamicViscosity(self.sR_eq) / self.rho

    def dynamicViscosity(self, sR=np.linspace(1e-2,1e3,101)):
        """
        Parameters
        ---------
        sr : np.array or float
            shear rate variable
        Returns
        -------
        mu : np.array or float
            dynamic viscosity
        """
        return self.muInf + (self.mu0 - self.muInf) * (1.0 + \
                (self.lmd * sR)**self.a)**((self.n-1.)/self.a)

