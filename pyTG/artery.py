#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import sys
import os
from warnings import warn


class Artery(object):
    """ Class for the object Artery. The constructor also initialize the artery
    and compare geometrical parameters with _[1], _[2], _[3], _[4].

    Parameters
    ----------
    R0 : float
        Initial uniform raduis of the artery
    L : float
        Total length of artery in monodimensional case
    e : float
        Total wall thickness in monodimensional case
    Vf_El : float
        Volume fraction of elastin in entire artery
    Vf_SMC : float
        Volume fraction of smooth muscle cells
    Vf_C : float
        Volume fraction of collagen
    e_E : float
        Endothelium thickness

    Attributes
    ----------
    R0 : float
        Initial uniform raduis of the artery
    L : float
        Total length of artery in monodimensional case
    e : float
        Total wall thickness in monodimensional case
    Vf_El : float
        Volume fraction of elastin in entire artery
    Vf_SMC : float
        Volume fraction of smooth muscle cells
    Vf_C : float
        Volume fraction of collagen
    e_E : float
        Endothelium thickness
    R : float
        ?????????????
    Rext : float
        External radius of the artery assumed constant
    Li : float
        Entrance length of artery in bidimensional case
    Lm : float
        Middle length of artery in bidimensional case
    Lo : float
        Outlet length of artery in bidimensional case
    x0 : float
        Entrance X coordinate of the inlet of the artery in bidimensional case
    x1 : float
         X coordinate of the inlet/middle interface in bidimensional case
    x2 : float
         X coordinate of the middle/outlet interface in bidimensional case
    x3 : float
        X coordinate of the outlet of the artery in bidimensional case
    Vf_O : float
        Volume fraction of others arterial constituants
    Vl : float 
        Volume of the lumen 
    Vw : float 
        Volume of arterial wall at physiologic state : intima + media + adventice 
    Vall : float 
        Volume of the lumen + arterial wall
    Vi : float
        Volume of intima layer
    Vm : float
        Volume of media layer
    Va : float
        Volume of adventicia layer
    Vf_Eli : float
        Volume fraction of elastin in intima
    Vf_Elm : float
        Volume fraction of elastin in media
    Vf_Ela : float
        Volume fraction of elastin in adventice
    Vf_SMCi : float 
        Volume fraction of vSMCs in intima
    Vf_SMCm : float
        Volume fraction of vSMCs in media
    Vf_SMCa : float
        Volume fraction of vSMCs in adventice
    Vf_Ci : float 
        Volume fraction of collagen in intima
    Vf_Cm : float
        Volume fraction of collagen in media
    Vf_Ca : float
        Volume fraction of collagen in adventice
    Vf_Oi : float
        Volume fraction of others constituants in intima
    Vf_Om : float
        Volume fraction of others constituants in media
    Vf_Oa : float
        Volume fraction of others constituants in adventice
    Vf_i : float 
        Sum of olume fraction  in intima
    Vf_m : float
        Sum of volume fraction  in media
    Vf_a : float
        Sum of volume fraction  in adventice
    V_Ei : float
        Volume of endothelial cells in intima layer
    V_Em : float
        Volume of endothelial cells in media layer
    V_Ci : float
        Volume of collagen in intima layer
    V_Cm : float
        Volume of collagen in media layer
    V_SMCi : float
        Volume of smooth muscle cells in intima layer
    V_SMCm : float
        Volume of smooth muscle cells in media layer
    V_Eli : float
        Volume of elastin in intima layer
    V_Elm : float
        Volume of elastin in media layer
    V_Oi : float
        Volume of constituants in intima layer
    V_Om : float
        Volume of other constituants in media layer
    R_LEE : float
        Radius of the external lamina
    e_a : float
        Thickness of the adventice
    R_LEI : float
        Radius of the internal lamina
    e_m : float
        Thickness of the media
    e_i : float
        Thickness of the intima

    References                                                              
    ----------                                                              
    .. [1] G. Karner, K. Perktold, "Effect of endothelial injury and increased
            blood pressure on albumin accumulation in the arterial wall: 
            a numerical study", pp. 705-715, Journal of Biomechanics, 1999.
    .. [2] H.C. Stary, D.H. Blankenhorn et al, "A Definition of the Intima 
        of Human Arteries and of Its Atherosclerosis-Prone Regions", pp. 120-134, 
            AHA Medical/Scientific Statement, 1991.
    .. [3] Fung
    .. [4] Connell 
    """

    def __init__(self, iterable=(), **kwargs):
        """Fast constructor of the object. As the class must be able to
        evolve quickly, the parameters are implicitly defined in the dictionary
        during the initialization. This is a method that allows flexibility in
        development but may cause a lot of errors.
        """
        print('\n----------\nArtery fast init\n----------')
        warn("Artery fast constructor")
        self.__dict__.update(iterable, **kwargs)
        self.R = self.R0
        self.Rext = self.R0 + self.e
        warn('In bidimensional or tridim case, L has not to be given by user')
        if kwargs.get('L') is None:
            self.L = self.R0 * (self.ratioLe_R +
                                   self.ratioLm_R + self.ratioLs_R)
            self.Li = self.R0 * self.ratioLe_R
            self.Lm = self.R0 * self.ratioLm_R
            self.Lo = self.R0 * self.ratioLs_R
            # x positions of boundaries zone points
            self.x0 = 0.0
            self.x1 = self.Li
            self.x2 = self.x1 + self.Lm
            self.x3 = self.x2 + self.Lo
            print('x0 = {:e}, x1 = {:e}, x2 = {:e}, x3 = {:e}'.format(self.x0,
                self.x1, self.x2, self.x3))
        print('R0 = {:e}'.format(self.R0))
        print('Rext = {:e}'.format(self.Rext))
        print('L = {:e}'.format(self.L))
        print('--------------------------------')

        self.Vf_O = 1.0 - (self.Vf_El + self.Vf_SMC + self.Vf_C)

        self.Vl = self.L * np.pi * self.R0**2
        # print('Vw', self.Vw)
        self.Vall = self.L * np.pi * (self.Rext**2)
        self.Vw = self.Vall - self.Vl
        self.Vendo = np.pi *((self.R0 + self.e_E)**2 -(self.R0)**2. ) * self.L

        self.Vf_E = self.Vendo / self.Vw
        self.Vf_O = self.Vf_O - self.Vf_E
        if self.Vf_O < 0.0:
            raise ValueError('Volume frac of others constituants is negative')


        #############################
        # definition of composition of each layers
        ############################
        self.definitionPourcentage()

        self.Vi = self.Vw * self.Vf_i
        self.Vm = self.Vw * self.Vf_m
        self.Va = self.Vw * self.Vf_a

        print("Vw = {:.8e}".format(self.Vw))
        print("Vi = {:.8e}, Vm = {:.8e}, Va = {:.8e},".format(self.Vi,
            self.Vm, self.Va))
        print("e = {:.8e} um".format(self.e * 1e6))
        
        if not np.isclose(self.Vw, self.Vi+self.Vm+self.Va):
            raise ValueError("Vw != sum(volumes of layers)")
        # def the volumes occupied by the species
        self.V_Ei = self.Vw * self.Vf_Ei
        print('V_Ei', self.V_Ei)
        self.V_Em = self.Vw * self.Vf_Em
        
        self.V_Ci = self.Vw * self.Vf_Ci
        self.V_Cm = self.Vw * self.Vf_Cm

        self.V_SMCi = self.Vw * self.Vf_SMCi
        self.V_SMCm = self.Vw * self.Vf_SMCm

        self.V_Eli = self.Vw * self.Vf_Eli
        self.V_Elm = self.Vw * self.Vf_Elm

        self.V_Oi = self.Vw * self.Vf_Oi
        self.V_Om = self.Vw * self.Vf_Om

        # definition geometrical metrics
        self.R_LEE = np.sqrt((self.Rext)**2 - self.Va / (np.pi * self.L))
        self.e_a = self.Rext - self.R_LEE
        self.R_LEI = np.sqrt((self.R_LEE)**2 - self.Vm / (np.pi * self.L))
        self.e_m = self.R_LEE - self.R_LEI
        self.e_i = self.R_LEI - self.R0

        print('Volume fractions of species within artery :')
        print('\\hline')
        print('\tIntima\t&\tMedia\t&\tAdventice\t&\tWhole wall \\\\')
        print('\\hline')
        print('e\t\t&\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}} \\\\'.format(
            self.Vf_Eli, self.Vf_Elm,self.Vf_Ela, self.Vf_El))
        print('SMC\t\t&\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\\\\'.format(
            self.Vf_SMCi, self.Vf_SMCm,self.Vf_SMCa, self.Vf_SMC))
        print('C\t\t&\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\\\\'.format(
            self.Vf_Ci,self.Vf_Cm, self.Vf_Ca, self.Vf_C))
        print('E\t\t&\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\\\\'.format(
            self.Vf_Ei, self.Vf_Em, self.Vf_Ea, self.Vf_E))
        print('others\t&\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}} \\\\'.format(
            self.Vf_Oi, self.Vf_Om, self.Vf_Oa, self.Vf_O))
        print('\\hline')
        print('Layer\t&\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}} \\\\'.format(
            self.Vf_i, self.Vf_m, self.Vf_a , self.Vf_i+self.Vf_m+self.Vf_a))
        print('\\hline')
        print('\\hline')
        print("Thickness (\\si{\\micro \\meter})"+
            "&\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}} \\\\".format(
            self.e_i * 1e6, self.e_m * 1e6, self.e_a * 1e6,
            (self.e_i+self.e_m+self.e_a)*1e6))
        print("Thickness \\cite{Karner2000} (\\si{\\micro \\meter})" +
                "&\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}}\t&\t\\num{{{:.4e}}} \\\\".format(
                    12, 302, 186, 12+302+186))
        print('\\hline')

        print('Rext = {:.8e}, R_LEE = {:.8e}; R_LEI = {:.8e} R0 = {:.8e}'.format(
            self.Rext, self.R_LEE, self.R_LEI, self.R0))
        print('Ratio intima/media = {:.8e}'.format(self.e_i/self.e_m))
        print('In literature, Ratio in [0.1, 1] from Stary 1991')

        print('*************************************************************')
        print('*** Data from Fung Mechanical Properties of Living Tissues ***')
        print('*** Table 8.2:1 chap 8 p324')
        print('the artery defined here compared with data of Thoracic & Plantar :')
        print('    min(Thoracic,Plantar) < myValue <  max(Thoracic,Plantar)')
        print('__________________|____________')
        print('Media')
        print('vSMCs^*            0.335 < {:.8e} < .605'.format(
            self.Vf_SMCm/self.Vf_m))
        print('^* Tonar 2008 Vf_vSMCm = 0.64-0.69 ds aorte adbominal != Fung 0.33')
        print('Elastin          0.013 < {:.8e} < 0.243'.format(
            self.Vf_Elm/self.Vf_m))
        print('Collagen         0.119 < {:.8e} < 0.68'.format(
            self.Vf_Cm/self.Vf_m))
        print('Ground subs      0.056 < < 0.264  ')
        print('Adventice')
        print('Fibro(vSMCs-like) 0.094 < {:.8e} < .114'.format(
            self.Vf_SMCa/self.Vf_a))
        print('Elastin           0.0 < {:.8e} < 0.024'.format(
            self.Vf_Ela/self.Vf_a))
        print('Collagen          0.639 < {:.8e} < 0.777'.format(
            self.Vf_Ca/self.Vf_a))
        print('Ground subs       0.106 < < 0.247  ')
        print('__________________|____________')



        print('Intima :')
        print('\tVf_SMC_i={:.8e} \n\tVf_Eli={:.8e} \n\tVf_Ci={:.8e}'.format(
            self.Vf_SMCi, self.Vf_Eli, self.Vf_Ci))
        print('Media : ')
        print('Vf_SMC_m={:.8e}  Vf_El_m={:.8e}  Vf_Cm={:.8e}'.format(
                     self.Vf_SMCm, self.Vf_Elm, self.Vf_Cm))
        print("V_Oi = {:.8e} V_Om = {:.8e}".format(self.V_Oi, self.V_Om))

    def definitionPourcentage(self):
        """From data of _[1] , Bellini 2014 A microstructurally motivated model
            of arterial wall mechanics with mechanobiological implications, _[3]

        Parameters
        ----------

        Returns
        -------
        a : 
            Volume fraction calculation of each population within layers (i+m+a)

        References                                                              
        ----------                                                              
        .. [1] M. O’Connell, S. Murthy, S. Phan, C. Xu, J. Buchanan, 
                R. Spilker, R. Dalman, C. Zarins, W. Denk, C. Taylor, "
                The three-dimensional micro- and nanostructure of the aortic 
                medial lamellar unit measured using 3D confocal & electron 
                microscopy imaging.", pp. 171-181, Matrix Biology : Journal 
                of the InternationalSociety for Matrix Biology, 2008.
        .. [2] S. Glagov, J. Grande, D. Vesselinovitch, C.K. Zarins, 
            "Quantitation of Cells and Fibers in Histologic Sections of 
            Arterial Walls: Advantages of Contour Tracing on a Digitizing 
            Plate", Springer New York, 1981.
        """
        # def statistique conditionnel à la main pour approcher Karner 1999
        Prop_CEs_ipm = 1.0
        Prop_CEs_a = 1. - Prop_CEs_ipm
        Prop_Elipm = .9 # avant 0.84 n'a pas d'influence sur C, vSMCs ...
        Prop_Ela = 1. - Prop_Elipm
        Prop_SMC_ipm = 0.9 # 0.95
        Prop_SMC_a = 1. - Prop_SMC_ipm
        Prop_Cipm = .1
        Prop_Ca = 1. - Prop_Cipm
        Prop_O_ipm = .1
        Prop_O_a = 1. - Prop_O_ipm

        Prop_CEs_i_sachant_ipm = 1
        Prop_CEs_m_sachant_ipm = 1. - Prop_CEs_i_sachant_ipm
        Prop_Eli_sachant_ipm = .01
        Prop_Elm_sachant_ipm = 1. - Prop_Eli_sachant_ipm
        Prop_SMC_i_sachant_ipm = .05
        Prop_SMC_m_sachant_ipm = 1. - Prop_SMC_i_sachant_ipm
        Prop_Ci_sachant_ipm = .05 # avant 0.1
        Prop_Cm_sachant_ipm = 1. - Prop_Ci_sachant_ipm
        Prop_O_i_sachant_ipm = .5
        Prop_O_m_sachant_ipm = 1. - Prop_O_i_sachant_ipm

        # stat finale par couche arterielle :
        # endothelium
        Prop_CEs_i = Prop_CEs_ipm * Prop_CEs_i_sachant_ipm
        Prop_CEs_m = Prop_CEs_ipm * Prop_CEs_m_sachant_ipm

        # elastine
        Prop_Eli = Prop_Elipm * Prop_Eli_sachant_ipm
        Prop_Elm = Prop_Elipm * Prop_Elm_sachant_ipm

        # CMLs
        Prop_SMC_i = Prop_SMC_ipm * Prop_SMC_i_sachant_ipm
        Prop_SMC_m = Prop_SMC_ipm * Prop_SMC_m_sachant_ipm
        
        # collagene
        Prop_Ci = Prop_Cipm * Prop_Ci_sachant_ipm
        Prop_Cm = Prop_Cipm * Prop_Cm_sachant_ipm

        # autres
        Prop_O_i = Prop_O_ipm * Prop_O_i_sachant_ipm
        Prop_O_m = Prop_O_ipm * Prop_O_m_sachant_ipm

        # stat finale par couche arterielle :
        # endothelium
        Prop_CEs_i = Prop_CEs_ipm * Prop_CEs_i_sachant_ipm
        Prop_CEs_m = Prop_CEs_ipm * Prop_CEs_m_sachant_ipm

        # elastine
        Prop_Eli = Prop_Elipm * Prop_Eli_sachant_ipm
        Prop_Elm = Prop_Elipm * Prop_Elm_sachant_ipm

        # CMLs
        Prop_SMC_i = Prop_SMC_ipm * Prop_SMC_i_sachant_ipm
        Prop_SMC_m = Prop_SMC_ipm * Prop_SMC_m_sachant_ipm
        
        # collagene
        Prop_Ci = Prop_Cipm * Prop_Ci_sachant_ipm
        Prop_Cm = Prop_Cipm * Prop_Cm_sachant_ipm

        # autres
        Prop_O_i = Prop_O_ipm * Prop_O_i_sachant_ipm
        Prop_O_m = Prop_O_ipm * Prop_O_m_sachant_ipm
        self.Vf_Ei = self.Vf_E * Prop_CEs_i
        self.Vf_Em = self.Vf_E * Prop_CEs_m
        self.Vf_Ea = self.Vf_E * Prop_CEs_a
        verif_CEs = self.Vf_Ei + self.Vf_Em + self.Vf_Ea
        if(np.abs(verif_CEs - self.Vf_E) > 1e-8):
            raise ValueError('pb Vf CEs')


        self.Vf_Eli = self.Vf_El * Prop_Eli
        self.Vf_Elm = self.Vf_El * Prop_Elm
        self.Vf_Ela = self.Vf_El * Prop_Ela
        verif_el = self.Vf_Eli + self.Vf_Elm + self.Vf_Ela
        if(np.abs(verif_el - self.Vf_El) > 1e-8):
            raise ValueError('pb Vf El')

        self.Vf_SMCi = self.Vf_SMC * Prop_SMC_i
        self.Vf_SMCm = self.Vf_SMC * Prop_SMC_m
        self.Vf_SMCa = self.Vf_SMC * Prop_SMC_a
        verif_SMC = self.Vf_SMCi + self.Vf_SMCm + self.Vf_SMCa
        if(np.abs(verif_SMC - self.Vf_SMC) > 1e-8):
            raise ValueError('pb Vf SMC')

        self.Vf_Ci = self.Vf_C * Prop_Ci
        self.Vf_Cm = self.Vf_C * Prop_Cm
        self.Vf_Ca = self.Vf_C * Prop_Ca
        verif_C = self.Vf_Ci + self.Vf_Cm + self.Vf_Ca
        if(np.abs(verif_C - self.Vf_C) > 1e-8):
            raise ValueError('pb Vf Col')

        self.Vf_Oi = self.Vf_O * Prop_O_i
        self.Vf_Om = self.Vf_O * Prop_O_m
        self.Vf_Oa = self.Vf_O * Prop_O_a
        verif_O = self.Vf_Oi + self.Vf_Om + self.Vf_Oa
        if(np.abs(verif_O - self.Vf_O) > 1e-8):
            raise ValueError('pb Vf others')

        self.Vf_i = self.Vf_Eli + self.Vf_SMCi + self.Vf_Ci + self.Vf_Ei + self.Vf_Oi
        self.Vf_m = self.Vf_Elm + self.Vf_SMCm + self.Vf_Cm + self.Vf_Em + self.Vf_Om
        self.Vf_a = self.Vf_Ela + self.Vf_SMCa + self.Vf_Ca + self.Vf_Ea + self.Vf_Oa
        verifSumLayers = self.Vf_i + self.Vf_m + self.Vf_a
        if(np.abs(verifSumLayers) - 1 > 1e-6):
            print("Sum=",verifSumLayers)
            raise ValueError('ERROR !!! Sum Vf Layers Vf_i + Vf_m + Vf_a =! 1')

        return
