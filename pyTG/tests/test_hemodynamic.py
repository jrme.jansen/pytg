#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np


from pyTG.artery import Artery
from pyTG.fluid import Fluid, Newtonian, Quemada, \
        CarreauYasuda, WalburnScheck, Casson
from pyTG.hemodynamic import Hemodynamic

def test_NewtonianCylUnsteady():
    dictAr = {'R0': 0.004, 'ratioLe_R': 10, 'ratioLm_R': 10, 'ratioLs_R': 10,
        'e': 500e-6, 'Vf_El': .25, 'Vf_SMC': .24,
        'Vf_C': .45, 'e_E': 2e-6}
    artery = Artery(**dictAr)

    dictN = {'model': 'Newtonian', 'rho': 1056.0, 'mu': 3.45e-3}
    N = Newtonian(**dictN)

    pathData = "dataFlowRate_SC_rest.dat"
    paramsWo = {'myArtery': artery,
                'myFluid': N,
                'planar': False,
                'unsteady': True,
                'dataFlowRate': pathData}
    hemo = Hemodynamic(**paramsWo)

    t_ = np.linspace(0, hemo.T, 11)
    x_ = np.linspace(0, artery.R0)
    v = hemo.womersleyVelocityField(t_, x_)
    head = ""
    for i in range(len(t_)):
        head += " u(t=%s)" % t_[i]
    np.savetxt("velocityWomerAna.dat", np.vstack((x_, v)).T,header = head)

def test_NewtonianPla():
    """ Newtonian Planar configuration 1 block
    """
    dictAr = {'R0': 0.004, 'ratioLe_R': 10, 'ratioLm_R': 10, 'ratioLs_R': 10,
            'e': 500e-6, 'Vf_El': .25, 'Vf_SMC': .24,
            'Vf_C': .45, 'e_E': 2e-6}
    artery = Artery(**dictAr)

    dictN = {'model': 'Newtonian', 'rho': 1056.0, 'mu': 3.45e-3}
    N = Newtonian(**dictN)

    # want Re = 300.0
    muN = 3.45e-3; rhoN = 1056.0
    Re = 300.0
    S = 2* artery.R
    D = 2 * artery.R
    Qv = Re * S * (muN / rhoN) / D
    dict_hemo = {'Qv': Qv,
                            'myArtery': artery,
                            'myFluid': N,
                            'planar': True,
                            'unsteady': False
                            }
    hemo = Hemodynamic(**dict_hemo)
    hemo.update_WSS(502e-6)
    hemo.update_WSS(508e-6)
    hemo.update_WSS(510e-6)


def test_NewtonianCyl():
    dictAr = {'R0': 0.004, 'ratioLe_R': 10, 'ratioLm_R': 10, 'ratioLs_R': 10,
        'e': 500e-6, 'Vf_El': .25, 'Vf_SMC': .24,
        'Vf_C': .45, 'e_E': 2e-6}
    artery = Artery(**dictAr)

    dictN = {'model': 'Newtonian', 'rho': 1056.0, 'mu': 3.45e-3}
    N = Newtonian(**dictN)

    # want Re = 300.0
    muN = 3.45e-3; rhoN = 1056.0
    Re = 300.0
    S = np.pi * artery.R**2
    D = 2 * artery.R
    Qv = Re * S * (muN / rhoN) / D
    dict_hemoCyl_Newt = {'Qv': Qv,
                            'myArtery': artery,
                            'myFluid': N,
                            'planar': False,
                            'unsteady': False
                            }
    hemo = Hemodynamic(**dict_hemoCyl_Newt)
    hemo.update_WSS(502e-6)
    hemo.update_WSS(508e-6)
    hemo.update_WSS(510e-6)


def test_WalburnSheckCyl():
    dictAr = {'R0': 0.004, 'ratioLe_R': 10, 'ratioLm_R': 10, 'ratioLs_R': 10,
        'e': 500e-6, 'Vf_El': .25, 'Vf_SMC': .24,
        'Vf_C': .45, 'e_E': 2e-6}
    artery = Artery(**dictAr)

    dictWS = {'model': 'WalburnScheck', 'rho': 1056.0,
             'D1': 0.797e-3, # Pa.s
             'D2': 0.0608, 'D3': 0.00499, #
             'D4': 14.585, # Ht
             'TPMA': 25.9, #
             'Ht': 40}
    WS = WalburnScheck(**dictWS)
    # want Re = 300.0
    muN = 3.45e-3; rhoN = 1056.0
    Re = 300.0
    S = np.pi * artery.R**2
    D = 2 * artery.R
    Qv = Re * S * (muN / rhoN) / D
    dict_hemoCyl_WS = {'Qv': Qv,
                            'myArtery': artery,
                            'myFluid': WS,
                            'planar': False,
                            'unsteady': False
                            }
    hemo = Hemodynamic(**dict_hemoCyl_WS)
    hemo.update_WSS(502e-6)
    hemo.update_WSS(508e-6)
    hemo.update_WSS(510e-6)


def test_CassonCyl():
    dictAr = {'R0': 0.004, 'ratioLe_R': 10, 'ratioLm_R': 10, 'ratioLs_R': 10,
        'e': 500e-6, 'Vf_El': .25, 'Vf_SMC': .24,
        'Vf_C': .45, 'e_E': 2e-6}
    artery = Artery(**dictAr)
    dictC = {'model': 'Casson',                                
                 'rho': 1056.0,                                
                 'muInf': 3.1e-3,                              
                 'tau0': 10.86e-3                              
              }                                                
    C = Casson(**dictC)                                        
    # want Re = 300.0
    muN = 3.45e-3; rhoN = 1056.0
    Re = 300.0
    S = np.pi * artery.R**2
    D = 2 * artery.R
    Qv = Re * S * (muN / rhoN) / D

    paramsHemoC = {'Qv': Qv,
                  'myArtery': artery,
                  'myFluid': C,
                  'planar': False,
                  'unsteady': False
                  }
    hemo = Hemodynamic(**paramsHemoC)
    hemo.update_WSS(502e-6)
    hemo.update_WSS(508e-6)
    hemo.update_WSS(510e-6)



def test_QuemadaCyl():

    dictAr = {'R0': 0.004, 'ratioLe_R': 10, 'ratioLm_R': 10, 'ratioLs_R': 10,
        'e': 500e-6, 'Vf_El': .25, 'Vf_SMC': .24,
        'Vf_C': .45, 'e_E': 2e-6}
    artery = Artery(**dictAr)

    dictQ = {'model': 'Quemada', 'rho': 1056.0, 'muF': 1.2e-3,
            'kInf': 2.07, 'k0': 4.33, #
            'Ht': 0.4, # Ht
            'sRC': 1.88 # s-1
            }

    Q = Quemada(**dictQ)
    # want Re = 300.0
    muN = 3.45e-3; rhoN = 1056.0
    Re = 300.0
    S = np.pi * artery.R**2
    D = 2 * artery.R
    Qv = Re * S * (muN / rhoN) / D
    dict_hemoCyl_Q = {'Qv': Qv,
                            'myArtery': artery,
                            'myFluid': Q,
                            'planar': False,
                            'unsteady': False
                            }
    hemo = Hemodynamic(**dict_hemoCyl_Q)
    hemo.update_WSS(502e-6)
    hemo.update_WSS(508e-6)
    hemo.update_WSS(510e-6)



