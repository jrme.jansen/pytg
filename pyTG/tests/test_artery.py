from pyTG.artery import Artery


def test_arteryInit():
    """ Test to init artery
    """
    dictAr = {'R0': 0.004,'L': 1e-2, 'e': 500e-6,
            'Vf_El': .25, 'Vf_SMC': .24, 'Vf_C': .45, 'e_E': 2e-6}
    dictAr2D = {'R0': 0.004, 'ratioLe_R': 10, 'ratioLm_R': 10, 'ratioLs_R': 10,
                  'e': 500e-6, 
            'Vf_El': .25, 'Vf_SMC': .24, 'Vf_C': .45, 'e_E': 2e-6}
    art = Artery(**dictAr)        
    art2D = Artery(**dictAr2D)    

