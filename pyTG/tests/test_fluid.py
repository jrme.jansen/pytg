#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

from pyTG.fluid import Fluid, Newtonian, Quemada, \
        CarreauYasuda, WalburnScheck, Casson

def test_fluid():
    dictN = {'model': 'Newtonian',
                 'rho': 1056.0,
                 'mu': 3.45e-3
              }
    N = Newtonian(**dictN)
    dictQ = {'model': 'Quemada',       
                 'rho': 1056.0,        
                 'muF': 1.2e-3,        
                 'kInf': 2.07,         
                 'k0': 4.33, #         
                 'Ht': 0.4, # Ht       
                 'sRC': 1.88 # s-1     
              }                        
    Q = Quemada(**dictQ)               
                                       
    dictWS = {'model': 'WalburnScheck',
                 'rho': 1056.0,        
                 'D1': 0.797e-3, # Pa.s
                 'D2': 0.0608,         
                 'D3': 0.00499, #      
                 'D4': 14.585, # Ht    
                 'TPMA': 25.9, #       
                 'Ht': 40              
              }                        
                                       
    WS = WalburnScheck(**dictWS)       
                                       
    dictCY = {'model': 'CarreauYasuda',
                 'rho': 1056.0,        
                 'lmd': 1.902,         
                 'n': 0.22,            
                 'a' : 1.25,           
                 'muInf': 3.45e-3, #   
                 'mu0': 56e-3, #       
              }                        
    CY = CarreauYasuda(**dictCY)                               
                                                               
    dictCarreau = {'model': 'CarreauYasuda',                   
                 'rho': 1056.0,                                
                 'lmd': 1.902,                                 
                 'n': 0.22,                                    
                 'a' : 2,                                      
                 'muInf': 3.45e-3, #                           
                 'mu0': 56e-3, #                               
              }                                                
    Car = CarreauYasuda(**dictCarreau)                         
                                                               
    dictC = {'model': 'Casson',                                
                 'rho': 1056.0,                                
                 'muInf': 3.1e-3,                              
                 'tau0': 10.86e-3                              
              }                                                
    C = Casson(**dictC)                                        
                                                               
    sR = np.power(10.0, np.linspace(1.0, 10.0, num=101)) * 1e-4
    # equi np.logspace(1.0, 5.0, num=101) * 1e-3               
    muC = C.dynamicViscosity(sR)                               
    muCY = CY.dynamicViscosity(sR)                             
    muCar = Car.dynamicViscosity(sR)                           
    muWS = WS.dynamicViscosity(sR)                             
    muQ = Q.dynamicViscosity(sR)                               
    muN = N.dynamicViscosity(sR)                               
    import matplotlib.pyplot as plt                      
    plt.figure()                                         
    plt.plot(sR, muC, label='C')                         
    plt.plot(sR, muCY, label='CY')                       
    plt.plot(sR, muCar, label='Carreau')                 
    plt.plot(sR, muWS, label='Walburn-Scheck')           
    plt.plot(sR, muQ,  label='Quemada')                  
    plt.plot(sR, muN,  label='Newotnian')                
    plt.legend()                                         
    plt.figure()                                         
    plt.loglog(sR, muC, label='C')                       
    plt.loglog(sR, muCY, label='CY')                     
    plt.loglog(sR, muCar, label='Carreau')               
    plt.loglog(sR, muWS, label='Walburn-Scheck')         
    plt.loglog(sR, muQ,  label='Quemada')                
    plt.loglog(sR, muN,  label='Newotnian')              
    plt.xlabel('$\dot{\gamma}$')                         
    plt.ylabel('$\mu$')                                  
    plt.legend()                                         
    plt.figure()                                         
    plt.loglog(sR, muC / C.rho, label='C')               
    plt.loglog(sR, muCY / CY.rho, label='CY')            
    plt.loglog(sR, muCar / CY.rho, label='Carreau')      
    plt.loglog(sR, muWS / CY.rho, label='Walburn-Scheck')
    plt.loglog(sR, muQ / CY.rho,  label='Quemada')       
    plt.loglog(sR, muN / CY.rho,  label='Newotnian')     
    plt.xlabel('$\dot{\gamma}$')                         
    plt.ylabel('$\nu$')                                  
    plt.legend()                                         
    plt.savefig('sRfluidFNG.png')
