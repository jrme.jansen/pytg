#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import time
import shutil
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from warnings import warn
from icecream import ic

import sys
import copy
import glob

from solve_dde import solve_dde

from pyTG.modelTG_1D import ModelTG_1D
from pyTG.artery import Artery
from pyTG.fluid import Newtonian, Quemada, WalburnScheck, Casson
from pyTG.hemodynamic import Hemodynamic, fromQvToG
from pyTG.utilities import diffusionBC_EC, paramsRelationsGFs, \
        colAging, findIndex_t0tf, message, calculVolumes_1D


def physioGFs(NtypeOfGFs, cp_dict, GFs_names,
        Vf_SiPhy, Vf_SmPhy, M_Phy, k_cp, zetaGFs):
    """ Computation of the steady state solution of GFs kinetic equations
    thanks to the cp_dict resolution of AX = B with numpy.linalg.solve
    hypothesis of the resolution ka=kp

    For the biochemical system of equation :
                        d/dt(X_bio)=B + A X_bio                     (1)
    If it is under equilibrium then :
                            B + A X_bio = 0                         (2)
    So, we construct the vector B and the Matrix A to find the X_bio solution of
    equation (2).
    For x a GF, B_x = M^x + ks*S, B is np.ndarray of shape (NtypeOfGFs,2)
    The matrix A is fill with coupling relation modeled, A is np.ndarray of
    shape (NtypeOfGFs,NtypeOfGFs,2)
    Parameters
    ---------
    NtypeOfGFs : int
        Number of family of GFs modeled
    cp_dict : dictionnary
        Dictionnary of coupling GFs modeled
    GFs_names : dictionnary
        DZA
    SiPhy : float
        Physiological count of S in intima layer
    SmPhy : float
        Physiological count of S in media layer
    M_Phy : np.ndarray shape (NtypeOfGFs,2)
        Sources terms
    k_cp : float
        couling rates
    zetaGFs : np.ndarray shape (NtypeOfGFs,)
    Returns
    -------
    Nc_GFs : np.ndarray shape (NtypeOfGFs,)
        Coupling number of each GF family
    steadyGFs : np.ndarray shape (NtypeOfGFs,2)
        equilibrium amount of GFs within layer
    """
    steadyGFs = np.zeros((NtypeOfGFs,2))
    # definition of N_cp for each growth factor
    Nc_GFs = np.zeros(NtypeOfGFs)
    for k in range(NtypeOfGFs):
        # print('self.GFs_names[k]', self.GFs_names[k])
        cp_list_k = cp_dict[GFs_names[k]]
        # print('cp_list_k', cp_list_k)
        for i in range(len(cp_list_k)):
            # print('cp_list_k[i]',cp_list_k[i])
            sign = cp_list_k[i][0]
            gf = cp_list_k[i][1:]
            idx = GFs_names.index(gf)
            if(sign == '+'):
                Nc_GFs[idx] += 1.0
            elif(sign == '-'):
                Nc_GFs[idx] -= 1.0
            else:
                raise ValueError('wrong syntax for cp_dict at GF : %s' % GFs_names[k])
    # self.Vf_SiPhy self.k_cp
    # print('self.Nc_GFs', self.Nc_GFs)
    # second member vector
    b = np.zeros((NtypeOfGFs,2))
    a = np.zeros((NtypeOfGFs, NtypeOfGFs, 2))
    VfS_Phy = [Vf_SiPhy, Vf_SmPhy]

    for i in range(2):
        for k in range(NtypeOfGFs):
            b[k,i] = M_Phy[k,i]
            a[k,k,i] = zetaGFs[k] + Nc_GFs[k] * VfS_Phy[i] * k_cp

            cp_list_k = cp_dict[GFs_names[k]]
            for l in range(len(cp_list_k)):
                sign = cp_list_k[l][0]
                gf = cp_list_k[l][1:]
                idx = GFs_names.index(gf)
                if(sign == '+'):
                    a[k,idx,i] = -VfS_Phy[i] * k_cp
                elif(sign == '-'):
                    a[k,idx,i] = +VfS_Phy[i] * k_cp
        steadyGFs[:,i] = np.linalg.solve(a[:,:,i],b[:,i])
    return Nc_GFs, steadyGFs

def termsGFs_equa(NtypeOfGFs, zetaGFs, Nc_GFs, k_cp, cp_dict, GFs_names, Vf_Si, Vf_Sm):
    """Computation of terms for GFs equation generic formulation for a GF x
    in layers k d/dt \eta_i^x = M^x_i + a \eta_i^x + cp.

    Parameters
    ----------
    NtypeOfGFs : int
        Number of family of GFs considered
    zetaGFs : np.ndarray (NtypeOfGFs,)
        Array of the zeta for each GFs
    Nc_GFs : np.ndarray (NtypeOfGFs,)
        Number of autocrine coupling mechanisms considered for each GFs family
    k_cp : float
        coupling rate
    cp_dict : dict
        The distionnary for the coupling autocrine relations
    GFs_names : list of string of len NtypeOfGFs
        the name of the GFs as written in dictionnay cp_dict
    Vf_Si : float
        volume fraction of Si
     Vf_Sm : float
        volume fraction of Sm
    Returns
    -------
    a : np.dnarray shape (NtypeOfGFs, 2)
        The
    cp : np.dnarray shape (NtypeOfGFs, NtypeOfGFs, 2)
        The
    """
    a = np.zeros((NtypeOfGFs, 2))
    cp = np.zeros((NtypeOfGFs, NtypeOfGFs,2))
    Vf_S = [Vf_Si, Vf_Sm]
    for i in range(2):
        for k in range(NtypeOfGFs):
            a[k,i] = -(zetaGFs[k] + Nc_GFs[k] * Vf_S[i] * k_cp)
            cp_list_k = cp_dict[GFs_names[k]]
            for l in range(len(cp_list_k)):
                sign = cp_list_k[l][0]
                gf = cp_list_k[l][1:]
                idx = GFs_names.index(gf)
                if(sign == '+'):
                    cp[k,idx,i] = Vf_S[i] * k_cp
                elif(sign == '-'):
                    cp[k,idx,i] = -Vf_S[i] * k_cp
    return a, cp


def rest_effort(t, Qv, T=50., Trest=50., I=0.0):
    """ Approxime the volumic variations during cycle of rest/effort
    Particular case of full rest if I = 0.0
    Parameters
    ---------
    t : float
        time
    T : float
        period of cycle rest/effort
    Trest : float
        period of rest in cycle rest/effort
    Qv : float
        mean flow rate
    I : float
        Intensity factor as in effort the flow rate is as Qv * (1 + I)
    Returns
    -------
    Q : float

    """
    if t % T < Trest:
        var = 0
    else:
        var = I * Qv
    return Qv + var

def checkPositive(Y):
    """Check if results are alway positive

    Parameters
    ----------
    Y : np.ndarray
        The solution array
    Returns
    -------


    """
    if np.any(Y < 0.0):
        message('Solutions have negative values somewhere!!!!!')
        raise ValueError(message)

def settings_WSS(WSS, Rmax, Rmin, GFs_names, RR_dict,
        keysWords, Rcoef_dict, alphaR, WSSR0,Pmax, Pmin, alphaP, WSSP0):
    """
    definition of mathematical function relation between ECs functional
    properties as $R_{WSS}$ and {P_E}_{WSS}
    
    Parameters
    ----------

    Returns
    -------
    R_GFs_WSS : np.ndarray of shape ()
    Perm_E_WSS : float

    """
    if isinstance(WSS,np.float):
        R1 = (Rmin + Rmax) * 0.5
        R2 = (Rmax - Rmin) * 0.5
        R_GFs_WSS = np.zeros(len(GFs_names))
        for k in range(len(GFs_names)):
            if(RR_dict[GFs_names[k]] == keysWords[0]):
                # i.p. relation between WSS and R_self.GFs_names[k]
                c = -1.
            elif(RR_dict[GFs_names[k]] == keysWords[1]):
                # p. relation between WSS and R_self.GFs_names[k]
                c = 1.
            else:
                print('given relation :', RR_dict[GFs_names[k]])
                raise ValueError('wrong keyword for Release GF relation. Accepted : i.p. / p.')
            R_GFs_WSS[k] = Rcoef_dict[GFs_names[k]] * (R1 + R2 * np.tanh(c * alphaR *
                                              (np.abs(WSS) - WSSR0)))
        Perm_E_WSS = ((Pmax + Pmin) / 2. + (Pmax - Pmin) / 2. *
                           np.tanh(alphaP * (np.abs(WSS) - WSSP0)))

    elif isinstance(WSS,np.ndarray):
        R_GFs_WSS, Perm_E_WSS = [],[]
        for i in range(len(WSS)):
            R_i, Perm_i = settings_WSS(WSS[i], Rmax, Rmin, GFs_names, RR_dict,
                    keysWords, Rcoef_dict, alphaR, WSSR0,Pmax, Pmin, alphaP, WSSP0)
            R_GFs_WSS.append(R_i)
            Perm_E_WSS.append(Perm_i)
        # to have np.ndarray (nbrGene,Nc_GFs)
        R_GFs_WSS = np.asarray(R_GFs_WSS).T
        Perm_E_WSS = np.asarray(Perm_E_WSS).reshape(1,len(Perm_E_WSS))
    else:
        print('typz(WSS)', type(WSS))
        raise TypeError('in settings_WSS input WSS is a float or np.ndarray')

    return R_GFs_WSS, Perm_E_WSS

def stimuli_WSS(WSS, e_E, Cf_GFs, Rmax, Rmin, GFs_names, RR_dict,
        keysWords, Rcoef_dict, alphaR, WSSR0,
        Pmax, Pmin, alphaP, WSSP0):
    """ update of functional properties according to the WSS sensed by ECs
        nothing: but a update of R_GFs_WSS_eE and Perm_CfGFs. Variables
                 used in method resolutionSystDDEs

    Parameters
    ----------
    WSS : float
    e_E : float
        Endothelium thickness
    Cf_GFs : np.ndarray of shape (NGFs,)
    Rmax :float
    Rmin :float
    GF_names :list f string
    RR_dict : dictionarry
    keysWords : tuple
    Rcoef_dict : list
    alphaR : float
    WSSR0 : float
    Pmax
    Pmin
    alphaP
    WSSP0

    Returns
    -------
    R_GFs_WSS_eE : 

    Perm_CfGFs : np.ndarray

    """
    (R_GFs_WSS, Perm_E_WSS) = settings_WSS(WSS, Rmax, Rmin,
            GFs_names, RR_dict, keysWords, Rcoef_dict, alphaR, WSSR0,
                Pmax, Pmin, alphaP, WSSP0)
    # print('type of R_GFs_WSS', type(R_GFs_WSS), R_GFs_WSS)
    R_GFs_WSS_eE = R_GFs_WSS * e_E
    if(isinstance(WSS,np.float)):
        Perm_CfGFs = Perm_E_WSS * Cf_GFs
    elif(isinstance(WSS,np.ndarray)):
        Perm_CfGFs = Perm_E_WSS * Cf_GFs.reshape(len(Cf_GFs),1)
    else:
        print('in stimuli_WSS wrong WSS input type, given : %s' % type(WSS))
        raise TypeError('expected type list of  float given')
    return R_GFs_WSS_eE, Perm_CfGFs

def fun(t, X, Z, R_hemo, r, rE, k_cp, GFs_adim,
        V_Oi, V_Om, Va, Vl,
        rho_E, rho_c, rho_s, beta,
        NtypeOfGFs, zetaGFs, Nc_GFs, cp_dict,
        set_params, dictRelations, th, csts, kappa, lEqQ,
        tau_col, T_d, epsRemod, epsEq):
    """ The function to use in resolutionSystDDEs

    Parameters
    ----------
    R_hemo : float
        Radius of the artery as hemo timescale
    r : float
        cSMCs regeneration rate in $d^{-1}$
    rE : float
        ECs production rate in $d^{-1}$
    k_cp : float
        Coupling rate between GFs $d^{-1}$
    GFs_adim : dict

    V_Oi : float
        Volumes of others populations in intima
    V_Om : float
        Volumes of others populations in media
    Va : float
        Volume of adventicial layer
    Vl : float
        Volume of luminal domain
    rho_E : float
        ECs density $cell/m^3$
    rho_c : float
        Collagen density in $g/m3$
    rho_s : float
        Cells density in $cell/m^3$
    R_GFs_WSS_eE : float
        Production rate of GFs by the EC layer
    Perm_CfGFs : float
        Rate of GFs entering in layers by transport though ECs
    beta : float

    NtypeOfGFs : int
        Number of equations for GFs dynamic
    zetaGFs : list
        Zeta for each GFs
    Nc_GFs : float
        Number of GFs
    cp_dict : dict
        dictionary for coupling processes
    GFs_names :

    set_params : dict
        general dictionnary attached to a growth model
    dictRelations : dict
        Relations between
    th_params : list
        List of float or of array with size (Nc_GFs,) gathering
        threshold value for each equation parameter in the specific
        order [th_p, th_a, th_m, th_l, th_chi, th_c]
    csts : np.array
        Array of the tissues growth constant populations
        [pEq, aEq, m0, lEq, chiEq, c0]
    kappa : float
        dimensionless parameter
    tau_col : float
        Aging collagen delay in $d$
    T_d : float
        Time used for dimensionalisation
    epsRemod : float
        remodeling sensitivity $epsilon$ to tissue growth without
        dimension
    epsEq : float
        Equilibrium sensitivity $varepsilon$ to tissue growth without
        dimension
    Return
    -------
    X : np.ndarray
            (dNOi_t, dNOm_t, dPDGFi_t, dPDGFm_t, dFGFi_t dFGFm_t
            dAgi_t dAgm_t dTGFi_t  dTGFm_t  dTNFi_t  dTNFm_t dMMPi_t dMMPm_t
            dQi_t, dQm_t, dSi_t, dSm_t, dCji_t dCjm_t, dCvi_t dCvm_t)
    """
    # print('t_d = %s T_d = %s' % (t*T_d, T_d))
    (Qimax, Qmmax, SiPhy, SmPhy,
     CjiPhy, CjmPhy, CjiPhy, CjmPhy, Emax) = set_params['species_dim']

    (e_E, Cf_GFs, Rmax, Rmin, GFs_names, RR_dict, keysWords,
     Rcoef_dict, alphaR, WSSR0, Pmax, Pmin,
     alphaP, WSSP0, FACT_PHI) = set_params['params']
    # get the flow rate as a stimuli
    (f_Q, args_f_Q, planar, modelF, argsF) = set_params['info_rythm']
    t_d = t * T_d
    Q_t = f_Q(t_d, *args_f_Q)
    # print('t_d %s Q_t %s' % (t_d, Q_t))
    G, uMax, WSS = fromQvToG(Q_t, R_hemo, planar, modelF, argsF)

    R_GFs_WSS_eE, Perm_CfGFs = stimuli_WSS(WSS, e_E, Cf_GFs,Rmax,
            Rmin, GFs_names, RR_dict, keysWords, Rcoef_dict, alphaR,
            WSSR0, Pmax, Pmin, alphaP, WSSP0)
    # unpack constant geometrical parameters
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    e_a = Rext - R_LEE # adventicia layer
    # none dimensional constant params which are not dependant time dependant
    r_nd = r * T_d
    rE_nd = rE * T_d
    # The damage param
    d = 1. - X[22]  # damage params

    criterium = False
    (ePlus, e_i, e_m, R_gr, Vf, Vs) = calculVolumes_1D(X, set_params, V_Oi,
            V_Om, Va, Vl, criterium)
    (Vf_Ei, Vf_Qi, Vf_Qm, Vf_Si, Vf_Sm,
        Vf_Ci, Vf_Cm, Vf_Oi, Vf_Om) = Vf

    # source term which depend of WSS, and current geometry
    B_GFs_d = R_GFs_WSS_eE * (1. - d) + Perm_CfGFs * (1. + beta * d)
    # print('\nt_nd', t, 'd', d, 'R_gr', R_gr)
    # print('R_GFs_WSS_eE', R_GFs_WSS_eE, 'Perm_CfGFs', Perm_CfGFs)
    # print('B_GFs_d', B_GFs_d)
    # compute the source coef as solution of one dim. diffusion reaction
    M_d = diffusionBC_EC(B_GFs_d, R_gr, Rext, e_i, e_m, L, kappa)
    a, cp = termsGFs_equa(NtypeOfGFs, zetaGFs, Nc_GFs, k_cp, cp_dict, GFs_names,
            Vf_Si, Vf_Sm)
    # adim
    M_nd = M_d * T_d / set_params['GFs_dim'].reshape(NtypeOfGFs,2)
    a_nd = a * T_d
    # get the GFs variables
    X_GFs =  X[:2*NtypeOfGFs]
    X_GFs_i = X_GFs[0::2]
    X_GFs_m = X_GFs[1::2]
    cp_i_nd = np.dot(GFs_adim[:,:,0] * cp[:,:,0] * T_d, X_GFs_i)
    cp_m_nd = np.dot(GFs_adim[:,:,1] * cp[:,:,1] * T_d, X_GFs_m)
    cp_nd = np.vstack((cp_i_nd,cp_m_nd)).T

    # parameters of species  equ are coupled with GFs bioavailability
    params = paramsRelationsGFs(np.asarray(X), Z, dictRelations, th, csts, FACT_PHI)
    [[p_i, p_m], [a_i, a_m], [m_i,m],
     [lS_i,lS_m], [chi_i, chi_m], [c_i, c_m]] = params
    # dim
    [[p_i_nd, p_m_nd], [a_i_nd, a_m_nd], [m_i_nd, m_m_nd],
     [lS_i_nd,lS_m_nd], [chi_i_nd, chi_m_nd], [c_i_nd, c_m_nd]] = params * T_d
    lQ_nd = lEqQ * T_d
    # unpack and compute equilibrium couple
    (Vi, Vm) = Vs
    (Vf_QiPhy, Vf_QmPhy, Vf_SiPhy, Vf_SmPhy,
        Vf_CiPhy, Vf_CmPhy, Vf_EiPhy) = set_params['VfPhy']

    Ci_eq = Vf_CiPhy * Vi * rho_c
    dCi_eq = Ci_eq / CjiPhy
    Cm_eq = Vf_CmPhy * Vm * rho_c
    dCm_eq = Cm_eq / CjmPhy
    dSi_eq = ((chi_i * Ci_eq / lS_i) - (lEqQ / lS_i) * Qimax) / SiPhy
    dSm_eq = ((chi_m * Cm_eq / lS_m) - (lEqQ / lS_m) * Qmmax) / SmPhy
    # delayed state of X
    X_tau_ag = Z[:,0]

    # compute volume at X(t-tau) and get Ceq(t-tau)
    res_tau_c = calculVolumes_1D(X_tau_ag, set_params, V_Oi, V_Om,
            Va, Vl, criterium)
    Vi_tau_c, Vm_tau_c = res_tau_c[-1]
    dCi_eq_tau_c = Vf_CiPhy * Vi_tau_c * rho_c / CjiPhy
    dCm_eq_tau_c = Vf_CmPhy * Vm_tau_c * rho_c / CjmPhy

    (phiE_Cji_nd, phiE_Cjm_nd) = colAging(X, X_tau_ag, dCi_eq,
                                          dCi_eq_tau_c, dCm_eq,
                                          dCm_eq_tau_c) * T_d

    # conversion for resolve the
    QmOverQi = Qmmax / Qimax
    QiOverSi = Qimax / SiPhy
    QmOverSm = Qmmax / SmPhy
    SmOverSi = SmPhy  / SiPhy
    SiOverCi = SiPhy / CjiPhy
    SmOverCm = SmPhy / CjmPhy
    QiOverCi = Qimax / CjiPhy
    QmOverCm = Qmmax / CjmPhy

    GFs = []
    for i in range(NtypeOfGFs):
        for j in range(2):
            GFs.append(M_nd[i,j] + a_nd[i,j] * X[2*i+j] + cp_nd[i,j])

    # Qi_nd =X[14], Qm_nd =X[15], Si_nd =X[16], Sm_nd =X[17]
    # Ci_nd =X[18], Cjm_nd =X[19], Cvi_nd =X[20]  dCm_nd =X[21] E_nd =X[22]
    pop = [r_nd*X[14]*(1.-X[14]) - c_i_nd *X[14] + m_i_nd*QmOverQi*X[15],
           r_nd*X[15]*(1.-X[15]) - c_m_nd *X[15] - m_i_nd*X[15],
           c_i_nd * X[14] * QiOverSi + (p_i_nd - a_i_nd)* \
                   (X[16] - dSi_eq) + m_i_nd * SmOverSi *X[17],
           c_m_nd * X[15] * QmOverSm + (p_m_nd - a_m_nd) *\
                   (X[17] -dSm_eq) - m_i_nd * X[17],
           lS_i_nd * X[16] * SiOverCi + lQ_nd * X[14] * QiOverCi -\
                   chi_i_nd * X[18] - 1. / tau_col * phiE_Cji_nd,
           lS_m_nd * X[17] * SmOverCm + lQ_nd * X[15] * QmOverCm -\
                   chi_m_nd * X[19] - 1. / tau_col * phiE_Cjm_nd,
           1.0 / tau_col * phiE_Cji_nd,
           1.0 / tau_col * phiE_Cjm_nd,
           rE_nd * X[22]*(1.-X[22])]
    return GFs + pop

def critNarrow(t, X, Z,R_hemo, r, rE, k_cp, GFs_adim,
        V_Oi, V_Om, Va, Vl,
        rho_E, rho_c, rho_s, beta,
        NtypeOfGFs, zetaGFs, Nc_GFs, cp_dict,
        set_params, dictRelations, th, csts, kappa, lEqQ,
        tau_col, T_d, epsRemod, epsEq):
    """Same docstring as for fun just above for Parameters
    Return
    -------
    """
    cr = calculVolumes_1D(X, set_params, V_Oi, V_Om, Va, Vl, True)
    # print('t = %s ratio=%s critNarrow = %s' % (t*T_d, cr, cr - (1. - epsRemod)))

    return cr - (1. - epsRemod)
critNarrow.direction = -1
critNarrow.terminal = True

def critEnlarg(t, X, Z,R_hemo, r, rE, k_cp, GFs_adim,
        V_Oi, V_Om, Va, Vl,
        rho_E, rho_c, rho_s, beta,
        NtypeOfGFs, zetaGFs, Nc_GFs, cp_dict,
        set_params, dictRelations, th, csts, kappa, lEqQ,
        tau_col, T_d, epsRemod, epsEq):
    """Same docstring as for fun just above for Parameters
    Return
    -------
    """
    cr = calculVolumes_1D(X, set_params, V_Oi, V_Om, Va, Vl, True)
    return cr - (1. + epsRemod)
critEnlarg.direction = 1
critEnlarg.terminal = True

def critOcc(t, X, Z,R_hemo, r, rE, k_cp, GFs_adim,
        V_Oi, V_Om, Va, Vl,
        rho_E, rho_c, rho_s, beta,
        NtypeOfGFs, zetaGFs, Nc_GFs, cp_dict,
        set_params, dictRelations, th, csts, kappa, lEqQ,
        tau_col, T_d, epsRemod, epsEq):
    """Same docstring as for fun just above for Parameters
    Return
    -------
    """
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    (ePlus, e_i, e_m, R_gr, Vf, Vs) = calculVolumes_1D(X, set_params,
             V_Oi, V_Om, Va, Vl, False)
    R_s = R_gr / R0

    Delta = R_s - epsEq
    return Delta
critOcc.direction = -1
critOcc.terminal = True

def critEq(t, X, Z,R_hemo, r, rE, k_cp, GFs_adim,
        V_Oi, V_Om, Va, Vl,
        rho_E, rho_c, rho_s, beta,
        NtypeOfGFs, zetaGFs, Nc_GFs, cp_dict,
        set_params, dictRelations, th, csts, kappa, lEqQ,
        tau_col, T_d, epsRemod, epsEq):
    """Same docstring as for fun just above
    Return
    -------
    norm - epsEq : float
        Equation of the equilibrium remodeling event $\frac{}{}-(1+\epsilon)=0$
    """
    f_nd = fun(t, X, Z,
            R_hemo, r, rE, k_cp,
            GFs_adim,
            V_Oi, V_Om, Va, Vl,
            rho_E, rho_c, rho_s,
            beta,
            NtypeOfGFs, zetaGFs, Nc_GFs, cp_dict,
            set_params, dictRelations, th, csts,
            kappa, lEqQ,
            tau_col, T_d, epsRemod, epsEq)
    # get variation in dimensional form
    norm_nd = np.linalg.norm(f_nd)
    return norm_nd - epsEq
    # ic(norm_nd, epsEq, f_nd)
    # f = f_nd * set_params['sys_dim'] / T_d
    # norm = np.linalg.norm(f)
    # ic(norm, epsEq, f)
    # return norm - epsEq
critEq.direction = -1
critEq.terminal = True

class Jansen_1D(ModelTG_1D):
    """class of the Jansen model [1]_ in loose coupling hypothesis

    Parameters
    ----------
        t0 : float
            Initial time in day
         tf : float
            final time step
        epsRemod : float
            remodeling sensitivity $epsilon$ to tissue growth without
            dimension.
        epsEq : float
            Equilibrium sensitivity $varepsilon$ to tissue growth without
            dimension
        myArtery : Artery
            The considered artery
        myHemo : Hemodynamic
            The considered hemodynamic flow
        r : float
            cSMCs regeneration rate in $d^{-1}$
        rE : float
            ECs production rate in $d^{-1}$
        P_E : float
            Apparent permeability coefficient in $m/s$
        pEq : float
            sSMCs proliferation rate in $d^{-1}$
        aEq : float
            sSMCs apoptosis rate in $d^{-1}$
        m0 : float
            sSMCs & cSMCs migration rate in $d^{-1}$
        c0 : float
            Phenotype switching rate in $d^{-1}$
        lEq : float:
            Collagen production rate in $d^{-1}$
        chiEq : float
            Collagen self-degradation rate in $d^{-1}$
        rho_s : float
            Cells density in $cell/m^3$
        rho_c : float
            Collagen density in $g/m3$
        w_EC : float
            width of an endothelial cell
        l_EC : float
            Length of an endothelial cell
        Dw : float
            Molecular diffusion coefficient in $m2/s$
        kw : float
            Environmental consumption rate in $d^{-1}$
        zeta : float or list
            GFs decay rate $d^{-1}$
        k_cp : float
            Coupling rate between GFs $d^{-1}$
        RRelations_dict : list

        Rcoef_dict : list

        Rmax : float
            Fitted maximal production rate of NO from [2]_ in ng/d
        Rmin : float
            Fitted minimal production rate of NO from [2]_ in ng/d
        alphaR : float
        WSSR0 : float
        Pmax : float
            Maximal permeability of in m/d
        Pmin : float
            Minimal permeability m/d
        alphaP : float
        WSSP0 : float
        Cf_GFs : list of float
            GFs concentration in blood or plasma in $ng/m3$.
            If list the order is the following:
                Cf_NO, Cf_PDGF, Cf_FGF, Cf_Ag, Cf_TGF, Cf_TNF, Cf_MMP
        d0 : float
            Initial damage of endothelium without dimension
            (dimensionless  param).
        beta : float
            Denudation parameter without dimension from [3]_
            (dimensionless  param)
        th_params : list
            List of float or of array with size (Nc_GFs,) gathering
            threshold value for each equation parameter in the specific
            order [th_p, th_a, th_m, th_l, th_chi, th_c]. This are
            dimensionless  params
        tau_col :float
            Aging collagen delay in days

    References
    ----------
    .. [1] J. Jansen, X. Escriva, F. S. Godeferd, P. Feugier,
        "Multiscale bio-chemo-mechanical model of intimal hyperplasia",
        Biomechanics and Modeling in Mechanobiology,
        DOI: 10.1007/s10237-022-01558-5, Vol. X, pp. XX-YY, 2022.
    .. [2] A. M. Andrews, D. Jaron, D. G. Buerk P. L. Kirby, K. A. Barbee
        "Direct, real-time measurement of shear stress-induced nitric oxide
        produced from endothelial cells in vitro", Nitric Oxide, Vol. 23,
        pp. 335–342, 2010.
    .. [3] Conklin, B. and Chen, beta. "Effect of Low Shear Stress on Permeability
            and Occludin Expression in Porcine Artery Endothelial Cells",
            World journal of surgery, volume 31, pp. 733-43, 2007.

    Examples
    ----------
    """

    def __init__(self, iterable=(), **kwargs):
        """Fast constructor of the object. As the class must be able to
        evolve quickly, the parameters are implicitly defined in the dictionary
        during the initialization. This is a method that allows flexibility in
        development but may cause a lot of errors.
        """
        super().__init__(iterable, **kwargs)
        if not os.path.exists('figures'):
            os.makedirs('figures')
        warn("Initializing the model requires a lot of dimensional data. The program must receive the parameters as indicated in the documentation!")

        self.T0 = self.t0
        self.Tf = self.tf
        self.keysWords = ('i.p.', 'p.', 'i.p.+th.e.', 'i.p.+th.d.',
                     'p.+th.e.', 'p.+th.d.')
        self.csts = np.array([self.pEq, self.aEq,
                                self.m0, self.lEqS, self.chiEq, self.c0])
        self.T_d = (self.m0)**-1

        # variables of
        if self.cardiac_rythm == 'rest':
            print('cardiac_rythm as in resting condition')
            self.T = 50. # random
            self.Trest = 50. # random
            self.I = 0.0 #
            # self.max_step = np.inf
            self.jumps = []
        elif self.cardiac_rythm == 'rest_effort':
            print('cardiac_rythm as in cycles of resting--efforts condition')
            if self.T < self.Trest:
                raise ValueError("Trest must be inferior to T, the total period")
            # self.max_step = np.inf
            # self.max_step = self.Trest / 5.
            # get the number of cycle in the stan of time
            self.Nc = int(self.tf / self.T)
            # construct the list of jumps time
            l_jumps = []
            for i in range(1, self.Nc+1):
                j_rest_eff_nd = (i*self.T-(self.T-self.Trest)) / self.T_d
                j_eff_rest_nd = (i*self.T) / self.T_d
                l_jumps.append([j_rest_eff_nd, j_eff_rest_nd])
            self.jumps = [item for sublist in l_jumps for item in sublist]

            if not all([hasattr(self, attr) for attr in ["I", "T", "Trest"]]):
                raise ValueError('In rest_effort but user not gave I, T, Trest')
        else:
            raise NotImplementedError("cardiac_rythm = %s, attempt 'rest' or 'rest_effort'" % self.cardiac_rythm)
        # variable list for the functon f_Q
        self.args_Q = (self.myHemo.Qv, self.T, self.Trest, self.I)
        self.f_Q = rest_effort

        # 1 / V_EC -> V_EC volume occupied by 1 cell
        # EC shape is ellipsoidal or spindle-shaped  4/3 pi a*b*c
        self.rho_E = ((4./3.) * np.pi * (self.w_EC*0.5) * (self.l_EC * 0.5) *\
                (self.myArtery.e_E*0.5))**-1
        print('rho_E = ', self.rho_E)
        self.set_params = {'GFs_nd': ('NOi_nd', 'NOm_nd', 'PDGFi_nd', 'PDGFm_nd',
                                   'FGFi_nd', 'FGFm_nd', 'Agi_nd', 'Agm_nd',
                                   'TGFi_nd', 'TGFm_nd', 'TNFi_nd', 'TNFm_nd',
                                   'MMPi_nd', 'MMPm_nd'),
                           'GFs_dim': None,
                           'species_nd': ('Qi_nd', 'Qm_nd', 'Si_nd', 'Sm_nd',
                                   'Cji_nd', 'Cjm_nd', 'Cvi_nd', 'Cvm_nd',
                                   'E_nd'),
                           'species_dim': None,
                           'sys_dim': None,
                           'Vf': ('Vf_Ei', 'Vf_Qi', 'Vf_Qm',
                                  'Vf_Si', 'Vf_Sm', 'Vf_Ci', 'Vf_Cm',
                                  'Vf_Oi', 'Vf_Om'),
                           'VfPhy': None,
                           'geom': ('e_i', 'e_m', 'ePlus'),
                           'geom_dim': None,
                           'T_d': self.T_d,
                           'model': self.__class__.__name__,
                           'densities' : (self.rho_s, self.rho_c, self.rho_E),
                           'info_rythm': (self.f_Q, self.args_Q, self.myHemo.planar,
                                self.myHemo.myFluid.model, self.myHemo.myFluid.argsF)
                           }
        self.NtypeOfGFs = int(len(self.set_params['GFs_nd']) * 0.5)
        self.NGFs = len(self.set_params['GFs_nd'])

        # transform in cp_dict a ['-'] list into a [] list if no cp relations
        for (k,v) in self.cp_dict.items():
            if(v==['-']):
                self.cp_dict[k] = []

        # get name of GFs modelied
        self.GFs_names = []
        for k in range(0,len(self.set_params['GFs_nd']),2):
            self.GFs_names.append(self.set_params['GFs_nd'][k][:-4])
        print('\nself.GFs_names', self.GFs_names)

        [th_p, th_a, th_m, th_l, th_chi, th_c] = self.th_params
        self.th = np.empty(((len(self.th_params),len(self.set_params['GFs_nd']))))
        for k in range(len(self.th_params)):
            self.th[k] = self.th_params[k]
        if(type(self.zeta) is list):
            self.zetaNO, self.zetaPDGF, self.zetaFGF, self.zetaAg, \
            self.zetaTGF, self.zetaTNF, self.zetaMMP = self.zeta
            self.zetaGFs = self.zeta
        else:
            self.zetaNO = self.zetaPDGF = self.zetaFGF = self.zetaAg = \
            self.zetaTGF = self.zetaTNF = self.zetaMMP = self.zeta
            self.zetaGFs = np.ones(self.NtypeOfGFs) * self.zeta

        if len(self.Cf_GFs) != self.NtypeOfGFs:
            raise ValueError('Not same number of Cf_GFs and NtypeOfGFs')

        (self.Cf_NO, self.Cf_PDGF, self.Cf_FGF,
         self.Cf_Ag, self.Cf_TGF, self.Cf_TNF, self.Cf_MMP) = self.Cf_GFs

        self.kappa = np.sqrt(self.kw / self.Dw)

        if(self.d0 > 1.0):
            raise ValueError('d0>1.0, fatal error')


    def reshapeRes(self, sol):
        """
        (NOi_nd, NOm_nd, PDGFi_nd, PDGFm_nd, FGFi_nd, FGFm_nd,
         Agi_nd, Agm_nd, TGFi_nd, TGFm_nd, TNFi_nd, TNFm_nd, MMPi_nd, MMPm_nd,
         Qi_nd, Qm_nd, Si_nd, Sm_nd, Cji_nd, Cjm_nd,
         Cvi_nd, Cvm_nd, E_nd) = sol.y

        (ddtNOi_nd, ddtNOm_nd, ddtPDGFi_nd, ddtPDGFm_nd, ddtFGFi_nd, ddtFGFm_nd,
         ddtAgi_nd, ddtAgm_nd, ddtTGFi_nd, ddtTGFm_nd, ddtTNFi_nd, ddtTNFm_nd,
         ddtMMPi_nd, ddtMMPm_nd, ddtQi_nd, ddtQm_nd, ddtSi_nd, ddtSm_nd,
         ddtCji_nd, ddtCjm_nd, ddtCvi_nd, ddtCvm_nd, E_nd) = sol.yp

        Parameters
        ----------
        sol : DdeResult
            The object from solve_dde (scipy)
        Returns
        -------
        GFs_nd : np.ndarray
            The non dimensional GFs variables
        ddtGFs_nd : np.ndarray
            The derivative of the non dimensional GFs variables
        pop_nd : np.ndarray
            The rescaled species
        ddtpop_nd : np.ndarray
            The derivative of rescaled species
        """
        t_nd, Y_nd, ddtY_nd = sol.t, sol.y, sol.yp

        species_nd = Y_nd[self.NGFs:,:]
        ddtspecies_nd = ddtY_nd[self.NGFs:,:]

        GFs_nd = Y_nd[:self.NGFs,:]
        ddtGFs_nd = ddtY_nd[:self.NGFs,:]
        return t_nd, GFs_nd, ddtGFs_nd, species_nd, ddtspecies_nd

    def GeneFusion(self, path, lastGene):
        """ Not like for donadoni computation, here we use solve__dde which take
        old computation to continue the integration. So ne need of concatenation
        of solutions. Just a copy of last file is needed
        """
        path_List = glob.glob(path + '/gene%s_*'%lastGene)
        if(len(path_List)>1):
            print("error rereading results")
            sys.exit(2)
        else:
            path_g = path_List[0]
        pathAll = '%s/allGenes/' % (path)
        if os.path.isdir(pathAll):
            shutil.rmtree(pathAll)
        shutil.copytree(path_g,pathAll)

    def initialization(self):
        """initialization of the model with possibility to damage the
        endothelium layer.

        Parameters
        ----------

        Returns
        -------
        y0_nd : list
            List of non dimensionalized initial state of the system of
            equations at t=t0
        h_nd : list
            Non dimensionalized historical state of the system of equations
        """

        # as jansen model don't take into account elastin
        self.V_Oi = self.myArtery.V_Oi + self.myArtery.V_Eli
        self.V_Om =  self.myArtery.V_Om + self.myArtery.V_Elm

        # hypothesis -> Q_{i,m} = r_{S-Q} S_{i,m}
        # calculate the  r_{S-Q} more Q cells than S cells
        # We assume that lEq for Q -> lEqQ < lEqS
        # and that Vf_S + Vf_Q = Vf_SMC for each layers
        print('lEqQ %s lEqS %s' % (self.lEqQ,self.lEqS))
        print('Vf_SMCi %s Vf_SMCm %s' % (self.myArtery.Vf_SMCi, self.myArtery.Vf_SMCm))
        print('Vf_Ci %s Vf_Cm %s' % (self.myArtery.Vf_Ci, self.myArtery.Vf_Cm))
        Ai = self.myArtery.Vf_SMCi / \
                ((self.rho_c / self.rho_s) * self.chiEq * self.myArtery.Vf_Ci)
        Am = self.myArtery.Vf_SMCm / \
                ((self.rho_c / self.rho_s) * self.chiEq * self.myArtery.Vf_Cm)

        self.rS_Q_i = (Ai * self.lEqS - 1.) / (1. - Ai * self.lEqQ)
        self.rS_Q_m = (Am * self.lEqS - 1.) / (1. - Am * self.lEqQ)
        self.myArtery.Vf_Si = ((self.rho_c / self.rho_s) * self.chiEq * \
                self.myArtery.Vf_Ci) / (self.lEqS + self.lEqQ * self.rS_Q_i)
        self.myArtery.Vf_Sm = ((self.rho_c / self.rho_s) * self.chiEq * \
                self.myArtery.Vf_Cm) / (self.lEqS + self.lEqQ * self.rS_Q_m)
        # media
        self.myArtery.Vf_Qi = self.myArtery.Vf_Si * self.rS_Q_i
        self.myArtery.Vf_Qm = self.myArtery.Vf_Sm * self.rS_Q_m


        print('self.rS_Q_i %s self.rS_Q_m %s' % (self.rS_Q_i, self.rS_Q_m))
        print('Vf_Si = %s Vf_Qi = %s Vf_Si+Vf_Qi = %s and Vf_SMCi = %s' %
                (self.myArtery.Vf_Si, self.myArtery.Vf_Qi,
            self.myArtery.Vf_Si + self.myArtery.Vf_Qi, self.myArtery.Vf_SMCi))
        print('Vf_Sm = %s Vf_Qm = %s Vf_Sm+Vf_Qm = %s and Vf_SMCm = %s' %
                (self.myArtery.Vf_Sm, self.myArtery.Vf_Qm,
            self.myArtery.Vf_Sm + self.myArtery.Vf_Qm, self.myArtery.Vf_SMCm))
        if self.myArtery.Vf_Si > self.myArtery.Vf_SMCi :
            raise ValueError('sSMC_i vol frac > vSMCs_i vol frac !')
        if self.myArtery.Vf_Sm > self.myArtery.Vf_SMCm :
            raise ValueError('sSMC_m vol frac > vSMCs_m vol frac !')

        self.V_Qi = self.myArtery.Vw * self.myArtery.Vf_Qi
        self.V_Qm = self.myArtery.Vw * self.myArtery.Vf_Qm

        self.V_Si = self.myArtery.Vw * self.myArtery.Vf_Si
        self.V_Sm = self.myArtery.Vw * self.myArtery.Vf_Sm

        self.V_Ci = self.myArtery.V_Ci
        self.V_Cm = self.myArtery.V_Cm

        self.V_Ei = self.myArtery.V_Ei
        self.V_Em = self.myArtery.V_Em

        self.ViPhy = (self.V_Ei + self.V_Si + self.V_Qi + self.V_Ci + self.V_Oi)
        self.VmPhy = (self.V_Em + self.V_Sm + self.V_Qm + self.V_Cm + self.V_Om)

        self.Vf_OiPhy = self.V_Oi / self.ViPhy
        self.Vf_OmPhy = self.V_Om / self.VmPhy

        self.Vf_EiPhy = self.V_Ei / self.ViPhy
        self.Vf_EmPhy = self.V_Em / self.VmPhy

        self.Vf_QiPhy = self.V_Qi / self.ViPhy
        self.Vf_QmPhy = self.V_Qm / self.VmPhy

        self.Vf_SiPhy = self.V_Si / self.ViPhy
        self.Vf_SmPhy = self.V_Sm / self.VmPhy

        self.Vf_CiPhy = self.V_Ci / self.ViPhy
        self.Vf_CmPhy = self.V_Cm / self.VmPhy

        self.set_params['VfPhy'] = (self.Vf_QiPhy, self.Vf_QmPhy,
                                    self.Vf_SiPhy, self.Vf_SmPhy,
                                    self.Vf_CiPhy, self.Vf_CmPhy, self.Vf_EiPhy)

        sum_Vfi = self.Vf_EiPhy + self.Vf_OiPhy + self.Vf_QiPhy + \
                  self.Vf_SiPhy + self.Vf_CiPhy
        sum_Vfm = self.Vf_EmPhy + self.Vf_OmPhy + self.Vf_QmPhy + \
                  self.Vf_SmPhy + self.Vf_CmPhy
        if(np.abs(sum_Vfi-1.) > 1e-8 or np.abs(sum_Vfm-1.) > 1e-8):
            print("FATAL ERROR sum of Vf_{i,m} !=1.0")
            raise ValueError("sum_Vfi = %s & sum_Vfm = %s" % (sum_Vfi, sum_Vf))
        # Phylogical conditions of population
        print('rho_E {:.12e} V_Ei {:.12e}'.format(self.rho_E, self.V_Ei))
        self.Emax = self.V_Ei * self.rho_E
        print('Emax {:.12e}'.format(self.Emax))
        self.Qimax = self.V_Qi * self.rho_s
        self.Qmmax = self.V_Qm * self.rho_s

        self.SiPhy = self.V_Si * self.rho_s
        self.SmPhy = self.V_Sm * self.rho_s

        self.CjiPhy = self.V_Ci * self.rho_c
        self.CjmPhy = self.V_Cm * self.rho_c
        self.CviPhy = 0.
        self.CvmPhy = 0.

        # init adimensionnal Deltaiables
        if(1.0 >= self.d0 and self.d0 >= 0.0):
            E_nd_t0 = 1.0 - self.d0
        else:
            raise ValueError("d0 =< 0.0 or d0 >= 1.0")
        Qi_nd_t0, Qm_nd_t0 = 1., 1.
        Si_nd_t0, Sm_nd_t0 = 1., 1.
        Cji_nd_t0, Cjm_nd_t0 = 1., 1.
        Cvi_nd_t0, Cvm_nd_t0 = 0., 0.

        # Biochimical part
        # definition param blessure b
        # production FCs par endothelium fct(WSS)

        # pack variable usefull for stimuli calcul
        self.set_params['params'] = (self.myArtery.e_E, self.Cf_GFs,
                self.Rmax, self.Rmin, self.GFs_names,
                self.RRelations_dict,
                self.keysWords, self.Rcoef_dict, self.alphaR, self.WSSR0,
                self.Pmax, self.Pmin, self.alphaP, self.WSSP0,
                self.FACT_PHI)

        print('self.Pmax %s self.Pmin %s ' % (self.Pmax, self.Pmin))
        self.R_GFs_WSS_eE, self.Perm_CfGFs = stimuli_WSS(self.myHemo.WSS,
                            self.myArtery.e_E, self.Cf_GFs,
                            self.Rmax, self.Rmin, self.GFs_names,
                            self.RRelations_dict,
                            self.keysWords, self.Rcoef_dict, self.alphaR, self.WSSR0,
                            self.Pmax, self.Pmin, self.alphaP, self.WSSP0)

        print('Endothelium and regulation of GFs dynamics : ')
        print('Production rates x e_E => R_GFs_WSS_eE = ', self.R_GFs_WSS_eE)
        print('Permeability to GFs in plasma => Perm_CfGFs = ', self.Perm_CfGFs)
        print('Concentration in plasma used => Cf_GFs = ', self.Cf_GFs)

        sumB = self.R_GFs_WSS_eE + self.Perm_CfGFs
        m = 0
        print("Proportions  of R_GFs_WSS_eE and Perm_CfGFs :")
        for k in range(0, len(self.set_params['GFs_nd']), 2):
            GF_k = self.set_params['GFs_nd'][k][:-1]
            perc_prod_k = self.R_GFs_WSS_eE[m] / sumB[m]
            # print("%s,  (R_%s *e_E, P_E * cf_%s) = (%.5s,%.5s)",
                         # GF_k, GF_k, GF_k, perc_prod_k, 1. - perc_prod_k)
            print("%s,  (R_%s *e_E, P_E * cf_%s) = (%.5s,%.5s)" % \
                         (GF_k, GF_k, GF_k, perc_prod_k, 1. - perc_prod_k))
            m = m + 1

        B_GFs_Phy = self.R_GFs_WSS_eE + self.Perm_CfGFs
        print('The B values => B_GFs_Phy = ', B_GFs_Phy)
        # condition Phylogique
        self.M_Phy = diffusionBC_EC(B_GFs_Phy, self.myArtery.R0,
                                           self.myArtery.Rext, self.myArtery.e_i,
                                           self.myArtery.e_m, self.myArtery.L,
                                           self.kappa)
        print('Source terms from ECs => M_Phy = ', self.M_Phy)
        # print('R0 %s Rex %s e_i %s e_m %s L %s kappa %s' % (self.myArtery.R0, self.myArtery.Rext, self.myArtery.e_i, self.myArtery.e_m, self.myArtery.L, self.kappa))
        # les conditions Phylogique dans l'intima
        self.Nc_GFs, self.GFs_physio = physioGFs(self.NtypeOfGFs, self.cp_dict,
                self.GFs_names, self.Vf_SiPhy, self.Vf_SmPhy,
                self.M_Phy, self.k_cp, self.zetaGFs)
        print('Nc_GFs', self.Nc_GFs)
        print('\n ViPhy = %s _mPhy = %s' % (self.ViPhy, self.VmPhy))
        print('GFs concentrations within intima at Phy')
        print('GFs_i/ViPhy = %s' % (self.GFs_physio[:,0] / self.ViPhy))
        print('GFs concentrations within media Phy')
        print('GFs_m/VmPhy = %s' % (self.GFs_physio[:,1] / self.VmPhy))
        print('GFs_{i+m} / V_{i+m}Phy %s ' % (np.sum(self.GFs_physio,axis=1) / (self.ViPhy+self.VmPhy)))
        print('plasma concentration %s' % (self.Cf_GFs))
        if np.any(self.GFs_physio < 0.0):
            raise ValueError('Physio GFs amount can not be negative')
        self.GFs_adim = np.zeros((self.NtypeOfGFs, self.NtypeOfGFs, 2))
        for i in range(self.NtypeOfGFs):
            for j in range(2):
                adim_j = self.GFs_physio[i,j]
                self.GFs_adim[i,:,j] = self.GFs_physio[:,j] / adim_j

        # print('adim GF i', self.GFs_adim[:,:,0])
        # print('adim GF m', self.GFs_adim[:,:,1])
        print('GFs_physio = ', self.GFs_physio)

        # definition of initial condition and historical state
        NOi_nd_t0 = PDGFi_nd_t0 = FGFi_nd_t0 = Agi_nd_t0 = TGFi_nd_t0 = \
                TNFi_nd_t0 = MMPi_nd_t0 = 1.0

        NOm_nd_t0 = PDGFm_nd_t0 = FGFm_nd_t0 = Agm_nd_t0 = TGFm_nd_t0 = \
                TNFm_nd_t0 = MMPm_nd_t0 = 1.0
        # initialy we denudate the endothelium
        y0_nd = [NOi_nd_t0, NOm_nd_t0, PDGFi_nd_t0, PDGFm_nd_t0,
                FGFi_nd_t0, FGFm_nd_t0, Agi_nd_t0, Agm_nd_t0,
                TGFi_nd_t0, TGFm_nd_t0, TNFi_nd_t0, TNFm_nd_t0,
                MMPi_nd_t0, MMPm_nd_t0,
                Qi_nd_t0, Qm_nd_t0, Si_nd_t0, Sm_nd_t0,
                Cji_nd_t0, Cjm_nd_t0, Cvi_nd_t0, Cvm_nd_t0, E_nd_t0]
        # historically the eno is normal i.e. confluent E* = 1
        h_nd = [NOi_nd_t0, NOm_nd_t0, PDGFi_nd_t0, PDGFm_nd_t0,
                FGFi_nd_t0, FGFm_nd_t0, Agi_nd_t0, Agm_nd_t0,
                TGFi_nd_t0, TGFm_nd_t0, TNFi_nd_t0, TNFm_nd_t0,
                MMPi_nd_t0, MMPm_nd_t0,
                Qi_nd_t0, Qm_nd_t0, Si_nd_t0, Sm_nd_t0,
                Cji_nd_t0, Cjm_nd_t0, Cvi_nd_t0, Cvm_nd_t0, 1.0]

        GFs_dim_conca = np.concatenate((self.GFs_physio))
        # print('GFs_dim_conca', GFs_dim_conca)
        self.set_params['GFs_dim'] = GFs_dim_conca
        self.set_params['species_dim'] = np.array([self.Qimax, self.Qmmax,
                                      self.SiPhy, self.SmPhy,
                                      self.CjiPhy, self.CjmPhy,
                                      self.CjiPhy, self.CjmPhy,
                                      self.Emax])

        self.set_params['sys_dim'] = np.concatenate((GFs_dim_conca,
            self.set_params['species_dim']))
        self.set_params['geom_dim'] = (self.myArtery.Rext, self.myArtery.R_LEE,
                self.myArtery.R0, self.myArtery.L)

        print('species_dim = {}'.format(self.set_params['species_dim']))
        print('geom_dim', self.set_params['geom_dim'])

        print('\nInitial WSS', self.myHemo.WSS)
        print('As init damage (d0=%s) is apply to arterial wall, ' % (self.d0))
        print('-> R(t0) not R(t<t0)')
        (eTot, e_i, e_m,
          R_gr, Vf, Vs) = calculVolumes_1D(y0_nd, self.set_params, self.V_Oi,
                  self.V_Om, self.myArtery.Va, self.myHemo.Vl, False)
        print('Damage induce -> R_l_damaged = {}, e_i = {}, e_m = {}, Vs = {}, R0 = {}'.format(
            R_gr, e_i, e_m, Vs, self.myArtery.R0))

        self.myHemo.update_WSS(eTot)
        self.R_GFs_WSS_eE, self.Perm_CfGFs = stimuli_WSS(self.myHemo.WSS,
                            self.myArtery.e_E, self.Cf_GFs,
                            self.Rmax, self.Rmin, self.GFs_names,
                            self.RRelations_dict,
                            self.keysWords, self.Rcoef_dict, self.alphaR, self.WSSR0,
                            self.Pmax, self.Pmin, self.alphaP, self.WSSP0)

        args = (self.myHemo.R, self.r, self.rE, self.k_cp,
                self.GFs_adim,
                self.V_Oi, self.V_Om,  self.myArtery.Va, self.myHemo.Vl,
                self.rho_E, self.rho_c, self.rho_s,
                self.beta,
                self.NtypeOfGFs, self.zetaGFs, self.Nc_GFs, self.cp_dict,
                self.set_params, self.dictRelations, self.th, self.csts,
                self.kappa, self.lEqQ,
                self.tau_col, self.T_d, self.epsRemod, self.epsEq)
        # reshape h_nd for define the state as Z
        Z_0 = np.array(h_nd).reshape(len(h_nd),1)
        # evaluation of f at t=t0
        dydt = fun(self.t0 / self.T_d, y0_nd, Z_0, *args)
        norm = np.linalg.norm(dydt)
        print('=================')
        print('Initial norm L2 of right hand side for initial conditions applied')
        print('norm %s epsEq %s' % (norm, self.epsEq))
        print('=================')

        if norm < self.epsEq:
            print("Equilibrium of the init volume of artery")
            self.initEq = True
        return y0_nd, h_nd


    def resolutionSystDDEs(self, CI_nd):
        """resolution of Jansen et al. modele during un time interval.
        It's our elementary brick then read the remodelingLoop function
        which calculates the remodeling between an initial condition and either
        a stop due to remodeling in the evening a stationary state reached.

        Parameters
        ----------
        CI_nd : tuple or DddeResult object
            If firt call to the function, CI_nd is a tuple as (y0,h) with y0 is
            state at t=t0 and h is the historical state of the variables.
        Return
        -------
        sol : object solver of solve_dde algo
            The DdeResult of the integrated system of equations
        """
        print("Beginning of DDEs resolution \n")
        # delays

        t0_nd = self.t0 / self.T_d
        tf_nd = self.tf / self.T_d
        print("T_d", self.T_d)
        print("t0", t0_nd * self.T_d)
        print("tf", tf_nd * self.T_d)
        tspan_nd = [t0_nd, tf_nd]
        delays_nd = [self.tau_col / self.T_d]

        args = (self.myHemo.R, self.r, self.rE, self.k_cp,
                self.GFs_adim,
                self.V_Oi, self.V_Om,  self.myArtery.Va, self.myHemo.Vl,
                self.rho_E, self.rho_c, self.rho_s,
                self.beta,
                self.NtypeOfGFs, self.zetaGFs, self.Nc_GFs, self.cp_dict,
                self.set_params, self.dictRelations, self.th, self.csts,
                self.kappa, self.lEqQ,
                self.tau_col, self.T_d, self.epsRemod, self.epsEq)

        events = [critNarrow, critEnlarg, critEq, critOcc]

        if(type(CI_nd) is tuple):
            y0, h = CI_nd
        else:
            y0 = CI_nd.y[:,-1]
            h = CI_nd
            # print('h.discont', h.discont, 'nxt', h.nxtDisc,'h.t', h.t)
            # print('h.data[0]', h.data[0])
            print('debut resolution Syst \ dans cas reprise solver')
            # print('CI_nd.t[-1]', CI_nd.t[-1], 'y0', y0, 'h.sol(CI_nd.t[-1])', h.sol(CI_nd.t[-1]))
            # print('error y0 h(t0)', y0-h.sol(CI_nd.t[-1]))
        print('tspan_nd', tspan_nd)
        jumps = [j for j in self.jumps if j > t0_nd - delays_nd[0]]
        sol = solve_dde(fun, tspan_nd, delays_nd, y0, h, args=args, method='RK23',
             dense_output=True,
                #max_step=self.max_step,
                jumps=jumps,
             atol=self.atol, rtol=self.rtol, events=events, warns=False)
        # sol.t = sol.t * self.T_d
        # sol.t_events = [self.T_d * j for j in sol.t_events]
        # for k in range(len(sol.t_events)):
            # sol.t_events[k] = sol.t_events[k] * self.T_d
        print("End of DDEs resolution \n")
        return sol

    def remodelingProcess(self, CI_nd):
        """remodelage arteriel cause par une croissance tissulaire
        (modele de Jansen)
        Parameters
        ---------
        CI_nd : tuple or DddeResult object
            Inital and history conditions

        Returns
        ------
        sol (object DDEsolver)
            The solver object of scipy.integrate.solve_dde
        """
        boolean = True
        self.remodBool = False
        i = 0

        sol = self.resolutionSystDDEs(CI_nd)
        # checkPositive(sol.y)
        tf_sol = sol.t[-1] * self.T_d
        # print('tf_sol', tf_sol, 't_events', sol.t_events)
        # update time interval for next computation as this case need
        # updated WSS para
        if (tf_sol < self.tf and not np.isclose(tf_sol,self.tf)) or \
                sol.t_events[2].size != 0:
            if sol.t_events[2].size == 0 and sol.t_events[3].size == 0:
                    message('REMODELING FOUND\n\tat t = {}', (tf_sol,))
                    self.t0 = tf_sol
                    self.remodBool = True
            elif sol.t_events[2].size == 0 and sol.t_events[3].size != 0:
                message('OCCLUSION FOUND\n\tat t = {}', (tf_sol,))
                self.remodBool = False # for stop while loop of coupling_1D
                return sol
            else:
                tf_sol = sol.t_events[2][-1] * self.T_d
                # update the new tf, as an equilibrium state is found
                self.tf = tf_sol
                self.remodBool = False # for stop while loop of coupling_1D
                message('EQUILIBRIUM FOUND\n \t at t = {}', (tf_sol,))
            return sol
        elif np.isclose(tf_sol, self.tf):
            message('Arrived to the final time tf = {}', (tf_sol,))
            self.finish = True
            return sol
        else:
            raise ValueError('case not handled properly')
        return sol


    def postProcessCalculCst(self, X):
        """ Post processing species functional properties as migration,
        proliferation, .....

        Parameters
        ----------
        X : np.ndarray of shape (Neq,Nt)
            Vector state adim

        Returns
        -------
        p_i : np.ndarray of shape (Nt,)
            The proliferation in intima
        p_m : np.ndarray of shape (Nt,)
            The prolif in media
        a_i : np.ndarray of shape (Nt,)
            The apopt in intima
        a_m : np.ndarray of shape (Nt,)
            The apopt in media
        m_i : np.ndarray of shape (Nt,)
            The migr in intima
        m : np.ndarray of shape (Nt,)
            The migr in media
        l_i : np.ndarray of shape (Nt,)
            The secreation of col in intima
        l_m : np.ndarray of shape (Nt,)
            The secreation of col in media
        chi_i : np.ndarray of shape (Nt,)
            self degradation of col in intima
        chi_m : np.ndarray of shape (Nt,)
            self degradation of col in media
        c_i : np.ndarray of shape (Nt,)
            Dedifferentiation of cSMCs to sSMCs in intima
        c_m : np.ndarray of shape (Nt,)
            Dedifferentiation of cSMCs to sSMCs in media

        """
        # print('type(X)', type(X))
        params = paramsRelationsGFs(X, None, self.dictRelations, self.th,
                self.csts, self.FACT_PHI)

        [[p_i, p_m], [a_i, a_m], [m_i,m],
         [l_i,l_m], [chi_i, chi_m], [c_i, c_m]] = params
        return p_i, p_m, a_i, a_m, m_i, m, l_i, l_m, chi_i, chi_m, c_i, c_m


    def sourceTerms(self, path, lastGene):
        """
        computation and save of production rate in GFs equation $M^x_{i,m}$
        """
        path_List = glob.glob(path + '/gene%s_*' % lastGene)
        if(len(path_List) > 1):
            print("error rereading results")
            sys.exit(2)
        else:
            path_g = path_List[0]
        pathGFs = '%s/tissueGrowth/GFs' % (path_g)
        pathGlobal = '%s/tissueGrowth/global' % (path_g)
        pathPop = '%s/tissueGrowth/Pop' % (path_g)

        # load dEc
        dE = np.load('%s/E_nd.npy' % pathPop)
        d = 1.0 - dE
        # load params geom
        e_i = np.load('%s/e_i.npy' % pathPop)
        e_m = np.load('%s/e_m.npy' % pathPop)
        ePlus = np.load('%s/ePlus.npy' % pathPop)
        Rext = self.myArtery.Rext
        R_gr = Rext - ePlus
        # load WSS
        wssR = np.load('%s/wssR.npy' % pathGlobal)
        t_gene = np.load('%s/t_gene.npy' % pathGlobal)
        t_d = np.load('%s/t_d.npy' % pathGlobal)
        idxS = np.zeros((len(t_gene)-1,4),dtype=np.int)
        for i in range(len(t_gene)-1):
            # print('t_gene i ', t_gene[i], 't_gene[i+1]', t_gene[i+1])
            idxS[i,:] = findIndex_t0tf(t_d, t_gene, t_gene[i], t_gene[i+1])
        R_GFs_WSS_eE, Perm_CfGFs = stimuli_WSS(wssR,
                            self.myArtery.e_E, self.Cf_GFs,
                            self.Rmax, self.Rmin, self.GFs_names,
                            self.RRelations_dict,
                            self.keysWords, self.Rcoef_dict, self.alphaR, self.WSSR0,
                            self.Pmax, self.Pmin, self.alphaP, self.WSSP0)
        # (R_GFs_WSS_eE, Perm_CfGFs) = self.stimuli_WSS(wssR)
        B_GFs = R_GFs_WSS_eE + Perm_CfGFs
        # reshape R_GFs for doing R * d and same for Perm_CF * d
        R_GFs_WSS_eE_d = np.zeros((len(R_GFs_WSS_eE), len(d)))
        Perm_CfGFs_d = np.zeros((len(Perm_CfGFs), len(d)))
        R_GFs_WSS_eE_gene = np.zeros((len(R_GFs_WSS_eE), len(d)))
        Perm_CfGFs_gene = np.zeros((len(Perm_CfGFs), len(d)))
        for i in range(len(t_gene)-1):
            idx0 = idxS[i,0]
            idx1 = idxS[i,1]
            N_d = len(d[idx0:idx1+1])
            R_GFs_WSS_eE_d[:,idx0:idx1+1] = np.outer(R_GFs_WSS_eE[:,i],
                                                   (1. - d[idx0:idx1+1]))
            Perm_CfGFs_d[:,idx0:idx1+1] = np.outer(Perm_CfGFs[:,i],
                                                 (1. + self.beta * d[idx0:idx1+1]))
            R_GFs_WSS_eE_gene[:,idx0:idx1+1] = np.outer(R_GFs_WSS_eE[:,i],
                                                        np.ones(N_d))
            Perm_CfGFs_gene[:,idx0:idx1+1] = np.outer(Perm_CfGFs[:,i],
                                                      np.ones(N_d))
        # operation de multiplication entre array 2D et 1D donc des transposes
        B_GFs_d = R_GFs_WSS_eE_d + Perm_CfGFs_d
        # def fo proportion of R^x over total BC_d
        # def fo proportion of P_E^x over total B_c
        np.save('%s/R.npy' % pathGFs, R_GFs_WSS_eE_gene)
        np.save('%s/P_E.npy' % pathGFs, Perm_CfGFs_gene)
        np.save('%s/B_GFs.npy' % pathGFs, B_GFs)
        np.save('%s/R_d.npy' % pathGFs, R_GFs_WSS_eE_d)
        np.save('%s/P_E_d.npy' % pathGFs, Perm_CfGFs_d)
        np.save('%s/B_GFs_d.npy' % pathGFs, B_GFs_d)
        # remplissage dun tableau
        # for k in range(self.NtypeOfGFs):
            # M_d[k] = diffusionBC_EC(B_GFs_d[:,k], R_gr, Rext,
                                    # e_i, e_m, self.myArtery.L, self.kappa)
        M_d = diffusionBC_EC(B_GFs_d, R_gr, Rext,
                                    e_i, e_m, self.myArtery.L, self.kappa)
        np.save('%s/sourceTerms.npy' % pathGFs, M_d)

