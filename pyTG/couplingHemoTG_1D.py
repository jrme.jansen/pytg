#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import glob
import os
import shutil
import sys
import numpy as np


from pyTG.artery import Artery
from pyTG.fluid import Newtonian, Quemada, WalburnScheck, Casson
from pyTG.hemodynamic import Hemodynamic
from pyTG.jansen.jansen_1D import Jansen_1D, paramsRelationsGFs
#from pyTG.jansen.jansenSansPerm_1D import JansenSansPerm_1D
from pyTG.utilities import sortAndCleanEvents, message, save_obj

from pyTG.utilities import calculVolumes_1D, calculVolumes_Infla_1D


def printGene(i):
    """ printing message at each iteration
    """
    print('\n\n\n/=========================')
    print('=========================')
    print('/*-----ITERATION N°%d----*\n' % i)
    print('=========================')
    print('=========================\n\n\n')


class CouplingHemoTG_1D(object):

    def __init__(self, iterable=(), **kwargs):
        """constructor of CouplingHemoTG_1D objet.

        Parameters
        ----------
        myTG : ModelTG_1D
            A tissue growth model heriting from class ModelTG_1D
        nameCase : str
            Name of the case
        Return:
            CouplingHemoTG_1D object
        """
        self.__dict__.update(iterable, **kwargs)
        path = os.path.dirname(os.path.realpath('__file__'))
        print('remove all folders named as gene*')
        os.system('rm -fr gene*')


    def postProcessCalculCst(self):
        """ process to calculation of cstes of the Jansen model and concatenate
                data from generations """
        if self.myTG.__class__.__name__[:6] == 'Jansen':
            path_List = glob.glob(self.path_abs + '/gene%s_*' % self.lastGene)
            if(len(path_List)>1):
                raise ValueError("error rereading results")
            else:
                path_g = path_List[0]

            pathGFs = '%s/tissueGrowth/GFs' % (path_g)
            pathCst = '%s/tissueGrowth/Csts' % (path_g)
            pathPop = '%s/tissueGrowth/Pop' % (path_g)
            t_d = np.load('%s/t_d.npy' % (pathPop))
            X = []
            for j in range(len(self.myTG.set_params['GFs_nd'])):
                X.append(
                         np.load('%s/%s.npy' %
                         (pathGFs, self.myTG.set_params['GFs_nd'][j])
                         ))
            (p_i, p_m, a_i,
             a_m, m_i, m_m, l_i, l_m,
             chi_i, chi_m, c_i, c_m) = self.myTG.postProcessCalculCst(X)
            r_i = p_i - a_i
            r_m = p_m - a_m

            if(self.myTG.__class__.__name__ == 'Jansen_1D_int'):
                ##################   changement phenotypique  ##################
                dci = np.load('%s/dci.npy' % pathPop)
                dcm = np.load('%s/dcm.npy' % pathPop)
                c_i = self.myTG.c0 * dci
                c_m = self.myTG.c0 * dcm


            dat = [a_i, a_m, p_i, p_m, r_i, r_m, c_i, c_m, m_i, m_m, l_i, l_m,
                    chi_i, chi_m]
            head = ['a_i', 'a_m', 'p_i', 'p_m', 'r_i', 'r_m', 'c_i', 'c_m',
                    'm_i', 'm_m', 'l_i', 'l_m', 'chi_i', 'chi_m']
            for i in range(len(dat)):
                np.save('%s/%s' % (pathCst, head[i]), dat[i])
                np.savetxt('%s/%s.dat' % (pathCst, head[i]),
                        np.transpose([t_d, dat[i]]),
                        header='t (d) %s (1/d)' %  head[i])

        elif(self.myTG.__class__.__name__ == 'Donadoni_1D'):
            print("donadoni j'ai pas implementer \
                                    le calcul des csts a_i, p_i ")

        return


    def remodelingLoop(self):
        """
        Remodeling loop of generation evolution and hemodynamical modifications
    	Returns:
            (list): t_gene, wssR
        """
        gene = 0
        wss = self.myTG.myHemo.WSS
        t_gene = [self.myTG.t0]
        R_gene = [self.myTG.myHemo.R]
        wssR = [wss]
        message('\n\n\n/========\n/*-----ITERATION N°{}----*\n========\n',
                (gene,))
        printGene(gene)

        # initialization des modeles

        if self.myTG.__class__.__name__[:6] == 'Jansen':
            dCI = self.myTG.initialization()
        elif self.myTG.__class__.__name__ == 'Donadoni_1D':
            dCI = self.myTG.initialization()
            t_events = []
            y_events = []

        #  integration of population equations
        sol = self.myTG.remodelingProcess(dCI)
        print('sol.t[-1] {}'.format(sol.t[-1] * self.myTG.T_d))
        print("self.myTG.__class__.__name__",
                self.myTG.__class__.__name__[:6] == 'Jansen')
        #  definitions des repertoires de sauvegardes
        if self.myTG.__class__.__name__[:6] ==  'Jansen':
            (t_nd, Y_nd, ddtY_nd) = sol.t, sol.y, sol.yp
            # test if no equilibrium state found
            # sol.t_events[2] is the array where equilibrium time are store
            # if empty, then no equilibrium

            # sort and range the events found
            # test if there is at least one event detected
            if([te for te in sol.t_events if te.size!=0] != []):
                t_events, y_events = sortAndCleanEvents(sol.t_events,
                                                        sol.y_events,
                                                        self.myTG.T_d)
            else:
                t_events, y_events = np.array([]), np.array([])

        elif (self.myTG.__class__.__name__ == 'Donadoni_1D'):
            (t_nd, Y_nd) = sol
            # need to add values
            t_events.append(t_nd[-1] * self.myTG.T_d)
            y_events.append(Y_nd[-1])

        # computation of derived variables
        if self.myTG.__class__.__name__.count('Infla') > 0:
            # is a inflammatory model sol calculVolumesInfla
            (ePlus, e_i, e_m, R_gr, Vf, Vs) = calculVolumes_Infla_1D(Y_nd,
                    self.myTG.set_params, self.myTG.V_Oi,
                    self.myTG.V_Om,0.0,0.0, False)
        elif self.myTG.__class__.__name__ == 'Jansen_1D_int':
            raise NotImplementedError

        # elif self.myTG.__class__.__name__ in ('Jansen_1D', 'JansenSansPerm_1D'):
        else:
            (ePlus, e_i, e_m, R_gr, Vf, Vs) = calculVolumes_1D(Y_nd,
                    self.myTG.set_params, self.myTG.V_Oi,
                    self.myTG.V_Om,0.0,0.0, False)


        t_d = t_nd * self.myTG.T_d
        t_gene.append(t_d[-1])
        R_gene.append(R_gr[-1])
        # 
        e_remodelage = ePlus[-1]

        if(self.myTG.remodBool == False):
            print('\n +++++++++++++++++++++++++++++++++++++++++++++')
            print('pas de remodelage on reste à la gene ', gene)
            print('+++++++++++++++++++++++++++++++++++++++++++++\n')
        print('t_d[-1] {} self.myTG.tf {}'.format(t_d[-1], self.myTG.tf))
        print('self.myTG.remodBool', self.myTG.remodBool)
        while t_d[-1] < self.myTG.tf and self.myTG.remodBool \
                and sol.t_events[3].size == 0:
            print('in loop')
            gene += 1
            message('\n\n\n/========\n/*-----ITERATION N°{}----*\n========\n',
                    (gene,))
            # update the hemodynamical configuration, i.e. R_hemo
            self.myTG.myHemo.update_WSS(e_remodelage)

            if self.myTG.__class__.__name__[:6]  == 'Jansen':
                CI = sol
            elif(self.myTG.__class__.__name__ == 'Donadoni_1D'):
                CI = np.zeros(len(Y))
                for i in range(len(Y)):
                    CI[i] = Y[i][-1]
            else:
                raise ValueError("No model found")

            # self.myTG.R_GFs_WSS_eE, self.myTG.Perm_CfGFs = stimuli_WSS(self.myTG.myHemo.WSS)
            sol = self.myTG.remodelingProcess(CI)

            if self.myTG.__class__.__name__[:6]  == 'Jansen':
                (t_nd, Y_nd, ddtY_nd) = sol.t, sol.y, sol.yp

                # test if no equilibrium state found
                if len(sol.t_events[2]) == 0:
                    print('no equilibrium state found')
                    # reshape of events
                    sol_t_events = sol.t_events[:2]
                    sol_y_events = sol.y_events[:2]
                    # reshape of events
                    if([te for te in sol_t_events if te.size!=0] != []):
                        t_ev_s = np.concatenate([te for te in sol_t_events if te.size!=0])
                        y_ev_s = np.concatenate([ye for ye in sol_y_events if ye.size!=0])
                        # sort in the same way t_ev and y_ev
                        idxs = np.argsort(t_ev_s)
                        t_ev = t_ev_s[idxs] * self.myTG.T_d
                        y_ev = y_ev_s[idxs]
                        # calcul of derived variables
                        t_events = t_ev
                        y_events = y_ev
                    else:
                        t_ev_s = y_ev_s = np.array([])
                        t_events = y_events = np.array([])
                else:
                    print('equilibrium state found')
                    print('final variables values : \n %s' % (sol.y[:,-1]))
                    print('rescaled and non-dim values %s' % (self.myTG.set_params['sys_dim']))

                    if([te for te in sol.t_events if te.size!=0] != []):
                        t_ev_s = np.concatenate([te for te in sol.t_events \
                                if te.size!=0])
                        y_ev_s = np.concatenate([ye for ye in sol.y_events \
                                if ye.size!=0])
                        # sort in the same way t_ev and y_ev
                        idxs = np.argsort(t_ev_s)
                        t_ev = t_ev_s[idxs] * self.myTG.T_d
                        y_ev = y_ev_s[idxs]
                        t_events = t_ev
                        y_events = y_ev
                        # all the values for events are already within sol.t_events
                    else:
                        print('pas de truc dit')
                        # t_ev_s = np.array([self.myTG.t_equi])
                        # y_ev_s = np.array([self.myTG.y_equi])
                        # t_events = np.array([self.myTG.t_equi])
                        # y_events = np.array([self.myTG.y_equi])
                    print('t_events', np.array2string(t_events, separator=','))

            elif(self.myTG.__class__.__name__ == 'Donadoni_1D'):
                (t_nd, Y_nd) = sol
                t_events.append(t_nd[-1] * self.myTG.T_d)
                y_events.append(Y_nd[-1])
            else:
                raise ValueError('model not found')

            # computation of derived variables
            if self.myTG.__class__.__name__.count('Infla') > 0:
                # is a inflammatory model sol calculVolumesInfla
                (ePlus, e_i, e_m, R_gr, Vf, Vs) = calculVolumes_Infla_1D(Y_nd,
                        self.myTG.set_params, self.myTG.V_Oi,
                        self.myTG.V_Om,0.0,0.0, False)
            elif self.myTG.__class__.__name__ == 'Jansen_1D_int':
                raise NotImplementedError

            # elif self.myTG.__class__.__name__ in ('Jansen_1D', 'JansenSansPerm_1D'):
            else:
                (ePlus, e_i, e_m, R_gr, Vf, Vs) = calculVolumes_1D(Y_nd,
                        self.myTG.set_params, self.myTG.V_Oi,
                        self.myTG.V_Om,0.0,0.0, False)
            # # computation of derived variables
            # if self.myTG.__class__.__name__ in ('Jansen_1D', 'JansenSansPerm_1D'):
                # (ePlus, e_i, e_m, R_gr, Vf, Vs) = calculVolumes(Y_nd,
                        # self.myTG.set_params, self.myTG.V_Oi,
                        # self.myTG.V_Om,0.0,0.0, self.myTG.rho_E,
                        # self.myTG.rho_c, self.myTG.rho_s, False)
            # elif self.myTG.__class__.__name__ in ('JansenInfla_1D'):
                # (ePlus, e_i, e_m, R_gr, Vf, Vs) = calculVolumesInfla(Y_nd,
                        # self.myTG.set_params, self.myTG.V_Oi,
                        # self.myTG.V_Om,0.0,0.0, self.myTG.rho_E,
                        # self.myTG.rho_c, self.myTG.rho_s, False)

            t_d = t_nd * self.myTG.T_d
            t_gene.append(t_d[-1])
            R_gene.append(R_gr[-1])
            wss = self.myTG.myHemo.WSS
            wssR.append(wss)

            e_remodelage = ePlus[-1]

            message('gene = {}', (gene,))

        # print('t_events', t_events)
        self.lastGene = gene

        # computation of derived variables
        if self.myTG.__class__.__name__.count('Infla') > 0:
            # is a inflammatory model sol calculVolumesInfla
            (ePlus, e_i, e_m, R_gr, Vf, Vs) = calculVolumes_Infla_1D(Y_nd,
                    self.myTG.set_params, self.myTG.V_Oi,
                    self.myTG.V_Om,0.0,0.0, False)
        elif self.myTG.__class__.__name__ == 'Jansen_1D_int':
            raise NotImplementedError

        # elif self.myTG.__class__.__name__ in ('Jansen_1D', 'JansenSansPerm_1D'):
        else:
            (ePlus, e_i, e_m, R_gr, Vf, Vs) = calculVolumes_1D(Y_nd,
                    self.myTG.set_params, self.myTG.V_Oi,
                    self.myTG.V_Om,0.0,0.0, False)
        # # computation of derived variables
        # if self.myTG.__class__.__name__ in ('Jansen_1D', 'JansenSansPerm_1D'):
            # (ePlus, e_i, e_m, R_gr, Vf, Vs) = calculVolumes(Y_nd,
                    # self.myTG.set_params, self.myTG.V_Oi,
                    # self.myTG.V_Om,0.0,0.0, self.myTG.rho_E,
                    # self.myTG.rho_c, self.myTG.rho_s, False)
        # elif self.myTG.__class__.__name__ in ('JansenInfla_1D'):
            # (ePlus, e_i, e_m, R_gr, Vf, Vs) = calculVolumesInfla(Y_nd,
                    # self.myTG.set_params, self.myTG.V_Oi,
                    # self.myTG.V_Om,0.0,0.0, self.myTG.rho_E,
                    # self.myTG.rho_c, self.myTG.rho_s, False)
        (Vi, Vm) = Vs
        print('t_gene', t_gene, 'gene', gene)
        interv = '_[%.4s_%.4s]' % (t_gene[-2], t_gene[-1])

        pathSavePop = \
                '%s/gene%s%s/tissueGrowth/Pop' % \
                                                (self.path_abs, gene, interv)
        pathSaveGFs = \
                '%s/gene%s%s/tissueGrowth/GFs' % \
                                                (self.path_abs, gene, interv)
        pathSaveCst = \
                '%s/gene%s%s/tissueGrowth/Csts' % \
                                                (self.path_abs, gene, interv)
        pathSaveGlobal = \
                '%s/gene%s%s/tissueGrowth/global' % \
                                                (self.path_abs, gene, interv)
        os.makedirs(pathSavePop)
        os.makedirs(pathSaveGFs)
        os.makedirs(pathSaveCst)
        os.makedirs(pathSaveGlobal)

        save_obj(sol,'%s/gene%s%s/tissueGrowth/' % (self.path_abs, gene,
            interv), 'sol.pkl')

        # print("before writeResult")
        self.myTG.writeResults(sol, Vf, pathSavePop, pathSaveGFs)
        e_remodelage = ePlus[-1]

        # give the last wss value for final time
        self.myTG.myHemo.update_WSS(e_remodelage)
        wssR.append(self.myTG.myHemo.WSS)
        print('last value of WSS is %s' % (self.myTG.myHemo.WSS))

        #  sauvegarde des donnees globales
        np.save('%s/t_d' % pathSaveGlobal, t_d)
        np.save('%s/t_gene' % pathSaveGlobal, t_gene)
        np.save('%s/R_gene' % pathSaveGlobal, R_gene)
        np.save('%s/t_events' % pathSaveGlobal, t_events)
        np.save('%s/y_events' % pathSaveGlobal, y_events)
        np.save('%s/wssR' % pathSaveGlobal, wssR)
        np.save('%s/e_i' % pathSavePop, e_i)
        np.save('%s/e_m' % pathSavePop, e_m)
        np.save('%s/ePlus' % pathSavePop, ePlus)
        np.save('%s/R_gr' % pathSavePop, R_gr)
        np.save('%s/Vi' % pathSaveGlobal, Vi)
        np.save('%s/Vm' % pathSaveGlobal, Vm)

        return t_gene, wssR

