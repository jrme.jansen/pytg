#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gmsh
from icecream import ic
import meshio
from warnings import warn
from scipy.interpolate import interp1d
from scipy.optimize import brentq
import shutil
import sys
import os
import numpy as np
import time
# import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D

def progression(r, a, b, N):
    """Progression between a and b, of N pts

    Parameters
    ----------
    r : float
    a : float
        starting point 
    b : float
        end point
    N : int
        Nbr of points

    Returns
    ----------
    x : np.array of shape N
     coordinate of point discretisation between a and b
    """
    x = [a]
    L = b-a
    dx = L * (1-r) / (1-r**(N-1))
    for i in range(1,N):
        x.append(x[-1] + dx)
        dx *= r
    return np.array(x)


def myBump(r, a, b, N):
    """

    Parameters
    ----------
    r : float
        center dense r > 1, edge r < 1
    Returns
    -------
    x : array of shape N+1
        coordinate

    """
    n = int((N + 1) / 2)
    right = progression(r, 0, 1, n)
    left = -right

    dom = np.concatenate((left[::-1], right[1:]))

    return 0.5 * (a + b) + .5 * (b - a) * dom



def findOptimNbr(fun, x0, x1, N1=30, dN=100, EPS=1e-8):
    """ Find the optimal number of points to sample the function ``fun(x)`` on
    the interval x in [x0,x1].

    Parameters
    ----------
    fun : callable
        The function on interest
    x0 : int
        Inferior limit on interval
    x1 : int
        Superior limit on interval
    N1 : int
        The initial number of points
    dN : int
        The increment nbr of pts
    EPS : float
        Relative threshold to conclude about convergence

    Returns
    -------
    N : int
        The optimal number of pts

    """
    N2 = N1 + dN
    xi1 = np.linspace(x0, x1, N1)
    xi2 = np.linspace(x0, x1, N2)
    # sampling on x1 and x2
    fi1 = fun(xi1); fi2 = fun(xi2)
    # interpolate fi1 on x2 pts

    fi1_interp_i2 = interp1d(xi1, fi1, kind = 'cubic')(xi2)
    err = np.nanmax(np.abs((fi2 - fi1_interp_i2) / fi2))
    # plt.figure()
    # plt.plot(xi1, fi1, 'o', label='fi1')
    # plt.plot(xi2, fi2, 'o', label='fi2')
    # plt.plot(xi2, fi1_interp_i2, 'o', label='fi1_interp_i2')
    # plt.legend()
    # plt.figure()
    # plt.plot(xi2, np.abs((fi2 - fi1_interp_i2) / fi2), label='err')
    # plt.legend()
    # plt.show()
    print('N1', N1, 'N2', N2, x0, x1)
    # print('xi1', xi1.shape, 'x2', xi2.shape)
    # print('fi1', fi1.shape)
    # print('fi2', fi2.shape)
    print('err', err)
    while err > EPS and N2 < 10000:
        N1 = N2
        xi1 = xi2
        fi2 = fi1
        N2 += dN
        xi2 = np.linspace(x0, x1, N2)
        fi1 = fun(xi1)
        fi2 = fun(xi2)
        # interpolate fi1 on x2 pts
        fi1_interp_i2 = interp1d(xi1, fi1, kind = 'cubic')(xi2)
        err = np.nanmax(np.abs((fi2 - fi1_interp_i2) / fi2))
        # print('N1', N1, 'N2', N2, x0, x1)
        # print('xi1', xi1.shape, 'x2', xi2.shape)
        # print('fi1', fi1.shape)
        # print('fi2', fi2.shape)
        # print('err', err)
    return N1

def rootGeomSerie(coef, nbpt, R0, delta):
    """ Geometric serie meshing progression.

    Return
    ------
    root : float
        The root of formula Delta_0 = R (r-1)/(r^(n-1) - 1)
    """
    return delta - R0 * (coef - 1) / (coef**(nbpt-1) - 1)

def rootBump(coef, nbpt, length, dx):
    """ Function computing the ``a`` first length of element in the ``Bump``
    progression tranfinite mesh of GMSH. Seen line 196 in meshGEdge.cpp file

    Parameters
    ----------
    coef : float
        The coefficient factor
    nbpt : int
        Nbr of point in the segment
    length : float
        Lenght of the segment
    dx : float
        The wanted value

    Returns
    -------
    root : float
        The root function coded line 196 in meshGEdge.cpp file from Gmsh
    """
    if not coef < 1.0:
        raise ValueError('In rootBump, coef > 1.0')
    a = (2. * np.sqrt(1. - coef) *
              np.log(np.abs((1. + 1. / np.sqrt(1. - coef)) /
                                (1. - 1. / np.sqrt(1. - coef)))) /
              (float(nbpt) * length))
    b = -a * length * length / (4. * (coef - 1.))
    return (-a * pow(0 * length - (length)*0.5, 2) + b) - dx


def ghostArray(x, x0, x_1):
    """
    Parameters
    ----------
    xE : np.array of shape (N,)
        center cell position
    x0 : float
        1er value
    x_1 : float
        Last value

    Returns
    -------
    x_ghost : np.array of shape (N + 2,)

    """
    x_ghost = np.zeros(len(x) + 2)
    x_ghost[1:-1] = x
    x_ghost[0] = x0
    x_ghost[-1] = x_1
    return x_ghost

def hemoVolumes2D(planar, R_gr, dxE, Rext, R_LEE):
    """ Function which update the luminal volume

    Parameters
    ----------
    planar : bool

    R_gr : float or np.ndarray
        Radius of the lumen, interface between whre blood flow and art. wall
    dxE : np.ndarray of float
        Length of element
    Rext : float
        External radius
    R_LEE : float
        External lamina radius

    Returns
    -------
    Vl : float of np.ndarray
        The luminal volume
    Vw : np.ndarray of shape (Ni_TG,)
        The arterial wall volume of each compartiment
    Vall : float or np.ndarray
        The entire volume
    Vlim : float or np.ndarray
        The volume of lumen + intima + media

    """
    # Defintion of luminal volume
    # from arterial discretisation, computation of volume of elements
    # for lumen, all (= lumen + arterial wall) and adventice
    if planar:
        Vl = 2 * R_gr * dxE
        Vall = 2 * Rext * dxE
        Vad = 2 * (Rext - R_LEE) * dxE
    else:
        # print("R_gr.shape {}, dxE.shape {}".format(R_gr.shape, dxE.shape))
        Vl =   R_gr**2 * np.pi * dxE
        Vall = Rext**2 * np.pi * dxE
        Vad = dxE * np.pi * (Rext**2 - R_LEE**2)
    Vw = Vall - Vl
    Vlim = Vall - Vad
    return Vl, Vw, Vall, Vlim

def volume3DmeshPrims(TCO, TCX, R_LEE, wallFaceCent=False):
    """ Calculate the volume of compartiment as it is a prisme.

    Parameters
    ----------
    TCO : np.ndarray of shape (Np_TG, 4)
        The coordinate table
    TCX : np.ndarray of shape (Ni_TG,8)
        The connextiovity table
    R_LEE : float
        External lamina radius
    wallFaceCent : bool
        Calculate or not the face center of each wall compartiment

    Returns
    -------
    Vl : np.ndarray of shape (Ni_TG,)
        The luminal volume of each compartiment
    Vw : np.ndarray of shape (Ni_TG,)
        The arterial wall volume of each compartiment
    Vall : np.ndarray of shape (Ni_TG,)
        The volume of all the entire artery lumen + arterial wall
    Vlim : np.ndarray of shape (Ni_TG,)
        The volume of lumen + intima + media
    xyzE : np.ndarray of shape (Ni_TG,3) or None
        If wallFaceCent=False, return the value of the face center of each
        wall compartiment
    """
    tol = 1e-8
    fromMtoMM = 1e3
    Ni_TG = TCX.shape[0]
    # from m to mm
    TCOcopy_mm = TCO.copy()
    TCOcopy_mm[:,:-1] *= fromMtoMM
    R_LEE_mm = R_LEE * fromMtoMM
    # total volume of all the artery as lumen + intima + media + adventice
    Vall = np.zeros(Ni_TG, dtype=float)
    # volume of lumen + intima + media
    Vlim = np.zeros(Ni_TG, dtype=float)
    # volume of lumen
    Vl = np.zeros(Ni_TG, dtype=float)
    if wallFaceCent:
        xyzE = np.zeros((Ni_TG, 3), dtype=float)
    else:
        xyzE = None

    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    # ax.set_xlabel('x')
    # ax.set_ylabel('y')
    # ax.set_zlabel('z')
    for i in range(Ni_TG):
        pts_i = TCX[i]
        # print('pts_i', pts_i)
        xyzth_pts_i = TCOcopy_mm[pts_i]
        # only x, y, z coord
        coord_pts_i = xyzth_pts_i[:,:-1]
        # print('coord_pts_i', coord_pts_i)
        # generate R_LEE coordinates
        # get the angle
        th0123 = xyzth_pts_i[:4, -1]
        # get the angle
        th4567 = xyzth_pts_i[4:,-1]
        # print('th0123', th0123, 'th4567', th4567)
        if np.abs(th0123[0] - th0123[3]) > 1e-8:
            raise ValueError('Not same angle {} and {}'.format(th0123[0], th0123[3]))
        if np.linalg.norm(th0123 - th4567) > 1e-8:
            raise ValueError('||th0123 - th4567|| > 1e-8 ({})'.format(
                np.linalg.norm(th0123 - th4567)))
        p0RLEE = np.array([coord_pts_i[0,0],
            R_LEE_mm * np.cos(th0123[0]), R_LEE_mm * np.sin(th0123[0])])
        p1RLEE = np.array([coord_pts_i[0,0],
            R_LEE_mm * np.cos(th0123[1]), R_LEE_mm * np.sin(th0123[1])])
        p2RLEE = np.array([coord_pts_i[4,0],
            R_LEE * np.cos(th4567[0]), R_LEE * np.sin(th4567[0])])
        p3RLEE = np.array([coord_pts_i[4,0],
            R_LEE_mm * np.cos(th4567[1]), R_LEE_mm * np.sin(th4567[1])])

        # print('coord_pts_i', coord_pts_i)
        dx = coord_pts_i[4,0] - coord_pts_i[0,0]
        p_8 = np.array([coord_pts_i[0][0], 0., 0.])
        p_9 = np.array([coord_pts_i[-1][0], 0., 0.])
        # ax.scatter(p_8[0], p_8[1], p_8[2])
        # for i, pi in enumerate(pts_i):
            # ax.scatter(TCO[pi,0], TCO[pi,1], TCO[pi,2], label='%s'%i)
            # plt.legend()
            # plt.pause(1)
        if np.any((coord_pts_i[:4,0] - coord_pts_i[0,0]) > tol) or\
                np.any((coord_pts_i[4:,0] - coord_pts_i[-1,0]) > tol):
            raise ValueError('Points should have same x position')
        # first 4 points of TCX is at the R sup of the layer
        # length for lumen
        al = np.linalg.norm(coord_pts_i[3] - coord_pts_i[2])
        bl = np.linalg.norm(coord_pts_i[2] - p_8)
        cl = np.linalg.norm(coord_pts_i[3] - p_8)
        # length for lim
        alim =  np.linalg.norm(p0RLEE - p1RLEE)
        blim =  np.linalg.norm(p1RLEE - p_8)
        clim =  np.linalg.norm(p0RLEE - p_8)
        # lenght for all
        aall = np.linalg.norm(coord_pts_i[1] - coord_pts_i[0])
        ball = np.linalg.norm(coord_pts_i[0] - p_8)
        call = np.linalg.norm(coord_pts_i[1] - p_8)
        if wallFaceCent:
            xyzE[i] = ((coord_pts_i[2] + coord_pts_i[3] + \
                coord_pts_i[6] + coord_pts_i[7]) / 4.) * fromMtoMM**-1
        # go back to m for volumes
        Vall[i] = dx * S_tri(aall, ball, call) * (fromMtoMM**-1)**3
        Vl[i]   = dx * S_tri(al, bl, cl)       * (fromMtoMM**-1)**3
        Vlim[i] = dx * S_tri(alim, blim, clim) * (fromMtoMM**-1)**3
        # print('Vl[i]', Vl[i], 'Vall[i]', Vall[i], 'Vlim[i]', Vlim[i])
        if Vall[i] < 1e-16 or Vl[i] < 1e-16 or Vlim[i] < 1e-16:
            print('p0RLEE', p0RLEE, p1RLEE)
            print('alim, blim, clim', alim, blim, clim)
            raise ValueError('Volume could not be negative or equal to 0.')
    Vw = Vall - Vl
    return Vl, Vw, Vall, Vlim, xyzE

def volume3Dmesh(R_gr, TCO, TCX, R_LEE, Rext, wallFaceCent=False):
    """ Calculate the volume of compartiment as it is piece on a cylinder.
    A compartiment luminal volume noted V_l^c is computed as

    .. math::
        V_l^c = \\frac{(\\theta_2 - \\theta1)}{2} (R_{l}^2)
        V_{all}^c = \\frac{(\\theta_2 - \\theta1)}{2} (R_{ext}^2 - R_{l}^2)
        V_{m}^c = \\frac{(\\theta_2 - \\theta1)}{2} (R_{LEE}^2 - R_{LEI}^2)
        V_{i}^c = \\frac{(\\theta_2 - \\theta1)}{2} (R_{LEI}^2 - R_{l}^2)
        V_{i+m}^c = V_{i}^c + V_{m}^c 
        V_{i+m+l}^c = V_{i}^c + V_{m}^c + V_l^c

    The center of the face is calculated as :
    
    .. math::
        C_c = ((x_1 + x2)/2,
            R_l \\frac{sin(\\theta_2) - sin(\\theta_1)}{\\theta_2 - \\theta_1},
            R_l \\frac{cos(\\theta_1) - cos(\\theta_2)}{\\theta_2 - \\theta_1})

    Parameters
    ----------
    TCO : np.ndarray of shape (Np_TG, 4)
        The coordinate table
    TCX : np.ndarray of shape (Ni_TG,8)
        The connextiovity table
    R_LEE : float
        External lamina radius
    wallFaceCent : bool
        Calculate or not the face center of each wall compartiment

    Returns
    -------
    Vl : np.ndarray of shape (Ni_TG,)
        The luminal volume of each compartiment
    Vw : np.ndarray of shape (Ni_TG,)
        The arterial wall volume of each compartiment
    Vall : np.ndarray of shape (Ni_TG,)
        The volume of all the entire artery lumen + arterial wall
    Vlim : np.ndarray of shape (Ni_TG,)
        The volume of lumen + intima + media
    xyzE : np.ndarray of shape (Ni_TG,3) or None
        If wallFaceCent=False, return the value of the face center of each
        wall compartiment
    """
    tol = 1e-8
    Ni_TG = TCX.shape[0]
    # from m to mm
    TCOcopy_mm = TCO.copy()
    Vall = np.zeros(Ni_TG, dtype=float)
    # volume of lumen + intima + media
    Vlim = np.zeros(Ni_TG, dtype=float)
    # volume of lumen
    Vl = np.zeros(Ni_TG, dtype=float)
    if wallFaceCent:
        xyzE = np.zeros((Ni_TG, 3), dtype=float)
    else:
        xyzE = None

    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    # ax.set_xlabel('x')
    # ax.set_ylabel('y')
    # ax.set_zlabel('z')
    for i in range(Ni_TG):
        pts_i = TCX[i]
        # print('pts_i', pts_i, 'i', i)
        xyzth_pts_i = TCOcopy_mm[pts_i]
        # print('xyzth_pts_i', xyzth_pts_i)
        # only x, y, z coord
        coord_pts_i = xyzth_pts_i[:,:-1]
        # print('coord_pts_i', coord_pts_i)
        # generate R_LEE coordinates
        # get the angle
        th0123 = xyzth_pts_i[:4, -1]
        # get the angle
        th4567 = xyzth_pts_i[4:,-1]
        theta1 = th0123[0]
        theta2 = th0123[1]
        dtheta = theta2 - theta1
        if dtheta < 0:
            # print('th0123', th0123)
            # print('i', i, 'th', xyzth_pts_i[:, -1])
            # print('dtheta', dtheta, 'th1', theta1, 'th2', theta2)
            theta1_prim = theta1 - 2 * np.pi
            dtheta = theta2 - theta1_prim
            if dtheta < 0:
            # if np.isclose(theta2,0.0):
                # theta2 = 2 * np.pi
                # dtheta = theta2 - theta1
            # else:
                raise ValueError('theta2 = {} and not 0, theta1prim = {}'.format(
                    theta2, theta1_prim))

        # print('th0123', th0123, 'th4567', th4567)
        if np.abs(th0123[0] - th0123[3]) > 1e-8:
            raise ValueError('Not same angle {} and {}'.format(th0123[0], th0123[3]))
        if np.linalg.norm(th0123 - th4567) > 1e-8:
            print('th0123', th0123, 'th4567', th4567)
            raise ValueError('||th0123 - th4567|| > 1e-8 ({})'.format(
                np.linalg.norm(th0123 - th4567)))
        # print('coord_pts_i', coord_pts_i)
        x1 =  coord_pts_i[0,0]
        x2 = coord_pts_i[4,0]
        dx = x2 - x1

        if wallFaceCent:
            xyzE[i] = np.array([(coord_pts_i[4,0] + coord_pts_i[0,0]) * 0.5,
                R_gr[i] * (np.sin(theta2) - np.sin(theta1)) / dtheta,
                R_gr[i] * (np.cos(theta1) - np.cos(theta2)) / dtheta])
        # go back to m for volumes
        Vall[i] = dx * dtheta * 0.5 * (Rext**2)
        Vl[i]   = dx * dtheta * 0.5 * R_gr[i]**2
        Vlim[i] = dx * dtheta * 0.5 * R_LEE**2
        # print('Vl[i]', Vl[i], 'Vall[i]', Vall[i], 'Vlim[i]', Vlim[i])
        if Vall[i] < 1e-16 or Vl[i] < 1e-16 or Vlim[i] < 1e-16:
            print('i', i, 'th', xyzth_pts_i[:, -1])
            print('dtheta', dtheta, 'th1', theta1, 'th2', theta2)
            # print('dtheta', dtheta, 'th1', theta1)
            raise ValueError('Volume could not be negative or equal to 0.')
    Vw = Vall - Vl
    return Vl, Vw, Vall, Vlim, xyzE


def S_tri(a,b,c):
    """ Stable Henon formula for area of a triangle of edges of length
    a, b, c
    """
    return 0.25 * np.sqrt((a + b + c) * (c - (a-b)) * \
            (c +(a-b)) * (a + (b - c)))


def from_yz_to_theta(y,z):
    """ Relation from coordinate to angle between 0 and 2 Pi
    https://math.stackexchange.com/questions/1327253/how-do-we-find-out-angle-from-x-y-coordinates

    Parameters
    ----------
    y :float or np.ndarray of shape (N,)
    z :float or np.ndarray of shape (N,)

    Returns
    -------
    angle : float or np.ndarray of shape (N,)
        Angle in [0;2*PI[

    """
    angle = np.pi - np.pi*0.5*(1 + np.sign(y))*(1 - np.sign(z**2)) - \
            np.pi*.25 * (2 + np.sign(y)) * np.sign(z) - np.sign(y*z) * \
            np.arctan((np.abs(y) - np.abs(z)) / (np.abs(y) + np.abs(z)))
    # rm 2*pi value
    if isinstance(z,float) and isinstance(z,float):
        if np.isclose(angle, 2*np.pi): angle = 0.0
    else:
        # print('np.isclose(angle, 2*np.pi)', np.isclose(angle, 2*np.pi))
        angle[np.isclose(angle, 2*np.pi)] = 0.0
    return angle


def transf(r, t, x, Np_r, Np_x):
    """ Transformation from points in the refe (r,theta,z) for store in ordered
    manner datas.

    Parameters
    ----------
    r : int
        Radial index
    t : int
        Circumferiential index
    x : int
        Longitudianl index
    Np_r : int
        Nbr of pts in radial direction
    Np_x : int
        Nbr of pts in longitudinal direction
    """
    return r + x * (Np_r) + t * ((Np_r) * (Np_x))


def generateTCO_TCX(Np_r, Np_x, Np_th, compartXYZT):
    """Generate table of coordinate and table of connexion knowing the node
    positions stored in compartXYZT in a way where
    The TCO is stored as for all th=cte -> values, and then th = th+ delta th,
    ...


    Parameters
    ----------
    Np_r : int
        Nbr of pts in radial direction
    Np_x : int
        Nbr of pts in longitudinal direction
    Np_th : int
        Nbr of pts in azimuthal direction
    compartXYZT : np.ndarray of shape(Np_r, Np_th * Np_x, 4)
        The positions of the nodes stored as
        where Np_W = Np_th * Np_x

    Returns
    -------
    TCO : np.ndarray of shape (Np_TG, 4)
        The coordinate table
    TCX : np.ndarray of shape (Ni_TG, 8)
        The connextiovity table

    """
    print('Np_th', Np_th, 'Np_x', Np_x)
    Np_W = Np_th * Np_x
    Np_c = int(Np_th / 4) + 1
    print('Np_W', Np_W, 'Np_c', Np_c)
    Np_TG = Np_W * Np_r
    Ni_TG = (Np_x - 1) * (Np_r - 1) * Np_th
    print('compartXYZT.shape', compartXYZT.shape)

    if compartXYZT.shape[1] != Np_W:
        raise TypeError("Shape of compartXYZT not suited" +
                "compartXYZT.shape[1] != N_W")

    # the TCO and TCX init
    TCO = np.zeros((Np_TG, 4)) # 4 for the coord x, y, z, and theta
    # 8 for 8 pts for constr a hexaeddron
    TCX = np.zeros((Ni_TG, 8), dtype=int)

    for r in range(Np_r):
        for x in range(Np_x):
            for t in range(Np_th):
                p = transf(r, t, x, Np_r, Np_x)
                k = t + x * Np_th
                # print('k', k, "p", p, 'coordinate ', compartXYZT[r,k,:])
                TCO[p,:] = compartXYZT[r,k,:]
                # print('TCO[p,:]', TCO[p,:])
    a = 0
    for r in range(Np_r - 1):
        for x in range(Np_x - 1):
            # # print('a', a)
            # managment of first cell
            if Np_c % 2 == 0:
                # odd value for Np_c, so center of first elements has theta=0
                t0 = 0
                tlast = t0 + Np_th-1
                p0 = transf(r,   tlast,   x,   Np_r, Np_x)
                p1 = transf(r,   t0,      x,   Np_r, Np_x)
                p2 = transf(r+1, t0,      x,   Np_r, Np_x)
                p3 = transf(r+1, tlast,   x,   Np_r, Np_x)
                p4 = transf(r,   tlast,   x+1, Np_r, Np_x)
                p5 = transf(r,   t0,      x+1, Np_r, Np_x)
                p6 = transf(r+1, t0,      x+1, Np_r, Np_x)
                p7 = transf(r+1, tlast,   x+1, Np_r, Np_x)
                pp = np.array([p0, p1, p2, p3, p4, p5, p6, p7])
                if np.any(pp > Np_TG - 1):
                    raise ValueError("pp nbr is > Np - 1 (equal {}). pp {}".format(
                        Np - 1, pp))
                TCX[a,:] = pp
                a += 1
            for t in range(Np_th - 1):
                # print('a', a)
                # print('r', r,'x', x,'t', t)
                p0 = transf(r,   t,   x, Np_r, Np_x)
                p1 = transf(r,   t+1, x, Np_r, Np_x)
                p2 = transf(r+1, t+1, x, Np_r, Np_x)
                p3 = transf(r+1, t,   x, Np_r, Np_x)
                p4 = transf(r,   t,   x+1, Np_r, Np_x)
                p5 = transf(r,   t+1, x+1, Np_r, Np_x)
                p6 = transf(r+1, t+1, x+1, Np_r, Np_x)
                p7 = transf(r+1, t,   x+1, Np_r, Np_x)
                pp = np.array([p0, p1, p2, p3, p4, p5, p6, p7])
                # for i, pi in enumerate(pp):
                    # ax.scatter(TCO[pi,0], TCO[pi,1], TCO[pi,2])#, label='t = %s i = %s'%(t, i))
                    # plt.legend()
                    # plt.pause(0.01)
                if np.any(pp > Np_TG - 1):
                    raise ValueError("pp nbr is > Np_TG - 1 (equal {})." +
                            " pp {}".format(Np_TG - 1, pp))
                TCX[a,:] = pp
                a += 1

            # managment of last cell
            if Np_c % 2 != 0:
                # even value for Np_c, so center of first elements hasn't theta=0
                t0 = 0
                tlast = Np_th - 1
                p0 = transf(r,   tlast,   x,   Np_r, Np_x)
                p1 = transf(r,   t0,      x,   Np_r, Np_x)
                p2 = transf(r+1, t0,      x,   Np_r, Np_x)
                p3 = transf(r+1, tlast,   x,   Np_r, Np_x)
                p4 = transf(r,   tlast,   x+1, Np_r, Np_x)
                p5 = transf(r,   t0,      x+1, Np_r, Np_x)
                p6 = transf(r+1, t0,      x+1, Np_r, Np_x)
                p7 = transf(r+1, tlast,   x+1, Np_r, Np_x)
                pp = np.array([p0, p1, p2, p3, p4, p5, p6, p7])
                if np.any(pp > Np_TG - 1):
                    raise ValueError("pp nbr is > Np - 1 (equal {}). pp {}".format(
                        Np - 1, pp))
                TCX[a,:] = pp
                a += 1
    return TCO, TCX


class Mesh(object):
    """Mesh object for CFD simulation with openFoam. This class is based on the
    python API of gmsh.

    Paramters
    --------
    Np_x : int
        Number of points along longitudianl axis
    Np_r : int
        Number of points along transversal axis
    progNr : float
        Progression in radial direction
    Np_xI : int
        Number of points along longitudianl axis at inlet
    Np_xO : int
        Number of points along longitudianl axis at outlet
    Np_xIO : int
        Number of points along longitudianl axis at inlet and outlet
    nameMesh : str
        Name mesh
    myArtery : Artery
        The considered artery
    blocks : bool
        Make a 3 blocks mesh or not
    angle : float
        Half the angle of axisym geom in degree

    Attributes
    ---------
    Np_r : int
        Number of points in radial direction
    Np_x : int
        Number of points in longitudial direction if not block mesh or number
        of point in the middle block for blocks mesh
    Np_xI : int
        Number of points along longitudianl axis at inlet
    Np_xO : int
        Number of points along longitudianl axis at outlet
    Np_xT : int
        Total number of points in the longitudinal direction
    Ni_x : int
        Number of intervall in x axis
    bumpIO float:
        Bump in axial direction for  the inlet block
    blocks : bool
        It is a mesh by blocks ?
    R0 : float
        Initial raduis
    Np_rTG : int
        Number of points in radial direction for TG mesh
    nameMesh : str
        Name mesh
    myArtery : Artery
        The considered artery
    angle : float
        Half the angle of axisym geom in degree
    TGconformCFD : bool
        Create or not conform TG meshs

    """
    # The dimension of the mesh
    dim = 2

    dim3 = 3
    dim2 = 2

    def __init__(self, iterable=(), **kwargs):
        """Fast constructor of the object. As the class must be able to
        evolve quickly, the parameters are implicitly defined in the dictionary
        during the initialization. This is a method that allows flexibility in
        development but may cause a lot of errors.
        """
        self.__dict__.update(iterable, **kwargs)

        print('\n----------\nMesh 2D fast init\n----------')
        if not hasattr(self, "updateType"):
            raise ValueError('No updateType attribute')
        else:
            if not self.updateType in ("algebraic", "laplacianSmoother"):
                raise ValueError('updateType not known {}, avail are '.format(
                    self.updateType)+ '``laplacianSmoother`` or ``algebraic``')
    
        if hasattr(self, 'progNr'):
            # if prog Nr is given
            if hasattr(self,'drMin'): raise ValueError('If progNr given, not drMin')
        if hasattr(self, 'drMin'):
            # print('YOUPIIIIII _\n', self.drMin)
            # if prog Nr is given
            if hasattr(self, 'progNr'): raise ValueError('If drMin given, not progNr')
            args_root = (self.Np_r, self.myArtery.R0, self.drMin)
            self.progNr = brentq(rootGeomSerie, 1.00000001, 10., args_root)
            print('self.progNr', self.progNr)

        if np.isclose(self.progNr, 1.0):
            self.dyMin = self.myArtery.R0 / (self.Np_r-1)
        else:
            self.dyMin = self.myArtery.R0 * (self.progNr - 1) / \
                (self.progNr**(self.Np_r-1) - 1)
            if hasattr(self, 'drMin'):
                if not np.isclose(self.dyMin, self.drMin):
                    raise ValueError('drMin not well calc')
        self.Ni_r = self.Np_r - 1

        if(self.blocks):
            if hasattr(self, 'Np_xIO'):
                self.Np_xI = self.Np_xIO
                self.Np_xO = self.Np_xIO

            self.dx_middle = self.myArtery.Lm / (self.Np_x - 1)

            self.Np_xT = self.Np_x + self.Np_xI - 1 + self.Np_xO - 1
            self.Ni_x = (self.Np_xT - 1)
            # define the coef factor for Bump progression according to the 
            # length of middle elements, to ensure continuity of elements size
            # at the transition from inlet to middle and middle to outlet
            if hasattr(self, 'bumpIO'):
                self.bumpI = self.bumpIO
                self.bumpO = self.bumpIO

            if self.bumpI is None:
                print('bumpI not given by user, so it is calc. for smooth'+
                        'transision between inlet / outlet & middle blocks')
                args_rootI = (self.Np_xI, self.myArtery.Li, self.dx_middle)
                self.bumpI = brentq(rootBump, 1e-5, 0.999999, args_rootI)
            if self.bumpO is None:
                print('bumpO not given by user, so it is calc. for smooth'+
                        'transision between inlet / outlet & middle blocks')
                args_rootO = (self.Np_xO, self.myArtery.Lo, self.dx_middle)
                self.bumpO = brentq(rootBump, 1e-5, 0.999999, args_rootO)
            elif hasattr(self, 'bumpI') and hasattr(self, 'bumpO'):
                print('User specified bumps')
            else:
                raise AttributeError('bumpIO or bumpI & bumpO not given.')

        else:
            if hasattr(self, 'Np_xIO') or hasattr(self, 'Np_xI') or \
                    hasattr(self, 'Np_xO'):
                raise AttributeError('not block mesh and Np_xIO given')
            self.Np_xT = self.Np_x
            self.Ni_x = (self.Np_xT - 1)
        self.Np_CFD = self.Np_xT * self.Np_r
        self.NCells_CFD = self.Ni_r * self.Ni_x
        self.Np_rTG = 2
        if self.TGconformCFD:
            # Ni_TG number of compartiment for TG
            self.Np_TG = self.Np_rTG * self.Np_xT
            self.Ni_TG = (self.Np_rTG - 1) * self.Ni_x
        else:
            l_attr = ["Np_x2TG", "Np_x1TG", "Np_x3TG"]
            if not all([hasattr(self, attr) for attr in l_attr]):
                raise ValueError('Not all attributes are given by user' +
                        "{}".format(l_attr))
            self.Np_xTTG = self.Np_x1TG + self.Np_x2TG - 1 + self.Np_x3TG - 1
            print('self.Np_xTTG', self.Np_xTTG)
            self.Np_TG = self.Np_xTTG * self.Np_rTG
            print('', self.Np_TG)
            self.Ni_xTG = self.Np_xTTG - 1
            self.Ni_TG = (self.Np_rTG - 1) * self.Ni_xTG
            print('', self.Ni_TG)
        print("TG ``mesh`` (TGconformCFD = {}):".format(self.TGconformCFD))
        print("\t Numbers of cells in TG           = {}".format(self.Ni_TG))
        print("\t Numbers of pts in x direction TG = {}".format(self.Np_TG))
        print('--------------------------------')
        print("CFD mesh:")
        print("\t Numbers of pts (total) in x dir = {}".format(self.Np_xT))
        print('\t Numbers of intervall x axis     = {}'.format(self.Ni_x))
        print('\t Number of pts nbr in r axis     = {}'.format(self.Np_r))
        print('\t Number of intervall in r axis   = {}'.format(self.Ni_r))
        print("\t Numbers of cells in mesh        = {}".format(self.NCells_CFD))
        self.R0 = self.myArtery.R0




