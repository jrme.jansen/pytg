#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from warnings import warn
import numpy as np
from scipy.special import jn, yn
from scipy.interpolate import interp1d
from scipy.fft import fft, fftfreq, ifft, rfft, rfftfreq, irfft

from scipy.optimize import newton
from .artery import Artery
from .fluid import Newtonian, Quemada, WalburnScheck, Casson,\
        AVAIL_HEMORHEO_MODELS

from pyTG.utilities import message


def funRootQ_Casson(G, R, Q, tau0, muInf):
    """ Root function to find the opposite pressure gradient value for
    Poiseuille flow with Casson fluid

    Parameters
    --------
    G : float
        Pressure gradient
    R : float
        Radius
    Q : float
        flow rate
    tau0 : float
    muInf : float

    Returns
    -------
        Equation relation defined in Mazumdar Biofluid Mechanic p.93

    """
    rp = (2.0 * tau0) / G
    cp = rp / R
    Q0 = ((np.pi * G * R**4.0) / (8.0 * muInf))
    return Q0 * (1. - (16./7.) * cp**.5 + (4./3.) * cp - (1./21.) * cp**4.) - Q


def alpha_Q(G, R, Q, tau0, muInf, lmd, q, Psum):
    """ Function used for computation of G of Quemada hemo
    """
    WSS = 0.5 * R * G
    return (np.sqrt(tau0) + np.sqrt(muInf * lmd)) / np.sqrt(WSS)


def F_Q(G, R, Q, tau0, muInf, lmd, q, Psum):
    """ Function used for computation of G of Quemada hemo
    """
    params = G, R, Q, muInf, tau0, lmd, q, Psum
    r = np.sqrt(1.0 - 2.0 * alpha_Q(*params) * q +\
            alpha_Q(*params)**2.0)
    sumQ = 0
    for i in range(7):
        sumQ += alpha_Q(*params)**(i+1) * Psum[i]

    return .5 * ( 1. - (8. / 7.) * alpha_Q(*params) * (1. + q)+\
        (4./3.) * alpha_Q(*params)**2. - alpha_Q(*params)**8. * Psum[-2]+\
        (1. + sumQ) * r + alpha_Q(*params)**8. * Psum[-1] * \
        np.log((1. - alpha_Q(*params) * q + r) / (alpha_Q(*params) * (1. - q))))


def funRootQ_Quemada(G, R, Q, tau0, muInf, lmd, q, Psum):
    """

    Parameters
    --------
    G : float
        Pressure gradient
    R : float
        Radius
    Q : float
        flow rate

    Returns
    -------
        Equation relation defined in Mazumdar Biofluid Mechanic p.93

    """
    params = (G, R, Q, tau0, muInf, lmd, q, Psum)
    C = ((np.pi * G * R**4.0) / (8 * muInf))
    return  C * F_Q(*params)- Q


def fromQvToG(Q, R, planar, modelF, argsF):
    """ Compute the hemodynamic variables of cylindrical Poiseuille flow :
        * The opposite pressure gradient

        .. math::
            \\frac{dP}{dz}=-G

        * Max velocity $u_M$
        * Wall shear stress

    Firstly, the relation between the pressure gradient and the flow rate
    must be defined. Note that for most generalized Newtonian fluid (GNF),
    the relation G=f(Q) is not explicitally defined and root finding is used.

    Parameters
    ----------
    Q : float
        Flow rate
    R : float
        Arterial Radius
    modelF : str
        The fluid model name
    planar : str
        The fluid model name
    argsF : tuple
        Constiutive parameters of the fluide model

    Returns
    ------
    G : float

    uMax : float
        maximal velocity
    WSS : float
        The wall shear stress

    """
    if planar:
        if modelF == 'Newtonian':
            mu, nu = argsF
            G =  (3. * mu * Q) / (2.* R**3)
            Umax = ((R**2) / (2.0 * mu)) * G
        if modelF == 'WalburnScheck':
            NotImplementedError
        elif modelF == 'Casson':
            NotImplementedError
        elif modelF == 'Quemada':
            NotImplementedError
        else:
            NotImplementedError

        wss = G * R
    else:
        if modelF == 'Newtonian':
            mu, nu = argsF
            G = (8. * mu * Q) / (np.pi * R**4)
            Umax = (R**2 / (4. * mu)) * G
        if modelF == 'WalburnScheck':
            n, K,  = argsF
            r_n = (3. * n + 1.) / n
            G = 2. * K * (Q * r_n / (np.pi * R**(3. + n**-1)))**n
            Umax = (1./(2. * K))**(n**-1) * G**(n**-1) * \
                        (n/(n +1.) * R**(1. + n**-1))
        elif modelF == 'Casson':
            tau0, muInf = argsF
            args_root = (R, Q, tau0, muInf)
            Gguess = (8. * muInf * Q) / (np.pi * R**4)
            G = newton(funRootQ_Casson, Gguess, args=args_root)
            Umax = (R**2 / (4. * muInf)) * G
        elif modelF == 'Quemada':
            (tau0, muInf, lmd, q, Psum) = argsF
            args_root = (R, Q, tau0, muInf, lmd, q, Psum)
            Gguess = (8. * muInf * Q) / (np.pi * R**4)
            G = newton(funRootQ_Quemada, Gguess, args=args_root)
            Umax = (R**2 / (4. * muInf)) * G
        else:
            NotImplementedError

        wss = 0.5 * G * R
    return G, Umax, wss

def velocityField(R, Umax, expo, r=None, plot=False):
    """The velocity field is :

    .. math::
        u_z=  Umax \left( 1-{r^*}^2 \\right)

    Parameters
    ----------
    R : float
        Radius
    Umax : float
        Maximal velocity
    expo : float
        the exposent for (r/R)^expo
    r : np.array
        radius coordinates
    plot : bool
        Plot or not plot that is the boolean

    Returns
    -------
    V np.array
        velocity field

    """
    if r is None:
        r = np.linspace(0, R, 301)
    elif not np.isclose(R,r[-1]):
        raise ValueError('R!=r[-1]')
    r_star = r / R
    V = Umax * (1.0 - r_star**expo)
    return V

def reconstruct_signal(serie, t, omega0):
    """

    Parameters
    ----------
    serie : np.array of shape (Nserie,)
    t : np.array of shape (N,)
    omega0 : float

    Returns
    -------
    signal : np.array of shape (N,)

    """
    # recompute new flow ratee waveform
    N = len(t)
    signal = np.zeros(N, dtype=np.complex64) + serie[0]
    for n in range(1,len(serie)):
        # print('i', i)
        # print('idx_p', idx_p, 'idx_n', idx_n)
        signal += np.conj(serie[n]) * \
                np.exp(1j * omega0 * t * -float(n)) + \
                serie[n] * np.exp(1j * omega0 * t * float(n))
    return np.real(signal)

def womersleyVelocity_(t, r_star, Gn, R, mu, omega0, Wo):
    """
    """
    # definition of poiseuille variable as basic flow for womersley is Poiseuille
    v =  (R**2 * np.real(Gn[0])) / (4. * mu) * (1.0 - r_star**2)
    beta = 1j**1.5 * Wo
    for n in range(1,len(Gn)):
        wt = omega0 * t
        beta_n =  beta * (float(n))**0.5

        z1_n = jn(0, beta_n * r_star)
        z2_n = jn(0, beta_n)
        z3_n = ((Gn[n] * R**2) / (1j * mu * Wo**2 *float(n))) * \
                (1.0 - (z1_n / z2_n))
        v_n = z3_n * np.exp(1j * float(n) * wt)
        v += np.real(v_n + np.conj(v_n))
    return v


def womersleyWSS_(t, Gn, R, Wo, omega0):
    """Compute the wall shear stress in womersley flow as

    .. math::
        \\tau_w = -\\mu \\frac{d}{dr} u_z in r=R
        \\tau_w = \\frac{ -R G_0}{2} + \\Re{ \\left(\\sum \\limits_{n \in
            \mathbb{Z}^*} \\frac{G_n R}{2}
            \\frac{2 J_1(\\beta_n)}{\\beta_n J_0(\\beta_n)}e^{i\\omega_n t}  \\right)}

    And an interessting relation is :

    .. math::
        <\\tau_w> = \\frac{1}{T} \int_0^T \\tau_w \, dt =
                \\tau_w^{\\text{Poi}} = -\\frac{ R G_0}{2}

    Parameters
    ---------
    t : float
        time
    Gn : (ndarray of complex float)
        Fourier coefficient of opposite pressure gradient for all n>0
    Wo : float
        Womersley number
    omega0 : float
        pulsation

        .. math::
            \omega=2\pi f


    Returns
    -------
    WSS : np.ndarray
        The wall shear stress as a function of time

    """
    WSS = R * np.real(Gn[0]) * 0.5
    beta = 1j**(1.5) * Wo
    for n in range(1,len(Gn)):
        beta_n = beta * float(n)**0.5
        F10_n = 2. * jn(1, beta_n) / (beta_n * jn(0, beta_n))
        WSS_n = (Gn[n] * R) / (2.) * F10_n * np.exp(1j * omega0 * float(n) * t)
        WSS += np.real(WSS_n + np.conj(WSS_n))
    return WSS

class Hemodynamic(object):
    """Constructor of Hemodynamic class.

    Parameters
    ----------
    Qv : float
        Flow rate [m3/s]
    myArtery : Artery
        Considered artery
    myFluid : Fluid
        A fluid
    nu : float
        Kinetamic viscosity in [m2/s]
    planar : boolean
        Is a planar pipe ?

    Attributes
    ----------
    Qv : float
        Flow rate
    Ud : float
        Mean velocity among the section of the pipe
    Umax : float
        Max velocity
    coefU : float
        Coefficient which connects center velocity and mean velocity
    G : float
        Opposite of the pressure gradient
    Re : float
        Reynolds number

        .. math::
            \\text{Re} = \\bar{u}D / \\nu

    Re0 : float
        Initial Reynolds number
    Wo : float
        Womersley number

        .. math::
            Wo = \\sqrt{\\frac{\omega}{nu}}R

    Wo0 : float
        Initial Womersley number
    R : (float)
        Current radius of the artery
    R0 : float
        Initial radius of the artery
    nu : float
        Kinematic viscosity
    Dh : float
        Initial hydraulic diameter of the artery
    S : float
        Hemodynamic section of the pipe
    Vl : float
        Hemodynamic volume of the flow domain
    Vl0 : float
        Initial hemodynamic volume of the flow domain
    unsteady : bool
        It is a unsteady hemo ?
    planar : bool
        It is a planar geometry
    T : float
        Period of the flow
    fr : float
        Frequency of the flow
    omega0 : float
        pulsation

        .. math::
            \omega=2\pi f

    t_dat : np.array
        time mesured in experiment
    Q_dat or uC_dat: np.array
        Flow rate in (ml/s) or center velocity (m/s) along time mesured
    Qn : np.array(complex64)
        Fourier coefficient of the decomposition of the flow rate
    Q_recon : np.array of shape (len(t_dat),)
        Reconsucted flow rate by Womersley theory [1]_
    WSS : float
        Wall shear stress
    WSS_list : list
        list of wall shear stress
    WSS_t : np.array
        Wall shear stress among time
    WSS : float
        Wall shear stress in Poiseuille hypothesis
    u_z : np.array
        VelocityField in the given hypothesis

    """

    def __init__(self, iterable=(), **kwargs):
        """Fast constructor of the object. As the class must be able to
        evolve quickly, the parameters are implicitly defined in the dictionary
        during the initialization. This is a method that allows flexibility in
        development but may cause a lot of errors.
        """
        self.__dict__.update(iterable, **kwargs)
        if not self.myFluid.model in AVAIL_HEMORHEO_MODELS:
            raise ValueError('hemo sol for the fluid model %s not coded'\
                    % self.myFluid.model)

        print('\n----------\nHemodynamic fast init\n----------')

        if not hasattr(self, 'unsteady'):
            self.unsteady = False

        self.R = self.myArtery.R0
        self.D = 2 * self.R
        # distinction between planar and cylindrical pipes
        if self.planar:
            geom = 'planar'
            self.S = 2 * self.R  # cross section
            self.Vl = self.myArtery.L * self.S
        else:
            geom = 'cylindrical'
            self.S = np.pi * self.R**2  # cross section
            self.Vl = self.myArtery.L * self.S

        self.S_list = [self.S]
        self.Vl_list = [self.Vl]


        if not self.unsteady:
            print('\n----------\nPoiseuille flow\n----------')
            print('Hypothesis : \n - steady \n - %s \n - %s fluid ' %
                    (geom, self.myFluid.model))
            # if steady flow, set unsteady variable to None
            self.T = self.fr = self.omega0 = \
                    self.Wo = self.t_dat = self.Q_dat = \
                    self.Q_recon = uC_dat = None
            self.Qn = self.Qv + 1j * 0.0

            # Re =
            self.Ud = self.Qv / self.S
            # Reynolds number are based of muNewtonian
            self.Re = self.Ud * self.D * self.myFluid.rho / self.myFluid.muN
            self.Re_list = [self.Re]

            # in attribut not given, then steady hemodynamic

            # compute the opposite of the -dPdz=G
            self.G, self.Umax, self.WSS = fromQvToG(self.Qv, self.R,
                    self.planar, self.myFluid.model, self.myFluid.argsF)
            # define WSS
            self.WSS_list = [self.WSS]
            self.WSS_t = np.array([self.WSS])
            # define velocity field and Reynolds number
            self.u_z = self.steadyVelocityField(self.R, self.Umax)
        else:
            print('\n----------\nWomersley flow\n----------')
            print('Hypothesis : \n - steady \n - Newtonian fluid')
            print(' - in cylindrical pipe')

            if hasattr(self, 'dataFlowRate'):
                print('\nIniti from flow rate data at : \n %s'
                        % self.dataFlowRate)
                warn('Data in dataFlowRate MUST BE IN l/min as' +
                        ' automatic conversion is made')
                data_flowRate = np.loadtxt(self.dataFlowRate)
                self.t_dat = data_flowRate[:,0]
                self.Q_dat = data_flowRate[:,1] * 1e-3/60  # conv to m3/s

                # print('t_dat', self.t_dat)
                self.T = self.t_dat[-1]
                self.fr = self.T**-1
                self.omega0 = 2.0 * np.pi * self.fr
                self.Wo = self.R * np.sqrt(self.omega0 / self.myFluid.nu)
                # compute mode of the fourrier serie
                self.nbrHarmo, self.Qn = self.modeCalcul(self.t_dat, self.Q_dat)
                self.nbrCoef = self.nbrHarmo + 1

            elif hasattr(self, 'dataVelocityCenter'):
                print('Initi from velocity at inlet at \n %s'
                        % self.dataVelocityCenter)
                warn('Data in dataVelocityCenter MUST BE IN m/s')
                data_uC = np.loadtxt(self.dataVelocityCenter)
                self.t_dat = data_uC[:,0]
                self.uC_dat = data_uC[:,1]  #

                self.T = self.t_dat[-1]
                self.fr = self.T**-1
                self.omega0 = 2.0 * np.pi * self.fr
                self.Wo = self.R * np.sqrt(self.omega0 / self.myFluid.nu)
                # compute mode of the fourrier serie
                self.nbrHarmo,self.uCn = self.modeCalcul(self.t_dat, self.uC_dat)
                self.nbrCoef = self.nbrHarmo + 1

                self.Qn = self.from_uC_To_Qn(self.uCn)

            ampl_Qn = np.abs(self.Qn)
            phase_Qn = np.angle(self.Qn)
            nbr_harmo_l = [i for i in range(self.nbrHarmo+1)]
            # print('len(phase_Qn)', len(phase_Qn), '', len(ampl_Qn), '', len(nbr_harmo_l))
            plt.figure()
            plt.bar(nbr_harmo_l, ampl_Qn)
            plt.xlabel('harmonic')
            plt.ylabel('Amplitude |Q_n|')
            plt.savefig('amplitudeQn')
            plt.close()
            plt.figure()
            plt.bar(nbr_harmo_l, phase_Qn)
            plt.xlabel('harmonic')
            plt.ylabel('Phase Phi of Q_n')
            plt.savefig('phaseQn')
            plt.close()
            # as p 226 Mc Donaled
            # compute variance of Qn fourier serie
            # Var_tot = (1./(self.nbrHarmo)) * np.sum(np.abs(self.Qn[1:])**2)
            # VarQn_harmo, VarQn_adim = varianceSerie(self.Qn)

            # print('Qn', np.array2string(self.Qn))

            # plt.figure()
            # plt.bar([i for i in range(1,self.nbrHarmo + 1)],  VarQn_adim)
            # plt.ylabel('Var(Q_n) / Var_tot')
            # plt.xlabel('n nbr of harmonic')
            # plt.savefig('varianceOfQn')

            plt.figure()
            plt.plot(nbr_harmo_l, np.abs(self.Qn))
            plt.ylabel('|Q_n|')
            plt.xlabel('n nbr of harmonic')
            plt.savefig('magQn')
            plt.close()

            # power spetrcum, lien avec auto correlation et produit
            # de convolution
            # phi_11 =
            # compute poiseuille values for check

            self.Qv = np.real(self.Qn[0])
            self.planar = False
            self.S = np.pi * self.R**2
            self.Vl = self.myArtery.L * self.S

            self.G, self.Umax, self.WSS = fromQvToG(self.Qv, self.R,
                    self.planar, self.myFluid.model, self.myFluid.argsF)
            self.Ud = self.Qv / self.S

            self.Re = self.Ud * self.D * self.myFluid.rho / self.myFluid.muN

            self.fromFlowRateToGradP()
            # VarGn_harmo, VarGn_adim = varianceSerie(self.Gn)
            # plt.figure()
            # plt.bar([i for i in range(1,self.nbrHarmo + 1)],  VarGn_adim)
            # plt.ylabel('Var(Q_n) / Var_tot')
            # plt.xlabel('n nbr of harmonic')
            # plt.savefig('varianceOfGn')

            plt.figure()
            plt.plot(nbr_harmo_l, np.abs(self.Gn))
            plt.ylabel('|G_n|')
            plt.xlabel('n nbr of harmonic')
            plt.savefig('magGn')

            self.u_z = self.womersleyVelocityField()
            self.WSS_t = self.womersleyWSS()

            self.G_recon = reconstruct_signal(self.Gn, self.t_dat, self.omega0)
            self.Q_recon = reconstruct_signal(self.Qn, self.t_dat, self.omega0)
            np.savetxt('Q_recon.dat', np.transpose([self.t_dat, self.Q_recon]),
                    header="t (s) Q (m3/s)")
            np.savetxt('dPdz_recon.dat', np.transpose([self.t_dat, self.G_recon]),
                    header="t (s) G (Pa/m)")

        # print in logs import hemodynamical params
        msg = "T (period) = {}\nG = -dpdx = {:e}\nQ_v = {:e}\nWSS = {:e}\n"+\
                "None dim params :\n" +\
                "Reynolds number; Re = {}\nWomersley number; Wo = {}"
        message(msg, (self.T, self.G, self.Qv, self.WSS, self.Re, self.Wo))
        # print('\n**********************************')
        # print('Period T {}'.format(self.T))
        # print('None dimensional parameters :')
        # print('Reynolds number; Re = {}'.format(self.Re))
        # print('Womersley number; Wo = {}'.format(self.Wo))
        # print('**********************************')

    def steadyVelocityField(self, R, Umax, r=None, plot=False):
        """Compute the velocity profil of the hemodynamic flow
        
        Parameters
        ----------

        Returns
        -------
        u_z
        """
        if self.myFluid.model == 'Casson':
            # As Casson fluid is a yield stress model, special function is
            # defined
            u_z = self.cassonVelocityField(self.R, self.Umax, r)
        elif self.myFluid.model == 'WalburnScheck':
            u_z = velocityField(self.R, self.Umax, 1.0+ self.myFluid.n**-1, r)
        elif self.myFluid.model == 'Quemada':
            u_z = self.quemadaVelocityField(self.R, self.Umax, r)
        else:
            u_z = velocityField(self.R, self.Umax, 2.0, r)
        return u_z

    def quemadaVelocityField(self, R, Umax, r=None, plot=False):
        """The velocity field is :

        .. math::
            u_z=  cf article

        Parameters
        ----------
        R : float
            Radius
        Umax : float
            Maximal velocity
        r : np.array
            radius coordinates
        plot : bool
            Plot or not plot that is the boolean

        Returns
        -------
        V : np.array
            velocity field

        """
        if r is None:
            r = np.linspace(0, R, 301)
        r_star = r / R

        alpha = (np.sqrt(self.myFluid.tau0) + np.sqrt(self.myFluid.muInf * \
                self.myFluid.lmd)) / (np.sqrt(self.WSS))

        A = 1.0 - alpha * self.myFluid.q
        B = np.sqrt(r_star) - alpha * self.myFluid.q

        V =  Umax * (0.5 * (A**4.0 - B**4.0) + (2. / 3.) * alpha * \
                (2. * self.myFluid.q - 1.) * (A**3. - B**3.) +\
                alpha**2.0 * (self.myFluid.q - 1.)**2. * (A**2. - B**2. ) - \
                2. * alpha**3. * self.myFluid.q * (self.myFluid.q - 1.) * \
                (1. - np.sqrt(r_star)) + 0.5 * ((1. + (1.0/3.0) * alpha * \
                (5. * self.myFluid.q - 4.)) * (1. - 2. * alpha * \
                self.myFluid.q + alpha**2.0)**(3.0/2.0) - (np.sqrt(r_star) + \
                (1./3.) * alpha * (5.0 * self.myFluid.q -4.0)) * \
                (r_star - 2.0 * alpha * self.myFluid.q * np.sqrt(r_star) + \
                alpha**2.0)**(3.0/2.0)) + \
                0.25 * alpha**2.0 * \
                (self.myFluid.q - 1.0) * (5.0 * self.myFluid.q + 1.0) * \
                # errur corriger c bien 5 q + 1
                (A * (1.0 -2.0 * alpha * self.myFluid.q + alpha**2.0)**0.5 - \
                (np.sqrt(r_star) - alpha * self.myFluid.q) *\
                (r_star - 2.0 * alpha * self.myFluid.q * np.sqrt(r_star) +\
                alpha**2.)**.5) - 0.25 * alpha**4. * (self.myFluid.q - 1.0)**2.\
                * (self.myFluid.q + 1.0) * (5.0 * self.myFluid.q + 1.0) * \
                np.log(((1. - 2. * alpha * self.myFluid.q + alpha**2.)**.5 +A)/\
                ((r_star - 2.0 * alpha * self.myFluid.q * np.sqrt(r_star) + \
                alpha**2.0)**0.5 + np.sqrt(r_star) - alpha * self.myFluid.q)))
        return V


    def cassonVelocityField(self, R, Umax, r=None, plot=False):
        """ The velocity field is :

        .. math::
            u_z =  U_{max}  \\left( 1-{r^*}^2 - 
                    \\frac{8}{3} \\sqrt{r_p}\\frac{R^{3/2} - r^{3/2}}{R^2} + 
                    2 r_p \\frac{R -r}{R^2}\\right) \\forall r \\in [r_p, R] \\\\
            u_z = U_p \\in [0, r_p] \\, , \\text{with } U_p = U_{max}  \\left(
                    1 + \\frac{2 r_p}{R} -
                    \\frac{8}{3} \\frac{\\sqrt{r_p R^3}}{R^2} -
                    \frac{r_p^2}{3 R^2}\\right)

        Parameters
        ----------
        R : float
            Radius
        Umax : float
            Maximal velocity
        r : np.array of shape (Nr,)
            radius coordinates, optional
        plot : bool
            Plot or not plot that is the boolean

        Returns
        -------
        V : np.ndarray of shape (Nr,) 
            velocity field

        """
        if r is None: r = np.linspace(0, R, 301)
        elif not np.isclose(R,r[-1]): raise ValueError('R!=r[-1]')
        r_star = r / R
        rp = (2.0 * self.myFluid.tau0) / self.G
        # velocity in plug region
        Vc = self.G / (4.0 * self.myFluid.muInf) *\
                ((R**0.5 - rp**0.5)**3.0) * (R**0.5 + (1.0/3.0) * rp**0.5)
        # init velo field as cst value (in the plug zone)
        V = np.ones(r.shape) * Vc
        print('plug region start at rp/R = %s' % (rp/R))
        yieldG = self.G * R * 0.5
        # test if G is sufficient for moving the fluid
        if yieldG > self.myFluid.tau0:
            idx = np.argwhere(r > rp)[:,0]
            V[idx] = self.Umax * ((1 - (r[idx] / R)**2.0) - \
                    (8.0 / 3.0) * rp**0.5 *
                        (R**(3.0/2.0) - r[idx]**(3.0/2.0)) / R**2.0\
                    +  2.0 * rp * (R - r[idx]) / R**2.0)
        else:
            raise ValueError("no flow, G is to small")
        return V


    def from_uC_To_Qn(self, uCn):
        """ Compute the coefficient of the Fourier transform of the flow rate
        knowing the center velocity coef

        .. math::
            u(r=0,t) = \sum_{-N/2}^{N/2} uC_n e^{i n \omega t}

        Parameters
        ----------
        uCn : np.array(complex64) of shape (Nmode,)
            Coefficient of Fourier tranform for the center velocity

        Returns
        ----------
        Qn : np.array(complex64) of shape (Nmode,)
            Coefficient of Fourier tranform for the flow rate

        """
        Qn = np.zeros(uCn.shape, dtype=np.complex64)
        print('uCn[0]', uCn[0])
        Qn[0] = uCn[0] * np.pi * self.R**2 *0.5
        beta = 1j**1.5 * self.Wo
        for n in range(1,len(uCn)):
            beta_n = beta * float(n)**0.5
            Qn[n] = np.pi * self.R**2 * uCn[n] * \
                    (jn(0, beta_n) - (2./beta_n)*jn(1, beta_n)) / \
                    (jn(0, beta_n) - 1.)
        return Qn


    def modeCalcul(self, t_dat, signal_dat, f_limit=2e-3):
        """ From signal with (possibly) none equi distant time points,
        create by spline interpolation, to have equi-distant points.
        Then apply fft + filter the
        mode which have relative magnitude less than f_limit. Return
        the total coefficient and the more important ones

        Parameters
        ----------
        t_dat : np.ndarray of shape (Ndat,)
            Time values
        f_dat : np.ndarray of shape (Ndat,)
            Signal values which as to be periodic
        N : int
            Number of interpolating pts
        f_limit : float
            Limit ratio to keep mode in Fourier serie

        Returns
        -------
        yf : np.array of shape (N_harmo+1,)
            Fourier coefficient for n>=0
        yf_filt : np.array of shape (N_harmo+1,)
            Fourier coefficient for n>=0, when not important mode has been set to 0

        """
        print('Compute  Fourier decomposition')
        N = len(t_dat)
        # get a power of 2 numbr of pts for interpolation
        if np.allclose(np.diff(t_dat) - np.diff(t_dat)[0], 0.0):
            print('Data are equally spaced')
            # do not change data
            N_fft = N
            te = np.diff(t_dat)[0]
            t_int = t_dat
            s_int = signal_dat
        else:
            print('Data are no equally spaced')
            N_fft = 2**(int(np.log(N) / np.log(2))+1)
            # get the next power of 2 nbr fo point
            # the sampling step
            te = t_dat[-1] / (N_fft)
            # ne va pas jusqu'à T
            t_int = np.arange(start=0.0, stop=t_dat[-1], step=te)
            s_int = interp1d(t_dat, signal_dat, kind='cubic')(t_int)

        freq_ = rfftfreq(N_fft, te)
        # fft do not divid by N_fft, so we did
        yf = rfft(s_int) / N_fft
        yf_adim = np.abs(yf) / np.max(np.abs(yf))

        # filter according to magnitude of coef
        yf_filt = yf.copy()
        yf_filt[yf_adim < f_limit] = 0.0

        nbrHarmo = np.argwhere(yf_adim >= f_limit)[-1,0]

        yf_ofInterest = yf[:nbrHarmo + 1]
        print('nbr of harmonic important', nbrHarmo)

        return nbrHarmo, yf_ofInterest


    def modeCalcul_old(self, t_dat, dat):
        """ Very old implementation of computation of fourier serie coefficient
        See self.modeCalcul()

        computation of the Fourrier serie decomposition of the cardia flow rate
        """
        an = np.zeros(self.nbrHarmo, dtype=np.float64)
        bn = np.zeros(self.nbrHarmo, dtype=np.float64)
        #mean flow rate
        a0 = self.fr * np.trapz(dat, t_dat)

        for k in range(self.nbrHarmo):
            an[k] = 2.0 * self.fr * np.trapz(dat *
                                np.cos(t_dat * float(k + 1) \
                                        * self.omega0), t_dat)
            bn[k] = 2.0 * self.fr * np.trapz(dat *
                                np.sin(t_dat * float(k + 1) \
                                        * self.omega0), t_dat)
        """
        # compute check is an bn are ok
        N = 100
        self.t_recon = np.linspace(0, self.T, N)
        s = np.zeros(N, dtype=np.float64)
        s = a0
        for k in range(self.nbrHarmo):
            s += + an[k] * np.cos(self.t_recon * self.omega0 * (k+1)) + \
                    bn[k] * np.sin(self.t_recon * self.omega0 * (k + 1))
        """
        signal_0 = a0
        signal_mn = np.zeros(self.nbrHarmo, dtype=np.complex64)
        signal_pn = np.zeros(self.nbrHarmo, dtype=np.complex64)

        signal_mn = 0.5 * (an + 1j * bn)
        signal_pn = 0.5 * (an - 1j * bn)
        # compute check
        """
        debitComplex = np.zeros(N, dtype=np.complex64) + a0
        for i in range(self.nbrHarmo):
            debitComplex+= signal_mn[i] * \
                    np.exp(1j * self.omega0 * self.t_recon * -(i+1)) + \
                    signal_pn[i] * np.exp(1j * self.omega0* self.t_recon * (i+1))
        self.Q_recon = np.real(debitComplex)
        """
        # plt.figure()
        # plt.plot(self.t_recon, s, 's', markersize=10, label='Fourier decomp. in R')
        # plt.plot(t_dat, dat, 'o', label="Q data")
        # plt.plot(self.t_recon, self.Q_recon,'o',
                # label='Q reconstructed with  %s harmonic' % self.nbrHarmo)
        # plt.legend()
        # plt.xlabel('t (s)')
        # plt.ylabel('flow rate $(m3/s)$')
        # plt.savefig('debit.png')
        # plt.close()
        # define only 1 array to store mode serie
        # from coef n<0 then n=0 and finally n>0

        print('Fourier decomposition coefficient are store as:')
        print('\t-first value is fondamental\n \t-others are harmonic for n>0.')
        print('Note that we do not store coef for n<0 as C_{-n} = conj(C_n)')
        self.nbrCoef = self.nbrHarmo + 1

        signal_n = np.zeros(self.nbrCoef, dtype=np.complex64)
        signal_n[0] = a0
        signal_n[1:] = signal_pn
        return signal_n
        # from scipy.fft import fft, ifft
        # Qfft = ifft(fft(self.Q_dat))
        # print('err = ', self.Q_dat - Qfft)

    def fromFlowRateToGradP(self):
        """
        According to womersley solution, we compute the Fourier mode of
        the opposite pressure gradient, G, knowing the flow rate modes coef as:

        .. math::
            G_0 = \\frac{Q_0 8 \\nu}{\pi R^4} \\forall n \in \mathbb{Z}^*,
                G_n = \\frac{Q_n i \mu \\alpha^2 n}{(1-F_n)i \pi R^4}

        With

        .. math::
            F_{n}=\\frac{2 J_1(\\beta_n)}{\\beta_n J_0(\\beta_n)}

        Parameters
        ----------

        Returns
        -------

        Attributes
        ----------
        Gn : np;ndarray of shape (Nmode,)
            The Fourier serie of the opposite pressure gradient

        """
        # modes for grad pressure
        self.Gn = np.zeros(self.nbrCoef, dtype=np.complex64)

        self.G = self.Qv * 8.0 *self.myFluid.mu / (np.pi * self.R**4.0)
        # print('**********************************')
        # print('modification of first serie mode C0')
        # ratioC = self.Re * (self.mu * self.nu) / \
                # (self.R**3.0 * 0.25) / np.real(C0)
        # self.C0 = C0 * ratioC
        # print('Re target = %s || Re from C0 = %s' % (self.Re, \
                # 0.25 * np.real(self.C0) * self.R**3.0 /(self.mu * self.nu)))
        # print('**********************************')

        self.Gn[0] = self.G
        beta = 1j**1.5 * self.Wo
        for n in range(1,len(self.Gn)):
            beta_n = beta * float(n)**0.5
            F10n = (2.0 * jn(1,beta_n)) / (beta_n * jn(0,beta_n))
            self.Gn[n] = 1j * self.myFluid.mu * self.Wo**2 * float(n)  * \
                            self.Qn[n] / (np.pi * self.R**4 * (1.0 - F10n))

        self.Gn_realPart_forOF = '('
        self.Gn_imPart_forOF = '('
        f = open("data_Pn_%sharmo.dat" % self.nbrCoef, "w")
        for i in range(self.nbrCoef):
            self.Gn_realPart_forOF += '{:.6e}  '.format(np.real(self.Gn[i]))
            self.Gn_imPart_forOF += '{:.6e}  '.format(np.imag(self.Gn[i]))
            f.write('{:.6e}'.format(np.real(self.Gn[i])) +
                    '  {:.6e}j'.format(np.imag(self.Gn[i]))+'\n')
        f.close()
        self.Gn_realPart_forOF += ')'
        self.Gn_imPart_forOF += ')'
        print("------------\nrealPartSerie %s" % self.Gn_realPart_forOF)
        print("imPartSerie  %s" % self.Gn_imPart_forOF)
        print("------------")
        return


    def womersleyWSS(self, times=None, plot=False):
        """
        
        Paramters
        ---------
        times : ndarray of shape (Ntime,)
            array of times where WSS in Womersley Solution is computed
        plot : bool
            Do you want plot of velocity profil along cardiac cycle ?

        Attributes
        ----------
        OSI : float
            Oscillatory shear index 
        RRT : float
            Relative Residence Time 
        Returns
        -------
        WSS : np.ndarray of shape (Ntimes,)
        """
        print('-----------\ncompute WSS\n-----------')
        if times is None:
            times = np.linspace(0, 1, 1001) * self.T
        WSS = np.zeros(len(times))
        for s in range(len(times)):
            WSS[s] = womersleyWSS_(times[s], self.Gn, self.R,
                    self.Wo, self.omega0)

        if plot:
            print('plot')
            # plt.figure()
            # plt.plot(times, WSS, label='WSS(t)')
            # plt.legend()
            # plt.xlabel('t (s)')
            # plt.ylabel('WSS (Pa)')
            # plt.savefig('tau_p.png')
            # plt.close()

        wssMag = np.abs(WSS)
        wssMagMean = self.T**-1 * np.trapz(wssMag, times)
        moy_WSS = self.T**-1 * np.trapz(WSS, times)
        # analytical wssMeanMag is poiseuille WSS self.WSS
        wssMeanMag = np.abs(self.WSS)

        self.OSI = 0.5 *(1.0 - wssMeanMag / wssMagMean)
        self.RRT = 1.0 / ((1.-2. * self.OSI) * wssMagMean)
        print('wssMeanMag', wssMeanMag)
        print('wssMagMean', wssMagMean)
        print('OSI', self.OSI, '(-)\tRRT', self.RRT, '(Pa)')
        print('WSS pois = MEAN(WSS_womer) =', moy_WSS)
        if not np.isclose(moy_WSS, self.WSS):
            if times is None:
                raise ValueError("Womersley WSS averaged in in period not " +
                        "equal to Poiseuille WSS")
            else:
                warn("average of WSS calculated {} not equal ".format(moy_WSS)+
                        "to WSS_Poi %s.".format(self.WSS) +
                 "Possible under sampling of times given")

        return WSS

    def womersleyVelocityField(self, times=None, r=None, plot=False):
        """With the Fourier decomposition of the pressure gradient :

        .. math::
            \\frac{\partial p}{\partial z} = \Re{\left( \sum \limits_{-N}^N
            P_n e^{i\omega_n t} \\right)}

        The velocity field is :
        .. math::
            u_z=  \\frac{R^2}{4 \mu} P_0 \left( 1-{r^*}^2 \\right) +
                \Re{\left(\sum \limits_{n \in \mathbb{Z}^*} \\frac{i P_n}{\\rho
                \omega_n} \left( 1-\\frac{J_0(\beta_n Wo r^*)}{J_0(\\beta_n)}
                \\right) e^{i \omega_n t} \\right)} \;.
        Avec
        .. math::
            \beta_n = i^{3/2}\sqrt{n} Wo

        Parameters
        ----------
        times : np.array
            discretised time between 0 and T
        r : np.array
            radius coordinates
        plot : bool
            Plot or not plot that is the boolean

        Returns
        -------
        V : np.array of shape (Nt, Nr)
            velocity field at any time and position

        """
        if r is None:
            r = np.linspace(0, self.R, 301)
        r_star = r / self.R
        if times is None:
            times = np.linspace(0,1,21) * self.T
        V = np.zeros((len(times), len(r)))
        Q = np.zeros(len(times))
        # definition of poiseuille variable as basic flow for womersley is Poiseuille
        for s in range(len(times)):
            v = womersleyVelocity_(times[s], r_star, self.Gn, self.R,
                    self.myFluid.mu, self.omega0, self.Wo)
            V[s,:] = v

            Q[s] = np.trapz(r * V[s,:], r) * 2.0 * np.pi

        if plot:
            print('plot')
            plt.figure()
            for s in range(len(times)):
                plt.plot(r_star, V[s,:], label=r"$t = %.1fT$ " % (times[s]/self.T))
                plt.legend(bbox_to_anchor=(1.02, 0.4), loc="center left", borderaxespad=0.)
            plt.xlabel(r'$r/R$', fontsize=16)
            plt.ylabel(r'$u_z$', fontsize=16)
            plt.savefig('womersleyprofils.png', bbox_inches="tight")
            plt.close()

            plt.figure()
            plt.plot(times, Q, 's', label='Q(t) from velocity fields')
            plt.plot(self.t_dat, self.Q_recon, label='Q data')
            plt.legend(bbox_to_anchor=(1.02, 0.4), loc="center left", borderaxespad=0.)
            plt.savefig('womersleyFlowRate.png', bbox_inches="tight")
            plt.close()
            # np.save('velocityWomersley.npy', V)
            plt.close()

        return V

    def update_WSS(self, ePlus):
        """hemodynamical update from gene i to gene i+1 using blood flow
        conservation under Poiseuille flow hypothesis:

        .. math::
            Q_{i+1} = Q_{i}

        """
        # update self.R ?
        print('--------------------------')
        print('Updating WSS')
        print('Previous geo params')
        print('R = %s ' % (self.R))
        print('Previous hemo params')
        print('Re = %s WSS = %s' % (self.Re, self.WSS))
        self.R = self.myArtery.Rext - ePlus
        if self.planar:
            self.S = 2 * self.R  # cross section
            self.Vl = self.myArtery.L * self.S
        else:
            self.S = np.pi * self.R**2  # cross section
            self.Vl = self.myArtery.L * self.S

        self.Ud = self.Qv / self.S
        self.G, self.Umax, self.WSS =  fromQvToG(self.Qv, self.R,
                    self.planar, self.myFluid.model, self.myFluid.argsF)

        self.Re = self.Ud * (2 * self.R) * self.myFluid.rho / self.myFluid.muN

        self.S_list.append(self.S)
        self.Vl_list.append(self.Vl)

        self.Re_list.append(self.Re)
        self.WSS_list.append(self.WSS)
        print('New geo params')
        print('R = %s ' % (self.R))
        print('New hemo params')
        print('Re = %s WSS = %s' % (self.Re, self.WSS))
        print('--------------------------')
        return

