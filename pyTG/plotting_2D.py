#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from icecream import ic
import sys
import os
import pickle
import numpy.ma as ma
import numpy as np
import glob
from configparser import ConfigParser
from warnings import warn
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.image import NonUniformImage
import matplotlib.colors as colors
plt.rc('font', family='serif', size='20')
plt.rcParams["figure.figsize"] = (20,16)
from mpl_toolkits.mplot3d import Axes3D

from pyTG.utilities import find_last_gene, findIndex_t0tf, message, pp2D, \
        load_obj, pp2D, save_gnuplot
from pyTG.cfd_OpenFoam import checkStrInFile
# shading parameter for pcolormesh 2D post processing
shading_2D = "gouraud" # "flat" # "gouraud"

dictCMAP = {"jet": cm.jet,
            "grey": cm.Greys,
            "viridis": cm.viridis}

# https://matplotlib.org/stable/gallery/images_contours_and_fields/interpolation_methods.html
# antialiazing effect change pcolormesh to NonUniformImage (imshow but with non uniform data)

def plottingPop_2D(iniF, path_abs, set_params, t0=None, tf=None,
        setting_t=('day',1.0), folder='', r1=None, r2=None, points=[], instants=[],
        pcm=True, noAxis=False, colormap='jet', dpi=100, scalingFile=''):
    """plot Pop in 2D cas

    Parameters
    ----------
    path_abs : str
        The path
    set_params : dict
        A big dictionnary with lots of data
    t0 : float
        Initial time
    tf : float
        Final time
    setting_t : tuple

    folder : str

    r1 : float
        First position normalized by init radius
    r2 : float
        Last position normalized by init radius
    points : list
        list of specifics points where data will be plotted as function of time
    instants : list
        list of instants where user want to return and plot data
    pcm : bool
        Plot 2D contour with the ``pcolormesh`` routine + gouraud shading.
        If pcm=False, plot with the ``NonUniformImage`` routine + bilinear
        interpolation.
    noAxis : bool
        Save for all graphs, a copy without axis and with better resolution
    scalingFile : str
        The function try to read a scaling file, named as ``scalingFile``, which
        contain vmin, vmax values for each variables plotted. If scalingFile not
        given, the function collect (vmin, vmax) and save it as scalingPop.dat.
    colormap : str
        The type of colormap, avail : ``jet``, ``grey``, ``viridis`` 


    Returns
    -------

    """
    myCMAP = dictCMAP[colormap]

    message('Pop 2D plotting')
    if scalingFile != '':
        try:
            scalingPop = np.loadtxt('{}/{}'.format(path_abs, scalingFile))
        except OSError:
            print("{} not found in {}".format(scalingFile, path_abs))
            scaling = False
        else:
            print("Successfully loaded {}".format(scalingFile))
            scaling = True
            [Qi_nd_min, Qi_nd_max, Qm_nd_min, Qm_nd_max,
             Si_nd_min, Si_nd_max, Sm_nd_min, Sm_nd_max, Cji_nd_min, Cji_nd_max,
             Cjm_nd_min, Cjm_nd_max, Cvi_nd_min, Cvi_nd_max, Cvm_nd_min, Cvm_nd_max,
             E_nd_min, E_nd_max,
             ddteTot_min, ddteTot_max, eTot_min, eTot_max,
             e_i_min, e_i_max, e_m_min, e_m_max,
             R_gr_min, R_gr_max] = scalingPop
            scal_l = [[Qi_nd_min, Qi_nd_max], [Qm_nd_min, Qm_nd_max],
                    [Si_nd_min, Si_nd_max], [Sm_nd_min, Sm_nd_max],
                    [Cji_nd_min, Cji_nd_max], [Cjm_nd_min, Cjm_nd_max],
                    [Cvi_nd_min, Cvi_nd_max], [Cvm_nd_min, Cvm_nd_max],
                    [E_nd_min, E_nd_max], [ddteTot_min, ddteTot_max],
                    [eTot_min, eTot_max], [e_i_min, e_i_max],
                    [e_m_min, e_m_max], [R_gr_min, R_gr_max]
                    ]
    else:
        # if not scaling creation of it
        scalingPop = []
        scalingPop_com = []


    # write bash file to read log file
    scriptBash = """#!/bin/bash
# collect from the main log file of the TG simulation, the information about
# remodeling event and location
# 1er arg is logfile
logFile=$1
# 2eme arg name output data file
resFile=$2
echo "resFile = $resFile logFile = $logFile "
if [ -f "$resFile" ]; then
\t echo "rm the previous res file"
\t rm -f $resFile
fi
echo "# g(-) t(d) x/R0(-) idxLoc" >> $resFile
echo "awk running"
awk -f eventsLoc.awk $logFile >> $resFile
    """
    sciptAwk = """BEGIN { tEv=0
    \t\tloc=0
    \t\tidxloc=0
    \t\tg=0}{
    \t\tif (NF > 1)
    \t\t{
    \t\t\t\tif ($1 == "at" && $2 = "t")
    \t\t\t\t\t{
    \t\t\t\t\ttEv=$4
    \t\t\t\t\tidxloc=$7
    \t\t\t\t\tloc=$10
    \t\t\t\t\tprint g, tEv, loc, idxloc
    \t\t\t\t\tg=g+1
    \t\t\t\t\t}
    \t\t}
    }
    """
    scrGplt = """set terminal postscript eps enhanced color font 'Helvetica,10' lw 2
    set terminal postscript eps enhanced color font 'Helvetica,10' lw 2
    set print
    file = "toto"
    set output 'eventsLoc_time.eps'
    set xlabel "t (d)"
    set ylabel "x/R0 (-)" tc lt 1
    set y2tics
    set ytics nomirror
    set y2label "R/R0" tc lt 2
    plot file u ($2):($3) t "",\
            file u ($2):($4) t ""

    set output 'eventsLoc_gene.eps'
    set xlabel "generation"
    set ylabel "x/R0 (-)" tc lt 1
    set y2tics
    set ytics nomirror
    set y2label "R/R0" tc lt 2
    plot file u ($1):($3) t "",\
            file u ($1):($4) t ""
    """
    with  open('%s/getEventInfo.sh' % path_abs, 'w') as myfile:
        myfile.write(scriptBash)
    with  open('%s/eventsLoc.awk' % path_abs, 'w') as myfile:
        myfile.write(sciptAwk)
    with  open('%s/plotEventLoc.gplt' % path_abs, 'w') as myfile:
        myfile.write(scrGplt)

    listLogFile = glob.glob('%s/log.jansen*' % path_abs)
    print('listLogFile', listLogFile)
    if len(listLogFile) > 1: warn('size too big')
    if len(listLogFile) == 1:
        myLogFile = listLogFile[0]
        os.system('bash {}/getEventInfo.sh {} {}'.format(path_abs,
            myLogFile, path_abs + '/resEventsLoc.dat'))
        # print('{}/resEventsLoc.dat'.format(path_abs))
        # resEventsLoc = np.loadtxt('{}/resEventsLoc.dat'.format(path_abs))
        # try:
            # idxLoc = resEventsLoc[:,-1].astype(int)
        # except IndexError:
            # print('resEventsLoc empty')
            # idxLoc = []


    sPath = '%s/%s/Pop' % (path_abs, folder)
    try:
        os.mkdir(sPath)
    except OSError:
        print ("Creation of the directory \n %s \nfailed" % sPath )
    else:
        print ("Successfully created the directory \n %s " % sPath)
    print('t in [%s, %s]' % (t0, tf))
    xlab, normTime = setting_t
    paths = glob.glob('gene*')
    path_last_gene = find_last_gene(paths)

    path_data = \
        '%s/%s/tissueGrowth/' % (path_abs, path_last_gene)

    t_interp = np.load('%s/t_interp.npy' % path_data)
    # print('t_interp', t_interp)
    (Rext, R_LEE, R0, L) = set_params['geom_dim']

    # load mesh data
    xE = np.load('%s/xE_TG.npy' % path_data)
    xP = np.load('%s/xP_TG.npy' % path_data)

    xE_star = xE / R0

    try:
        # a restart error forget the next event in t_gene,  so load all t_gene
        # in :
        resEventsLoc = np.loadtxt('{}/resEventsLoc.dat'.format(path_abs))
        with open('{}/resEventsLoc.dat'.format(path_abs)) as f:
            f1_resEv = f.readline().replace('\n','')
        t_events = resEventsLoc[:,1]
        idxLoc = resEventsLoc[:,-1].astype(int)
    except:
        print("'resEventsLoc.dat' not found")
        resEventsLoc, f1_resEv = None, None
        t_events = np.load('%s/t_gene.npy' % path_data)[1:-1]
        idxLoc = []

    Ngene = len(t_events)
    # locate index where event occurs
    idxEvents = []
    # for ti in t_events[1:-1]:
    for ti in t_events:
        idx_i = np.argwhere(np.isclose(t_interp, ti))[:,0]
        if idx_i.size > 1:
            raise ValueError('More than 1 time event found')
        idxEvents.append(idx_i[0])
    # load sol_list_geneX.pkl
    # l_sol_list = glob.glob("{}/sol_list*".format(path_abs))
    # l_cp_list = glob.glob("{}/coupling_gene*".format(path_abs))
    # if len(l_sol_list) > 1 or len(l_cp_list) > 1:
        # raise ValueError('More than 1 sol_list or cp found '+
                # 'l_sol ={} l_cp {}'.format(
            # l_sol_list, l_cp_list))
    # # print('l_sol_list', l_sol_list, 'l_cp_list', l_cp_list)
    # sol_list = load_obj(path_abs, l_sol_list[0].split('/')[-1])
    # cp = load_obj(path_abs, l_cp_list[0].split('/')[-1])
    l_sol_list = glob.glob('{}/sol_list*'.format(path_abs))
    l_sol_g = [
        int(
      li.replace(path_abs+'/','').replace('sol_list_gene_','').replace('.pkl','')
            )
            for li in l_sol_list]
    sol_list_load = "sol_list_gene_{}.pkl".format(np.max(l_sol_g))
    coupling_gene_load = "coupling_gene_{}.pkl".format(np.max(l_sol_g))
    sol_list = load_obj(path_abs, sol_list_load)
    cp = load_obj(path_abs, coupling_gene_load)
    T_d = set_params['T_d']
    # definition of the type of events detected at each remodeling CFD time
    # 0 :  Narrowing
    # 1 :  Enlargment
    # 2 :  Equilibrium
    # 3 :  Occulsion
    # 
    if isinstance(resEventsLoc, np.ndarray):
        # fill the array with '-1'
        typeEvents = np.ones(Ngene, dtype=int) * -1
        for i in range(Ngene):
            # loop over all type of events tested
            for j, t_ev_arr_j in enumerate(sol_list[idxLoc[i]].t_events):
                # t_event[i] is in an array of an event ?
                any_close = np.any(np.isclose(t_ev_arr_j, t_events[i] / T_d))
                if any_close:
                    typeEvents[i] = j
        np.savetxt('{}/typeEvents.dat'.format(path_abs), typeEvents,
                header="typeEvents", fmt='%d')
        os.system("paste resEventsLoc.dat typeEvents.dat > resEventsLoc_extend.dat")
    # np.savetxt('{}/resEventsLoc_extend.dat'.format(path_abs),
            # resEventsLoc_extend, header=f1_resEv, fmt='%1.4E')
    # load R_gr in meter
    R_gr = np.load('{}/var_1D.npy'.format(path_data))[-1,:,:]
    if len(listLogFile) == 1:
        if len(idxLoc) == len(t_events):
            R_gr_events = R_gr[idxEvents, idxLoc]
            np.savetxt('%s/R_gr_events.dat' % path_abs,
                np.transpose([t_events, R_gr_events]), header='t(d) R_gr(m)',
                fmt='%1.4E')
            os.system(
            "paste resEventsLoc.dat R_gr_events.dat > dataInfoEvents.dat"
                    )
            os.system(
            "paste resEventsLoc_extend.dat R_gr_events.dat > dataInfoEvents_extend.dat"
                    )
        else:
            R_gr_events = R_gr[idxEvents,:]
            # shape of event and loc get from log.jansen
            # so just save R_gr_events as a matrix
            np.save('%s/R_gr_events.npy' % path_abs, R_gr_events)
    np.savetxt("{}/R_nd_tf.dat".format(sPath),
            np.transpose([xE_star, R_gr[-1,:] / R0]),
            # header='xE_start R_nd(t={})'.format(TTT))
            header='xE_start R_nd(tf)')

    # specific case of restriction of t events 
    (i_t0, i_tf, ie_t0, ie_tf) = findIndex_t0tf(t_interp, t_events, t0, tf)
    if t0==None and tf==None:
        print('default values for t0 tf')
        t0 = t_interp[0]; tf = t_interp[-1]
    else:
        i_tf = i_tf + 1
        notGood = t0 < t_interp[0] or tf > t_interp[-1]
        if notGood: raise ValueError('t0 tf wrong')
    t_interp = t_interp[i_t0:i_tf] / normTime
    print('t0 {} tf {} and t_interp in [{}, {}]'.format(
        t0, tf, t_interp[0], t_interp[-1]))
    t_events = t_events[ie_t0:ie_tf] / normTime


    # check if r1,r2 are well defined
    default = r1==None and r2==None
    if default:
        # if None for the both, then default values, we plot for all along the
        # artery !
        print('default values for r1 r2')
        r1 = 0.0
        r2 = xE_star[-1]
    else:
        notGood = r1 < xE_star[0] or r2 > xE_star[-1]
        if notGood:
            raise ValueError('r1 < xE_star[0] or r2 > xE_star[-1]')


    Y_interp_nd = np.load('%s/Y_interp_nd.npy' % path_data)[:,i_t0:i_tf,:]
    var_1D = np.load('%s/var_1D.npy' % path_data)[:,i_t0:i_tf,:]
    # load GFs
    if set_params['model'][:6] == 'Jansen':
        T, X = np.meshgrid(t_interp, xE_star)
        NGFs = len(set_params['GFs_nd'])
        species_nd = Y_interp_nd[NGFs:]

        scal = np.array([1e6, 1e6, 1e6, 1e3])
        var_1D_scaled = var_1D * scal[:,np.newaxis, np.newaxis]
        eTot = var_1D_scaled[0, :, :] # in um
        ddteTot = np.gradient(eTot, t_interp, axis=0, edge_order=2)

        PopDatas = np.concatenate((species_nd, ddteTot[np.newaxis,:,:],
            var_1D_scaled), axis=0)
        PopNames = list(set_params['species_nd']) + ['ddeTot_umPerDays',
                'eTot_um', 'e_i_um', 'e_m_um', 'R_gr_mm']
        N_pop = len(PopNames)

        R_gr = var_1D[-1, :, :]
        (time_idx, position_idx) = np.unravel_index(np.argmin(R_gr), R_gr.shape)
        # print('time_idx, position_idx', time_idx, position_idx)
        print('-----------------------------------------------------')
        print('R_gr minimal at t_d = {} and x/R0 = {}, R_gr_min = {} verif {}'.format(
            t_interp[time_idx], xE_star[position_idx],
            R_gr[time_idx,position_idx], R_gr.min()))
        print('-----------------------------------------------------')

        # loooop
        for k in range(N_pop):
            fig, ax = plt.subplots()
            levels = 80
            # cs = ax.contourf(T,X,Y_interp_nd[j, :, :].T,levels,cmap=myCMAP)
            # print('k', k)
            # print('scalingFile', scalingFile, scalingFile != '')
            if scalingFile != '':
                vmin = scal_l[k][0]
                vmax = scal_l[k][1]
            else:
                vmin = PopDatas[k, :, :].min()
                vmax = PopDatas[k, :, :].max()
                scalingPop += [vmin, vmax]
                scalingPop_com += [ PopNames[k] + "_min ",
                                    PopNames[k]  + "_max "]
            # print('PopNames[k]', PopNames[k])
            # print('vmin= {}, max = {}'.format(vmin, vmax))

            if pcm:
                cs = ax.pcolormesh(T, X, PopDatas[k, :, :].T,
                    shading=shading_2D, vmin=vmin, vmax=vmax, cmap=myCMAP)
            else:
                im = NonUniformImage(ax, interpolation='bilinear',
                        extent=[T.min(),T.max(),X.min(),X.max()], cmap=myCMAP)
                im.set_data(t_interp, xE_star, PopDatas[k, :, :].T)
                im.set_clim(vmin=vmin, vmax=vmax)
                cs = ax.add_image(im)
            ax.set_xlabel('%s' % xlab)
            ax.set_ylabel('x/R0')
            ax.set_title('{}, vmin={:.4}, vmax={:.4}'.format(
                PopNames[k], vmin, vmax))
            ax.set_ylim(r1,r2)
            ax.set_xlim(t0, tf)
            cb = plt.colorbar(cs)
            plt.savefig('%s/%s' % (sPath, PopNames[k]))
            if noAxis:
                ax.set_title('')
                plt.axis('off')
                cb.remove()
                plt.savefig('%s/%s_noAxis.png' % (sPath, PopNames[k]),
                        bbox_inches='tight', pad_inches=0, dpi=dpi)
            plt.close()


        if scalingFile == '':
            np.savetxt("{}/scalingPop.dat".format(sPath),
                    np.array(scalingPop).reshape(1,len(scalingPop)),
                    header=' '.join(scalingPop_com), fmt='%1.4E')

        if points:
            sPath_spe = '%s/specificPoints' % (sPath)
            try:
                os.mkdir(sPath_spe)
            except OSError:
                print ("Creation of the directory \n %s \nfailed" % sPath_spe )
            else:
                print ("Successfully created the directory \n %s " % sPath_spe)
            # specific points plot
            idxs = []
            print('specific points plot are at :')
            for k in range(len(points)):
                idxs.append(np.abs(xE_star - points[k]).argmin())
                print('points[k]', points[k],
                      'correspond to xE_stat:', xE_star[idxs[k]])
            np.savetxt('%s/specificPoints.dat' % (sPath_spe), xE_star[idxs],
                    header='positions x/R0')

            # print('PopNmes', PopNames, PopDatas.shape)
            for k in range(N_pop):
                # print('k', k, 'PopNames[k] = ', PopNames[k])
                plt.figure()
                for l in range(len(points)):
                    plt.plot(t_interp, PopDatas[k, :, idxs[l]],'o',
                                        label=r'$x/R_0=%.4f$' % xE_star[idxs[l]])
                plt.xlabel('{}'.format(xlab))
                # print('k', k, 'PopNames[k] = ', PopNames[k])
                plt.ylabel('{}'.format(PopNames[k]))
                plt.legend()
                # print('k', k, 'PopNames[k] = ', PopNames[k])
                plt.savefig('{}/{}_specificPoints'.format(
                    sPath_spe, PopNames[k]))
                plt.close()
                # print('PopNames[k] = ', PopNames[k])
                # print('{}/{}_specificPoints.dat'.format(sPath_spe, PopNames[k]))
                np.savetxt('{}/{}_specificPoints.dat'.format(
                    sPath_spe, PopNames[k]),
                    np.vstack((t_interp, PopDatas[k,:,idxs])).T,
                            header='t_d xE=%s' % xE_star[idxs])
        if instants:
            sPath_spe = '%s/specificInstants' % (sPath)
            try:
                os.mkdir(sPath_spe)
            except OSError:
                print ("Creation of the directory \n %s \nfailed" % sPath_spe )
            else:
                print ("Successfully created the directory \n %s " % sPath_spe)

            (Y_interp_nd_instants, tInstants, var_1D_instants,
              Vf_stack_instants, V_stack_instants) = pp2D(sol_list, set_params,
                      cp.myTG.myCFD.dxE_TG, cp.myTG.V_Oi, cp.myTG.V_Om,
                      np.asarray(instants) / T_d)

            (eTot_instants, e_i_instants,
                    e_m_instants, R_instants) = var_1D_instants

            NGFs = len(set_params['GFs_nd'])
            species_nd_instants = Y_interp_nd_instants[NGFs:]
            scal = np.array([1e6, 1e6, 1e6, 1e3])
            var_1D_instants_scaled = var_1D_instants * \
                    scal[:,np.newaxis, np.newaxis]
            PopDatas = np.concatenate((species_nd_instants,
                var_1D_instants_scaled), axis=0)
            PopNames = list(set_params['species_nd']) + \
                    ['eTot_um', 'e_i_um', 'e_m_um', 'R_gr_mm']
            N_pop = len(PopNames)
            for i in range(N_pop):
                plt.figure()
                plt.title("variable  = {}".format(PopNames[i]))
                for j in range(len(instants)):
                    plt.plot(cp.myTG.myCFD.xE_TG / R0, PopDatas[i, j, :],
                            label='t = {}'.format(instants[j]))
                plt.xlim(r1,r2)
                plt.xlabel('x')
                plt.ylabel('{}'.format(PopNames[i]))
                plt.legend()
                figname = '{}/{}_specificPoints'.format( sPath_spe, PopNames[i])
                plt.savefig(figname)
                plt.close()
                np.savetxt('{}/{}_specificInstants.dat'.format(sPath_spe,
                    PopNames[i]),
                    np.vstack((cp.myTG.myCFD.xE_TG / R0,  PopDatas[i, :, :])).T,
                    header ="xE_TG {} at t = {}".format(PopNames[i], instants))

    else:
        print(" not done for donadoni")

def plottingGFs_2D(iniF, path_abs, set_params, t0=None, tf=None,
        setting_t=('day',1.0), folder='', r1=None, r2=None, points=[], instants=[],
        pcm=True, noAxis=False, colormap='jet', dpi=100, scalingFile=''):
    """Plot GFs in 2D cas.

    Parameters
    ----------
    path_abs : str
        The path
    set_params : dict
        A big dictionnary with lots of data
    t0 : float
        Initial time
    tf : float
        Final time
    setting_t : tuple

    folder : str

    r1 : float
        First position normalized by init radius
    r2 : float
        Last position normalized by init radius
    points : list
        list of specifics points where data will be plotted as function of time
    instants : list
        list of instants where user want to return and plot data
    pcm : bool
        Plot 2D contour with the ``pcolormesh`` routine + gouraud shading.
        If pcm=False, plot with the ``NonUniformImage`` routine + bilinear
        interpolation.
    noAxis : bool
        Save for all graphs, a copy without axis and with better resolution
    scalingFile : str
        The function try to read a scaling file, named as ``scalingFile``, which
        contain vmin, vmax values for each variables plotted. If scalingFile not
        given, the function collect (vmin, vmax) and save it as scalingGFs.dat.
    colormap : str
        The type of colormap, avail : ``jet``, ``grey``, ``viridis`` 

    Returns
    -------

    """
    # config = ConfigParser(converters={'list': lambda x: [i.strip() for i in x.split(',')]})
    # # conserve upper or lower type of characters
    # config.optionxform = lambda option: option
    # r = config.read('%s' % iniF)
    # if not r:
        # raise ValueError('*********** wrong file *******************')
                    # # help='in 2D cases, point where specific attention is useful')

    myCMAP = dictCMAP[colormap]

    message('GF 2D plotting')
    if scalingFile != '':
        try:
            scalingGFs = np.loadtxt('{}/{}'.format(path_abs, scalingFile))
        except OSError:
            print("{} not found in {}".format(scalingFile, path_abs))
            scaling = False
        else:
            print("Successfully loaded {}".format(scalingFile))
            scaling = True
            [NOi_nd_min, NOi_nd_max, NOm_nd_min, NOm_nd_max,
             PDGFi_nd_min, PDGFi_nd_max, PDGFm_nd_min, PDGFm_nd_max,
             FGFi_nd_min, FGFi_nd_max, FGFm_nd_min, FGFm_nd_max,
             Agi_nd_min, Agi_nd_max, Agm_nd_min, Agm_nd_max,
             TGFi_nd_min, TGFi_nd_max, TGFm_nd_min, TGFm_nd_max,
             TNFi_nd_min, TNFi_nd_max, TNFm_nd_min, TNFm_nd_max,
             MMPi_nd_min, MMPi_nd_max, MMPm_nd_min, MMPm_nd_max] = scalingGFs
            scal_l = [[NOi_nd_min, NOi_nd_max], [NOm_nd_min, NOm_nd_max],
                      [PDGFi_nd_min, PDGFi_nd_max], [PDGFm_nd_min, PDGFm_nd_max],
                      [FGFi_nd_min, FGFi_nd_max], [FGFm_nd_min, FGFm_nd_max],
                      [Agi_nd_min, Agi_nd_max], [Agm_nd_min, Agm_nd_max],
                      [TGFi_nd_min, TGFi_nd_max], [TGFm_nd_min, TGFm_nd_max],
                      [TNFi_nd_min, TNFi_nd_max], [TNFm_nd_min, TNFm_nd_max],
                      [MMPi_nd_min, MMPi_nd_max], [MMPm_nd_min, MMPm_nd_max]]
    else:
        # if not scaling creation of it
        scalingGFs = []
        scalingGFs_com = []

    sPath = '{}/{}/GFs'.format(path_abs, folder)
    try:
        os.mkdir(sPath)
    except OSError:
        print("Creation of the directory \n {} \nfailed".format(sPath))
    else:
        print("Successfully created the directory \n {} ".format(sPath))
    xlab, normTime = setting_t
    paths = glob.glob('gene*')
    path_last_gene = find_last_gene(paths)

    path_data = '{}/{}/tissueGrowth/'.format(path_abs, path_last_gene)

    t_interp = np.load('%s/t_interp.npy' % path_data)
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    # load mesh data
    xE = np.load('%s/xE_TG.npy' % path_data)
    xP = np.load('%s/xP_TG.npy' % path_data)

    xE_star = xE / R0


    # check if r1,r2 are well defined
    default = r1==None and r2==None
    if default:
        # if None for the both, then default values, we plot for all along the
        # artery !
        print('default values for r1 r2')
        r1 = 0.0
        r2 = xE_star[-1]
    else:
        notGood = r1 < xE_star[0] or r2 > xE_star[-1]
        if notGood:
            raise ValueError('r1 < xE_star[0] or r2 > xE_star[-1]')

    try:
        # a restart error forget the next event in t_gene,  so load all t_gene
        # in :
        resEventsLoc = np.loadtxt('{}/resEventsLoc.dat'.format(path_abs))
        t_events = resEventsLoc[:,1]
    except:
        print("'resEventsLoc.dat' not found")
        t_events = np.load('%s/t_gene.npy' % path_data)[1:-1]

    Ngene = len(t_events)
    (i_t0, i_tf, ie_t0, ie_tf) = findIndex_t0tf(t_interp, t_events, t0, tf)
    if t0==None and tf==None:
        print('default values for t0 tf')
        t0 = t_interp[0]; tf = t_interp[-1]
    else:
        # get t_interp[-1] > tf for having  graph finishing by tf ! and not before
        i_tf = i_tf + 1
        notGood = t0 < t_interp[0] or tf > t_interp[-1]
        if notGood: raise ValueError('t0 tf wrong')
    t_interp = t_interp[i_t0:i_tf] / normTime
    print('t0 {} tf {} and t_interp in [{}, {}]'.format(
        t0, tf, t_interp[0], t_interp[-1]))
    t_events = t_events[ie_t0:ie_tf] / normTime

    Y_interp_nd = np.load('%s/Y_interp_nd.npy' % path_data)[:,i_t0:i_tf,:]
    # load GFs
    if set_params['model'][:6] == 'Jansen':
        T, X = np.meshgrid(t_interp, xE_star)
        NGFs = len(set_params['GFs_nd'])
        for j in range(NGFs):
            GFname = set_params['GFs_nd'][j]
            fig, ax = plt.subplots()
            levels = 80
            # cs = ax.contourf(T,X,Y_interp_nd[j, :, :].T,levels,cmap=myCMAP)

            if scalingFile != '':
                vmin = scal_l[j][0]
                vmax = scal_l[j][1]
            else:
                vmin = Y_interp_nd[j, :, :].min()
                vmax = Y_interp_nd[j, :, :].max()
                scalingGFs += [vmin, vmax]
                scalingGFs_com += [GFname+"_min ",
                                    GFname+"_max "]
            if pcm:
                cs = ax.pcolormesh(T, X, Y_interp_nd[j, :, :].T, shading=shading_2D,
                                    vmin = vmin, vmax = vmax, cmap=myCMAP)
            else:
                im = NonUniformImage(ax, interpolation='bilinear',
                        extent=[T.min(),T.max(),X.min(),X.max()], cmap=myCMAP)
                im.set_data(t_interp, xE_star, Y_interp_nd[j, :, :].T)
                im.set_clim(vmin=vmin, vmax=vmax)
                cs = ax.add_image(im)
                # cs = ax.imshow(Y_interp_nd[j, :, :].T, interpolation='bicubic',
                        # extent=[T.min(),T.max(),X.min(),X.max()],
                        # vmin=vmin, vmax=vmax, cmap=myCMAP)

            ax.set_xlabel('%s' % xlab)
            ax.set_ylabel('x/R0')
            ax.set_title('{}, vmin={:.4} vmax={:.4}'.format(GFname, vmin, vmax))
            ax.set_ylim(r1,r2)
            ax.set_xlim(t0, tf)
            cb = plt.colorbar(cs)
            plt.savefig('%s/%s' % (sPath, GFname))
            if noAxis:
                ax.set_title('')
                plt.axis('off')
                cb.remove()
                plt.savefig('%s/%s_noAxis.png' % (sPath, GFname),
                        bbox_inches='tight', pad_inches=0, dpi=dpi)
            plt.close()
            # write isocontour of delta = 1
            fig, ax = plt.subplots()
            CS = ax.contour(T, X, Y_interp_nd[j, :, :].T, levels=np.array([1.0]))
            ax.clabel(CS, inline=True, fontsize=10)
            fig.savefig('{}/{}_isocontour1'.format(sPath, GFname))
            # get data
            out = CS.allsegs
            if len(out) != 1: raise ValueError("only 1 contour should be in out")
            for j in range(len(out[0])):
                np.savetxt('{}/{}_isocontour_val=1_{}.dat'.format(
                    sPath, GFname, j), out[0][j])

        if scalingFile == '':
            np.savetxt("{}/scalingGFs.dat".format(sPath),
                    np.array(scalingGFs).reshape(1,len(scalingGFs)),
                    header=' '.join(scalingGFs_com), fmt='%1.4E')

        if points:
            sPath_spe = '%s/specificPoints' % (sPath)
            try:
                os.mkdir(sPath_spe)
            except OSError:
                print ("Creation of the directory \n %s \nfailed" % sPath_spe )
            else:
                print ("Successfully created the directory \n %s " % sPath_spe)
            # specific points plot
            idxs = []
            print('specific points plot are at :')
            for k in range(len(points)):
                idxs.append(np.abs(xE_star - points[k]).argmin())
                print('points[k]', points[k],
                      'correspond to xE_stat:', xE_star[idxs[k]])
            np.savetxt('%s/specificPoints.dat' % (sPath_spe), xE_star[idxs],
                    header='points %s' % xE_star[idxs])

            for j in range(NGFs):
                plt.figure()
                for k in range(len(points)):
                    plt.plot(t_interp, Y_interp_nd[j, :, idxs[k]],
                                        label=r'$x/R_0=%.4f$' % xE_star[idxs[k]])
                plt.xlabel('%s' % xlab)
                plt.ylabel('%s' % GFname)
                plt.legend()
                plt.savefig('%s/%s_specificPoints' % (sPath_spe,
                                                GFname))
                plt.close()

                # print('\n\n Y_interp_nd[j, :,idxs].shape', Y_interp_nd[j, :,idxs].shape)
                # print('t_interp.shape', t_interp.shape)
                # print('np.vstack(t_interp, Y_interp_nd[j, :,idxs]).shape', np.vstack((t_interp, Y_interp_nd[j, :,idxs])).shape)
                np.savetxt('%s/%s_specificPoints.dat' % (sPath_spe,
                    # GFname), np.transpose(t_interp, Y_interp_nd[j, :,idxs]))
                    GFname), np.vstack((t_interp, Y_interp_nd[j, :,idxs])).T,
                    header="t_d Y[i=%s,:] at x/R0 = %s" % (j, xE_star[idxs]))
        if instants:
            sPath_spe = '%s/specificInstants' % (sPath)
            try:
                os.mkdir(sPath_spe)
            except OSError:
                print ("Creation of the directory \n %s \nfailed" % sPath_spe )
            else:
                print ("Successfully created the directory \n %s " % sPath_spe)
            print('not implemented yet')
    else:
        print(" je n'ai pas encore ajouter ces valeurs au event pour donadoni")
    return

def plottingCst_2D(iniF, path_abs, set_params, t0=None, tf=None,
        setting_t=('day',1.0), folder='', r1=None, r2=None, points=[], instants=[],
        pcm=True, noAxis=False, colormap='jet', dpi=100, scalingFile=''):
    """plot Cst in 2D cas

    Parameters
    ----------
    path_abs : str
        The path
    set_params : dict
        A big dictionnary with lots of data
    t0 : float
        Initial time
    tf : float
        Final time
    setting_t : tuple

    folder : str

    r1 : float
        First position normalized by init radius
    r2 : float
        Last position normalized by init radius
    points : list
        list of specifics points where data will be plotted as function of time
    instants : list
        list of instants where user want to return and plot data
    pcm : bool
        Plot 2D contour with the ``pcolormesh`` routine + gouraud shading.
        If pcm=False, plot with the ``NonUniformImage`` routine + bilinear
        interpolation.
    noAxis : bool
        Save for all graphs, a copy without axis and with better resolution
    scalingFile : str
        The function try to read a scaling file, named as ``scalingFile``, which
        contain vmin, vmax values for each variables plotted. If scalingFile not
        given, the function collect (vmin, vmax) and save it as scalingCsts.dat.
    colormap : str
        The type of colormap, avail : ``jet``, ``grey``, ``viridis`` 


    Returns
    -------

    """
    myCMAP = dictCMAP[colormap]

    message('Csts 2D plotting')
    if scalingFile != '':
        try:
            scalingCsts = np.loadtxt('{}/{}'.format(path_abs, scalingFile))
        except OSError:
            print("{} not found in {}".format(scalingFile, path_abs))
            scaling = False
        else:
            print("Successfully loaded {}".format(scalingFile))
            scaling = True
            [p_i_min, p_i_max, p_m_min, p_m_max, a_i_min, a_i_max,
                a_m_min, a_m_max, m_i_min, m_i_max, m_m_min, m_m_max,
                l_i_min, li_max, l_m_min, l_m_max,
                chi_i_min, chi_i_max, chi_m_min, chi_m_max,
                 c_i_min, c_i_max, c_m_min, c_m_max,
                 r_i_min, r_i_max, r_m_min, r_m_max] = scalingCsts
            scal_l = [[p_i_min, p_i_max], [p_m_min, p_m_max],
                    [a_i_min, a_i_max], [a_m_min, a_m_max],
                    [m_i_min, m_i_max], [m_m_min, m_m_max],
                    [l_i_min, li_max], [l_m_min, l_m_max],
                    [chi_i_min, chi_i_max],[chi_m_min, chi_m_max],
                    [c_i_min, c_i_max], [c_m_min, c_m_max],
                    [r_i_min, r_i_max], [r_m_min, r_m_max]]
    else:
        # if not scaling creation of it
        scalingCsts = []
        scalingCsts_com = []


    sPath = '{}/{}/Cst'.format(path_abs, folder)
    try:
        os.mkdir(sPath)
    except OSError:
        print ("Creation of the directory \n %s \nfailed" % sPath )
    else:
        print ("Successfully created the directory \n %s " % sPath)
    xlab, normTime = setting_t
    paths = glob.glob('gene*')
    path_last_gene = find_last_gene(paths)

    path_data = \
        '%s/%s/tissueGrowth/' % (path_abs, path_last_gene)

    t_interp = np.load('%s/t_interp.npy' % path_data)
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    # load mesh data
    xE = np.load('%s/xE_TG.npy' % path_data)
    xP = np.load('%s/xP_TG.npy' % path_data)

    xE_star = xE / R0

    # check if r1,r2 are well defined
    default = r1==None and r2==None
    if default:
        # if None for the both, then default values, we plot for all along the
        # artery !
        print('default values for r1 r2')
        r1 = 0.0
        r2 = xE_star[-1]
    else:
        notGood = r1 < xE_star[0] or r2 > xE_star[-1]
        if notGood:
            raise ValueError('r1 < xE_star[0] or r2 > xE_star[-1]')

    t_events = np.load('%s/t_gene.npy' % path_data)
    Ngene = len(t_events)
    (i_t0, i_tf, ie_t0, ie_tf) = findIndex_t0tf(t_interp, t_events, t0, tf)
    if t0==None and tf==None:
        print('default values for t0 tf')
        t0 = t_interp[0]; tf = t_interp[-1]
    else:
        i_tf = i_tf + 1
        notGood = t0 < t_interp[0] or tf > t_interp[-1]
        if notGood: raise ValueError('t0 tf wrong')
    t_interp = t_interp[i_t0:i_tf] / normTime
    print('t0 {} tf {} and t_interp in [{}, {}]'.format(
        t0, tf, t_interp[0], t_interp[-1]))
    t_events = t_events[ie_t0:ie_tf] / normTime

    Cst_interp = np.load('%s/Cst_interp.npy' % path_data)[:,i_t0:i_tf,:]

    # load GFs
    if set_params['model'][:6] == 'Jansen':
        T, X = np.meshgrid(t_interp, xE_star)

        r_i = Cst_interp[0, :, :] - Cst_interp[2, :, :]
        r_m = Cst_interp[1, :, :] - Cst_interp[3, :, :]
        Cst_interp = np.concatenate((Cst_interp,
            r_i[np.newaxis,:,:], r_m[np.newaxis,:,:]), axis=0)
        lgd = ['p_i', 'p_m',
                'a_i', 'a_m',
                'm_i', 'm_m',
                'l_i', 'l_m',
                'chi_i', 'chi_m',
                'c_i', 'c_m', 'r_i', 'r_m']

        for k in range(len(lgd)):
            fig, ax = plt.subplots()
            levels = 80
            # cs = ax.contourf(T,X,Cst_interp[k, :, :].T, levels, cmap=myCMAP)
            if scalingFile != '':
                vmin = scal_l[k][0]
                vmax = scal_l[k][1]
            else:
                vmin = Cst_interp[k, :, :].min()
                vmax = Cst_interp[k, :, :].max()
                scalingCsts += [vmin, vmax]
                scalingCsts_com += [lgd[k] + "_min ", lgd[k] + "_max "]
            if pcm:
                cs = ax.pcolormesh(T, X, Cst_interp[k, :, :].T,
                                shading=shading_2D,
                                vmin=vmin, vmax=vmax, cmap=myCMAP)
            else:
                im = NonUniformImage(ax, interpolation='bilinear',
                        extent=[T.min(),T.max(),X.min(),X.max()], cmap=myCMAP)
                im.set_data(t_interp, xE_star, Cst_interp[k, :, :].T)
                im.set_clim(vmin=vmin, vmax=vmax)
                cs = ax.add_image(im)
                # cs = ax.imshow(Cst_interp[k, :, :].T, interpolation='bicubic',
                        # extent=[T.min(),T.max(),X.min(),X.max()], vmin=vmin, vmax=vmax, cmap=myCMAP)
            ax.set_xlabel('%s' % xlab)
            ax.set_ylabel('x/R0')
            ax.set_title('{}, vmin={:.4}, vmax={:.4}'.format(lgd[k], vmin, vmax))
                    # Cst_interp[k, :, :].min(), Cst_interp[k, :, :].max()))
            ax.set_ylim(r1,r2)
            ax.set_xlim(t0, tf)
            cb = plt.colorbar(cs)
            plt.savefig('{}/{}'.format(sPath, lgd[k]))
            if noAxis:
                ax.set_title('')
                plt.axis('off')
                cb.remove()
                plt.savefig('%s/%s_noAxis.png' % (sPath, lgd[k]),
                        bbox_inches='tight', pad_inches=0, dpi=dpi)
            plt.close()

            # output isocontour of all SFPs = 0
            # if lgd[k] == "r_i" or lgd[k] == "r_m":
            fig, ax = plt.subplots()
            CS = ax.contour(T, X, Cst_interp[k, :, :].T, levels=np.array([1e-5]))
            ax.clabel(CS, inline=True, fontsize=10)
            fig.savefig('{}/{}_isocontour0'.format(sPath, lgd[k]))
            # get data
            out = CS.allsegs
            if len(out) != 1: raise ValueError("only 1 contour should be in out")
            for j in range(len(out[0])):
                np.savetxt('{}/{}_isocontour_val=1_{}.dat'.format(
                    sPath, lgd[k], j), out[0][j])
            # save_gnuplot(T, X, Cst_interp[k, :, :].T,
                    # sPath, "{}.dat".format(lgd[k]),
                    # "# t x/R0 {}".format(lgd[k]))

        if scalingFile == '':
            np.savetxt("{}/scalingCsts.dat".format(sPath),
                    np.array(scalingCsts).reshape(1,len(scalingCsts)),
                    header=' '.join(scalingCsts_com), fmt='%1.4E')

        if points:
            sPath_spe = '%s/specificPoints' % (sPath)
            try:
                os.mkdir(sPath_spe)
            except OSError:
                print ("Creation of the directory \n %s \nfailed" % sPath_spe )
            else:
                print ("Successfully created the directory \n %s " % sPath_spe)
            # specific points plot
            idxs = []
            print('specific points plot are at :')
            for k in range(len(points)):
                idxs.append(np.abs(xE_star - points[k]).argmin())
                print('points[k]', points[k],
                      'correspond to xE_stat:', xE_star[idxs[k]])
            np.savetxt('%s/specificPoints.dat' % (sPath_spe), xE_star[idxs])
            for j in range(len(lgd)):
                plt.figure()
                for k in range(len(points)):
                    plt.plot(t_interp, Cst_interp[j, :, idxs[k]],
                                        label=r'$x/R_0=%.4f$' % xE_star[idxs[k]])
                plt.xlabel('%s' % xlab)
                plt.ylabel('%s' % lgd[j])
                plt.legend()
                plt.savefig('%s/%s_specificPoints' % (sPath_spe, lgd[j]))
                plt.close()
                # print('Cst_interp[j, :,idxs].shape', Cst_interp[j, :,idxs].shape)
                np.savetxt('%s/%s_specificPoints.dat' % (sPath_spe,
                    lgd[j]), np.vstack((t_interp, Cst_interp[j, :,idxs])).T,
                    header="t_d(%s) x/R=%s C[j=%s,:]" % (xlab, xE_star[idxs], j))

                # r_i
                plt.figure()
                for k in range(len(points)):
                    plt.plot(t_interp, r_i[:, idxs[k]],
                                        label=r'$x/R_0=%.4f$' % xE_star[idxs[k]])
                plt.xlabel('%s' % xlab)
                plt.ylabel('r_i')
                plt.legend()
                plt.savefig('%s/r_i_specificPoints' % (sPath_spe))
                # r_m
                plt.close()
                # print('r_i[:,idxs].shape', r_i[:,idxs].shape)
                np.savetxt('%s/r_i_specificPoints.dat' % (sPath_spe),
                    np.vstack((t_interp, r_i[:,idxs].T)).T,
                    header='t_d(%s) xE/R0=%s r_i' % (xlab,xE_star[idxs]))
                plt.figure()
                for k in range(len(points)):
                    plt.plot(t_interp, r_m[:, idxs[k]],
                                        label=r'$x/R_0=%.4f$' % xE_star[idxs[k]])
                plt.xlabel('%s' % xlab)
                plt.ylabel('r_m')
                plt.legend()
                plt.savefig('%s/r_m_specificPoints' % (sPath_spe))
                plt.close()
                np.savetxt('%s/r_m_specificPoints.dat' % (sPath_spe),
                    np.vstack((t_interp, r_m[:,idxs].T)).T,
                    header='t_d(%s) xE=%s r_m' % (xlab,xE_star[idxs]))
        if instants:
            sPath_spe = '%s/specificInstants' % (sPath)
            try:
                os.mkdir(sPath_spe)
            except OSError:
                print ("Creation of the directory \n %s \nfailed" % sPath_spe )
            else:
                print ("Successfully created the directory \n %s " % sPath_spe)
            print('not implemented yet')

    else:
        print("Donadoni not done")



def plottingVf_2D(iniF, path_abs, set_params, t0=None, tf=None,
        setting_t=('day',1.0), folder='', r1=None, r2=None, points=[], instants=[],
        pcm=True, noAxis=False, colormap='jet', dpi=100, scalingFile=''):
    """plot Vf in 2D cas

    Parameters
    ----------
    path_abs : str
        The path
    set_params : dict
        A big dictionnary with lots of data
    t0 : float
        Initial time
    tf : float
        Final time
    setting_t : tuple

    folder : str

    r1 : float
        First position normalized by init radius
    r2 : float
        Last position normalized by init radius
    points : list
        list of specifics points where data will be plotted as function of time
    instants : list
        list of instants where user want to return and plot data
    pcm : bool
        Plot 2D contour with the ``pcolormesh`` routine + gouraud shading.
        If pcm=False, plot with the ``NonUniformImage`` routine + bilinear
        interpolation.
    noAxis : bool
        Save for all graphs, a copy without axis and with better resolution
    scalingFile : str
        The function try to read a scaling file, named as ``scalingFile``, which
        contain vmin, vmax values for each variables plotted. If scalingFile not
        given, the function collect (vmin, vmax) and save it as scalingVf.dat.
    colormap : str
        The type of colormap, avail : ``jet``, ``grey``, ``viridis`` 

    Returns
    -------

    """
    myCMAP = dictCMAP[colormap]

    message('Vf 2D plotting')
    if scalingFile != '':
        try:
            scalingVf = np.loadtxt('{}/{}'.format(path_abs, scalingFile))
        except OSError:
            print("{} not found in {}".format(scalingFile, path_abs))
            scaling = False
        else:
            print("Successfully loaded {}".format(scalingFile))
            scaling = True
            [Vf_Ei_min, Vf_Ei_max, Vf_Qi_min, Vf_Qi_max, Vf_Qm_min, Vf_Qm_max,
                    Vf_Si_min, Vf_Si_max, Vf_Sm_min, Vf_Sm_max,
                    Vf_Ci_min, Vf_Ci_max, Vf_Cm_min, Vf_Cm_max,
                    Vf_Oi_min, Vf_Oi_max, Vf_Om_min, Vf_Om_max] = scalingVf
            Vf_sSMCi_min = Vf_Qi_min + Vf_Si_min
            Vf_sSMCi_max = Vf_Qi_max + Vf_Si_max
            Vf_sSMCm_min = Vf_Qm_min + Vf_Sm_min
            Vf_sSMCm_max = Vf_Qm_max + Vf_Sm_max
            scal_l = [[Vf_Ei_min, Vf_Ei_max], [Vf_Qi_min, Vf_Qi_max],
                    [Vf_Qm_min, Vf_Qm_max], [Vf_Si_min, Vf_Si_max],
                    [Vf_Sm_min, Vf_Sm_max], [Vf_Ci_min, Vf_Ci_max],
                    [Vf_Cm_min, Vf_Cm_max],
                    [Vf_Oi_min, Vf_Oi_max], [Vf_Om_min, Vf_Om_max]]
    else:
        # if not scaling creation of it
        scalingVf = []               
        scalingVf_com = []           


    sPath = '{}/{}/Vf'.format(path_abs, folder)
    try:
        os.mkdir(sPath)
    except OSError:
        print ("Creation of the directory \n %s \nfailed" % sPath )
    else:
        print ("Successfully created the directory \n %s " % sPath)
    xlab, normTime = setting_t
    paths = glob.glob('gene*')
    path_last_gene = find_last_gene(paths)

    path_data = '{}/{}/tissueGrowth/'.format(path_abs, path_last_gene)

    t_interp = np.load('%s/t_interp.npy' % path_data)
    (Rext, R_LEE, R0, L) = set_params['geom_dim']

    # load mesh data
    xE = np.load('%s/xE_TG.npy' % path_data)
    xP = np.load('%s/xP_TG.npy' % path_data)

    xE_star = xE / R0
    print('xE_star[0] %s xE_star[-1 %s]' % (xE_star[0], xE_star[-1]))

    # check if r1,r2 are well defined
    default = r1==None and r2==None
    if default:
        # if None for the both, then default values, we plot for all along the
        # artery !
        print('default values for r1 r2')
        r1 = 0.0
        r2 = xE_star[-1]
    else:
        notGood = r1 < xE_star[0] or r2 > xE_star[-1]
        if notGood:
            raise ValueError('r1 < xE_star[0] or r2 > xE_star[-1]')

    t_events = np.load('%s/t_gene.npy' % path_data)
    Ngene = len(t_events)
    (i_t0, i_tf, ie_t0, ie_tf) = findIndex_t0tf(t_interp, t_events, t0, tf)
    if t0==None and tf==None:
        print('default values for t0 tf')
        t0 = t_interp[0]; tf = t_interp[-1]
    else:
        i_tf = i_tf  + 1
        notGood = t0 < t_interp[0] or tf > t_interp[-1]
        if notGood: raise ValueError('t0 tf wrong')
    t_interp = t_interp[i_t0:i_tf] / normTime
    print('t0 {} tf {} and t_interp in [{}, {}]'.format(
        t0, tf, t_interp[0], t_interp[-1]))
    t_events = t_events[ie_t0:ie_tf] / normTime

    Vf = np.load('%s/Vf_stack.npy' % path_data)[:,i_t0:i_tf,:]
    # load GFs
    if set_params['model'][:6] == 'Jansen':
        T, X = np.meshgrid(t_interp, xE_star)
        N_Vfs = len(set_params['Vf'])

        for k in range(N_Vfs):
            fig, ax = plt.subplots()
            # levels = 80
            # cs = ax.contourf(T,X,Vf[k, :, :].T, cmap=myCMAP)
            Vf_k = Vf[k, :, :].T
            if scalingFile != '':
                vmin = scal_l[k][0]
                vmax = scal_l[k][1]
            else:
                vmin = Vf_k.min()
                vmax = Vf_k.max()
                scalingVf += [vmin, vmax]
                scalingVf_com += [set_params['Vf'][k] + '_min ',
                            set_params['Vf'][k] + '_max ']
            if pcm:
                cs = ax.pcolormesh(T, X, Vf_k, shading=shading_2D,
                    vmin=vmin, vmax=vmax, cmap=myCMAP)
            else:
                im = NonUniformImage(ax, interpolation='bilinear',
                        extent=[T.min(),T.max(),X.min(),X.max()], cmap=myCMAP)
                im.set_data(t_interp, xE_star, Vf_k)
                im.set_clim(vmin=vmin, vmax=vmax)
                cs = ax.add_image(im)
                # cs = ax.imshow(Vf_k, interpolation='bicubic',
                        # extent=[T.min(),T.max(),X.min(),X.max()], vmin=vmin, vmax=vmax, cmap=myCMAP)
            ax.set_xlabel('%s' % xlab)
            ax.set_ylabel('x/R0')
            ax.set_ylim(r1,r2)
            ax.set_xlim(t0, tf)
            ax.set_title('{}, vmin={:.4} vmax={:.4}'.format(
                set_params['Vf'][k], vmin, vmax))
            cb = plt.colorbar(cs)
            plt.savefig('%s/%s' % (sPath, set_params['Vf'][k]))
            if noAxis:
                ax.set_title('')
                plt.axis('off')
                cb.remove()
                plt.savefig('%s/%s_noAxis.png' % (sPath, set_params['Vf'][k]),
                            bbox_inches='tight', pad_inches=0, dpi=dpi)
            # plt.savefig('%s/%s_noAxis' % (sPath, set_params['Vf'][k]))
            plt.close()
        Vf_vSMCi = Vf[1, :, :].T + Vf[3, :, :].T
        Vf_vSMCm = Vf[2, :, :].T + Vf[4, :, :].T
        Vf_ECMi = Vf[5, :, :].T
        Vf_ECMm = Vf[6, :, :].T
        names_vSMC = ['Vf_vSMCi', 'Vf_vSMCm', 'Vf_ECMi', 'Vf_ECMm']
        # intima
        for k, Vf_k in enumerate([Vf_vSMCi, Vf_vSMCm, Vf_ECMi, Vf_ECMm]):
            fig, ax = plt.subplots()
            if scalingFile != '':
                # absolute max and min
                vmin = 0.0
                vmax = 1.0
            else:
                vmin = Vf_k.min()
                vmax = Vf_k.max()
            if pcm:
                cs = ax.pcolormesh(T, X, Vf_k, shading=shading_2D,
                        vmin=vmin, vmax=vmax, cmap=myCMAP)
            else:
                im = NonUniformImage(ax, interpolation='bilinear',
                            extent=[T.min(),T.max(),X.min(),X.max()], cmap=myCMAP)
                im.set_data(t_interp, xE_star, Vf_k)
                im.set_clim(vmin=vmin, vmax=vmax)
                cs = ax.add_image(im)
            ax.set_xlabel('%s' % xlab)
            ax.set_ylabel('x/R0')
            ax.set_ylim(r1,r2)
            ax.set_xlim(t0, tf)
            ax.set_title('{}, vmin={:.4} vmax={:.4}'.format(
                names_vSMC[k], vmin, vmax))
            cb = plt.colorbar(cs)
            plt.savefig('%s/%s' % (sPath, names_vSMC[k]))
            if noAxis:
                ax.set_title('')
                plt.axis('off')
                cb.remove()
                plt.savefig('%s/%s_noAxis.png' % (sPath, names_vSMC[k]),
                            bbox_inches='tight', pad_inches=0, dpi=dpi)
            # plt.savefig('%s/%s_noAxis' % (sPath, set_params['Vf'][k]))
            plt.close()

        if scalingFile == '':
            np.savetxt("{}/scalingVf.dat".format(sPath),
                    np.array(scalingVf).reshape(1,len(scalingVf)),
                    header=' '.join(scalingVf_com), fmt='%1.4E')

        #################################
        # POSITIONS
        #################################
        if points:
            sPath_spe = '%s/specificPoints' % (sPath)
            try:
                os.mkdir(sPath_spe)
            except OSError:
                print ("Creation of the directory \n %s \nfailed" % sPath_spe )
            else:
                print ("Successfully created the directory \n %s " % sPath_spe)
            # specific points plot
            idxs = []
            print('specific points plot are at :')
            for k in range(len(points)):
                idxs.append(np.abs(xE_star - points[k]).argmin())
                print('points[k]', points[k],
                      'correspond to xE_stat:', xE_star[idxs[k]])
            np.savetxt('%s/specificPoints.dat' % (sPath_spe), xE_star[idxs])
            for j in range(N_Vfs):
                plt.figure()
                for k in range(len(points)):
                    plt.plot(t_interp, Vf[j, :, idxs[k]],
                                        label=r'$x/R_0=%.4f$' % xE_star[idxs[k]])
                plt.xlabel('%s' % xlab)
                plt.ylabel('%s' % set_params['Vf'][j])
                plt.legend()
                plt.savefig('%s/%s_specificPoints' % (sPath_spe,
                                                set_params['Vf'][j]))
                plt.close()
                np.savetxt('%s/%s_specificPoints.dat' % (sPath_spe, set_params['Vf'][j]),
                            np.vstack((t_interp, Vf[j,:,idxs])).T,
                            header='t_d xE=%s' % xE_star[idxs])
        #################################
        # INSTANTS
        #################################
        if instants:
            sPath_spe = '%s/specificInstants' % (sPath)
            try:
                os.mkdir(sPath_spe)
            except OSError:
                print ("Creation of the directory \n %s \nfailed" % sPath_spe )
            else:
                print ("Successfully created the directory \n %s " % sPath_spe)
            print('not implemented yet')

    else:
        print(" je n'ai pas encore ajouter ces valeurs au event pour donadoni")
    return

def plottingWSS_2D(iniF, path_abs, set_params, t0=None, tf=None,
     setting_t=('day',1.0), folder='', r1=None, r2=None, points=[], instants=[],
        pcm=True, noAxis=False, colormap='jet', dpi=100, scalingFile=''):
    """plot WSS in 2D cas

    Parameters
    ----------
    path_abs : str
        The path
    set_params : dict
        A big dictionnary with lots of data
    t0 : float
        Initial time
    tf : float
        Final time
    setting_t : tuple

    folder : str

    r1 : float
        First position normalized by init radius
    r2 : float
        Last position normalized by init radius
    points : list
        list of specifics points where data will be plotted as function of time
    instants : list
        list of instants where user want to return and plot data
    pcm : bool
        Plot 2D contour with the ``pcolormesh`` routine + gouraud shading.
        If pcm=False, plot with the ``NonUniformImage`` routine + bilinear
        interpolation.
    noAxis : bool
        Save for all graphs, a copy without axis and with better resolution
    scalingFile : str
        The function try to read a scaling file, named as ``scalingFile``, which
        contain vmin, vmax values for each variables plotted. If scalingFile not
        given, the function collect (vmin, vmax) and save it as scalingWSS.dat.
    colormap : str
        The type of colormap, avail : ``jet``, ``grey``, ``viridis`` 

    Returns
    -------

    """
    myCMAP = dictCMAP[colormap]

    rho = 1056.0
    message('WSS 2D plotting')
    sPath = '%s/%s/WSS' % (path_abs, folder)
    if scalingFile != '':
        print('to do ...')
    try:
        os.mkdir(sPath)
    except OSError:
        print ("Creation of the directory \n %s \nfailed" % sPath )
    else:
        print ("Successfully created the directory \n %s " % sPath)
    xlab, normTime = setting_t
    paths = glob.glob('{}/gene*'.format(path_abs))
    # print('paths', paths)
    l = glob.glob('{}/gene*'.format(path_abs))         
    for i in range(len(l)):                         
        #l[i] = l[i].replace(self.path_abs + '/','') 
        # print('l[i]', l[i])                       
        l[i] = l[i].replace(path_abs + '/gene', '')             
        # print('l[i]', l[i])                       
        l[i] = int(l[i][:l[i].find('_')])           
    # print('l', l)                                 
    prev_gene = np.asarray(l)
    paths = np.asarray(paths)[prev_gene.argsort()].tolist()
    # print('paths', paths)
    path_last_gene = paths[-1]

    path_data = path_last_gene + '/tissueGrowth/'
    try:
        # a restart error forget the next event in t_gene,  so load all t_gene
        # in :
        resEventsLoc = np.loadtxt('{}/resEventsLoc.dat'.format(path_abs))
        t_events = resEventsLoc[:,1]
    except:
        print("'resEventsLoc.dat' not found")
        t_events = np.load('%s/t_gene.npy' % path_data)[1:-1]

    Ngene = len(t_events)
    print('paths', paths, 't_events', t_events)

    # test if steady CFD
    if checkStrInFile('{}/system/controlDict'.format(path_last_gene),
            'pimpleFoam'):
        steady = False
    else:
        steady = True
    print('CFD steady type ?', steady)
    (Rext, R_LEE, R0, L) = set_params['geom_dim']

    # load mesh data
    xE = np.load('%s/xE_TG.npy' % path_data)
    xP = np.load('%s/xP_TG.npy' % path_data)

    xE_star = xE / R0


    # check if r1,r2 are well defined
    default = r1==None and r2==None
    if default:
        # if None for the both, then default values, we plot for all along the
        # artery !
        print('default values for r1 r2')
        r1 = 0.0
        r2 = xE_star[-1]
    else:
        notGood = r1 < xE_star[0] or r2 > xE_star[-1]
        if notGood:
            raise ValueError('r1 < xE_star[0] or r2 > xE_star[-1]')

    # print('t_events.shape', t_events.shape)
    # print('t_events.shape', t_events.shape)
    t_interp = np.load('%s/t_interp.npy' % path_data)
    (i_t0, i_tf, ie_t0, ie_tf) = findIndex_t0tf(t_interp, t_events, t0, tf)
    if t0==None and tf==None:
        print('default values for t0 tf')
        t0 = t_interp[0]; tf = t_interp[-1]
    else:
        i_tf = i_tf + 1
        notGood = t0 < t_interp[0] or tf > t_interp[-1]
        if notGood: raise ValueError('t0 tf wrong')
    t_interp = t_interp[i_t0:i_tf] / normTime  
    print('t0 {} tf {} and t_interp in [{}, {}]'.format(
        t0, tf, t_interp[0], t_interp[-1]))
    # print('t_events', t_events)
    t_events = t_events[ie_t0:ie_tf] / normTime
    # print('t_events', t_events, ie_t0, ie_tf)


    t_wss = [-1] + t_events.tolist()
    print('t_wss', t_wss)
    N_wss = len(t_wss)
    wssR = []
    xE_CFD = []
    wss_CFD = []
    for i, path_i in enumerate(paths[:N_wss]):
        # print('path_i', path_i)
        wss_i = np.load('%s/tissueGrowth/WSS_TG.npy' % path_i)#[ie_t0:ie_tf]
        wssR.append(wss_i)
        # get the directory of wss postprocessing

        if steady:
            directories = next(
                    os.walk('{}/postProcessing/wssMagOnPatch/.'.format(path_i))
                            )[1]
            # print("directories", directories)
            directoriesInt = []
            for name in directories:
                if(str.isdigit(name)):
                    # print("name", name)
                    directoriesInt.append(int(name))
            # print('directoriesInt', directoriesInt)
            nameFileWSS=max(directoriesInt)
            # print('nameFileWSS', nameFileWSS, path_i)
            fWSS = np.loadtxt('{}/postProcessing/wssMagOnPatch/'.format(path_i)+
                    '{}/wallShearStressMag_valeurs.raw'.format(nameFileWSS))
        else:
            directories = next(
                    os.walk('{}/postProcessing/wssMeanMagOnPatch/.'.format(path_i))
                            )[1]
            # print("directories", directories)
            directoriesFloat = []
            for name in directories:
                # is a float ?
                if(str.isdigit(name.replace('.','',1))):
                    # print("name", name)
                    directoriesFloat.append(float(name))
            # print('directoriesFloat', directoriesFloat)
            nameFileWSS=max(directoriesFloat)
            # print('nameFileWSS', nameFileWSS, path_i)
            fWSS = np.loadtxt('{}/postProcessing/wssMeanMagOnPatch/'.format(path_i)+
                '{}/wallShearStressMeanMag_valeurs.raw'.format(nameFileWSS))
        # print('WSS.max()', fWSS[:,-1].max())
        xE_CFD.append(fWSS[:,0])
        wss_CFD.append(fWSS[:,-1] * rho)
        np.savetxt('{}/wss_gene_{}.dat'.format(sPath, i),
                np.transpose([fWSS[:,0], fWSS[:,-1] * rho]),
                header='xE (m) WSS (Pa) ')
    if len(wssR) != len(t_wss):
        print('t_events', t_wss)
        raise ValueError("len(wssR) {} != len(t_ws) {}".format(len(wssR),
            len(t_wss)))
    # print('len(wssR)', len(wssR), len(t_wss), t_wss)
    
    # concatenate data xE and WSS
    # concaDat shape == (nbrGene+1, len(xE))
    concaDat = np.concatenate((np.array([xE]), wssR))

    head = "xE/R0 (-)"
    plt.figure()
    for k in range(len(wssR)):
        head += " WSS_{} (Pa)".format(k)
        plt.plot(xE_star, wssR[k], 'o')#, label='TG')
        plt.plot(xE_CFD[k] / R0, wss_CFD[k], '-')#, label='CFD')
    plt.title("gene{} 'o' TG, '-' CFD".format(k))
    # plt.legend()
    plt.xlabel('x/R0')
    # plt.xlim(r1,r2)
    plt.ylabel("$\tau_p$ $[Pa]$")
    plt.savefig('%s/wss_gene' % sPath)

    # np.savetxt('%s/xE_star.dat' % sPath, xE_star)
    np.savetxt('%s/xE_star_WSS.dat' % sPath, concaDat.T,
            header=head)

    # from matplotlib.collections import PolyCollection
    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # verts = []
    gene = [i-1 for i in range(len(wssR))]
    path_WSS_pts = sPath + '/scatterWSS'
    if not os.path.exists(path_WSS_pts):
        os.makedirs(path_WSS_pts)

    wssMin = np.asarray(wss_CFD).min()
    wssMax = np.asarray(wss_CFD).max()
    for g in range(len(gene)):
        # print('g', g, wss_CFD[g].max())
        plt.figure()
        plt.title('t = {}'.format(t_wss[g]))
        plt.plot(xE_star, wssR[g], 'o', color='b', label='TG')
        plt.plot(xE_CFD[g] / R0, wss_CFD[g], '-', color='r', label='CFD')
        # plt.xlim(r1,r2)
        plt.ylim(wssMin, wssMax)
        plt.legend()
        plt.savefig('%s/wss_gene_%s' % (sPath, gene[g]))
        plt.close()

        plt.figure()
        plt.title('t = {}'.format(t_wss[g]))
        plt.plot(xE_star, wssR[g], 'o', color='b', label='gene = %s' % gene[g])
        plt.plot(xE_CFD[g] / R0, wss_CFD[g], 'o', color='r', label='CFD')
        # plt.xlim(r1,r2)
        plt.ylim(wssMin, wssMax)
        plt.legend()
        plt.savefig('%s/wss_gene_pts_%s' % (path_WSS_pts, gene[g]))
        plt.close()

    figTG = plt.figure()
    axTG = figTG.add_subplot(projection='3d')
    figCFD = plt.figure()
    axCFD = figCFD.add_subplot(projection='3d')
    # axTG = figTG.gca(projection='3d')
    # axCFD = figCFD.gca(projection='3d')
    for g in range(len(gene)):
        # print('g', g)
        wssTG = wssR[g]
        wssCFD = wss_CFD[g]
        if r1 is not None:
            wssTG[xE_star < r1] = np.nan
            wssCFD[(xE_CFD[g] / R0) < r1] = np.nan
        if r2 is not None:
            wssTG[xE_star > r2] = np.nan
            wssCFD[(xE_CFD[g] / R0) > r2] = np.nan

            wssCFD[(xE_CFD[g] / R0) < r1] = np.nan
        axTG.plot(xE_star,  gene[g] * np.ones((xE_star.shape)), wssTG, color='C0')
        axCFD.plot(xE_CFD[g] / R0,  gene[g] * np.ones((xE_CFD[g].shape)), wssCFD, color='C0')
        # print('wss', wss)
    axTG.set_xlim3d(r1, r2)
    axTG.set_ylim3d(min(gene), max(gene))
    axTG.set_zlim3d(np.nanmin(np.asarray(wssR)), np.nanmax(np.asarray(wssR)))
    axTG.set_xlabel(r'x/R_0')
    axTG.set_ylabel(' gene (-)')
    axTG.set_zlabel('WSS (Pa)')
    figTG.savefig('%s/wss_gene_TG_coll' % sPath)
    plt.close()

    axCFD.set_xlim3d(r1, r2)
    axCFD.set_ylim3d(min(gene), max(gene))
    axCFD.set_zlim3d(np.nanmin(np.asarray(wss_CFD)), np.nanmax(np.asarray(wss_CFD)))
    axCFD.set_xlabel(r'x/R_0')
    axCFD.set_ylabel(' gene (-)')
    axCFD.set_zlabel('WSS (Pa)')
    figCFD.savefig('%s/wss_gene_CFD_coll' % sPath)
    plt.close()

    print('gene', gene)
    if points:
        sPath_spe = '%s/specificPoints' % (sPath)
        try:
            os.mkdir(sPath_spe)
        except OSError:
            print ("Creation of the directory \n %s \nfailed" % sPath_spe )
        else:
            print ("Successfully created the directory \n %s " % sPath_spe)
        # specific points plot
        idxs = []
        print('specific points plot are at :')
        for k in range(len(points)):
            idxs.append(np.abs(xE_star - points[k]).argmin())
            print('points[k]', points[k],
                  'correspond to xE_stat:', xE_star[idxs[k]])
        np.savetxt('%s/specificPoints.dat' % (sPath_spe), xE_star[idxs])

        print('len(gene)', len(gene))
        print('len(t_wss)', len(t_wss))
        print('%s/WSS_gene_specificPoints' % (sPath_spe))
        plt.figure()
        for k in range(len(points)):
            # print('idxs[k]', idxs[k], 'np.asarray(wssR).shape', np.asarray(wssR).shape)
            plt.plot(gene, np.asarray(wssR)[:,idxs[k]],
                                label=r'$x/R_0=%.4f$' % xE_star[idxs[k]])
        plt.xlabel('generation')
        plt.ylabel('WSS (Pa)')
        plt.legend()
        plt.savefig('%s/WSS_gene_specificPoints' % (sPath_spe))
        plt.close()

        plt.figure()
        for k in range(len(points)):
            # print('idxs[k]', idxs[k], 'np.asarray(wssR).shape', np.asarray(wssR).shape)
            plt.plot(t_wss, np.asarray(wssR)[:,idxs[k]],
                                label=r'$x/R_0=%.4f$' % xE_star[idxs[k]])
        plt.xlabel('t in ({})'.format(xlab))
        plt.ylabel('WSS (Pa)')
        plt.legend()
        plt.savefig('%s/WSS_time_specificPoints' % (sPath_spe))
        plt.close()
        # print('len(gene)', len(gene), 'wssR[:,idxs].shape', wssR[:,idxs].shape)
        np.savetxt('%s/wss_specificPoints.dat' % (sPath_spe),
                    np.vstack((gene, t_wss, np.asarray(wssR)[:,idxs].T)).T,
                            header='gene (-) t (day) xE=%s' % xE_star[idxs])
        return


def plottingSourceTerms_2D(iniF, path_abs, set_params, t0=None, tf=None,
        setting_t=('day',1.0), folder='', r1=None, r2=None, points=[], instants=[],
        pcm=True, noAxis=False, colormap='jet', dpi=100, scalingFile=''):
    """plot SourceTerms in 2D cas

    Parameters
    ----------
    path_abs : str
        The path
    set_params : dict
        A big dictionnary with lots of data
    t0 : float
        Initial time
    tf : float
        Final time
    setting_t : tuple

    folder : str

    r1 : float
        First position normalized by init radius
    r2 : float
        Last position normalized by init radius
    points : list
        list of specifics points where data will be plotted as function of time
    instants : list
        list of instants where user want to return and plot data
    pcm : bool
        Plot 2D contour with the ``pcolormesh`` routine + gouraud shading.
        If pcm=False, plot with the ``NonUniformImage`` routine + bilinear
        interpolation.
    noAxis : bool
        Save for all graphs, a copy without axis and with better resolution
    scalingFile : str
        The function try to read a scaling file, named as ``scalingFile``, which
        contain vmin, vmax values for each variables plotted. If scalingFile not
        given, the function collect (vmin, vmax) and save it as scalingSource.dat.
    colormap : str
        The type of colormap, avail : ``jet``, ``grey``, ``viridis`` 


    Returns
    -------

    """
    myCMAP = dictCMAP[colormap]

    message('Source terms 2D plotting')
    if scalingFile != '':
        print('to do')

    sPath = '%s/%s/SourceTerms' % (path_abs, folder)
    try:
        os.mkdir(sPath)
    except OSError:
        print ("Creation of the directory \n %s \nfailed" % sPath )
    else:
        print ("Successfully created the directory \n %s " % sPath)
    xlab, normTime = setting_t
    paths = glob.glob('gene*')
    path_last_gene = find_last_gene(paths)

    path_data = \
        '%s/%s/tissueGrowth/' % (path_abs, path_last_gene)

    t_interp = np.load('%s/t_interp.npy' % path_data)
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    # load mesh data
    xE = np.load('%s/xE_TG.npy' % path_data)
    xP = np.load('%s/xP_TG.npy' % path_data)

    xE_star = xE / R0


    # check if r1,r2 are well defined
    default = r1==None and r2==None
    if default:
        # if None for the both, then default values, we plot for all along the
        # artery !
        print('default values for r1 r2')
        r1 = 0.0
        r2 = xE_star[-1]
    else:
        notGood = r1 < xE_star[0] or r2 > xE_star[-1]
        if notGood:
            raise ValueError('r1 < xE_star[0] or r2 > xE_star[-1]')

    t_events = np.load('%s/t_gene.npy' % path_data)
    Ngene = len(t_events)
    (i_t0, i_tf, ie_t0, ie_tf) = findIndex_t0tf(t_interp, t_events, t0, tf)
    if t0==None and tf==None:
        print('default values for t0 tf')
        t0 = t_interp[0]; tf = t_interp[-1]
    else:
        i_tf = i_tf + 1
        notGood = t0 < t_interp[0] or tf > t_interp[-1]
        if notGood: raise ValueError('t0 tf wrong')
    t_interp = t_interp[i_t0:i_tf] / normTime
    print('t0 {} tf {} and t_interp in [{}, {}]'.format(
        t0, tf, t_interp[0], t_interp[-1]))
    t_events = t_events[ie_t0:ie_tf] / normTime

    # shape NGFs, N_layers, Nx, Nt
    M_d = np.load('%s/sourceTerms.npy' % path_data)[:,:,:,i_t0:i_tf]
    # shape N_GF, Nx, Nt
    R_d = np.load('%s/R_d.npy' % path_data)[:,:,i_t0:i_tf]
    R = np.load('%s/R_GFs_WSS_eE_gene.npy' % path_data)[:,:,i_t0:i_tf]
    #shape N_GF, Nx, Nt
    P_E_d = np.load('%s/P_E_d.npy' % path_data)[:,:,i_t0:i_tf]
    P_E = np.load('%s/P_E_gene.npy' % path_data)[:,:,i_t0:i_tf]
    # shape N_GF, Nx, Nt
    B_GFs_d = np.load('%s/B_GFs_d.npy' % path_data)[:,:,i_t0:i_tf]
    B_GFs = np.load('%s/B_GFs_gene.npy' % path_data)[:,:,i_t0:i_tf]
    # load GFs
    if set_params['model'][:6] == 'Jansen':
        lgd = [['{M_d}^{NO}_i','{M_d}^{NO}_m'],
               ['{M_d}^{PDGF}_i','{M_d}^{PDGF}_m'],
               ['{M_d}^{FGF}_i','{M_d}^{FGF}_m'],
               ['{M_d}^{Ag}_i','{M_d}^{Ag}_m'],
               ['{M_d}^{TGF}_i','{M_d}^{TGF}_m'],
               ['{M_d}^{TNF}_i','{M_d}^{TNF}_m'],
               ['{M_d}^{MMP}_i','{M_d}^{MMP}_m']]

        T, X = np.meshgrid(t_interp, xE_star)
        print('T.shape', T.shape,'X.shape', X.shape)

        N_M = len(lgd)
        for k in range(N_M):
            for j in range(2):
                print('M_d[k, j, :, :].shape', M_d[k, j, :, :].shape)
                fig, ax = plt.subplots()
                # levels = 80
                # cs = ax.contourf(T,X,M_d[k, j, :, :], cmap=myCMAP)
                M_d_k_j = M_d[k, j, :, :]
                # if scalingFile != '':
                    # vmin = scal_l[j][0]
                    # vmax = 
                # else:
                    # vmin = M_d_k_j.min()
                    # vmax = M_d_k_j.max()
                vmin = M_d_k_j.min()
                vmax = M_d_k_j.max()
                if pcm:
                    cs = ax.pcolormesh(T, X, M_d_k_j, shading = shading_2D,
                                    vmin = vmin,   vmax = vmax, cmap=myCMAP)
                else:
                    im = NonUniformImage(ax, interpolation='bilinear',
                            extent=[T.min(),T.max(),X.min(),X.max()], cmap=myCMAP)
                    im.set_data(t_interp, xE_star, M_d_k_j)
                    im.set_clim(vmin=vmin, vmax=vmax)
                    cs = ax.add_image(im)
                    # cs = ax.imshow(M_d_k_j, interpolation='bicubic',
                            # extent=[T.min(),T.max(),X.min(),X.max()], vmin=vmin, vmax=vmax, cmap=myCMAP)
                ax.set_xlabel('%s' % xlab)
                ax.set_ylabel('x/R0')
                ax.set_title('{}, vmin={:.4} vmax={:.4}'.format(
                        lgd[k][j], vmin, vmax))
                ax.set_ylim(r1, r2)
                ax.set_xlim(t0, tf)
                cb = plt.colorbar(cs)
                plt.savefig('%s/%s' % (sPath, lgd[k][j]))
                if noAxis:
                    ax.set_title('')
                    plt.axis('off')
                    cb.remove()
                    plt.savefig('%s/%s_noAxis.png' % (sPath, lgd[k]),
                                bbox_inches='tight', pad_inches=0, dpi=dpi)
                plt.close()

        lgd_P_E = ['{P_E}_{NO}', '{P_E}_{PDGF}', '{P_E}_{FGF}',
               '{P_E}_{Ag}', '{P_E}_{TGF}', '{P_E}_{TNF}',
               '{P_E}_{MMP}']
        lgd_R = ['{R}_{NO}', '{R}_{PDGF}', '{R}_{FGF}',
               '{R}_{Ag}', '{R}_{TGF}', '{R}_{TNF}',
               '{R}_{MMP}']
        lgd_BC = ['{BC}_{NO}', '{BC}_{PDGF}', '{BC}_{FGF}',
               '{BC}_{Ag}', '{BC}_{TGF}', '{BC}_{TNF}',
               '{BC}_{MMP}']
        for k in range(N_M):
            fig, ax = plt.subplots()
            # levels = 80
            # cs = ax.contourf(T,X,R_d[k,:, :], cmap=myCMAP)
            R_d_k = R_d[k, :, :]
            vmin = R_d_k.min()
            vmax = R_d_k.max()
            if pcm:
                cs = ax.pcolormesh(T, X, R_d_k, shading = shading_2D,
                                    vmin = vmin, vmax = vmax, cmap=myCMAP)
            else:
                im = NonUniformImage(ax, interpolation='bilinear',
                        extent=[T.min(),T.max(),X.min(),X.max()], cmap=myCMAP)
                im.set_data(t_interp, xE_star, R_d_k)
                im.set_clim(vmin=vmin, vmax=vmax)
                cs = ax.add_image(im)
                # cs = ax.imshow(R_d_k, interpolation='bicubic',
                        # extent=[T.min(),T.max(),X.min(),X.max()], vmin=vmin, vmax=vmax, cmap=myCMAP)
            ax.set_xlabel('%s' % xlab)
            ax.set_ylabel('x/R0')
            ax.set_title('{}, vmin={:.4} vmax={:.4}'.format(lgd_R[k], vmin, vmax))
            cb = plt.colorbar(cs)
            plt.savefig('%s/%s_dam' % (sPath, lgd_R[k]))
            if noAxis:
                ax.set_title('')
                plt.axis('off')
                cb.remove()
                plt.savefig('%s/%s_dam_noAxis.png' % (sPath, lgd_R[k]),
                            bbox_inches='tight', pad_inches=0, dpi=dpi)
            plt.close()
            fig, ax = plt.subplots()
            # levels = 80
            # cs = ax.contourf(T,X,R[k, :, :], cmap=myCMAP)
            R_k = R[k, :, :]
            vmin = R_k.min()
            vmax = R_k.max()
            if pcm:
                cs = ax.pcolormesh(T, X, R_k, shading = shading_2D,
                                    vmin = vmin, vmax = vmax, cmap=myCMAP)
            else:
                im = NonUniformImage(ax, interpolation='bilinear',
                        extent=[T.min(),T.max(),X.min(),X.max()], cmap=myCMAP)
                im.set_data(t_interp, xE_star, R_k)
                im.set_clim(vmin=vmin, vmax=vmax)
                cs = ax.add_image(im)
                # cs = ax.imshow(R_k, interpolation='bicubic',
                        # extent=[T.min(),T.max(),X.min(),X.max()], vmin=vmin, vmax=vmax, cmap=myCMAP)
            ax.set_xlabel('{}'.format(xlab))
            ax.set_ylabel('x/R0')
            ax.set_title('{}'.format(lgd_R[k]))
            cb = plt.colorbar(cs)
            plt.savefig('{}/{}'.format(sPath, lgd_R[k]))
            if noAxis:
                ax.set_title('')
                plt.axis('off')
                cb.remove()
                plt.savefig('{}/{}_noAxis.png'.format(sPath, lgd_R[k]),
                            bbox_inches='tight', pad_inches=0, dpi=dpi)
            plt.close()

            fig, ax = plt.subplots()
            # levels = 80
            # cs = ax.contourf(T,X,P_E_d[k, :, :], cmap=myCMAP)
            P_E_d_k = P_E_d[k, :, :]
            vmin = P_E_d_k.min()
            vmax = P_E_d_k.max()
            if pcm:
                cs = ax.pcolormesh(T, X, P_E_d_k, shading = shading_2D,
                                    vmin = vmin, vmax = vmax, cmap=myCMAP)
            else:
                im = NonUniformImage(ax, interpolation='bilinear',
                        extent=[T.min(),T.max(),X.min(),X.max()], cmap=myCMAP)
                im.set_data(t_interp, xE_star, P_E_d_k)
                im.set_clim(vmin=vmin, vmax=vmax)
                cs = ax.add_image(im)
                # cs = ax.imshow(P_E_d_k, interpolation='bicubic',
                        # extent=[T.min(),T.max(),X.min(),X.max()], vmin=vmin, vmax=vmax, cmap=myCMAP)
            ax.set_xlabel('{}'.format(xlab))
            ax.set_ylabel('x/R0')
            ax.set_title('{} dam, vmin={:.4} vmax={:.4}'.format(
                lgd_P_E[k], vmin, vmax))
            cb = plt.colorbar(cs)
            plt.savefig('{}/{}_dam'.format(sPath, lgd_P_E[k]))
            if noAxis:
                ax.set_title('')
                plt.axis('off')
                cb.remove()
                plt.savefig('{}/{}_dam_noAxis.png'.format(sPath, lgd_P_E[k]),
                            bbox_inches='tight', pad_inches=0, dpi=dpi)
            plt.close()
            fig, ax = plt.subplots()
            # levels = 80
            # cs = ax.contourf(T,X,P_E[k, :, :], cmap=myCMAP)
            P_E_k = P_E[k, :, :]
            vmin = P_E_k.min()
            vmax = P_E_k.max()
            if pcm:
                cs = ax.pcolormesh(T, X, P_E_k, shading = shading_2D,
                                    vmin = vmin, vmax = vmax, cmap=myCMAP)
            else:
                im = NonUniformImage(ax, interpolation='bilinear',
                        extent=[T.min(),T.max(),X.min(),X.max()], cmap=myCMAP)
                im.set_data(t_interp, xE_star, P_E_k)
                im.set_clim(vmin=vmin, vmax=vmax)
                cs = ax.add_image(im)
                # cs = ax.imshow(P_E_k, interpolation='bicubic',
                        # extent=[T.min(),T.max(),X.min(),X.max()], vmin=vmin, vmax=vmax, cmap=myCMAP)
            ax.set_xlabel('{}'.format(xlab))
            ax.set_ylabel('x/R0')
            ax.set_title('{} , vmin={:.4} vmax={:.4}'.format(lgd_P_E[k], vmin, vmax))
            cb = plt.colorbar(cs)
            plt.savefig('{}/{}'.format(sPath, lgd_P_E[k]))
            if noAxis:
                ax.set_title('')
                plt.axis('off')
                cb.remove()
                plt.savefig('{}/{}_noAxis.png'.format(sPath, lgd_P_E[k]),
                            bbox_inches='tight', pad_inches=0, dpi=dpi)
            plt.close()

            fig, ax = plt.subplots()
            # levels = 80
            print('B_GFs_d[k, :, :].shape', B_GFs_d[k, :, :].shape)
            # cs = ax.contourf(T,X,B_GFs_d[k, :, :], cmap=myCMAP)
            B_GFs_d_k = B_GFs_d[k, :, :]
            vmin = B_GFs_d_k.min()
            vmax =B_GFs_d_k.max()
            if pcm:
                cs = ax.pcolormesh(T, X, B_GFs_d_k, shading = shading_2D,
                                    vmin = vmin, vmax = vmax, cmap=myCMAP)
            else:
                im = NonUniformImage(ax, interpolation='bilinear',
                        extent=[T.min(),T.max(),X.min(),X.max()], cmap=myCMAP)
                im.set_data(t_interp, xE_star, B_GFs_d_k)
                im.set_clim(vmin=vmin, vmax=vmax)
                cs = ax.add_image(im)
                # cs = ax.imshow(B_GFs_d_k, interpolation='bicubic',
                        # extent=[T.min(),T.max(),X.min(),X.max()], vmin=vmin, vmax=vmax, cmap=myCMAP)
            ax.set_xlabel('{}'.format(xlab))
            ax.set_ylabel('x/R0')
            ax.set_title('{} dam, vmin={:.4} vmax={:.4}'.format(lgd_BC[k], vmin, vmax))
            cb = plt.colorbar(cs)
            plt.savefig('{}/{}_dam'.format(sPath, lgd_BC[k]))
            if noAxis:
                ax.set_title('')
                plt.axis('off')
                cb.remove()
                plt.savefig('{}/{}_dam_noAxis.png'.format(sPath, lgd_BC[k]),
                            bbox_inches='tight', pad_inches=0, dpi=dpi)
            plt.close()
            fig, ax = plt.subplots()
            # levels = 80
            print('B_GFs[k, :, :].shape', B_GFs[k, :, :].shape)
            # cs = ax.contourf(T,X,B_GFs[k, :, :], cmap=myCMAP)
            B_GFs_k = B_GFs_d[k, :, :]
            vmin = B_GFs_k.min()
            vmax =B_GFs_k.max()
            if pcm:
                cs = ax.pcolormesh(T, X, B_GFs_k, shading = shading_2D,
                                    vmin = vmin, vmax = vmax, cmap=myCMAP)
            else:
                im = NonUniformImage(ax, interpolation='bilinear',
                        extent=[T.min(),T.max(),X.min(),X.max()], cmap=myCMAP)
                im.set_data(t_interp, xE_star, B_GFs_k)
                im.set_clim(vmin=vmin, vmax=vmax)
                cs = ax.add_image(im)
                # cs = ax.imshow(B_GFs_k, interpolation='bicubic',
                        # extent=[T.min(),T.max(),X.min(),X.max()], vmin=vmin, vmax=vmax, cmap=myCMAP)
            ax.set_xlabel('{}'.format(xlab))
            ax.set_ylabel('x/R0')
            ax.set_title('{}, vmin={:.4} vmax={:.4}'.format(
                lgd_BC[k], vmin, vmax))
            plt.colorbar(cs)
            plt.savefig('{}/{}'.format(sPath, lgd_BC[k]))
            if noAxis:
                ax.set_title('')
                plt.axis('off')
                cb.remove()
                plt.savefig('{}/{}_noAxis.png'.format(sPath, lgd_BC[k]),
                            bbox_inches='tight', pad_inches=0, dpi=dpi)
            plt.close()
    else:
        print(" je n'ai pas encore ajouter ces valeurs au event pour donadoni")

