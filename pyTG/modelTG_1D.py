#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import glob
import sys
import time
import numpy as np
import os
import matplotlib.pyplot as plt

# from .artery import Artery
# from .hemodynamic import Hemodynamic



class ModelTG_1D(object):
    """objet Model1D of tissue growth model
        management of donadoni et al modelisation 2017 & Jansen et al 20??

        constructor objet Model1D of tissue growth model

        Les attributs de la classe sont les paramètres de la resolution temporel
        du systeme d'EDOs
        Args:
            percentRemodeling (float): pourcentage limite avant remodelage
            dT (int): intervalle entre 2 increment de temps
            T0 (float): temps initiale en jours
            Tf (float): temps finale en jours
            Nt (float): nbr de point de discretisation entre T0 et Tf
            dt (float): pas de temps en jours

        Return:
            ModeleCT:  un modele de croissance Donadoni
        
    """

    def __init__(self, iterable=(), **kwargs):
        """Fast constructor of the object. As the class must be able to
        evolve quickly, the parameters are implicitly defined in the dictionary
        during the initialization. This is a method that allows flexibility in
        development but may cause a lot of errors.
        """
        self.__dict__.update(iterable, **kwargs)

    def writeResults(self, sol, Vf, pathSavePop, pathSaveGFs):
        """
        Parameters
        ----------
        sol : solver object
            The retuned solver if 
                * Jansen model -> DdeResult 
                * Donadoni model -> ????
        Vf : np.ndarray
            The array with all the volume fraction of species
        pathSavePop : str
            The path where species will be saved
        pathSaveGFs : str
            The path where GFs will be saved
        Returns
        -------
        Files written in given paths
        """
        print('write result \n %s %s ' % (pathSavePop, pathSaveGFs))
        if(self.__class__.__name__ == 'Donadoni_1D'):
            (t_nd, GFs, species) = self.reshapeRes(sol)
            t_d = t_nd * self.T_d
            # saving time array
            np.save('%s/t_d' % pathSavePop, t_d)
            # saving dimensional variables
            for i in range(len(self.set_params['species_nd'])):
                np.save('%s/%s' %
                        (pathSavePop, self.set_params['species_nd'][i]),
                        species[i] )  # * self.set_params['species_dim'][i])
            # saving  volume factions
            for i in range(len(self.set_params['Vf'])):
                np.save('%s/%s' % (pathSavePop, self.set_params['Vf'][i]),
                        Vf[i])
            # saving dimensional GFs
            for i in range(len(self.set_params['GFs_nd'])):
                np.save('%s/%s' %
                        (pathSaveGFs, self.set_params['GFs_nd'][i]),
                        GFs[i] )  # * self.set_params['GFs_dim'][i])

        elif self.__class__.__name__[:6] == 'Jansen':

            (t_nd, GFs_nd, ddtGFs_nd, species_nd, ddtspecies_nd) = self.reshapeRes(sol)
            # print('save sol.y')
            # print('GFs_b.shape', GFs_b.shape, ddtGFs_b.shape)
            np.save('%s/y_nd' % pathSavePop, sol.y)
            np.save('%s/yp_nd' % pathSavePop, sol.yp)
            np.save('%s/t_nd' % pathSavePop, t_nd)

            y = sol.y * self.set_params['sys_dim'][:,np.newaxis]
            yp = sol.yp * self.set_params['sys_dim'][:,np.newaxis] / self.T_d
            print("self.set_params['species_dim'].shape %s species_nd.shape %s ddtspecies_nd.shape %s" % (
                self.set_params['species_dim'].shape, species_nd.shape, ddtspecies_nd.shape))
            species = species_nd * self.set_params['species_dim'][:,np.newaxis]
            ddtspecies = ddtspecies_nd * self.set_params['species_dim'][:,np.newaxis] / self.T_d
            GFs = GFs_nd * self.set_params['GFs_dim'][:,np.newaxis]
            ddtGFs = ddtGFs_nd * self.set_params['GFs_dim'][:,np.newaxis] / self.T_d

            t_d = t_nd * self.T_d
            np.save('%s/t_d' % pathSavePop, t_d)
            np.save('%s/y' % pathSavePop, y)
            np.save('%s/yp' % pathSavePop, yp)

            # data related to species
            for i in range(len(self.set_params['species_nd'])):
                ########################
                # species_nd
                #######################
                # non human readable
                np.save('%s/%s' %
                        (pathSavePop, self.set_params['species_nd'][i]),
                        species_nd[i])
                # human readable
                np.savetxt('%s/%s.dat' %
                        (pathSavePop, self.set_params['species_nd'][i]),
                        np.transpose([t_d, species_nd[i]]),
                        header='t(days) %s' % (self.set_params['species_nd'][i]))
                
                ########################
                # ddtspecies_nd
                #######################
                # non human readable
                np.save('%s/%s' %
                        (pathSavePop, 'ddt' + self.set_params['species_nd'][i]),
                        ddtspecies_nd[i] )
                # human readable
                np.savetxt('%s/%s.dat' %
                        (pathSavePop, 'ddt' + self.set_params['species_nd'][i]),
                        np.transpose([t_d, ddtspecies_nd[i]]),
                        header='t(days) %s' % ('ddt' + self.set_params['species_nd'][i]))
                ########################
                # species
                #######################
                # non human readable
                np.save('%s/%s' %
                        (pathSavePop, self.set_params['species_nd'][i][:-3]),
                        species[i])
                # human readable
                np.savetxt('%s/%s.dat' %
                        (pathSavePop, self.set_params['species_nd'][i][:-3]),
                        np.transpose([t_d, species[i]]),
                        header='t(days) %s' % (self.set_params['species_nd'][i][:-3]))
                ########################
                # ddtspecies
                #######################
                # non human readable
                np.save('%s/%s' %
                        (pathSavePop, 'ddt' + self.set_params['species_nd'][i][:-3]),
                        ddtspecies[i])
                # human readable
                np.savetxt('%s/%s.dat' %
                        (pathSavePop, 'ddt' + self.set_params['species_nd'][i][:-3]),
                        np.transpose([t_d, ddtspecies[i]]),
                        header='t(days) %s' % ('ddt' + self.set_params['species_nd'][i][:-3]))

            # data related to GFs
            for i in range(len(self.set_params['GFs_nd'])):
                ########################
                # GFs_nd
                #######################
                # non human readable
                np.save('%s/%s' %
                        (pathSaveGFs, self.set_params['GFs_nd'][i]),
                        GFs_nd[i] )
                # human readable
                np.savetxt('%s/%s.dat' %
                        (pathSaveGFs, self.set_params['GFs_nd'][i]),
                        np.transpose([t_d, GFs_nd[i]]),
                        header = 't(days) %s' % (self.set_params['GFs_nd'][i]))
                ########################
                # ddtGFs_nd
                #######################
                # non human readable
                np.save('%s/%s' %
                        (pathSaveGFs, 'ddt'+self.set_params['GFs_nd'][i]),
                        ddtGFs_nd[i] )
                # human readable
                np.savetxt('%s/%s.dat' %
                        (pathSaveGFs, 'ddt'+self.set_params['GFs_nd'][i]),
                        np.transpose([t_d, ddtGFs_nd[i]]),
                        header = 't(days) %s' % ('ddt'+self.set_params['GFs_nd'][i]))
                ########################
                # GFs
                #######################
                # non human readable
                np.save('%s/%s' %
                        (pathSaveGFs, self.set_params['GFs_nd'][i][:-3]),
                        GFs[i] )
                # human readable
                np.savetxt('%s/%s.dat' %
                        (pathSaveGFs, self.set_params['GFs_nd'][i][:-3]),
                        np.transpose([t_d, GFs[i]]),
                        header = 't(days) %s' % (self.set_params['GFs_nd'][i][:-3]))
                ########################
                # ddtGFs
                #######################
                # non human readable
                np.save('%s/%s' %
                        (pathSaveGFs, 'ddt' + self.set_params['GFs_nd'][i][:-3]),
                        ddtGFs[i] )
                # human readable
                np.savetxt('%s/%s.dat' %
                        (pathSaveGFs, 'ddt' + self.set_params['GFs_nd'][i][:-3]),
                        np.transpose([t_d, ddtGFs[i]]),
                        header = 't(days) %s' % ('ddt' + self.set_params['GFs_nd'][i][:-3]))

            # saving  volume factions
            # print("self.set_params['Vf']", self.set_params['Vf'])
            # print('len(Vf)', len(Vf))
            for i in range(len(self.set_params['Vf'])):
                # non human readable
                # print(' pathSavePop', pathSavePop, self.set_params['Vf'][i])
                # print('Vf[i]', Vf[i])
                np.save('%s/%s' % (pathSavePop, self.set_params['Vf'][i]),
                        Vf[i])
                # human readable
                np.savetxt('%s/%s.dat' % (pathSavePop, self.set_params['Vf'][i]),
                        np.transpose([t_d, Vf[i]]),
                        header = 't(days) %s' % (self.set_params['Vf'][i]))


    def plottingDonadoni(self, path):
        """

        """
        pathSaveCompoArt = '%s/allGenes/Pop' % (path)
        pathGlobal = '%s/allGenes/global' % (path)
        t_gene = np.load('%s/t_gene.npy' % pathGlobal)
        t_d = np.load('%s/t_d.npy' % pathGlobal)

        dQ_i = np.load('%s/dQi.npy' % pathSaveCompoArt)
        dQ_m = np.load('%s/dQm.npy' % pathSaveCompoArt)
        dS_i = np.load('%s/dSi.npy' % pathSaveCompoArt)
        dS_m = np.load('%s/dSm.npy' % pathSaveCompoArt)
        dC_i = np.load('%s/dCi.npy' % pathSaveCompoArt)
        dC_m = np.load('%s/dCm.npy' % pathSaveCompoArt)
        e_i = np.load('%s/e_i.npy' % pathSaveCompoArt)
        e_m = np.load('%s/e_m.npy' % pathSaveCompoArt)
        ePlus = np.load('%s/ePlus.npy' % pathSaveCompoArt)
        Composante = [dQ_i*self.Qimax, dQ_m*self.Qmmax,
                      dS_i*self.Siphysio, dS_m*self.Smphysio,
                      dC_i*self.Ciphysio, dC_m*self.Cmphysio]
        legendes = ['Q_i (c)', 'Q_m (c)',
                     'S_i (c)', 'S_m (c)',
                     'C_i (g)', 'C_m (g)']
        bilan = [e_i*1e6, e_m*1e6, ePlus*1e6]
        bilanLegend = ['e_i', 'e_m',' e_{+}']

        N = len(Composante)
        M = len(bilan)

        f, axarr = plt.subplots(1)
        for j in range(len(t_gene)):
            axarr.axvline(x=t_gene[j], color='g')
        for k in range(M):
            axarr.plot(t_d, bilan[k], label=r'$%s$' % bilanLegend[k] )
        axarr.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
        axarr.legend(bbox_to_anchor=(1.02, 0.4),
                     loc="center left", borderaxespad=0.)
        axarr.set_ylabel('$\mu m$')
        plt.savefig('%s/bilan' % (path),
                    bbox_inches="tight")

        f, axarr = plt.subplots(N, sharex=True)
        for i in range(N):
            for j in range(len(t_gene)):
                axarr[i].axvline(x=t_gene[j], color='g' )
            axarr[i].plot(t_d, Composante[i], 'b', label=r'$%s$' % legendes[i])
            axarr[i].ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
            axarr[i].legend(bbox_to_anchor=(1.02, 0.4),
                            loc="center left", borderaxespad=0.)
        f.subplots_adjust(left=None, bottom=None,
                          right=None, top=None,
                          wspace=None, hspace=0.4 )
        plt.savefig('%s/species' % (path),
                    bbox_inches="tight")
        plt.close()

        R_t = (self.myArtery.Rext - ePlus) /\
                        self.myArtery.R0
        plt.figure()
        my_label = "remodeling Process"
        for j in range(len(t_gene)):
            plt.axvline(x=t_gene[j]/7, color='g', label=my_label)
            my_label = "_nolegend_"
        plt.plot(t_d/7., R_t ,color='r',label='$R_w$')
        plt.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
        plt.xlabel(r'weeks')
        plt.ylabel(r'$R_w/R^0$ $(\%)$')
        plt.legend()
        plt.savefig('%s/rayon' % (path),
                    bbox_inches="tight")
        plt.close()
        #plt.figure()
        #plt.plot(t_d/7., R_t,color='r',label='$R_w$')
        #plt.xlabel(r'weeks')
        #plt.ylabel(r'$R_w/R^0$ $(\%)$')
        #plt.legend()
        #import tikzplotlib
        #tikzplotlib.save('%s/rayon.tex' % path)
        #plt.close()

        Delta_t = np.diff(t_d)
        v = (np.diff(ePlus)*1e6) / Delta_t
        plt.figure()
        my_label = "remodeling Process"
        for j in range(len(t_gene)):
            plt.axvline(x=t_gene[j], color='g', label=my_label)
            my_label = "_nolegend_"
        plt.plot(t_d[:-1],v,label='$\Delta e_+ / \Delta t$')
        plt.xlabel(r'$days$')
        plt.ylabel(r'$\mu m /j$')
        plt.legend()
        plt.savefig('%s/vitesseCroissance.png' % path)



if __name__ == '__main__':


    dict_TG = {'t0': 0,
               'tf': 10}

    TG = ModelTG_1D(**dict_TG)

