#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import pickle
import numpy.ma as ma
import numpy as np
import glob
from configparser import ConfigParser
from warnings import warn
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib.colors as colors
plt.rc('font', family='serif', size='20')
plt.rcParams["figure.figsize"] = (20,16)
from mpl_toolkits.mplot3d import Axes3D

from pyTG.utilities import *

def plottingGFs_1D(iniF, path_abs, set_params, t0=None, tf=None, setting_t=('day',1.0),
                folder='', dpi=100):
    """
    plot GFs

        path_abs ():
        set_params ():
        plotAtEachGene ():
        t0 ():
        tf ():
        folder ():
    """
    # config = ConfigParser(converters={'list': lambda x: [i.strip() for i in x.split(',')]})
    # # conserve upper or lower type of characters
    # config.optionxform = lambda option: option
    # r = config.read('%s' % iniF)
    # if not r:
        # raise ValueError('*********** wrong file *******************')
                    # # help='in 2D cases, point where specific attention is useful')
    sPath = '%s/%s/GFs' % (path_abs, folder)
    try:
        os.mkdir(sPath)
    except OSError:
        print ("Creation of the directory \n %s \nfailed" % sPath )
    else:
        print ("Successfully created the directory \n %s " % sPath)
    xlab, normTime = setting_t
    pathGFs = \
        '%s/allGenes/tissueGrowth/GFs' % (path_abs)
    pathGlobal = \
        '%s/allGenes/tissueGrowth/global' % (path_abs)

    t_d = np.load('%s/t_d.npy' % pathGlobal)
    t_events = np.load('%s/t_events.npy' % pathGlobal)
    (i_t0, i_tf, ie_t0, ie_tf) = findIndex_t0tf(t_d, t_events, t0, tf)
    t_d = t_d[i_t0:i_tf] / normTime
    t_events = t_events[ie_t0:ie_tf] / normTime
    # load GFs
    GFs = []
    for i in range(len(set_params['GFs_nd'])):
        GFs.append(np.load('%s/%s.npy' %
                           (pathGFs, set_params['GFs_nd'][i]))[i_t0:i_tf])
    N = len(GFs)
    print('N', N)
    # load GFs value for events times
    if set_params['model'][:6] == 'Jansen':
        # je n'ai pas encore ajouter ces valeurs au event pour donadoni
        print('ie_t0 %s ie_tf %s shape y_events %s' % (ie_t0, ie_tf, np.load('%s/y_events.npy' % (pathGlobal)).shape))
        # GFs_events = np.load('%s/y_events.npy' % (pathGlobal))[ie_t0:ie_tf,:]
        f, axarr = plt.subplots(int(N * .5), sharex=True)
        m = 0
        for i in range(0, int(N * .5)):
            # axarr[i].plot(t_events, GFs_events[:, m], 'g o')
            # axarr[i].plot(t_events, GFs_events[:, m + 1], 'g o')
            axarr[i].plot(t_d, GFs[m], 'r', label=r'%s' % set_params['GFs_nd'][m])
            axarr[i].plot(t_d, GFs[m + 1], 'C0', label=r'%s' % set_params['GFs_nd'][m + 1])
            axarr[i].ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
            axarr[i].legend(bbox_to_anchor=(1.02, 0.4), loc="center left", borderaxespad=0.)
            m = m + 2
    else:
        print(" je n'ai pas encore ajouter ces valeurs au event pour donadoni")
        f, axarr = plt.subplots(N, sharex=True)
        for i in range(N):
            for j in range(len(t_events)):
                axarr[i].axvline(x=t_events[j], color='g')
            axarr[i].plot(t_d, GFs[i], 'r', label=r'%s' % set_params['GFs_nd'][i])
            axarr[i].ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
            axarr[i].legend(bbox_to_anchor=(1.02, 0.4), loc="center left", borderaxespad=0.)
    axarr[-1].set_xlabel('%s' % xlab)
    f.subplots_adjust(left=None, bottom=None, right=None, top=None,
                      wspace=None, hspace=0.4)
    plt.savefig('%s/GFs' % sPath, bbox_inches="tight")
    plt.close()

    # save data in readable file
    m = 0
    for i in range(0, int(N * .5)):
        np.savetxt('{}/GFs_{}_nd.dat'.format(sPath, set_params['GFs_nd'][m][:-4]),
                np.transpose([t_d, GFs[m], GFs[m+1]]),
                header='t_d ({}) {} (-) {} (-)'.format(
                    xlab, set_params['GFs_nd'][m], set_params['GFs_nd'][m + 1]))
        # np.savetxt('%s/GFs_events_%s.dat' % (sPath, k),
                # np.transpose([t_events, GFs_events[:,k]]))
        m += 2
    return

def plottingCst_1D(iniF, path_abs, set_params, t0=None, tf=None, setting_t=('day',1.0),
                folder='', dpi=100):
    """
    """
    sPath = '%s/%s/Cst' % (path_abs, folder)
    try:
        os.mkdir(sPath)
    except OSError:
        print ("Creation of the directory \n %s \nfailed" % sPath )
    else:
        print ("Successfully created the directory \n %s " % sPath)

    xlab, normTime = setting_t
    pathCsts = '%s/allGenes/tissueGrowth/Csts' % (path_abs)
    pathPop = '%s/allGenes/tissueGrowth/Pop' % (path_abs)
    pathGlobal = '%s/allGenes/tissueGrowth/global' % (path_abs)
    t_d = np.load('%s/t_d.npy' % pathGlobal)
    t_gene = np.load('%s/t_gene.npy' % pathGlobal)
    (i_t0, i_tf, iG_t0, iG_tf) = findIndex_t0tf(t_d, t_gene, t0, tf)
    t_d = t_d[i_t0:i_tf] / normTime
    t_gene = t_gene[iG_t0:iG_tf] / normTime

    if set_params['model'][:6] == 'Jansen':
        p_i = np.load('%s/p_i.npy' % pathCsts)[i_t0:i_tf]
        p_m = np.load('%s/p_m.npy' % pathCsts)[i_t0:i_tf]
        a_i = np.load('%s/a_i.npy' % pathCsts)[i_t0:i_tf]
        a_m = np.load('%s/a_m.npy' % pathCsts)[i_t0:i_tf]
        l_i = np.load('%s/l_i.npy' % pathCsts)[i_t0:i_tf]
        l_m = np.load('%s/l_m.npy' % pathCsts)[i_t0:i_tf]
        chi_i = np.load('%s/chi_i.npy' % pathCsts)[i_t0:i_tf]
        chi_m = np.load('%s/chi_m.npy' % pathCsts)[i_t0:i_tf]
        m_i = np.load('%s/m_i.npy' % pathCsts)[i_t0:i_tf]
        m_m = np.load('%s/m_m.npy' % pathCsts)[i_t0:i_tf]
        r_i = np.load('%s/r_i.npy' % pathCsts)[i_t0:i_tf]
        r_m = np.load('%s/r_m.npy' % pathCsts)[i_t0:i_tf]

        if False: # the previous test -> (qui est faux) set_params['species_nd']== 'dcm':
            c_i = np.load('%s/dci.npy' % pathPop)[i_t0:i_tf]
            c_m = np.load('%s/dcm.npy' % pathPop)[i_t0:i_tf]
            lgds = [r'$p_i$', r'$a_i$', r'$r_i$', r'$p_m$', r'$a_m$',
                        r'$r_m$', r'$r_m-m$', r'$dc_i$', r'$dc_m$',
                        r'$\lambda_i', r'$\lambda_m$', r'$\chi_i$',
                        r'$\chi_m$', r'$m$']
        else:
            c_i = np.load('%s/c_i.npy' % pathCsts)[i_t0:i_tf]
            c_m = np.load('%s/c_m.npy' % pathCsts)[i_t0:i_tf]
            lgds = [r'$p_i$', r'$a_i$', r'$r_i$', r'$m_i$', r'$p_m$', r'$a_m$',
                        r'$r_m$', r'$c_i$', r'$c_m$', r'$\lambda_i', r'$\lambda_m$',
                        r'$\chi_i$', r'$\chi_m$', r'$m_m$']
        csts = [p_i, a_i, r_i, m_i, p_m, a_m, r_m, c_i, c_m,
                l_i, l_m, chi_i, chi_m, m_m]
        N = len(csts)
        f, axarr = plt.subplots(N, sharex=True)
        for i in range(N):
            for j in range(len(t_gene)):
                axarr[i].axvline(x=t_gene[j], color='g')
            axarr[i].plot(t_d, csts[i], 'C0', label=lgds[i])
            axarr[i].ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
            axarr[i].legend(bbox_to_anchor=(1.02, 0.4),
                            loc="center left", borderaxespad=0.)
        axarr[-1].set_xlabel('%s' % xlab)
        f.subplots_adjust(left=None, bottom=None, right=None, top=None,
                          wspace=None, hspace=0.4)
        plt.savefig('%s/constantes' % sPath, bbox_inches="tight")
        plt.close()

        plt.figure()
        plt.plot(t_d, p_i, 'C0 ^', label=r'$p_i$')
        plt.plot(t_d, a_i, 'C0 o', label=r'$a_i$')
        plt.plot(t_d, r_i, 'C0', label=r'$r_i$')
        plt.plot(t_d, p_m, 'C1 ^', label=r'$p_m$')
        plt.plot(t_d, a_m, 'C1 o', label=r'$a_m$')
        plt.plot(t_d, r_m, 'C1', label=r'$r_m$')
        plt.legend()
        plt.savefig('%s/param_S' % sPath, bbox_inches="tight")
        plt.close()
        # save readable data file
        np.savetxt('%s/p_i.dat' % sPath, np.transpose([t_d, p_i, p_i-a_i]),
                header='t_d(%s) p_i r_i' % xlab)
        np.savetxt('%s/p_m.dat' % sPath, np.transpose([t_d,p_m, p_m-a_m]),
                header='t_d(%s) p_m r_m' % xlab)
        np.savetxt('%s/a_i.dat' % sPath, np.transpose([t_d,a_i]),
                header='t_d(%s) a_i' % xlab)
        np.savetxt('%s/a_m.dat' % sPath, np.transpose([t_d,a_m]),
                header='t_d(%s) a_m' % xlab)

        plt.figure()
        plt.plot(t_d, c_i, 'C0', label=r'$c_i$')
        plt.plot(t_d, c_m, 'C1', label=r'$c_m$')
        plt.legend()
        plt.savefig('%s/chgtPeno' % sPath, bbox_inches="tight")
        plt.close()
        # save readable data file
        np.savetxt('%s/c_i.dat' % sPath, np.transpose([t_d,c_i]),
                header='t_d(%s) c_i' % xlab)
        np.savetxt('%s/c_m.dat' % sPath, np.transpose([t_d,c_m]),
                header='t_d(%s) c_m' % xlab)

        plt.figure()
        plt.plot(t_d, m_m, 'C0')
        plt.xlabel('%s' % xlab)
        plt.ylabel(r'$ m_m\; [d^{-1}]$')
        plt.savefig('%s/m_m' % sPath, bbox_inches="tight")
        plt.close()
        plt.figure()
        plt.plot(t_d, m_i, 'C0')
        plt.xlabel('%s' % xlab)
        plt.ylabel(r'$ m_i\; [d^{-1}]$')
        plt.savefig('%s/m_i' % sPath, bbox_inches="tight")
        plt.close()
        # save readable data file
        np.savetxt('%s/m_i.dat' % sPath, np.transpose([t_d,m_i]),
                header='t_d(%s) m_i' % xlab)
        np.savetxt('%s/m_m.dat' % sPath, np.transpose([t_d,m_m]),
                header='t_d(%s) m_m' % xlab)
    elif set_params['model'] == 'Donadoni_1D':
        raise NotImplementedError
    else:
        raise ValueError("model not handled %s" % set_params['model'])
    return


def plottingPop_1D(iniF, path_abs, set_params, t0=None, tf=None, setting_t=('day',1.0),
                folder='', dpi=100):
    """
    plot Popu
    """
    xlab, normTime = setting_t

    #######################
    ### Data
    #######################
    # data from gentle denudation carotid artery
    # Fingerle 1990 rat carotid
    tFingerle = np.array([7., 14., 28., 84.]) / normTime
    areaGentle_iFingerle = np.array([0.0118, 0.1, 0.0994, 0.0821])
    areaBalloon_iFingerle = np.array([0.04818, 0.10030, 0.17030, 0.21152])

    # Schwartz 1996 qualititave data Scharwte
    # days
    tRatTab1a_Schwartz96 = np.array([2., 14.0, 84.0]) / normTime
    CRatTab1a_Schwartz96 = np.array([0.42e5, 2.33e5, 1.98e5]) # in cell / mm

    # Rat
    tRatTab1b_Schwartz96 = np.array([7., 14., 28., 90.]) / normTime
    CRatTab1b_Schwartz96 = np.array([0.65e3, 2.72e3, 2.73e3, 2.11e3])
    areaRatTab1b_Schwartz96 = np.array([0.04, 0.20, 0.23, 0.22])
    # Porcine
    tPorcineTab2_Schwartz96 = np.array([14., 28., 168.]) / normTime
    CPorcineTab2_Schwartz96 = np.array([2.46e3, 1.03e4, 7.86e3])
    thicknessPorcineTab2_Schwartz = np.array([0.12, 0.54, 0.54])
    areaPorcineTab2_Schwartz96 = np.array([0.897, 3.33, 3.33])
    # human in restenosis case
    tHumanTab3_Schwartz96 = np.array([23., 150., 240., 360.]) / normTime
    CHumanTab3_Schwartz96 = np.array([0.22e3, 1.82e3, 1.59e3, 1.90e3])
    areaHumanTab3_Schwartz96 = np.array([0.126, 0.848, 0.911, 0.911])
    sPath = '%s/%s/Pop' % (path_abs, folder)

    # platelet deposition kinetics data, Humphrey 2002, Cardiov. Sol. Mech.p.575
    # source ??
    tP_humphrey = np.array([1./24., 1., 4., 7., 14., 30., 60.])
    rhoP = np.array([44.7, 40.5, 4.4,
        1.2, 0.2, 0.2, 0.1]) * 1e6 # in Plat/cm²
    # Badimon 1986, Influence of arterial damage and wall shear rate on platelet
    # Figure 3, data between 0 and 30 minutes no table 
    tBadimon = np.array([1, 3, 5, 10, 15, 20, 30])
    # by eyes, Re = 70, gamR = 1690
    plaRe70 = np.array([3, 8, 13, 16, 12, 10, 7.8]) * 1e6 # in Pla / cm2 
    # by eyes, Re 35, gamR = 212
    plaRe35 = np.array([3, 4.5, 7.5, 9.5, 7.5, 9.5, 7.9]) * 1e6 # in Pla / cm2
    # by eyes, Re 17, gamR = 106
    plaRe17 = np.array([0.1, 0.4, 2.5, 7.5, 6, 7.5, 7.8]) * 1e6 # in Pla / cm2

    try:
        os.mkdir(sPath)
    except OSError:
        print ("Creation of the directory \n %s \nfailed" % sPath )
    else:
        print ("Successfully created the directory \n %s " % sPath)

    pathPop = '%s/allGenes/tissueGrowth/Pop' % (path_abs)
    pathCst = '%s/allGenes/tissueGrowth/Csts' % (path_abs)
    pathGlobal = '%s/allGenes/tissueGrowth/global' % (path_abs)
    t_d = np.load('%s/t_d.npy' % pathGlobal)
    t_events = np.load('%s/t_events.npy' % pathGlobal)
    (i_t0, i_tf, iG_t0, iG_tf) = findIndex_t0tf(t_d, t_events, t0, tf)
    t_d = t_d[i_t0:i_tf] / normTime
    t_events = t_events[iG_t0:iG_tf] / normTime

    NGFs = len(set_params['GFs_nd'])
    y_nd = np.load('%s/y_nd.npy' % (pathPop))[:,i_t0:i_tf]
    yp_nd = np.load('%s/yp_nd.npy' % (pathPop))[:,i_t0:i_tf]
    y = np.load('%s/y.npy' % (pathPop))[:,i_t0:i_tf]
    yp = np.load('%s/yp.npy' % (pathPop))[:,i_t0:i_tf]

    species_nd = np.load('%s/y_nd.npy' % (pathPop))[NGFs:,i_t0:i_tf]
    ddtspecies_nd = np.load('%s/yp_nd.npy' % (pathPop))[NGFs:,i_t0:i_tf]
    species= np.load('%s/y.npy' % (pathPop))[NGFs:,i_t0:i_tf]
    ddtspecies = np.load('%s/yp.npy' % (pathPop))[NGFs:,i_t0:i_tf]
    # dimensional variables
    print('y.shape %s yp.shape %s' % (y_nd.shape,yp_nd.shape))
    # popC = []
    # # load pop
    # for i in range(len(set_params['species_nd'])):
        # # print('i', i)
        # # print("set_params['species_nd'][i]", set_params['species_nd'][i])
        # # print('loaded file ', pathPop+'/'+set_params['species_nd'][i])
        # popC.append(np.load('%s/%s.npy' %
                            # (pathPop, set_params['species_nd'][i]))[i_t0:i_tf])

    geomC = []
    # load params geom
    for i in range(len(set_params['geom'])):
        geomC.append(np.load('%s/%s.npy' %
                             (pathPop, set_params['geom'][i]))[i_t0:i_tf])
    if set_params['model'][:6] == 'Jansen':
        # comment because if no y_events -> bug
        # popC_events = np.load('%s/y_events.npy' % (pathGlobal))[iG_t0:iG_tf,:]

        print("set_params['model']", set_params['model'])
        print("", )
        if set_params['model'].count('Infla') > 0:
            # dimensional values
            if set_params['model'].count('Pl') > 0:
                raise NotImplementedError
                # # platelet
                # (Qimax, Qmmax, SiPhy, SmPhy, CjiPhy, CjmPhy,
                    # CjiPhy,CjmPhy, Emax, Pmax)  = set_params['species_dim']
                # #dim
                # (ddtQi, ddtQm, ddtSi, ddtSm,
                    # ddtCji, ddtCjm, ddtCvi, ddtCvm, ddtE, ddtP) = ddtspecies
                # (Qi, Qm, Si, Sm,
                    # Cji, Cjm, Cvi, Cvm, E, P) = species
                # # non dim
                # (ddtQi_nd, ddtQm_nd, ddtSi_nd, ddtSm_nd,
                    # ddtCji_nd, ddtCjm_nd, ddtCvi_nd, ddtCvm_nd,
                    # ddtE_nd, ddtP_nd) = ddtspecies_nd
                # (Qi_nd, Qm_nd, Si_nd, Sm_nd,
                    # Cji_nd, Cjm_nd, Cvi_nd, Cvm_nd, E_nd, P_nd) = species_nd

                # plt.figure()
                # plt.plot(t_d, P_nd, label='P_nd')
                # plt.legend()
                # plt.xlabel('%s' % xlab)
                # plt.ylabel('Platelet cells [-]')
                # # plt.title('MoPhy = %s MaPhy = %s' % (MoPhy, MaPhy))
                # plt.savefig('%s/Platelet_nd' % sPath)
            else:
                (Qimax, Qmmax, SiPhy, SmPhy, CjiPhy, CjmPhy,
                    CjiPhy,CjmPhy, Emax, MoPhy, MaPhy)  = set_params['species_dim']

                #dim
                (ddtQi, ddtQm, ddtSi, ddtSm,
                    ddtCji, ddtCjm, ddtCvi, ddtCvm, ddtE, ddtMo, ddtMa) = ddtspecies
                (Qi, Qm, Si, Sm,
                    Cji, Cjm, Cvi, Cvm, E, Mo, Ma) = species
                # non dim
                (ddtQi_nd, ddtQm_nd, ddtSi_nd, ddtSm_nd,
                    ddtCji_nd, ddtCjm_nd, ddtCvi_nd, ddtCvm_nd,
                    ddtE_nd, ddtMo_nd, ddtMa_nd) = ddtspecies_nd
                (Qi_nd, Qm_nd, Si_nd, Sm_nd,
                    Cji_nd, Cjm_nd, Cvi_nd, Cvm_nd, E_nd, Mo_nd, Ma_nd) = species_nd

            (Vf_QiPhy, Vf_QmPhy, Vf_SiPhy, Vf_SmPhy,
               Vf_CiPhy, Vf_CmPhy, Vf_EiPhy, Vf_MoPhy, Vf_MaPhy) = set_params['VfPhy']

            plt.figure()
            plt.plot(t_d, Mo_nd, label='Mo_nd')
            plt.plot(t_d, Ma_nd, label='Ma_nd')
            plt.legend()
            plt.xlabel('%s' % xlab)
            plt.ylabel('Immune cells [-]')
            plt.title('MoPhy = %s MaPhy = %s' % (MoPhy, MaPhy))
            plt.savefig('%s/ImmuneCells_nd' % sPath)

        # elif set_params['model'] == 'Jansen_1D':
        else:
            # dimensional values
            if set_params['model'].count('Pl') > 0:
                # platelet
                (Qimax, Qmmax, SiPhy, SmPhy, CjiPhy, CjmPhy,
                    CjiPhy,CjmPhy, Emax, Pmax)  = set_params['species_dim']
                #dim
                (ddtQi, ddtQm, ddtSi, ddtSm,
                    ddtCji, ddtCjm, ddtCvi, ddtCvm, ddtE, ddtP) = ddtspecies
                (Qi, Qm, Si, Sm,
                    Cji, Cjm, Cvi, Cvm, E, P) = species
                # non dim
                (ddtQi_nd, ddtQm_nd, ddtSi_nd, ddtSm_nd,
                    ddtCji_nd, ddtCjm_nd, ddtCvi_nd, ddtCvm_nd,
                    ddtE_nd, ddtP_nd) = ddtspecies_nd
                (Qi_nd, Qm_nd, Si_nd, Sm_nd,
                    Cji_nd, Cjm_nd, Cvi_nd, Cvm_nd, E_nd, P_nd) = species_nd

                plt.figure()
                plt.plot(t_d, P_nd, label='P_nd')
                plt.legend()
                plt.xlabel('%s' % xlab)
                plt.ylabel('Platelet cells [-]')
                # plt.title('MoPhy = %s MaPhy = %s' % (MoPhy, MaPhy))
                plt.savefig('%s/Platelet_nd' % sPath)
            else:
                # NotImplementedError
                # dimensional values
                (Qimax, Qmmax, SiPhy, SmPhy, CjiPhy, CjmPhy,
                    CjiPhy,CjmPhy, Emax)  = set_params['species_dim']

                #dim
                (ddtQi, ddtQm, ddtSi, ddtSm,
                    ddtCji, ddtCjm, ddtCvi, ddtCvm, ddtE) = ddtspecies
                (Qi, Qm, Si, Sm,
                    Cji, Cjm, Cvi, Cvm, E) = species
                # non dim
                (ddtQi_nd, ddtQm_nd, ddtSi_nd, ddtSm_nd,
                    ddtCji_nd, ddtCjm_nd, ddtCvi_nd, ddtCvm_nd, ddtE_nd) = ddtspecies_nd
                (Qi_nd, Qm_nd, Si_nd, Sm_nd,
                    Cji_nd, Cjm_nd, Cvi_nd, Cvm_nd, E_nd) = species_nd

        (Vf_QiPhy, Vf_QmPhy,
        Vf_SiPhy, Vf_SmPhy,
        Vf_CiPhy, Vf_CmPhy, Vf_EiPhy )= set_params['VfPhy']
        (rho_s, rho_c, rho_E) = set_params['densities']
        NGFs = len(set_params['GFs_nd'])
        (Rext, R_LEE, R0, L) = set_params['geom_dim']


        e_i = geomC[0]
        e_m = geomC[1]
        eTot = geomC[-1]
        # popC_events = popC_events[:,NGFs:]
        # radius calc
        Rl = Rext - eTot
        RLEI = Rl + e_i
        Sw = 2 * np.pi * Rl * L

        # calcul of dSieq et dSmeq
        Vi = np.load('%s/Vi.npy' % pathGlobal)[i_t0:i_tf]
        Vm = np.load('%s/Vm.npy' % pathGlobal)[i_t0:i_tf]
        l_i = np.load('%s/l_i.npy' % pathCst)[i_t0:i_tf]
        l_m = np.load('%s/l_m.npy' % pathCst)[i_t0:i_tf]
        chi_i = np.load('%s/chi_i.npy' % pathCst)[i_t0:i_tf]
        chi_m = np.load('%s/chi_m.npy' % pathCst)[i_t0:i_tf]

        R_gene = np.load('%s/R_gene.npy' % pathGlobal)[1:]
        t_gene = np.load('%s/t_gene.npy' % pathGlobal)[1:]
        R_gene = R_gene[iG_t0:iG_tf]
        t_gene = t_gene[iG_t0:iG_tf] / normTime
        print('t_events', t_gene.shape, 'R_events', R_gene.shape)

        Vf_Ci_eq = Vf_CiPhy * Vi

        # cCjvi = (Cji + Cvi)  / Vi
        # cCjvm = (Cjm + Cvm) / Vm
        cCji = (Cji)  / Vi
        cCjm = (Cjm) / Vm

        print('cCji[0]', cCji[0], 'cCji[-1]', cCji[-1])
        print('cCjm[0]', cCjm[0], 'cCjm[-1]', cCjm[-1])
        plt.figure()
        # plt.plot(t_d, cCjvi, label='(Cji + Cvi)  / Vi')
        # plt.plot(t_d, cCjvm, label='(Cjm + Cvm)  / Vm')
        plt.plot(t_d, cCji, label='(Cji)  / Vi')
        plt.plot(t_d, cCjm, label='(Cjm)  / Vm')
        plt.legend()
        plt.xlabel(r't')
        plt.ylabel(r'concentration c')
        plt.savefig('%s/concentrationC' % sPath)

        Ci_eq = Vf_CiPhy * Vi * rho_c
        Cm_eq = Vf_CmPhy * Vm * rho_c

        Ci_eq_nd = Ci_eq / CjiPhy
        Cm_eq_nd = Cm_eq / CjmPhy
        np.savetxt('%s/Ci_eq_nd.dat' % sPath, np.transpose([t_d, Ci_eq_nd]),
                header='t_d(%s) Ci_eq_nd' % xlab)
        np.savetxt('%s/Cm_eq_nd.dat' % sPath, np.transpose([t_d, Cm_eq_nd]),
                header='t_d(%s) Cm_eq_nd' % xlab)
        # print('Ci', Ci_eq, 'Cm_eq', Cm_eq)

        Si_eq_nd = (chi_i * (Ci_eq)/ l_i ) / SiPhy
        Sm_eq_nd = (chi_m * (Cm_eq)/ l_m ) / SmPhy

        # print('SiPhy', SiPhy,'SmPhy',SmPhy )
        # print('dSi', dSi, 'dSm', dSm)
        # print('dSi_eq', dSi_eq, 'dSm_eq', dSm_eq)
        delta_Si_nd = Si_nd - Si_eq_nd
        delta_Sm_nd = Sm_nd - Sm_eq_nd

        plt.figure()
        plt.plot(Si_nd, Cji_nd, label=r'i')
        plt.plot(Sm_nd, Cjm_nd, label=r'm')
        plt.legend()
        plt.xlabel(r'$S^*$')
        plt.ylabel(r'$C_{j}^*$')
        plt.savefig('%s/phasePlanS_Cj' % sPath)
        plt.figure()
        plt.plot(Si_nd, Cji_nd + Cvi_nd, label=r'i')
        plt.plot(Sm_nd, Cjm_nd + Cvm_nd, label=r'm')
        plt.legend()
        plt.xlabel(r'$S^*$')
        plt.ylabel(r'$C_{j+o}^*$')
        plt.savefig('%s/phasePlanS_C' % sPath)

        plt.figure()
        plt.plot(t_d,delta_Si_nd,'C0',label=r'$\Delta Si^*$')
        plt.plot(t_d,delta_Sm_nd,'C1',label=r'$\Delta Sm^*$')
        plt.xlabel('%s' % xlab)
        plt.legend()
        plt.ylabel(r'$\Delta S^* = S^* - S^{eq}*$ $[-]$')
        plt.savefig('%s/DeltaS' % sPath)
        plt.close()
        # save readable data files
        np.savetxt('%s/delta_Si_nd.dat' % sPath, np.transpose([t_d, delta_Si_nd]),
                header='t_d(%s) delta_Si_nd' % xlab)
        np.savetxt('%s/delta_Sm_nd.dat' % sPath, np.transpose([t_d, delta_Sm_nd]),
                header='t_d(%s) delta_Sm_nd' % xlab)

        plt.figure()
        plt.plot(t_d,Cji_nd,'C0',label='$ dCj_i$')
        plt.plot(t_d,Cjm_nd,'C1',label='$ dCj_m$')
        plt.xlabel('%s' % xlab)
        plt.legend()
        plt.ylabel(r'$ dC$ $[-]$')
        plt.savefig('%s/dCj' % sPath)
        plt.close()
        # save readable data files
        np.savetxt('%s/Cji_nd.dat' % sPath, np.transpose([t_d, Cji_nd]),
                header='t_d(%s) Cji_nd' % xlab)
        np.savetxt('%s/Cjm_nd.dat' % sPath, np.transpose([t_d, Cjm_nd]),
                header='t_d(%s) Cjm_nd' % xlab)

        np.savetxt('%s/Cvi_nd.dat' % sPath, np.transpose([t_d, Cvi_nd]),
                header='t_d(%s) Cvi_nd' % xlab)
        np.savetxt('%s/Cvm_nd.dat' % sPath, np.transpose([t_d, Cvm_nd]),
                header='t_d(%s) Cvm_nd' % xlab)

        # calcul intiaml area from simulation
        area_i = np.pi * (RLEI**2. - Rl**2.) * 1e6 # to mm2

        fig, ax1 = plt.subplots()
        area = ax1.plot(t_d, area_i, 'C0', label = r'area intima')
        ax1.set_ylabel(r'$\pi (R_{LEI}^2 - R_{l}^2)$ in $mm^2$')
        ax1.set_xlabel('%s' % xlab)
        ax1.set_title('Comparison with Fingler 1990 study')
        ax2 = ax1.twinx()
        ax2.set_ylabel(r'area in $mm^2$')
        ratSch = ax2.plot(tRatTab1b_Schwartz96,
                areaRatTab1b_Schwartz96, '-o', label = 'Rat Schwartz 96')
        porSch = ax2.plot(tPorcineTab2_Schwartz96,
                areaPorcineTab2_Schwartz96, '-o', label = 'Porcine Schwartz 96')
        humSch = ax2.plot(tHumanTab3_Schwartz96,
                areaHumanTab3_Schwartz96, '-o', label = 'Human Schwartz 96')
        gentle = ax2.plot(tFingerle,
                areaGentle_iFingerle, '-o', label = 'Gentle Fingler 90')
        ballon = ax2.plot(tFingerle,
                areaBalloon_iFingerle, '-o', label = 'Balloon Fingler 90')

        lns = ratSch + porSch + humSch + gentle + ballon + area
        labs = [l.get_label() for l in lns]
        ax1.legend(lns, labs, loc=0)
        plt.savefig('%s/areaIntima' % sPath, bbox_inches="tight")

        f, axarr = plt.subplots(1)
        for k in range(len(geomC)):
            axarr.plot(t_d, geomC[k], label=r'$%s$' % set_params['geom'][k])
        axarr.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
        axarr.legend(bbox_to_anchor=(1.02, 0.4),
                     loc="center left", borderaxespad=0.)
        axarr.set_xlabel('%s' % xlab)
        axarr.set_ylabel('thickness [?]')
        plt.savefig('%s/bilan' % sPath, bbox_inches="tight")
        plt.close()
        # save readable data files
        for k in range(len(geomC)):
            np.savetxt('%s/%s.dat' % (sPath, set_params['geom'][k]),
                    np.transpose([t_d, geomC[k]]),
                    header='t_d(%s) %s (?)' % (xlab, set_params['geom'][k]))

        # plot none dimensional variables
        N = species_nd.shape[0]
        f, axarr = plt.subplots(N, sharex=True)
        for i in range(N):
            if i==4:
                axarr[i].plot(t_d, Ci_eq_nd, 'k', label='Ci_eq_nd')
            if i==5:
                axarr[i].plot(t_d, Cm_eq_nd, 'k', label='Cm_eq_nd')
            # axarr[i].plot(t_events, popC_events[:,i], 'r o',)
            axarr[i].plot(t_d, species_nd[i], 'C0', label=r'$%s$' % set_params['species_nd'][i])
            axarr[i].ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
            axarr[i].legend(bbox_to_anchor=(1.02, 0.4),
                            loc="center left", borderaxespad=0.)
        axarr[-1].set_xlabel('%s' % xlab)
        f.subplots_adjust(left=None, bottom=None, right=None, top=None,
                          wspace=None, hspace=0.4)
        plt.savefig('%s/species_nd' % sPath, bbox_inches="tight")
        plt.close()
        # plot dimensional variables
        N = species_nd.shape[0]
        f, axarr = plt.subplots(N, sharex=True)
        for i in range(N):
            if i==4:
                axarr[i].plot(t_d, Ci_eq, 'k', label='Ci_eq')
            if i==5:
                axarr[i].plot(t_d, Cm_eq, 'k', label='Cm_eq')
            # axarr[i].plot(t_events, popC_events[:,i], 'r o',)
            axarr[i].plot(t_d, species[i], 'C0', label=r'$%s$' % set_params['species_nd'][i][:-3])
            axarr[i].ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
            axarr[i].legend(bbox_to_anchor=(1.02, 0.4),
                            loc="center left", borderaxespad=0.)
        axarr[-1].set_xlabel('%s' % xlab)
        f.subplots_adjust(left=None, bottom=None, right=None, top=None,
                          wspace=None, hspace=0.4)
        plt.savefig('%s/species' % sPath, bbox_inches="tight")
        plt.close()

        np.savetxt('%s/E_nd.dat' % sPath, np.transpose([t_d, E_nd]),
                header='t_d(%s) E_nd' % xlab)

        plt.figure()
        plt.plot(t_d,E_nd,'C0')
        plt.xlabel('%s' % xlab)
        plt.ylabel(r'$E^*$ $[-]$')
        plt.savefig('%s/E_nd' % sPath)
        plt.close()

        # look at equilibrium state for each layer
        yp_i = yp[0::2,:]; yp_m = yp[1::2,:]
        yp_nd_i = yp_nd[0::2,:]; yp_nd_m = yp_nd[1::2,:]

        norm_yp = np.linalg.norm(yp, axis=0)
        norm_yp_i = np.linalg.norm(yp_i, axis=0)
        norm_yp_m = np.linalg.norm(yp_m, axis=0)

        norm_yp_nd = np.linalg.norm(yp_nd, axis=0)
        norm_yp_nd_i = np.linalg.norm(yp_nd_i, axis=0)
        norm_yp_nd_m = np.linalg.norm(yp_nd_m, axis=0)

        epsEq = 1e-3
        print('Test eq norm_yp', norm_yp[-1],
                'norm_yp_m[-1] < epsEq', (norm_yp_m[-1] < epsEq))
        print('Test eq norm_yp_nd', norm_yp_nd[-1],
                "norm_yp_nd_m[-1] < epsEq", (norm_yp_nd_m[-1] < epsEq))
        
        if norm_yp_nd_m[-1] < epsEq or norm_yp_nd_i[-1] < epsEq:
            # tf is None because check about equilibrium if only pp until tf
            print('=======================================')
            print('Equilibrium')
            print('=======================================')
            if norm_yp_nd_i[-1] < epsEq:
                print('intima t_d = %s' % (np.min(t_d[np.argwhere(norm_yp_nd_i < epsEq)])))
            elif norm_yp_nd_m[-1] < epsEq:
                print('media t_d = %s' % (np.min(t_d[np.argwhere(norm_yp_nd_m < epsEq)])))
            plt.figure()
            plt.plot(t_d, norm_yp_nd, label='norm L2 yp_nd')
            plt.plot(t_d, norm_yp_nd_i, label='norm L2 yp_nd_i')
            plt.plot(t_d, norm_yp_nd_m, label='norm L2 yp_nd_m')
            plt.legend()
            plt.yscale('log')
            plt.savefig('%s/norm_yp_nd' % sPath)
            np.savetxt('%s/norm_yp_nd.dat' % sPath, np.transpose([t_d, norm_yp_nd]),
                header='t_d(%s) norm_yp_nd' % xlab)

            plt.figure()
            plt.plot(t_d, norm_yp, label='norm L2 yp')
            plt.plot(t_d, norm_yp_i, label='norm L2 yp_i')
            plt.plot(t_d, norm_yp_m, label='norm L2 yp_m')
            plt.legend()
            plt.yscale('log')
            plt.savefig('%s/norm_yp' % sPath)
            np.savetxt('%s/norm_yp.dat' % sPath, np.transpose([t_d, norm_yp]),
                header='t_d(%s) norm_yp' % xlab)


            y_eq_nd = y_nd[:,-1]
            ddty_eq = yp[:,-1]
            ddty_eq_nd = yp_nd[:,-1]
            print(y_eq_nd.shape, ddty_eq_nd.shape, set_params['sys_dim'].shape)
            to_save = np.vstack((y_eq_nd, ddty_eq_nd, set_params['sys_dim'])).T
            np.savetxt('%s/equilibrium.dat' % sPath, to_save, delimiter=" ",
                    fmt='%2.4e', header='final time %s %s\n Y^*\t dY^*/dt^*\t Ydim' % (t_d[-1], xlab))

            def writeXdagger(gf,i):
                """
                gf : str
                    Name of the GFs
                i : int:
                    index pair -> i impair -> m
                """
                if i % 2 == 0:
                    l = '\\mathrm{i}'
                else:
                    l = '\\mathrm{m}'
                return '$\\delta^{\\mathrm{%s}}_{%s} =' % (gf, l) + \
                        '\\eta^{\\mathrm{%s}}_{%s}/' % (gf, l) + \
                        '\\widetilde{\\eta}^{\\mathrm{%s}}_{%s}$' % (gf, l)
            formul = []
            for i in range(len(set_params['GFs_nd'])):
                formul.append(writeXdagger(set_params['GFs_nd'][i][:-4],i))
            formul.append("$Q^\\dagger_\\mathrm{i}=Q_\\mathrm{i}/{Q_\\mathrm{i}}_{\\mathrm{max}}$")
            formul.append("$Q^\\dagger_\\mathrm{m}=Q_\\mathrm{m}/{Q_\\mathrm{m}}_{\\mathrm{max}}$")
            formul.append("$S_\\mathrm{i}^\\dagger=S_\\mathrm{i}/S_\\mathrm{i}^{\\mathrm{ph}}$")
            formul.append("$S_\\mathrm{m}^\\dagger=S_\\mathrm{m}/S_\\mathrm{m}^{\\mathrm{ph}}$")
            formul.append("${C_y}_\\mathrm{i}^\\dagger={C_y}_\\mathrm{i}/{C_y}_\\mathrm{i}^{\\mathrm{ph}}$")
            formul.append("${C_y}_\\mathrm{m}^\\dagger={C_y}_\\mathrm{m}/{C_y}_\\mathrm{m}^{\\mathrm{ph}}$")
            formul.append("${C_o}_\\mathrm{i}^\\dagger={C_o}_\\mathrm{i}/{C_y}_\\mathrm{i}^{\\mathrm{ph}}$")
            formul.append("${C_o}_\\mathrm{m}^\\dagger={C_o}_\\mathrm{m}/{C_y}_\\mathrm{m}^{\\mathrm{ph}}$")
            formul.append("$E^\\dagger=E/E_{\\mathrm{max}}$")
            if set_params['model'] == 'JansenInfla_1D':
                formul.append("$Mo^\\dagger=Mo/Mo_{\\mathrm{ph}}$")
                formul.append("$Ma^\\dagger=Ma/Ma_{\\mathrm{ph}}$")
            f = open("%s/equilibriumTable.dat" % sPath, "w")
            f.write('\\renewcommand{\\arraystretch}{1.2}\n')
            f.write('\\begin{tabular}{l  c c}\n')
            # f.write('\\hline \\\\[-1em]\n')
            f.write('\\toprule \n')
            # f.write('$\\boldsymbol{y}^\\dagger$ formulation & $\\boldsymbol{y}^\\dagger$  & $\\boldsymbol{y}^{\\mathrm{ref}}$ \\\\ \n')
            f.write('$y^\\dagger$ formulation & $y^\\dagger$  & $y^{\\mathrm{ref}}$ \\\\ \n')
            f.write("& \\multicolumn{2}{c}{Value}  \\\\ \n")
            f.write("\\cmidrule{2-3} \n")
            # f.write('$t_f^\dagger = t m_0$ & {:.2e} & 10.0 days \\\\ \n'.format(t_d[-1] / 10.0))
            # f.write('\\hline \\\\[-1em]\n')
            # print('len(y_eq_nd) %s  sys_dim %s' % (len(y_eq_nd), len(set_params['sys_dim'])))
            dimensions = [' ng'] * len(set_params['GFs_nd']) + [' cells'] * 4 + \
                    [' g'] * 4 + [' cells']
            for i in range(len(y_eq_nd)):
                f.write(formul[i] + ' & \\num{' + '{:.2e}'.format(y_eq_nd[i]) + '} & \\num{' +
                    '{:.2e}'.format(set_params['sys_dim'][i]) + '}'+
                    dimensions[i] + ' \\\\ \n')
            # f.write('\\hline \\\\[-1em]\n')
            f.write('\\bottomrule \n')
            f.write('\\end{tabular}')
            f.close()
        # write
        np.savetxt('%s/ddtSi_nd.dat' % sPath, np.transpose([t_d, ddtSi_nd]),
                header='t_d(%s) ddtSi_nd' % xlab)
        np.savetxt('%s/ddtSm_nd.dat' % sPath, np.transpose([t_d, ddtSm_nd]),
                header='t_d(%s) ddtSm_nd' % xlab)
        np.savetxt('%s/Si_nd.dat' % sPath, np.transpose([t_d, Si_nd]),
                header='t_d(%s) Si_nd' % xlab)
        np.savetxt('%s/Sm_nd.dat' % sPath, np.transpose([t_d, Sm_nd]),
                header='t_d(%s) Sm_nd' % xlab)

        # compute growth fraction as defined by Schwartz 1996
        gf_Si = ddtSi / Si
        gf_Sm = ddtSm / Sm

        # from Table 9.3 p564 Humphrey 2002 Cardiovascular Solid
        # from Kinetics of cellular proliferation after arterial injury. I
        t_gf_Si_Clowes83 = np.array([1, 2, 4, 7, 14, 28, 49, 84]) / normTime
        gf_Si_Clowes83 = np.array([0., 0., 73., 58., 5.8, 1.2, 0.3, 0.3]) /100.
        t_cells_Creighton = np.array([0.0, 3.0, 7.0, 14., 28.])
        gf_cells_mCreighton = np.array([0.8, 13.8, 4.0, 2.0, 0.7]) / 100.
        #                                               ??
        gf_cells_iCreighton = np.array([0.0, 0.0, 3.0, 47.2, 0.2]) / 100.
        #                               ??    ??  ??         ??
        i_max_gf_Si = np.argmax(gf_Si)
        i_max_gf_Sm = np.argmax(gf_Sm)

        print('Maximal of dev log of S_{i,m} are \n')
        print('intima \t t_d = %s gf_Si = %s' % (t_d[i_max_gf_Si],
            gf_Si[i_max_gf_Si]))
        print('media \t t_d = %s gf_Sm = %s' % (t_d[i_max_gf_Sm],
            gf_Sm[i_max_gf_Sm]))
        # write
        np.savetxt('%s/gf_Si.dat' % sPath, np.transpose([t_d, gf_Si]),
                    header='t_d(%s) dSi_gf' % xlab)
        np.savetxt('%s/gf_Sm.dat' % sPath, np.transpose([t_d, gf_Sm]),
                    header='t_d(%s) dSm_gf' % xlab)

        plt.figure()
        plt.plot(t_d,ddtSi, label = r'ddtSi')
        plt.plot(t_d,ddtSm,label = r'ddtSm')
        plt.ylabel(r'$ddtS^*$ $[-]$')
        plt.xlabel('%s' % xlab)
        plt.legend()
        plt.savefig('%s/ddtS' % sPath)
        plt.close()

        plt.figure()
        plt.plot(t_d, gf_Si, label = r'gf_Si')
        plt.plot(t_d, gf_Sm, label = r'gf_Sm')
        plt.ylabel(r'$1/S * dS/dt$ $[-]$')
        plt.xlabel('%s' % xlab)
        plt.legend()
        plt.savefig('%s/gf_S' % sPath)
        plt.close()


        fig, ax1 = plt.subplots()
        lns1 = ax1.plot(t_d, Si_nd,'C0', label = r'$S^*_i$')
        lns2 = ax1.plot(t_d, Sm_nd,'C1', label = r'$S^*_m$')
        ax1.set_ylabel(r'$S^*$ $[-]$')
        ax1.set_xlabel('%s' % xlab)

        ax2 = ax1.twinx()
        ax2.set_ylabel(r'$\frac{dS^*}{dt}/S^* \; [d^{-1}]$')
        ax2.set_xlabel('%s' % xlab)
        lns3 = ax2.plot(t_d, gf_Si,'C0--',label = r'$\frac{dS_i}{dt}/S_i$')
        lns4 = ax2.plot(t_d, gf_Sm,'C1--',label = r'$\frac{dS_m}{dt}/S_m$')
        lns5 = ax2.plot(t_gf_Si_Clowes83, gf_Si_Clowes83, 'o-', label = 'Clowes83')
        l6 = ax2.plot(t_cells_Creighton, gf_cells_mCreighton, 'o-', label = 'm Creighton')
        l7 = ax2.plot(t_cells_Creighton, gf_cells_iCreighton, 'o-', label = 'i Creighton')

        # added these three lines
        lns = lns1 + lns2 + lns3 + lns4 + lns5 + l6 + l7
        labs = [l.get_label() for l in lns]
        ax1.legend(lns, labs, loc=0)

        plt.savefig('%s/S_gfS' % sPath)

        plt.figure()
        plt.plot(t_d,Si_nd,label = r'$S^*_i$')
        plt.plot(t_d,ddtSi_nd,'g',label = r'$dS^*_i/dt^*$')
        plt.legend()
        plt.xlabel('%s' % xlab)
        plt.savefig('%s/Si_nd_ddtSi_nd' % sPath)

        # write
        np.savetxt('%s/Qi_nd.dat' % sPath, np.transpose([t_d, Qi_nd]),
                    header='t_d(%s) Qi_nd' % xlab)
        np.savetxt('%s/Qm_nd.dat' % sPath, np.transpose([t_d, Qm_nd]),
                    header='t_d(%s) Qm_nd' % xlab)
        np.savetxt('%s/Qi.dat' % sPath, np.transpose([t_d, Qi]),
                    header='t_d(%s) Qi' % xlab)
        np.savetxt('%s/Qm.dat' % sPath, np.transpose([t_d, Qm]),
                    header='t_d(%s) Qm' % xlab)
        np.savetxt('%s/Si.dat' % sPath, np.transpose([t_d, Si]),
                    header='t_d(%s) Si' % xlab)
        np.savetxt('%s/Sm.dat' % sPath, np.transpose([t_d, Sm]),
                    header='t_d(%s) Sm' % xlab)

        UCI0 = (Qi + Qm) / (Qi[0] + Qm[0])

        UCI1 = (Qi + Qm) / ((Qi + Qm) + (Si + Sm))
        UCI2 = (Qi + Qm) / ((Qi + Qm) + ((Si + Sm)-(Si[0] + Sm[0])))
        UCI3 = (Qi + Qm) / ((Qi + Qm) + (Si + Sm)) + \
                (Si[0] + Sm[0]) / ((Qi[0] + Qm[0]) + (Si[0] + Sm[0]))
        np.savetxt('%s/UCI0.dat' % sPath,
                        np.transpose([t_d, UCI0]), header='t_d(%s) UCI0' % xlab)
        np.savetxt('%s/UCI1.dat' % sPath,
                        np.transpose([t_d, UCI1]), header='t_d(%s) UCI1' % xlab)
        np.savetxt('%s/UCI2.dat' % sPath,
                        np.transpose([t_d, UCI2]), header='t_d(%s) UCI2' % xlab)
        np.savetxt('%s/UCI3.dat' % sPath,
                        np.transpose([t_d, UCI3]),header='t_d(%s) UCI3' % xlab)

        # A.W. Clowes 1985, Significance of Quiescent Smooth Muscle Cells Mig..
        timesClowes = np.array([0., 3., 7., 14.]) / normTime
        # data rovering with information from article and the tool :
        # https://apps.automeris.io/wpd/index.fr_FR.html
        dataClowes = np.array([100.0, 56., 18., 13.])
        erreurClowes = np.array([0, 1.5, 3, 1])

        np.savetxt('%s/UCIClowes.dat' % sPath,
                            np.transpose([timesClowes,dataClowes]),
                            header='t_d(%s) UCI_Clowes' % xlab)

        plt.figure()
        plt.plot(t_d, UCI0 * 100, label=r'UCI0 Q(t_d)/Q(t0) in CMBBE')
        plt.plot(t_d, UCI1 * 100, label=r'UCI1 = $Q/(S+Q)$')
        plt.plot(t_d, UCI2 * 100, label=r'UCI2 = $Q/(\\Delta S+Q)$')
        plt.plot(t_d, UCI3 * 100, label=r'UCI3 = $Q/(S+Q) + UCI_0$')
        plt.errorbar(timesClowes, dataClowes, yerr=erreurClowes,
                     fmt='o-', capsize=5, elinewidth=3,
                     markeredgewidth=4, label=r'Clowes data $[8]$')
        plt.xticks()
        plt.yticks()
        plt.xlabel('%s' % xlab)
        plt.ylabel(r'unlabel cell index (\%)')
        plt.legend()
        # plt.xlim(0, 14)
        # plt.savefig('%s/comparaisonClowes' % sPath, bbox_inches="tight")
        plt.close()


        R_nd = (Rext - eTot) / R0
        arg_Rmin = np.argmin(R_nd)
        print('min(R(t) = {:.4e} at t = {}'.format(np.min(R_nd), t_d[arg_Rmin]))

        # data Clowes 1983 Mechanisms Of Stenosis After Arterial Injury
        t_Clowes = np.array([7.0, 14.0, 28.0, 56.0, 84.0])  / normTime
        reducLumen = np.array([9.455958549222794,24.222797927461144,
                                38.21243523316062,45.33678756476684,
                                38.471502590673566]) * 1e-2
        R_ClowesRedu = 1 - reducLumen
        # write
        np.savetxt('%s/R_nd.dat' % sPath, np.transpose([t_d, R_nd]),
                header='t_d(%s) R(t_d)/R0' % xlab)
        np.savetxt('%s/R_clowesRedu.dat' % sPath,
                np.transpose([t_Clowes, R_ClowesRedu]), header='t_d(%s) RClowes' % xlab)
        np.savetxt('%s/R_events.dat' % sPath,
                np.transpose([t_gene, R_gene / R0]), header='t_d(%s) R_ev(t_d)/R0' % xlab)
        #R_nd_gene = (Rext - geomC_gene[-1]) / R0

        fig, ax = plt.subplots()
        plt.grid(False)
        ax.plot(t_d, R_nd, 'C0', label='$R_l$')
        ax.plot(t_gene, R_gene / R0, 'o', color='r', label='update')
        # ax.plot(t_Clowes, R_ClowesRedu, 'o', label='$R_l$ Clowes 1983')
        #ax.plot(t_gene, R_nd_gene,'r o')
        ax.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
        ax.set_xlabel('%s' % xlab)
        ax.set_ylabel(r'$R_l/R_0$ $[-]$ ')
        ax.legend(loc='upper right')
        plt.savefig('%s/rayon' % sPath, bbox_inches="tight")

        ax.plot(t_d, R_nd*0, '--', label='R=0')
        plt.savefig('%s/rayon_0' % sPath, bbox_inches="tight")
        plt.close()


        D = 8e-10
        D_days = D / (1. / (24. * 3600.))
        eTot_event = Rext - R_gene
        JJ = eTot_event[1:]**2 / (np.diff(t_gene * normTime) * D_days)
        # print('normTime', normTime, 't_gene', t_gene, 't_gene * normTime', t_gene * normTime)
        fig, ax = plt.subplots()
        #ax.plot(interv_sort, JJ_sort, 'C0', label='$JJ = e^2/(T_{gr}D)$')
        ax.loglog(t_gene[1:], JJ, 'o-', label='$JJ = e^2/(\\Delta T_{gene}D)$')
        ax.legend(loc='upper right')
        ax.set_xlabel('$t_{gene}$ in %s' % xlab)
        ax.set_ylabel('JJ')
        # ax.set_title('max(JJ) = %s' % (np.max(JJ)))
        # ax.set_title('max JJ = %s' % JJ.max())
        plt.savefig('%s/JJ_dimensiolessNumber' % sPath, bbox_inches="tight")
        plt.close()
        np.savetxt('%s/JJ_dimensiolessNumber.dat' % sPath,
                np.transpose([t_gene[1:], JJ]), header='t_gene[1:](%s) JJ' % xlab)

        # nbr cell in neointima Nbr_i - Nbr_i(t0)
        nbrCi = Qi + Si
        nbrCm = Qm + Sm
        nbrCim = nbrCi + nbrCm
        nbrCneoi = nbrCi - nbrCi[0]

        fig, ax = plt.subplots()
        plt.grid(False)
        ax.set_title('Cross sec cell nbr Clowes 83 Table 3')
        ax.plot(tRatTab1a_Schwartz96, CRatTab1a_Schwartz96, 'o-',label='Clowes Kine I 83')
        nbr_ax = ax.twinx()
        nbr_ax.plot(t_d,   nbrCim , label='(Si+Qi + Sm + Qm)')
        ax.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
        ax.set_xlim(0.0, np.max(tRatTab1a_Schwartz96))
        ax.set_xlabel('%s' % xlab)
        ax.set_ylabel(r'cells/mm')
        ax.legend(loc='upper right')
        nbr_ax.legend(loc='lower right')
        nbr_ax.set_ylabel(r'Si+Qi - (Si(t0)+Qi(t0)) cells in %s m'% L)
        fig.savefig('%s/nbrCellsTotClowes' % sPath, bbox_inches="tight")

        fig, ax = plt.subplots()
        plt.grid(False)
        ax.set_title('Comparaison intimal cell nbr and Schwartz 96 Table 1b 2 3')
        ax.plot(tPorcineTab2_Schwartz96, CPorcineTab2_Schwartz96, 'o-',label='Porc')
        ax.plot(tRatTab1b_Schwartz96, CRatTab1b_Schwartz96, 'o-', label='Rat')
        ax.plot(tHumanTab3_Schwartz96, CHumanTab3_Schwartz96, 'o-', label='Human')
        nbr_ax = ax.twinx()
        nbr_ax.plot(t_d, nbrCneoi)#, label='Si+Qi - Si(t0)+Qi(t0)')
        ax.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
        ax.set_xlim(0.0, np.max(tHumanTab3_Schwartz96))
        ax.set_xlabel('%s' % xlab)
        ax.set_ylabel(r'cells')
        ax.legend(loc='upper right')
        nbr_ax.legend(loc='lower right')
        nbr_ax.set_ylabel(r'Si+Qi - (Si(t0)+Qi(t0)) cells')
        fig.savefig('%s/nbrCells' % sPath, bbox_inches="tight")

        if set_params['model'].count('Pl') > 0:
            plt.figure()
            plt.title("verifier les dim de Sw")
            plt.plot(t_d, P / Sw, label='P / Sw')
            plt.plot(tP_humphrey, rhoP, label='Pla depo kinetic Humphrey Book')
            plt.legend()
            plt.xlabel('%s' % xlab)
            plt.ylabel('Platelet density [Pla/cm2]')
            # plt.title('MoPhy = %s MaPhy = %s' % (MoPhy, MaPhy))
            plt.savefig('%s/PlateletVsHumphreyData' % sPath)


    elif set_params['model'] == 'Donadoni_1D':
        NGFs = len(set_params['GFs_nd'])

        N = len(popC)
        M = len(geomC)
        f, axarr = plt.subplots(1)
        for k in range(M):
            for j in range(len(t_events)):
                axarr.axvline(x=t_events[j], color='g')
            axarr.plot(t_d, geomC[k], label=r'$%s$' % set_params['geom'][k])
        axarr.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
        axarr.legend(bbox_to_anchor=(1.02, 0.4),
                     loc="center left", borderaxespad=0.)
        axarr.set_xlabel('%s' % xlab)
        axarr.set_ylabel('thickness [?]')
        plt.savefig('%s/bilan' % sPath, bbox_inches="tight")
        plt.close()

        f, axarr = plt.subplots(N, sharex=True)
        for i in range(N):
            for j in range(len(t_events)):
                axarr[i].axvline(x=t_events[j], color='g')
            #axarr[i].plot(t_events, popC_events[:,i], 'r o',)
            axarr[i].plot(t_d, popC[i], 'C0', label=r'$%s$' % set_params['species_nd'][i])
            axarr[i].ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
            axarr[i].legend(bbox_to_anchor=(1.02, 0.4),
                            loc="center left", borderaxespad=0.)
        axarr[-1].set_xlabel('%s' % xlab)
        f.subplots_adjust(left=None, bottom=None, right=None, top=None,
                          wspace=None, hspace=0.4)
        plt.savefig('%s/species' % sPath, bbox_inches="tight")
        plt.close()

        (Rext, R_LEE, R0, L) = set_params['geom_dim']
        R_nd = (Rext - geomC[-1]) / R0
        #R_nd_gene = (Rext - geomC_gene[-1]) / R0

        fig, ax = plt.subplots()
        plt.grid(False)
        for j in range(len(t_events)):
            ax.axvline(x=t_events[j], color='g')
        ax.plot(t_d, R_nd, 'C0', label='$R_w$')
        #ax.plot(t_gene, R_nd_gene,'r o')
        ax.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
        ax.set_xlabel('%s' % xlab)
        ax.set_ylabel(r'$R_w/R^0$ ')
        ax.legend(loc='upper right')
        plt.savefig('%s/rayon' % sPath, bbox_inches="tight")

        ax.plot(t_d, R_nd*0, '--', label='R=0')
        plt.savefig('%s/rayon_0' % sPath, bbox_inches="tight")
        plt.close()
    else:
        print("model no handle by the code")
        sys.exit(2)
    return

def plottingVf_1D(iniF, path_abs, set_params, t0=None, tf=None, setting_t=('day',1.0),
               folder='', dpi=100):
    sPath = '%s/%s/Vfs' % (path_abs, folder)
    try:
        os.mkdir(sPath)
    except OSError:
        print ("Creation of the directory \n %s \nfailed" % sPath )
    else:
        print ("Successfully created the directory \n %s " % sPath)

    xlab, normTime = setting_t
    pathPop = '%s/allGenes/tissueGrowth/Pop' % (path_abs)
    pathGlobal = '%s/allGenes/tissueGrowth/global' % (path_abs)
    t_d = np.load('%s/t_d.npy' % pathGlobal)
    t_events = np.load('%s/t_events.npy' % pathGlobal)
    (i_t0, i_tf, iG_t0, iG_tf) = findIndex_t0tf(t_d, t_events, t0, tf)
    t_d = t_d[i_t0:i_tf] / normTime
    t_events = t_events[iG_t0:iG_tf] / normTime

    VfC = []

    for i in range(len(set_params['Vf'])):
        VfC.append(np.load('%s/%s.npy' %
                           (pathPop, set_params['Vf'][i]))[i_t0:i_tf])

    if set_params['model'][:6] == 'Jansen':
        if set_params['model'].count('Infla') > 0:
            # 'Vf' :
            # ('Vf_Ei', 'Vf_Qi', 'Vf_Qm', 'Vf_Si', 'Vf_Sm',
            # 'Vf_Ci', 'Vf_Cm', 'Vf_Oi', 'Vf_Om', 'Vf_m, 'Vf_M'),
            Vf_i = [VfC[0], VfC[1], VfC[3], VfC[5], VfC[7], VfC[9], VfC[10]]
            Vf_m = [VfC[2], VfC[4], VfC[6], VfC[8]]
            # add the sum of all volume fraction at Vf_i and Vf_m
            Vf_i.append(np.sum(Vf_i, axis=0))
            Vf_m.append(np.sum(Vf_m, axis=0))
            color_i = ['y', 'C0', 'C1', 'r', 'g', 'c', 'C2', 'k']
            color_m = ['C0', 'r',  'g', 'c', 'k']
            lgd_i = ['\\gamma_{E}',  '\\gamma_{cSMC}', '\\gamma_{sSMC}',
                        '\\gamma_{C}', '\\gamma_{O_i}', '\\gamma_{m}', '\\gamma_{M}',
                        '\\sum_{k} Vf_{k}']
            lgd_m = ['\\gamma_{cSMC}', '\\gamma_{sSMC}',
                        '\\gamma_{C}', '\\gamma_{O_m}', '\\sum_{k} \\gamma_{k}']
        else:
            # model without inflmmation
            # 'Vf' :
            # ('Vf_Ei', 'Vf_Qi', 'Vf_Qm', 'Vf_Si', 'Vf_Sm',
            # 'Vf_Ci', 'Vf_Cm', 'Vf_Oi', 'Vf_Om'),
            Vf_i = [VfC[0], VfC[1], VfC[3], VfC[5], VfC[7]]
            Vf_m = [VfC[2], VfC[4], VfC[6], VfC[8]]
            # add the sum of all volume fraction at Vf_i and Vf_m
            Vf_i.append(np.sum(Vf_i, axis=0))
            Vf_m.append(np.sum(Vf_m, axis=0))
            color_i = ['y', 'C0', 'r', 'g', 'c', 'k']
            color_m = ['C0', 'r',  'g', 'c', 'k']

            lgd_i = ['\\gamma_{E}', '\\gamma_{cSMC}', '\\gamma_{sSMC}',
                        '\\gamma_{C}', '\\gamma_{O_i}', '\\sum_{k} \\gamma_{k}']
            lgd_m = ['\\gamma_{cSMC}', '\\gamma_{sSMC}',
                        '\\gamma_{C}', '\\gamma_{O_m}', '\\sum_{k} \\gamma_{k}']

        Vf_Ci = VfC[5]
        Vf_vSMCi = VfC[1] + VfC[3]
        Vf_Cm = Vf_i[2]
        Vf_vSMCm = Vf_m[0] + Vf_m[1]

    elif set_params['model'] == 'Donadoni_1D':
        lgd_i = ['\\gamma_{cSMC}', '\\gamma_{sSMC}', '\\gamma_{C}', '\\gamma_{O_i}',
                 '\\sum_{k} Vf_{k}']
        lgd_m = ['\\gamma_{cSMC}', '\\gamma_{sSMC}', '\\gamma_{C}', '\\gamma_{O_m}',
                '\\sum_{k} \\gamma_{k}']
        color_i = ['C0', 'r',  'g', 'c', 'k']
        color_m = ['C0', 'r',  'g', 'c', 'k']
    else:
        print('model not handle by the code')
        sys.exit(2)

    Ni = len(Vf_i)
    Nm = len(Vf_m)
    print("len(lgd_i) %s len(Vf_i) %s len(color_i) %s" %
            (len(lgd_i), len(Vf_i), len(color_i)))
    if len(lgd_i) != len(Vf_i) or len(color_i) != len(Vf_i):
        raise ValueError("len(lgd_i) %s len(Vf_i) %s len(color_i) %s" %
                (len(lgd_i), len(Vf_i), len(color_i)))
    if len(lgd_m) != len(Vf_m) or len(color_m) != len(Vf_m):
        raise ValueError("len(lgd_m) %s len(Vf_m) %s len(color_m) %s" %
                (len(lgd_m), len(Vf_m), len(color_m)))
    f, axarr = plt.subplots(2, sharex=True)
    for i in range(Ni):
        axarr[0].plot(t_d, Vf_i[i],
                      color_i[i], label=r'$%s$' % lgd_i[i])
        axarr[0].legend(bbox_to_anchor=(1.02, .6),
                        loc="center left", borderaxespad=0.)
        axarr[0].set_title('intima layer')
    for i in range(Nm):
        axarr[1].plot(t_d, Vf_m[i],
                      color_m[i], label=r'$%s$' % lgd_m[i])
        axarr[1].legend(bbox_to_anchor=(1.02, .6),
                        loc="center left", borderaxespad=0.)
        axarr[1].set_title('media layer')
        axarr[1].set_xlabel('%s' % xlab)
    f.subplots_adjust(left=None, bottom=None,
                      right=None, top=None,
                      wspace=None, hspace=0.4)
    plt.savefig('%s/Vf' % sPath, bbox_inches="tight")
    plt.close()
    # # write readable data file
    # for i in range(Ni):
        # np.savetxt('%s/%s' % (sPath, lgd_i[i]), np.transpose([t_d, Vf_i[i]]))
    # for i in range(Nm):
        # np.savetxt('%s/%s' % (sPath, lgd_m[i]), np.transpose([t_d, Vf_m[i]]))

    # plt.figure()
    # plt.plot(t_d, DeltaFi_C_S, label='$\Delta F_i$')
    # plt.plot(t_d, DeltaFm_C_S, label='$\Delta F_m$')
    # plt.xlabel('%s' % xlab)
    # plt.legend()
    # plt.savefig('%s/fraction_col_sCMLs' % sPath)
    # plt.close()


    plt.figure()
    plt.plot(t_d, Vf_Ci, label=r' $Vf_{C}$')
    plt.plot(t_d, Vf_vSMCi, label=r'$Vf_{Q+S}$')
    plt.xlabel('%s' % xlab)
    plt.title(r'intima')
    plt.ylabel(r'lesion composition $(\%)$')
    plt.legend()
    plt.savefig('%s/compositionLesion_i' % sPath)
    plt.close()
    # write readable data file
    np.savetxt('%s/Vf_ECM_i.dat' % sPath, np.transpose([t_d, Vf_Ci]))
    np.savetxt('%s/Vf_vSMCs_i.dat' % sPath, np.transpose([t_d, Vf_vSMCi]))

    plt.figure()
    plt.plot(t_d, Vf_Cm, label=r'$Vf_{C}^{m}$')
    plt.plot(t_d, Vf_vSMCm, label=r'$Vf_{S+Q}^{m}$')
    plt.xlabel('%s' % xlab)
    plt.title(r'media')
    plt.ylabel(r'lesion composition $(\%)$')
    plt.legend(bbox_to_anchor=(1.02, .6), loc="center left", borderaxespad=0.)
    plt.savefig('%s/compositionLesion_m' % sPath,
                bbox_inches="tight")
    plt.close()
    # write readable data file
    np.savetxt('%s/Vf_ECM_m.dat' % sPath, np.transpose([t_d, Vf_Cm]))
    np.savetxt('%s/Vf_vSMCs_m.dat' % sPath, np.transpose([t_d, Vf_vSMCm]))
    return

def plottingSourceTerms_1D(iniF, path_abs, set_params, t0=None, tf=None, setting_t=('day',1.0),
                        folder = ''):
    """
    """
    sPath = '%s/%s/sourceTerms' % (path_abs, folder)
    try:
        os.mkdir(sPath)
    except OSError:
        print ("Creation of the directory \n %s \nfailed" % sPath )
    else:
        print ("Successfully created the directory \n %s " % sPath)

    xlab, normTime = setting_t
    pathGFs = '%s/allGenes/tissueGrowth/GFs' % (path_abs)
    pathGlobal = '%s/allGenes/tissueGrowth/global' % (path_abs)
    t_d = np.load('%s/t_d.npy' % pathGlobal)
    # print('t_d.shape', t_d.shape)
    t_gene = np.load('%s/t_gene.npy' % pathGlobal)
    (i_t0, i_tf, iG_t0, iG_tf) = findIndex_t0tf(t_d, t_gene, t0, tf)
    t_d = t_d[i_t0:i_tf] / normTime
    t_gene = t_gene[iG_t0:iG_tf] / normTime

    M_d = np.load('%s/sourceTerms.npy' % pathGFs)[:,:,i_t0:i_tf]
    # print('M_d.shape', M_d.shape)
    N = len(set_params['GFs_nd']) #  14
    m = 0
    lgd = [['{M_d}^{NO}_i','{M_d}^{NO}_m'],
           ['{M_d}^{PDGF}_i','{M_d}^{PDGF}_m'],
           ['{M_d}^{FGF}_i','{M_d}^{FGF}_m'],
           ['{M_d}^{Ag}_i','{M_d}^{Ag}_m'],
           ['{M_d}^{TGF}_i','{M_d}^{TGF}_m'],
           ['{M_d}^{TNF}_i','{M_d}^{TNF}_m'],
           ['{M_d}^{MMP}_i','{M_d}^{MMP}_m']]
    f, axarr = plt.subplots(int(N * .5), sharex=True)
    for j in range(0, int(N * .5)):
        for i in range(2):
            axarr[j].plot(t_d, M_d[j,i,:], 'r', label=r'$%s$' % lgd[j][i])
        axarr[j].legend(bbox_to_anchor=(1.02, .6),
                            loc="center left", borderaxespad=0.)
    axarr[-1].set_xlabel('%s' % xlab)
    f.subplots_adjust(left=None, bottom=None, right=None, top=None,
                      wspace=None, hspace=0.4)
    plt.savefig('%s/sourceTerms' % sPath, bbox_inches="tight")
    plt.close()
    # write readable data files
    for j in range(0, int(N * .5)):
        for i in range(2):
            np.savetxt('%s/%s.dat' % (sPath, lgd[j][i]), np.transpose([t_d, M_d[j,i,:]]))

    P_E= np.load('%s/P_E.npy' % pathGFs)[:,i_t0:i_tf]
    R = np.load('%s/R.npy' % pathGFs)[:,i_t0:i_tf]
    P_E_d = np.load('%s/P_E_d.npy' % pathGFs)[:,i_t0:i_tf]
    R_d = np.load('%s/R_d.npy' % pathGFs)[:,i_t0:i_tf]

    lgd_P_E = ['{P_E}_{NO}', '{P_E}_{PDGF}', '{P_E}_{FGF}',
           '{P_E}_{Ag}', '{P_E}_{TGF}', '{P_E}_{TNF}',
           '{P_E}_{MMP}']
    lgd_R = ['{R}_{NO}', '{R}_{PDGF}', '{R}_{FGF}',
           '{R}_{Ag}', '{R}_{TGF}', '{R}_{TNF}',
           '{R}_{MMP}']

    f, axarr = plt.subplots(int(N * .5), sharex=True)
    for i in range(0, int(N * .5)):
        # print('t_d.shape', t_d.shape, 'B_star_d[i,:].shape', B_star_d[i,:].shape)
        axarr[i].plot(t_d, R_d[i,:], 'C1', label=r'$%s$ _d' % lgd_R[i])
        axarr[i].legend(bbox_to_anchor=(1.02, .6),
                        loc="center left", borderaxespad=0.)
    axarr[-1].set_xlabel('%s' % xlab)
    f.subplots_adjust(left=None, bottom=None, right=None, top=None,
                      wspace=None, hspace=0.4)
    plt.savefig('%s/R_GFs_d' % sPath, bbox_inches="tight")
    f, axarr = plt.subplots(int(N * .5), sharex=True)
    for i in range(0, int(N * .5)):
        # print('t_d.shape', t_d.shape, 'B_star_d[i,:].shape', B_star_d[i,:].shape)
        axarr[i].plot(t_d, P_E_d[i,:], 'C0', label=r'$%s$ _d' % lgd_P_E[i])
        axarr[i].legend(bbox_to_anchor=(1.02, .6),
                        loc="center left", borderaxespad=0.)
    axarr[-1].set_xlabel('%s' % xlab)
    f.subplots_adjust(left=None, bottom=None, right=None, top=None,
                      wspace=None, hspace=0.4)
    plt.savefig('%s/P_E_GFs_d' % sPath, bbox_inches="tight")

    f, axarr = plt.subplots(int(N * .5), sharex=True)
    for i in range(0, int(N * .5)):
        # print('t_d.shape', t_d.shape, 'B_star_d[i,:].shape', B_star_d[i,:].shape)
        axarr[i].plot(t_d, P_E[i,:], 'C0', label=r'$%s$' % lgd_P_E[i])
        axarr[i].legend(bbox_to_anchor=(1.02, .6),
                        loc="center left", borderaxespad=0.)
    axarr[-1].set_xlabel('%s' % xlab)
    f.subplots_adjust(left=None, bottom=None, right=None, top=None,
                      wspace=None, hspace=0.4)
    plt.savefig('%s/P_E_GFs' % sPath, bbox_inches="tight")

    f, axarr = plt.subplots(int(N * .5), sharex=True)
    for i in range(0, int(N * .5)):
        # print('t_d.shape', t_d.shape, 'B_star_d[i,:].shape', B_star_d[i,:].shape)
        axarr[i].plot(t_d, R[i,:], 'C1', label=r'$%s$' % lgd_R[i])
        axarr[i].legend(bbox_to_anchor=(1.02, .6),
                        loc="center left", borderaxespad=0.)
    axarr[-1].set_xlabel('%s' % xlab)
    f.subplots_adjust(left=None, bottom=None, right=None, top=None,
                      wspace=None, hspace=0.4)
    plt.savefig('%s/R_GFs' % sPath, bbox_inches="tight")
    plt.close()

    B_GFs_d =  np.load('%s/B_GFs_d.npy' % pathGFs)[:, i_t0:i_tf]
    B_GFs =  np.load('%s/B_GFs.npy' % pathGFs)[:, i_t0:i_tf]
    # print('B_GFs.shape', B_GFs.shape)
    physioValue = B_GFs[:,0]
    B_star_d = np.divide(B_GFs_d.T,physioValue).T
    percent_R_d = np.divide(R_d.T,physioValue).T
    percent_P_E_d = np.divide(P_E_d.T,physioValue).T

    lgd = ['B^{*}_{NO}', 'B^{*}_{PDGF}', 'B^{*}_{FGF}',
           'B^{*}_{Ag}', 'B^{*}_{TGF}', 'B^{*}_{TNF}',
           'B^{*}_{MMP}']

    f, axarr = plt.subplots(int(N * .5), sharex=True)
    # print('t_d.shape', t_d.shape)
    # print('B_star_d.shape', B_star_d.shape)
    # print('N',N)
    for i in range(0, int(N * .5)):
        # print('t_d.shape', t_d.shape, 'B_star_d[i,:].shape', B_star_d[i,:].shape)
        axarr[i].plot(t_d, B_star_d[i,:], 'C0', label=r'$%s$' % lgd[i])
        axarr[i].legend(bbox_to_anchor=(1.02, .6),
                        loc="center left", borderaxespad=0.)
    axarr[-1].set_xlabel('%s' % xlab)
    f.subplots_adjust(left=None, bottom=None, right=None, top=None,
                      wspace=None, hspace=0.4)
    plt.savefig('%s/BC_star' % sPath, bbox_inches="tight")
    plt.close()
    # plt.show()
    return

def plottingWSS_1D(iniF, path_abs, set_params, t0=None, tf=None,
        setting_t=('day',1.0), folder='', dpi=100):
    """
    get the wss plot against generation
    """
    print('plottingWSS_1D')
    sPath = '%s/%s/WSS' % (path_abs, folder)
    try:
        os.mkdir(sPath)
    except OSError:
        print ("Creation of the directory \n %s \nfailed" % sPath )
    else:
        print ("Successfully created the directory \n %s " % sPath)

    pathGlobal = '%s/allGenes/tissueGrowth/global' % (path_abs)

    t_gene = np.load('%s/t_gene.npy' % pathGlobal)
    WSS = np.load('%s/wssR.npy' % pathGlobal)
    gene = np.array([i for i in range(len(WSS))])
    # print('WSS', type(WSS))
    np.savetxt('%s/WSS_gene.dat' % (sPath), np.vstack((gene, t_gene, WSS)).T,
            header='gene \t t_gene \t WSS')

    plt.figure()
    plt.plot(t_gene, WSS, 'o', label=r'$\tau_p$')
    plt.xlabel('t in days')
    plt.ylabel(r'WSS (Pa)')
    plt.legend(bbox_to_anchor=(1.02, .6), loc="center left", borderaxespad=0.)
    plt.savefig('%s/WSS_t' % sPath,
            bbox_inches="tight")

    plt.figure()
    plt.plot(gene, WSS, 'o', label=r'$\tau_p$')
    plt.xlabel('generation')
    plt.ylabel(r'WSS (Pa)')
    plt.legend(bbox_to_anchor=(1.02, .6), loc="center left", borderaxespad=0.)
    plt.savefig('%s/WSS_gene' % sPath,
            bbox_inches="tight")

    return
