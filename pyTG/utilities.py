#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import pickle
import numpy.ma as ma
import numpy as np
import shutil
import glob
from warnings import warn
from scipy.interpolate import interp1d, interp2d, griddata
from pyevtk.hl import gridToVTK
from pyevtk.hl import unstructuredGridToVTK
from pyevtk.vtk import VtkGroup, VtkHexahedron
from scipy.special import k1, i1, k0, i0

from pyTG.fluid import Newtonian, Quemada, WalburnScheck, Casson,\
    AVAIL_HEMORHEO_MODELS
from pyTG.mesh import generateTCO_TCX


def save_gnuplot(X, Y, Z, path, name, header="# X Y Z"):
    """ Save 2D data in gnuplot convension 

    Parameters
    ----------
    X : np.ndarray of shape (Nx, Ny)
        X grid
    Y : np.ndarray of shape (Nx, Ny)
        Y grid
    Z : np.ndarray of shape (Nx, Ny)
        Z values
    path : str
        Dict to save
    name : str
        name

    Returns
    -------

    """
    (Nx, Ny) = X.shape
    # save matrix S
    with open('{}/{}'.format(path, name), 'w') as f:
        f.write(header)
        for i in range(Ny):
            f.write('\n')
            for j in range(Nx):
                f.write('{} {} {}\n'.format(X[j,i], Y[j,i], Z[j,i]))
    return

def save_dict_to_file(dic, name):
    """

    Parameters
    ----------
    dict : dict
        Dict to save
    name : str
        name

    Returns
    -------
    """
    f = open(name,'w')
    f.write(str(dic))
    f.close()

def load_dict_from_file(name):
    """

    Parameters
    name : str
        name

    Returns
    dict 
    """
    f = open(name,'r')
    data=f.read()
    f.close()
    return eval(data)



def generateLayers3DCylVTK(pathToVTK, set_params, xP, thP, xE_ghost, thE_extend,
        R_l_g, R_LEI_g, t_interp, Y_interp_nd, tupleCst_interp):
    """ Use unstructuredGridToVTK grid from pyevtk to export simulation results
        and visualisation in ParaView.


    Parameters
    ----------
    pathToVTK : str
        The path where file will be saved
    set_params : dict
        The dictionnary
    xP : np.ndarray of shape (Np_xTG)
        The x coordinate of TG vertex
    thP : np.ndarray of shape (Np_th_TG)
        The theta angle of TG vertex
    xE_ghost : np.ndarray of shape (Ni_x_TG + 2)
        The x coordinate of TG center of compartiment + 2 ghost cells outside
        domain
    thE_period : np.ndarray of shape (Ni_x_TG + 2)
        The theta angle of TG center of compartiment extended by ``periodicity``
    R_l_g : np.ndarray of shape (Nt, Ni_TG)
        Values of radius at the position xE
    R_LEI_g : np.ndarray of shape (Nt, Ni_TG)
        Values of internal lamina radius at the position xE
    t_interp : np.ndarray of shape (Nt,)
        time array
    Y_interp_nd : np.ndarray of shape (Neq, Nt, Ni_TG)
        Result of integration
    tupleCst_nd : np.ndarray of shape (N_cst, Nt, Np_x)
        Species functional properties rate of ODEs-DDEs in [1/d] as
        (p_i, p_m, a_i,a_m, m_i, m_m, l_i, l_m,
            chi_i, chi_m, c_i, c_m) = Cst_interp


    Returns
    -------

    """
    (p_i, p_m, a_i, a_m, m_i, m_m, l_i, l_m,
             chi_i, chi_m, c_i, c_m) = tupleCst_interp

    Np_xT_TG = len(xP)
    Np_th_TG = len(thP)
    Ni_x_TG = Np_xT_TG - 1
    Np_W_TG = Np_th_TG * Np_xT_TG
    Nf_W_TG = Np_th_TG * Ni_x_TG

    # 4 pts in radial direction as 3 layers
    Np_rTG = 4
    # number of compartiment, where a compartiment is i + m + a
    Ni_TG = Nf_W_TG * 1
    Ncells = Ni_TG * (Np_rTG - 1)
    # print('Np_xT_TG = {}, Ni_x_TG {}, Np_th_TG {}, Np_W_TG {}, Nf_W_TG {}, Ni_TG {}, Ncells {}'.format(Np_xT_TG, Ni_x_TG, Np_th_TG, Np_W_TG, Nf_W_TG, Ni_TG, Ncells))
    # index of points which are concern by update of R_LEI, R_l
    idxOfInterest = [2, 3, 6, 7]

    # print('Shapes R_l_g {} R_LEI_g {} Yinterp {}'.format(
        # R_l_g.shape, R_LEI_g.shape, Y_interp_nd.shape))
    if R_l_g.shape[1] != Ni_TG or R_LEI_g.shape[1] != Ni_TG:
        raise ValueError('R_l_g.shape[1] ({}) != Ni_TG ({}) or ({})'.format(
            R_l_g.shape[1], Ni_TG, R_LEI_g.shape[1]))

    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    R_LEI0 = R_LEI_g[0,0]
    # print('Rext {}, R_LEE {}, R_LEI0 {}, R0 {}'.format(Rext, R_LEE, R_LEI0, R0))
    # def offsets
    offsets = np.zeros(Ncells,dtype=np.int64)
    for i in range(Ncells):
        # we have hexahedron so 8 points !
        offsets[i] = (i+1) * 8
    # def cell type
    cell_type = np.zeros(Ncells, dtype=int) + VtkHexahedron.tid
    # def id_layers which not depend on time
    id_layers = np.zeros(Ncells, dtype=int)
    id_layers[:Ni_TG] = 3
    id_layers[Ni_TG:2*Ni_TG] = 2
    id_layers[2*Ni_TG:3*Ni_TG] = 1

    Nt = len(t_interp)
    # generate the TCO & TCX for physiological state of the artery
    ptsR_l = np.zeros((Np_W_TG, 4))
    ptsR_LEI = np.zeros((Np_W_TG, 4))
    ptsR_LEE = np.zeros((Np_W_TG, 4))
    ptsRext = np.zeros((Np_W_TG, 4))
    for i in range(Np_xT_TG):
        xNp_th = i * Np_th_TG
        X_x_i = np.ones(Np_th_TG) * xP[i]

        yRext, zRext = np.cos(thP) * Rext, np.sin(thP) * Rext
        yR_LEE, zR_LEE = np.cos(thP) * R_LEE, np.sin(thP) * R_LEE
        yR_LEI, zR_LEI = np.cos(thP) * R_LEI0, np.sin(thP) * R_LEI0
        yR_l, zR_l = np.cos(thP) * R0, np.sin(thP) * R0
        
        # print('i', i, '')
        ptsRext[xNp_th:(xNp_th + Np_th_TG)] = np.vstack((X_x_i, yRext,
                                zRext, thP)).T
        ptsR_LEE[xNp_th:(xNp_th + Np_th_TG)] = np.vstack((X_x_i, yR_LEE,
                                zR_LEE, thP)).T
        ptsR_LEI[xNp_th:(xNp_th + Np_th_TG)] = np.vstack((X_x_i, yR_LEI,
                                zR_LEI, thP)).T
        ptsR_l[xNp_th:(xNp_th + Np_th_TG)] = np.vstack((X_x_i, yR_l,
                                zR_l, thP)).T

    compartXYZT = np.vstack((ptsRext, ptsR_LEE,
                        ptsR_LEI, ptsR_l)).reshape(Np_rTG, Np_W_TG, 4)
    TCO_phy, TCX_phy = generateTCO_TCX(Np_rTG, Np_xT_TG, Np_th_TG, compartXYZT)
    # np.save("generateLayers3D_TCO_phy.npy", TCO_phy)
    # np.save("generateLayers3D_TCX_phy.npy", TCX_phy)

    # out = unstructuredGridToVTK('testVTK',
            # np.ascontiguousarray(TCO_phy[:,0]),
            # np.ascontiguousarray(TCO_phy[:,1]),
            # np.ascontiguousarray(TCO_phy[:,2]),
            # TCX_phy.flatten(), offsets, cell_type)

    # TCX_ and TCO_ will be table which will change of time
    TCX_ = TCX_phy.copy()
    TCO_ = TCO_phy.copy()
    # get the table of con of each layers of compartiment
    TCX_adv = TCX_[:Ni_TG]
    TCX_m = TCX_[Ni_TG:2*Ni_TG]
    TCX_i = TCX_[2*Ni_TG:3*Ni_TG]

    # dir creation
    if not os.path.exists(pathToVTK):
        os.makedirs(pathToVTK)

    group = VtkGroup("%s/layers" % pathToVTK)
    for i in range(Nt):
        # print('\n\n new time to save as VTK')
        filepath = "%s/layers_%s" % (pathToVTK, i)
        # print('R0', R0, 'R_LEI0', R_LEI0)
        # print('R_l_g[i,:]', R_l_g[i,:])
        # print('R_LEI_g[i,:]', R_LEI_g[i,:])
        # interpolate values at nodes from center of compartiments
        # need to modify the TCO_

        # create ghost data for R_l and R_LEI
        R_l_extend = np.zeros((Np_th_TG + 2, Ni_x_TG + 2))
        R_l_g_i_resh = R_l_g[i,:].reshape(Ni_x_TG, Np_th_TG).T
        R_l_extend[1:-1, 1:-1] = R_l_g_i_resh
        # ghost cells in x outside domain
        R_l_extend[:, 0] = R0
        R_l_extend[:, -1] = R0
        # the first is the last
        # put all R to R0 (as in ghost cells)
        R_l_extend[0, :] = R0
        # then, get the R_gr_reshape[-1,:]
        R_l_extend[0, 1:-1] = R_l_g_i_resh[-1,:]
        # put all R to R0 (as in ghost cells)
        R_l_extend[-1, :] = R0
        # then, get the R_gr_reshape[0,:]
        R_l_extend[-1, 1:-1] = R_l_g_i_resh[0,:]


        R_LEI_g_extend = np.zeros((Np_th_TG + 2, Ni_x_TG + 2))
        R_LEI_g_i_resh = R_LEI_g[i,:].reshape(Ni_x_TG, Np_th_TG).T
        R_LEI_g_extend[1:-1, 1:-1] = R_LEI_g_i_resh
        R_LEI_g_extend[:, 0] = R_LEI0
        R_LEI_g_extend[:, -1] = R_LEI0
        # the first is the last
        # put all R to R0 (as in ghost cells)
        R_LEI_g_extend[0, :] = R_LEI0
        # then, get the R_gr_reshape[-1,:]
        R_LEI_g_extend[0, 1:-1] = R_LEI_g_i_resh[-1,:]
        # put all R to R0 (as in ghost cells)
        R_LEI_g_extend[-1, :] = R_LEI0
        # then, get the R_gr_reshape[-1,:]
        R_LEI_g_extend[-1, 1:-1] = R_LEI_g_i_resh[0,:]

        # # def cubic interpolation to move data at the center of compartments
        # # to vertex data
        # # print('R_l_extend.shape {}'.format(R_l_extend.shape))
        # # print('xE_ghost.shape {}'.format(xE_ghost.shape))
        # # print('thE.shape {}'.format(thE.shape))
        # # print('Np_th_TG', Np_th_TG)
        # # print('thP', thP)
        # # print('thE', thE)
        fR_l_inter = interp2d(xE_ghost, thE_extend, R_l_extend, kind='cubic',
                bounds_error=True)
        fR_LEI_inter = interp2d(xE_ghost, thE_extend, R_LEI_g_extend, kind='cubic',
                bounds_error=True)

        f_inter = [fR_LEI_inter, fR_l_inter]
        # update for media and intima layers
        for j, TCX_layer in enumerate([TCX_m, TCX_i]):
            # print('0 == media, 1 == intima \nj', j)
            TCX_layer_Pts = TCX_layer[:,idxOfInterest].flatten('F')
            TCX_layer_PtsUni = np.unique(TCX_layer_Pts)
            ptsUni = TCO_[TCX_layer_PtsUni]
            x_arr, y_arr, z_arr, th_arr = ptsUni[:,0], ptsUni[:,1], \
                                            ptsUni[:,2], ptsUni[:,3]
            for k in range(len(th_arr)):
                # print('x_arr[k]', x_arr[k], 'y_arr[k]', y_arr[k], 'z_arr[k]', z_arr[k])
                # print('current R, before update; R_k = ',
                        # y_arr[k] / np.cos(th_arr[k]),
                        # z_arr[k] / np.sin(th_arr[k]))
                R_new = f_inter[j](x_arr[k], th_arr[k])
                # print('R_new', R_new)
                # print('new y = ', np.cos(th_arr[k]) * R_new)
                # print('new z = ', np.sin(th_arr[k]) * R_new)
                TCO_[TCX_layer_PtsUni[k],1] = np.cos(th_arr[k]) * R_new
                TCO_[TCX_layer_PtsUni[k],2] = np.sin(th_arr[k]) * R_new


        # realloc to a=have contiguous arrays
        x_conti = np.ascontiguousarray(TCO_[:,0])
        y_conti = np.ascontiguousarray(TCO_[:,1])
        z_conti = np.ascontiguousarray(TCO_[:,2])

        # NO
        delta_NO   = np.zeros(Ncells)
        delta_NO[:Ni_TG] = np.nan                            # adventica
        delta_NO[Ni_TG:2*Ni_TG] = Y_interp_nd[1, i, :]    # media
        delta_NO[2*Ni_TG:3*Ni_TG] = Y_interp_nd[0, i, :]  # intima

        # PDGF
        delta_PDGF   = np.zeros(Ncells)
        delta_PDGF[:Ni_TG] = np.nan                            # adventica
        delta_PDGF[Ni_TG:2*Ni_TG] = Y_interp_nd[3, i, :]    # media
        delta_PDGF[2*Ni_TG:3*Ni_TG] = Y_interp_nd[2, i, :]  # intima
        # FGF
        delta_FGF  = np.zeros(Ncells)
        delta_FGF[:Ni_TG] = np.nan                            # adventica
        delta_FGF[Ni_TG:2*Ni_TG] = Y_interp_nd[5, i, :]    # media
        delta_FGF[2*Ni_TG:3*Ni_TG] = Y_interp_nd[4, i, :]  # intima
        # Ag
        delta_Ag   = np.zeros(Ncells)
        delta_Ag[:Ni_TG] = np.nan                            # adventica
        delta_Ag[Ni_TG:2*Ni_TG] = Y_interp_nd[7, i, :]    # media
        delta_Ag[2*Ni_TG:3*Ni_TG] = Y_interp_nd[6, i, :]  # intima
        # TGF
        delta_TGF  = np.zeros(Ncells)
        delta_TGF[:Ni_TG] = np.nan                            # adventica
        delta_TGF[Ni_TG:2*Ni_TG] = Y_interp_nd[9, i, :]    # media
        delta_TGF[2*Ni_TG:3*Ni_TG] = Y_interp_nd[8, i, :]  # intima
        # TNF
        delta_TNF  = np.zeros(Ncells)
        delta_TNF[:Ni_TG] = np.nan                            # adventica
        delta_TNF[Ni_TG:2*Ni_TG] = Y_interp_nd[11, i, :]    # media
        delta_TNF[2*Ni_TG:3*Ni_TG] = Y_interp_nd[10, i, :]  # intima
        # MMP
        delta_MMP  = np.zeros(Ncells)
        delta_MMP[:Ni_TG] = np.nan                            # adventica
        delta_MMP[Ni_TG:2*Ni_TG] = Y_interp_nd[13, i, :]    # media
        delta_MMP[2*Ni_TG:3*Ni_TG] = Y_interp_nd[12, i, :]  # intima
        # prod rate of sSMCs
        p_d      = np.zeros(Ncells)
        p_d[:Ni_TG] = np.nan
        p_d[Ni_TG:2*Ni_TG] = p_m[i, :]
        p_d[2*Ni_TG:3*Ni_TG] = p_i[i, :]
        # apop rate of sSMCsNcells
        a_d      = np.zeros(Ncells)
        a_d[:Ni_TG] = np.nan
        a_d[Ni_TG:2*Ni_TG] = a_m[i, :]
        a_d[2*Ni_TG:3*Ni_TG] = a_i[i, :]
        #
        r_d      = p_d - a_d
        # migr
        m_d      = np.zeros(Ncells)
        m_d[:Ni_TG] = np.nan
        m_d[Ni_TG:2*Ni_TG] = m_m[i, :]
        m_d[2*Ni_TG:3*Ni_TG] = m_i[i,:]
        # EMC prod rate
        lS_d      = np.zeros(Ncells)
        lS_d[:Ni_TG] = np.nan
        lS_d[Ni_TG:2*Ni_TG] = l_m[i, :]
        lS_d[2*Ni_TG:3*Ni_TG] = l_i[i, :]
        # EMC degra rate
        chi_d    = np.zeros(Ncells)
        chi_d[:Ni_TG] = np.nan
        chi_d[Ni_TG:2*Ni_TG] = chi_m[i, :]
        chi_d[2*Ni_TG:3*Ni_TG] = chi_i[i, :]
        # switch cSMCs -> sSMCs
        c_d      = np.zeros(Ncells)
        c_d[:Ni_TG] = np.nan
        c_d[Ni_TG:2*Ni_TG] = c_m[i, :]
        c_d[2*Ni_TG:3*Ni_TG]  = c_i[i, :]
        # E_nd
        E_nd     = np.zeros(Ncells)
        E_nd[:Ni_TG] = np.nan
        E_nd[Ni_TG:2*Ni_TG]  = np.nan
        E_nd[2*Ni_TG:3*Ni_TG] = Y_interp_nd[22, i, :]
        # cSMCs
        Q_nd     = np.zeros(Ncells)
        Q_nd[:Ni_TG] = np.nan
        Q_nd[Ni_TG:2*Ni_TG]  = Y_interp_nd[15, i, :]
        Q_nd[2*Ni_TG:3*Ni_TG] = Y_interp_nd[14, i, :]
        # sSMCs
        S_nd     = np.zeros(Ncells)
        S_nd[:Ni_TG]= np.nan
        S_nd[Ni_TG:2*Ni_TG] = Y_interp_nd[17, i, :]
        S_nd[2*Ni_TG:3*Ni_TG] = Y_interp_nd[16, i, :]
        # young Col
        Cj_nd    = np.zeros(Ncells)
        Cj_nd[:Ni_TG] = np.nan
        Cj_nd[Ni_TG:2*Ni_TG] = Y_interp_nd[19, i, :]
        Cj_nd[2*Ni_TG:3*Ni_TG] = Y_interp_nd[18, i, :]
        # old Col
        Cv_nd    = np.zeros(Ncells)
        Cv_nd[:Ni_TG] = np.nan
        Cv_nd[Ni_TG:2*Ni_TG] = Y_interp_nd[21, i, :]
        Cv_nd[2*Ni_TG:3*Ni_TG] = Y_interp_nd[20, i, :]

        if set_params['model'].count("Infla") > 0:
            if set_params['model'].count("Pl") > 0:
                raise NotImplementedError
            else:
                m_nd = np.zeros(Ncells)
                m_nd[:Ni_TG] = np.nan
                m_nd[Ni_TG:2*Ni_TG] = np.nan
                m_nd[2*Ni_TG:3*Ni_TG] = Y_interp_nd[23, i, :]
                M_nd = np.zeros(Ncells)
                M_nd[:Ni_TG] = np.nan
                M_nd[Ni_TG:2*Ni_TG] = np.nan
                M_nd[2*Ni_TG:3*Ni_TG] = Y_interp_nd[24, i, :]

        if set_params['model'].count("Infla") > 0:
            # data_dict = {"id_layer" : id_layer}
            data_dict = {"id_layers" : id_layers, "delta_NO": delta_NO,
                    "delta_PDGF": delta_PDGF, "delta_FGF": delta_FGF,
                    "delta_Ag": delta_Ag, "delta_TGF": delta_TGF,
                    "delta_TNF": delta_TNF, "delta_MMP": delta_MMP,
                    "p_d": p_d, "a_d": a_d, "r_d": r_d, "m_d": m_d, "lS_d": lS_d,
                    "chi_d": chi_d, "c_d": c_d,
                    "E_nd" : E_nd, "Q_nd" : Q_nd, "S_nd" : S_nd, "Cj_nd" : Cj_nd,
                    "Cv_nd" : Cv_nd, "m_nd": m_nd, "M_nd": M_nd}
        else:
            # data_dict = {"id_layer" : id_layer}
            data_dict = {"id_layers" : id_layers, "delta_NO": delta_NO,
                    "delta_PDGF": delta_PDGF, "delta_FGF": delta_FGF,
                    "delta_Ag": delta_Ag, "delta_TGF": delta_TGF,
                    "delta_TNF": delta_TNF, "delta_MMP": delta_MMP,
                    "p_d": p_d, "a_d": a_d, "r_d": r_d, "m_d": m_d, "lS_d": lS_d,
                    "chi_d": chi_d, "c_d": c_d,
                    "E_nd" : E_nd, "Q_nd" : Q_nd, "S_nd" : S_nd,
                    "Cj_nd" : Cj_nd, "Cv_nd" : Cv_nd}

        out = unstructuredGridToVTK(filepath, x_conti, y_conti, z_conti,
                TCX_.flatten(), offsets, cell_type,
                cellData = data_dict)
        group.addFile(filepath + '.vtu', sim_time=format(t_interp[i],"1.6E"))
    group.save()

    return

def generateLayersAxiSymCylVTK(pathToVTK, set_params, xP, xE, xE_ghost,
        R_l_g, R_LEI_g, angle,
        t_interp, Y_interp_nd, tupleCst_interp):
    """ Generate a 3D rendering file in VTK of the arterial wall layers
    configuration. From data express at the center of each compartiment, a
    cubic interpolation (with ghost cells for boundary conditions) is made for
    update the nodes of each compartiment. For adventical values of data, we set
    them to NaN.

    Parameters
    ----------
    pathToVTK : str
        The path where file will be saved
    set_params : dict
        The dictionnary
    xP : np.ndarray of shape (Np_TG,)
        Nodes x positions
    xE : np.ndarray of shape (Ni_TG)
        Center of elements positions
    xE_ghost : np.ndarray of shape (Ni_TG + 2,)
        Center of elements positions + 1 ghost points for `BCs`
    R_l_g : np.ndarray of shape (Nt, Ni_TG)
        Values of radius at the position xE
    R_LEI_g : np.ndarray of shape (Nt, Ni_TG)
        Values of internal lamina radius at the position xE
    angle : float
        Hald angle of the axisym geometry in rad
    t_interp : np.ndarray of shape (Nt,)
        time array
    Y_interp_nd : np.ndarray of shape (Neq, Nt, Ni_TG)
        Result of integration
    tupleCst_nd : np.ndarray of shape (N_cst, Nt, Ni_TG)
        Species functional properties rate of ODEs-DDEs in [1/d] as
        (p_i, p_m, a_i,a_m, m_i, m_m, l_i, l_m,
            chi_i, chi_m, c_i, c_m) = Cst_interp

    Returns
    -------
    VTKfile

    """
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    R_LEI0 = R_LEI_g[0,0]
    (p_i, p_m, a_i, a_m, m_i, m_m, l_i, l_m,
             chi_i, chi_m, c_i, c_m) = tupleCst_interp
    # Cst_interp = np.vstack((p_i[np.newaxis,:,:], p_m[np.newaxis,:,:],
                # a_i[np.newaxis,:,:], a_m[np.newaxis,:,:], l_i[np.newaxis,:,:],
                # l_m[np.newaxis,:,:], chi_i[np.newaxis,:,:],
                # chi_m[np.newaxis,:,:], m_i[np.newaxis,:,:],
                # m_m[np.newaxis,:,:], c_i[np.newaxis,:,:], c_m[np.newaxis,:,:]))

    # dir creation
    if not os.path.exists(pathToVTK):
        os.makedirs(pathToVTK)

    Nt = len(t_interp)
    Ni_x = R_LEI_g.shape[1]
    Np_x = len(xP)
    print('generateLayersAxiSymCylVTK Nt %s Np_x %s Ni_x %s Y_interp_nd.shape %s' %
            (Nt, Np_x, Ni_x, Y_interp_nd.shape))

    # print('R_LEI_g[0,:]', R_LEI_g[0,:])
    # print('R_l_g[0,:]', R_l_g[0,:])
    Rext_g_in = np.ones(Np_x) * Rext
    R_LEE_g_in = np.ones(Np_x) * R_LEE

    # # unpack Cst_interp arrays
    # (p_i, p_m, a_i,a_m, m_i, m_m, l_i, l_m,
            # chi_i, chi_m, c_i, c_m) = Cst_interp

    group = VtkGroup("%s/layers" % pathToVTK)

    for i in range(Nt):
        # print('\n i %s-------------------------' % (i))
        filepath = "%s/layers_%s" % (pathToVTK, i)

        R_l_g_ghost = np.zeros(Ni_x + 2)
        R_l_g_ghost[1:-1] = R_l_g[i,:]
        R_l_g_ghost[0] = R0; R_l_g_ghost[-1] = R0

        R_LEI_g_ghost = np.zeros(Ni_x + 2)
        R_LEI_g_ghost[1:-1] = R_LEI_g[i,:]
        R_LEI_g_ghost[0] = R_LEI0; R_LEI_g_ghost[-1] = R_LEI0

        R_l_g_in = interp1d(xE_ghost, R_l_g_ghost, kind = 'cubic')(xP)
        R_LEI_g_in  =  interp1d(xE_ghost, R_LEI_g_ghost, kind = 'cubic')(xP)

        # print('Rext_g_in.shape %s R_LEE_g_in.shape %s R_LEI_g_in.shape %s R_l_g_in.shape %s' %
            # (Rext_g_in.shape, R_LEE_g_in.shape, R_LEI_g_in.shape, R_l_g_in.shape))
        # arr of shape (nx, nr) -> nbr of node in X dir, nbr node in radial dir
        Y = np.vstack((Rext_g_in, R_LEE_g_in, R_LEI_g_in, R_l_g_in)).T
        # print('k=0 Rext %s RLEE %s RLEI %s Rl %s' % (Rext_g_in[0], R_LEE_g_in[0], R_LEI_g_in[0], R_l_g_in[0]))
        # nbr of node in the longitudinal direction
        nx = Np_x
        # nbr of node in the radial direction
        nr = Y.shape[1]
        # only 2 points (1 cell) in circumferential direction (theta direction)
        nt = 2
        # longitudinal nbr of cells (x direction)
        nx_c = len(xP) - 1
        # radial nbr of cells (r direction)
        nr_c = nr - 1
        # circumferential nbr of cells (r direction)
        nt_c = nt - 1
        # print('nx %s, nr %s, nt %s' % (nx, nr, nt))
        # print('nx_c %s, nr_c %s, nt_c %s' % (nx_c, nr_c, nt_c))
        # total nbr of cells
        ncells = nx_c * nr_c * nt_c

        ##########
        # def arrays of cells nodes positions
        ##########
        x = np.zeros((nx, nr, nt))
        y = np.zeros((nx, nr, nt))
        z = np.zeros((nx, nr, nt))
        # first projection
        l = 0
        for j in range(nr):
            for k in range(nx):
                # if k == 0:
                    # print('xP[k] %s, Y[k,j] %s, Y[k,j] %s' % (xP[k], Y[k,j], Y[k,j]))
                x[k,j,l] = xP[k]
                y[k,j,l] = Y[k,j] * np.cos(-angle)
                z[k,j,l] = Y[k,j] * np.sin(-angle)
        # second projection
        l = 1
        for j in range(nr):
            for k in range(nx):
                # if k == 0:
                    # print('xP[k] %s, Y[k,j] %s, Y[k,j] %s' % (xP[k], Y[k,j], Y[k,j]))
                x[k,j,l] = xP[k]
                y[k,j,l] = Y[k,j] * np.cos(angle)
                z[k,j,l] = Y[k,j] * np.sin(angle)

        ##########
        # add to the cells data from integration of ODEs-DDEs
        ##########
        ###
        # Init arrays
        ###
        # the id layer 1 adv, 2 media, 3 intima
        id_layer = np.zeros((nx_c, nr_c, nt_c))
        # NO
        delta_NO   = np.zeros((nx_c, nr_c, nt_c))
        # NO
        delta_PDGF = np.zeros((nx_c, nr_c, nt_c))
        # NO
        delta_FGF  = np.zeros((nx_c, nr_c, nt_c))
        # NO
        delta_Ag   = np.zeros((nx_c, nr_c, nt_c))

        delta_TGF  = np.zeros((nx_c, nr_c, nt_c))
        # NO
        delta_TNF  = np.zeros((nx_c, nr_c, nt_c))
        # MMP
        delta_MMP  = np.zeros((nx_c, nr_c, nt_c))
        # prod rate of sSMCs
        p_d      = np.zeros((nx_c, nr_c, nt_c))
        # apop rate of sSMCs
        a_d      = np.zeros((nx_c, nr_c, nt_c))
        #
        r_d      = np.zeros((nx_c, nr_c, nt_c))
        # migr
        m_d      = np.zeros((nx_c, nr_c, nt_c))
        # EMC prod rate
        lS_d      = np.zeros((nx_c, nr_c, nt_c))
        # EMC degra rate
        chi_d    = np.zeros((nx_c, nr_c, nt_c))
        # switch cSMCs -> sSMCs
        c_d      = np.zeros((nx_c, nr_c, nt_c))
        # E_nd
        E_nd     = np.zeros((nx_c, nr_c, nt_c))
        # cSMCs
        Q_nd     = np.zeros((nx_c, nr_c, nt_c))
        # sSMCs
        S_nd     = np.zeros((nx_c, nr_c, nt_c))
        # young Col
        Cj_nd    = np.zeros((nx_c, nr_c, nt_c))
        # old Col
        Cv_nd    = np.zeros((nx_c, nr_c, nt_c))
        if set_params['model'].count("Infla") > 0:
            if set_params['model'].count("Pl") > 0:
                raise NotImplementedError
            else:
                m_nd = np.zeros((nx_c, nr_c, nt_c))
                M_nd = np.zeros((nx_c, nr_c, nt_c))
        # print('x.shape %s, y.shape %s, z.shape %s'
                # % (x.shape, y.shape, z.shape))
        # print('p_d.shape %s, Q_nd.shape %s'
                # % (p_d.shape, Q_nd.shape))

        ###
        # Fill  arrays and adventical data are set to nan
        ###
        for j in range(nr_c):
            id_layer[:,j,:] = (j+1)

        # print('p_i.shape', p_i.shape)
        # print('p_i[i,:].shape', p_i[i,:].shape,  p_i[i].shape)
        p_d[:,2,0] = p_i[i, :]
        p_d[:,1,0] = p_m[i, :]
        p_d[:,0,0] = np.nan

        a_d[:,2,0] = a_i[i, :]
        a_d[:,1,0] = a_m[i, :]
        a_d[:,0,0] = np.nan

        r_d = p_d - a_d

        m_d[:,2,0] = m_i[i, :]
        m_d[:,1,0] = m_m[i, :]
        m_d[:,0,0] = np.nan

        lS_d[:,2,0] = l_i[i, :]
        lS_d[:,1,0] = l_m[i, :]
        lS_d[:,0,0] = np.nan

        chi_d[:,2,0] = chi_i[i, :]
        chi_d[:,1,0] = chi_m[i, :]
        chi_d[:,0,0] = np.nan

        c_d[:,2,0] = c_i[i, :]
        c_d[:,1,0] = c_m[i, :]
        c_d[:,0,0] = np.nan

        delta_NO[:,2,0] = Y_interp_nd[0, i, :]
        delta_NO[:,1,0] = Y_interp_nd[1, i, :]
        delta_NO[:,0,0] =  np.nan

        delta_PDGF[:,2,0] = Y_interp_nd[2, i, :]
        delta_PDGF[:,1,0] = Y_interp_nd[3, i, :]
        delta_PDGF[:,0,0] =  np.nan

        delta_FGF[:,2,0] = Y_interp_nd[4, i, :]
        delta_FGF[:,1,0] = Y_interp_nd[5, i, :]
        delta_FGF[:,0,0] =  np.nan

        delta_Ag[:,2,0] = Y_interp_nd[6, i, :]
        delta_Ag[:,1,0] = Y_interp_nd[7, i, :]
        delta_Ag[:,0,0] =  np.nan

        delta_TGF[:,2,0] = Y_interp_nd[8, i, :]
        delta_TGF[:,1,0] = Y_interp_nd[9, i, :]
        delta_TGF[:,0,0] =  np.nan

        delta_TNF[:,2,0] = Y_interp_nd[10, i, :]
        delta_TNF[:,1,0] = Y_interp_nd[11, i, :]
        delta_TNF[:,0,0] =  np.nan

        delta_MMP[:,2,0] = Y_interp_nd[12, i, :]
        delta_MMP[:,1,0] = Y_interp_nd[13, i, :]
        delta_MMP[:,0,0] =  np.nan

        E_nd[:,2,0] = Y_interp_nd[22, i, :]
        E_nd[:,1,0] = np.nan
        E_nd[:,0,0] =  np.nan

        Q_nd[:,2,0] = Y_interp_nd[14, i, :]
        Q_nd[:,1,0] = Y_interp_nd[15, i, :]
        Q_nd[:,0,0] =  np.nan
        S_nd[:,2,0] = Y_interp_nd[16, i, :]
        S_nd[:,1,0] = Y_interp_nd[17, i, :]
        S_nd[:,0,0] = np.nan
        Cj_nd[:,2,0] = Y_interp_nd[18, i, :]
        Cj_nd[:,1,0] = Y_interp_nd[19, i, :]
        Cj_nd[:,0,0] = np.nan
        Cv_nd[:,2,0] = Y_interp_nd[20, i, :]
        Cv_nd[:,1,0] = Y_interp_nd[21, i, :]
        Cv_nd[:,0,0] = np.nan
        if set_params['model'].count("Infla") > 0:
            if set_params['model'].count("Pl") > 0:
                raise NotImplementedError
            else:
                m_nd[:,2,0] = Y_interp_nd[23, i, :]
                m_nd[:,1,0] = np.nan
                m_nd[:,0,0] = np.nan
                M_nd[:,2,0] = Y_interp_nd[24, i, :]
                M_nd[:,1,0] = np.nan
                M_nd[:,0,0] = np.nan



        if set_params['model'].count("Infla") > 0:
            # data_dict = {"id_layer" : id_layer}
            data_dict = {"id_layer" : id_layer, "delta_NO": delta_NO,
                    "delta_PDGF": delta_PDGF, "delta_FGF": delta_FGF,
                    "delta_Ag": delta_Ag, "delta_TGF": delta_TGF,
                    "delta_TNF": delta_TNF, "delta_MMP": delta_MMP,
                    "p_d": p_d, "a_d": a_d, "r_d": r_d, "m_d": m_d, "lS_d": lS_d,
                    "chi_d": chi_d, "c_d": c_d,
                    "E_nd" : E_nd, "Q_nd" : Q_nd, "S_nd" : S_nd, "Cj_nd" : Cj_nd,
                    "Cv_nd" : Cv_nd, "m_nd": m_nd, "M_nd": M_nd}
        else:
            # data_dict = {"id_layer" : id_layer}
            data_dict = {"id_layer" : id_layer, "delta_NO": delta_NO,
                    "delta_PDGF": delta_PDGF, "delta_FGF": delta_FGF,
                    "delta_Ag": delta_Ag, "delta_TGF": delta_TGF,
                    "delta_TNF": delta_TNF, "delta_MMP": delta_MMP,
                    "p_d": p_d, "a_d": a_d, "r_d": r_d, "m_d": m_d, "lS_d": lS_d,
                    "chi_d": chi_d, "c_d": c_d,
                    "E_nd" : E_nd, "Q_nd" : Q_nd, "S_nd" : S_nd,
                    "Cj_nd" : Cj_nd, "Cv_nd" : Cv_nd}
        # print('x.shape %s, y.shape %s, z.shape %s idxlayer.shape %s'
                # % (x.shape, y.shape, z.shape, id_layer.shape))
        #
        # print('before gridToVTK')
        gridToVTK(filepath, x, y, z, cellData = data_dict)
        # print('before addFile')
        # print('i %s---------' % (i))

        group.addFile(filepath + '.vts', sim_time=format(t_interp[i],"1.6E"))
    group.save()

def calculVolumes_1D(X_nd, set_params,
                    V_Oi, V_Om, Va, Vl, criterium=False):
    """ from jansenBessel


    computation of
        V_Oi volume of others types of arterial wall contituent in intima
        V_Om volume of others types of arterial wall contituent in media
        Vall
        citerium (bool):
            If True function return the cr = $\frac{V_a-V_{gr}}{V_l}$, else
            return values used for integrate in time
    Parameters
    ----------

    Returns
    -------
    if criterium=False:
        eTot :
            total arterial wall thickness
        e_i :
            intimal thickness
        e_m :
            medila thickness
        R_gr : float
            Luminal radius
        Vf : tuple
            Collection of the volume fraction of all species in intima and media
        Vs : tuple
            Volume of intima and media layers
    else:
        cr : float
            The value of (Vall - Vgr) / Vl
    """
    (rho_s, rho_c, rho_E) = set_params['densities']
    X_d = set_params['sys_dim']
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    e_a = Rext - R_LEE

    if isinstance(X_nd,np.ndarray) and X_nd.ndim > 1:
        # case of postProcessing where X_nd.shape = (Ny, Nt)
        X = X_nd * X_d[:,np.newaxis]
    else:
        X = X_nd * X_d
    if 'P_nd' in set_params['species_nd']:
        (NOi_t, NOm_t, PDGFi_t, PDGFm_t, FGFi_t, FGFm_t, Agi_t, Agm_t,
        TGFi_t, TGFm_t, TNFi_t, TNFm_t, MMPi_t, MMPm_t,
        Qi_t, Qm_t, Si_t, Sm_t, Cji_t, Cjm_t, Cvi_t, Cvm_t, E_t, P_t) = X
    else:
        (NOi_t, NOm_t, PDGFi_t, PDGFm_t, FGFi_t, FGFm_t, Agi_t, Agm_t,
        TGFi_t, TGFm_t, TNFi_t, TNFm_t, MMPi_t, MMPm_t,
        Qi_t, Qm_t, Si_t, Sm_t, Cji_t, Cjm_t, Cvi_t, Cvm_t, E_t) = X

    # computation volume
    Vi_E = E_t / rho_E
    Vi_S = Si_t / rho_s
    Vi_Q = Qi_t / rho_s
    Vi_Cj = Cji_t / rho_c
    Vi_Cv = Cvi_t / rho_c

    Vm_S = Sm_t / rho_s
    Vm_Q = Qm_t / rho_s
    Vm_Cj = Cjm_t / rho_c
    Vm_Cv = Cvm_t / rho_c

    Vgr_i = Vi_E + Vi_S + Vi_Q + Vi_Cj + Vi_Cv
    Vgr_m = Vm_S + Vm_Q + Vm_Cj + Vm_Cv

    Vi = Vgr_i + V_Oi
    Vm = Vgr_m + V_Om

    if criterium:
        Vgr = Vi + Vm + Va
        Vall = L * np.pi * Rext**2
        # print('Vl', Vl, 'Vall', Vall, 'Vgr', Vgr)

        cr = (Vall - Vgr) / Vl
        return cr
    else:
        Vs = (Vi, Vm)
        # calcul of geometric thickness and radius
        e_m = R_LEE - np.sqrt((R_LEE)**2 - Vm / (np.pi * L))
        e_i = R_LEE - e_m - np.sqrt((R_LEE - e_m)**2 - Vi / (np.pi * L))
        eTot = e_m + e_i + e_a
        R_gr = Rext - eTot
        # computation volume fraction
        Vf_Ei = Vi_E / Vi
        Vf_Qi = Vi_Q / Vi
        Vf_Qm = Vm_Q / Vm
        Vf_Si = Vi_S / Vi
        Vf_Sm = Vm_S / Vm
        Vf_Ci = (Vi_Cj + Vi_Cv) / Vi
        Vf_Cm = (Vm_Cj + Vm_Cv) / Vm
        Vf_Oi = V_Oi / Vi
        Vf_Om = V_Om / Vm
        sumVf_i = Vf_Ei + Vf_Qi + Vf_Si + Vf_Ci + Vf_Oi
        sumVf_m = Vf_Qm + Vf_Sm + Vf_Cm + Vf_Om

        if(np.max(sumVf_i) > 1. and np.abs(np.max(sumVf_i) - 1.0) > 1e-8):
            raise ValueError('sum gamma i >0')
        if(np.max(sumVf_m) > 1. and np.abs(np.max(sumVf_m) - 1.0) > 1e-8):
            raise ValueError('sum gamma m >0')

        Vf = (Vf_Ei, Vf_Qi, Vf_Qm, Vf_Si, Vf_Sm, Vf_Ci, Vf_Cm, Vf_Oi, Vf_Om)
        return eTot, e_i, e_m, R_gr, Vf, Vs

def calculVolumes_Infla_1D(X_nd, set_params, V_Oi, V_Om,
        Va, Vl, criterium=False):
    """ from jansenBessel


    computation of
        V_Oi volume of others types of arterial wall contituent in intima
        V_Om volume of others types of arterial wall contituent in media
        Vall
        citerium (bool):
            If True function return the cr = $\frac{V_a-V_{gr}}{V_l}$, else
            return values used for integrate in time
    Parameters
    ----------

    Returns
    -------
    if criterium=False:
        eTot :
            total arterial wall thickness
        e_i :
            intimal thickness
        e_m :
            medila thickness
        R_gr : float
            Luminal radius
        Vf : tuple
            Collection of the volume fraction of all species in intima and media
        Vs : tuple
            Volume of intima and media layers
    else:
        cr : float
            The value of (Vall - Vgr) / Vl
    """
    (rho_s, rho_c, rho_E) = set_params['densities']
    X_d = set_params['sys_dim']
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    e_a = Rext - R_LEE

    if isinstance(X_nd,np.ndarray) and X_nd.ndim > 1:
        # case of postProcessing where X_nd.shape = (Ny, Nt)
        X = X_nd * X_d[:,np.newaxis]
    else:
        X = X_nd * X_d

    if 'P_nd' in set_params['species_nd']:
        (NOi_t, NOm_t, PDGFi_t, PDGFm_t, FGFi_t, FGFm_t, Agi_t, Agm_t,
        TGFi_t, TGFm_t, TNFi_t, TNFm_t, MMPi_t, MMPm_t,
        Qi_t, Qm_t, Si_t, Sm_t, Cji_t, Cjm_t, Cvi_t, Cvm_t,
        E_t, P_t, m_t, M_t) = X
    else:
        (NOi_t, NOm_t, PDGFi_t, PDGFm_t, FGFi_t, FGFm_t, Agi_t, Agm_t,
        TGFi_t, TGFm_t, TNFi_t, TNFm_t, MMPi_t, MMPm_t,
        Qi_t, Qm_t, Si_t, Sm_t, Cji_t, Cjm_t, Cvi_t, Cvm_t,
        E_t, m_t, M_t) = X

    # computation volume
    Vi_M = M_t / rho_s
    Vi_m = m_t / rho_s
    Vi_E = E_t / rho_E
    Vi_S = Si_t / rho_s
    Vi_Q = Qi_t / rho_s
    Vi_Cj = Cji_t / rho_c
    Vi_Cv = Cvi_t / rho_c

    Vm_S = Sm_t / rho_s
    Vm_Q = Qm_t / rho_s
    Vm_Cj = Cjm_t / rho_c
    Vm_Cv = Cvm_t / rho_c

    Vgr_i = Vi_E + Vi_S + Vi_Q + Vi_Cj + Vi_Cv + Vi_m + Vi_M
    Vgr_m = Vm_S + Vm_Q + Vm_Cj + Vm_Cv

    Vi = Vgr_i + V_Oi
    Vm = Vgr_m + V_Om

    if criterium:
        Vgr = Vi + Vm + Va
        Vall = L * np.pi * Rext**2
        # print('Vl', Vl, 'Vall', Vall, 'Vgr', Vgr)

        cr = (Vall - Vgr) / Vl
        return cr
    else:
        Vs = (Vi, Vm)
        # calcul of geometric thickness and radius
        e_m = R_LEE - np.sqrt((R_LEE)**2 - Vm / (np.pi * L))
        e_i = R_LEE - e_m - np.sqrt((R_LEE - e_m)**2 - Vi / (np.pi * L))
        eTot = e_m + e_i + e_a
        R_gr = Rext - eTot
        # computation volume fraction
        Vf_Ei = Vi_E / Vi
        Vf_m = Vi_m / Vi
        Vf_M = Vi_M / Vi
        Vf_Qi = Vi_Q / Vi
        Vf_Qm = Vm_Q / Vm
        Vf_Si = Vi_S / Vi
        Vf_Sm = Vm_S / Vm
        Vf_Ci = (Vi_Cj + Vi_Cv) / Vi
        Vf_Cm = (Vm_Cj + Vm_Cv) / Vm
        Vf_Oi = V_Oi / Vi
        Vf_Om = V_Om / Vm
        sumVf_i = Vf_Ei + Vf_Qi + Vf_Si + Vf_Ci + Vf_Oi + Vf_m + Vf_M
        sumVf_m = Vf_Qm + Vf_Sm + Vf_Cm + Vf_Om

        if(np.max(sumVf_i) > 1. and np.abs(np.max(sumVf_i) - 1.0) > 1e-8):
            raise ValueError('sum gamma i >0')
        if(np.max(sumVf_m) > 1. and np.abs(np.max(sumVf_m) - 1.0) > 1e-8):
            raise ValueError('sum gamma m >0')

        Vf = (Vf_Ei, Vf_Qi, Vf_Qm, Vf_Si, Vf_Sm,
                Vf_Ci, Vf_Cm, Vf_Oi, Vf_Om, Vf_m, Vf_M)
        return eTot, e_i, e_m, R_gr, Vf, Vs


def calculVolumes_2D(X_nd, set_params, z, dxE_z, V_Oi_z, V_Om_z,
        Vl_z, Vlim_z, criterium=False):
    """ Computation of
        V_Oi volume of others types of arterial wall contituent in intima
        V_Om volume of others types of arterial wall contituent in media
        Vall
        citerium (bool):
            If True function return the cr = $\frac{V_a-V_{gr}}{V_l}$, else
            return values used for integrate in time

    Parameters
    ----------
    X_nd : np.ndarray shape (Neq,) or (Neq,Ni_TG)
        The variables of the system of equations
    set_params : dict
        The dictionnary
    z : int
        Spacial index, the zth compartiment
    dxE_z : float or np.ndarray shape (Ni_TG,)
        The length of the compartiment
    V_Oi_z : float or np.ndarray shape (Ni_TG,)
        Volume of others species in intima
    V_Om_z : float or np.ndarray shape (Ni_TG,)
        Volume of others species in media
    Vl_z : float or np.ndarray shape (Ni_TG,)
        Volume of the lumen
    Vlim_z : float or np.ndarray shape (Ni_TG,)
        Volume of the lumen + intima and media
    rho_E : float
        Density of ECs
    rho_c : float
        Density of collagen
    rho_s : float
        Density of cells
    criterium : bool
        Is the function used for criterium or not
    Returns
    -------
    if criterium==True:
        cr : float
            The criterium as (Vall - Vgr) / Vl_z
    else:
        eTot :
        e_i :
        e_m :
        R_gr :
        Vf :
        Vs :
    """
    (rho_s, rho_c, rho_E) = set_params['densities']
    # print('X_d %s' % X_d)
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    e_a = Rext - R_LEE

    if isinstance(X_nd,np.ndarray) and X_nd.ndim > 1:
        if z is not None:
            raise ValueError('z!=None and X_nd is np.ndarray not possible')
        # case of postProcessing where X_nd.shape = (Ny, Nt)
        # print("set_params['sys_dim'].shape", set_params['sys_dim'].shape)
        # print("set_params['sys_dim'].shape", set_params['sys_dim'].shape)
        # print('', X_nd.shape)
        X_d = set_params['sys_dim']
        # print("X_d.shape", X_d.shape, "X_nd.shape", X_nd.shape)
        X = X_nd * X_d
        # X = X_nd * X_d[:,np.newaxis]
        # print('X.shape', X.shape)
    else:
        # print("set_params['sys_dim'].shape", set_params['sys_dim'].shape)
        X_d = set_params['sys_dim'][z,:]
        X = X_nd * X_d
        # print('X.shape', X.shape)

    # print("set_params['species_nd']", set_params['species_nd'])
    if 'P_nd' in set_params['species_nd']:
        (NOi_t, NOm_t, PDGFi_t, PDGFm_t, FGFi_t, FGFm_t, Agi_t, Agm_t,
        TGFi_t, TGFm_t, TNFi_t, TNFm_t, MMPi_t, MMPm_t,
        Qi_t, Qm_t, Si_t, Sm_t, Cji_t, Cjm_t, Cvi_t, Cvm_t, E_t, P_t) = X.T
    else:
        # print('X.shape', X.shape)
        (NOi_t, NOm_t, PDGFi_t, PDGFm_t, FGFi_t, FGFm_t, Agi_t, Agm_t,
        TGFi_t, TGFm_t, TNFi_t, TNFm_t, MMPi_t, MMPm_t,
        Qi_t, Qm_t, Si_t, Sm_t, Cji_t, Cjm_t, Cvi_t, Cvm_t, E_t) = X.T
        (NOi_t, NOm_t, PDGFi_t, PDGFm_t, FGFi_t, FGFm_t, Agi_t, Agm_t,
        TGFi_t, TGFm_t, TNFi_t, TNFm_t, MMPi_t, MMPm_t,
        Qi_t, Qm_t, Si_t, Sm_t,
        Cji_t, Cjm_t, Cvi_t, Cvm_t, E_t) = X.T

    # computation volume
    Vi_E = E_t / rho_E
    Vi_S = Si_t / rho_s
    Vi_Q = Qi_t / rho_s
    Vi_Cj = Cji_t / rho_c
    Vi_Cv = Cvi_t / rho_c

    Vm_S = Sm_t / rho_s
    Vm_Q = Qm_t / rho_s
    Vm_Cj = Cjm_t / rho_c
    Vm_Cv = Cvm_t / rho_c

    Vgr_i = Vi_E + Vi_S + Vi_Q + Vi_Cj + Vi_Cv
    Vgr_m = Vm_S + Vm_Q + Vm_Cj + Vm_Cv
    # print('Vgr_i %s Vgr_m %s' % (Vgr_i, Vgr_m))
    Vi = Vgr_i + V_Oi_z
    Vm = Vgr_m + V_Om_z
    # print('Va.shape', Va.shape, 'Vm.shape', Vm.shape,'Vi.shape', Vi.shape,)
    if criterium:
        Vgr = Vi + Vm
        # print('Vgr ={},   Vlim_z = {},   Vl_z = {}'.format(Vgr, Vlim_z, Vl_z))
        cr = (Vlim_z - Vgr) / Vl_z
        # print('cr', cr)
        return cr
    else:
        Vs = (Vi, Vm)
        # calcul of geometric thickness and radius
        # print('Vm[0]', Vm[0], 'R_LEE', R_LEE, 'dxE_z[0]', dxE_z[100],
                # 'sqrt(...)', np.sqrt((R_LEE)**2 - Vm[0] / (np.pi * dxE_z[0])))
        e_m = R_LEE - np.sqrt((R_LEE)**2 - Vm / (np.pi * dxE_z))
        e_i = R_LEE - e_m - np.sqrt((R_LEE - e_m)**2 - Vi / (np.pi * dxE_z))
        # print('R_LEE %s e_m %s e_i %s' % (R_LEE, e_m, e_i))
        eTot = e_m + e_i + e_a
        R_gr = Rext - eTot
        # computation volume fraction
        Vf_Ei = Vi_E / Vi
        Vf_Qi = Vi_Q / Vi
        Vf_Qm = Vm_Q / Vm
        Vf_Si = Vi_S / Vi
        Vf_Sm = Vm_S / Vm
        Vf_Ci = (Vi_Cj + Vi_Cv) / Vi
        Vf_Cm = (Vm_Cj + Vm_Cv) / Vm
        Vf_Oi = V_Oi_z / Vi
        Vf_Om = V_Om_z / Vm
        sumVf_i = Vf_Ei + Vf_Qi + Vf_Si + Vf_Ci + Vf_Oi
        sumVf_m = Vf_Qm + Vf_Sm + Vf_Cm + Vf_Om
        Fi_C_S = Vf_Ci / Vf_Si
        Fm_C_S = Vf_Cm / Vf_Sm
        if(np.max(sumVf_i) > 1. and np.abs(np.max(sumVf_i) - 1.0) > 1e-8):
            print('sumVf_i %s' % sumVf_i)
            raise ValueError('sum gamma i >0')
        if(np.max(sumVf_m) > 1. and np.abs(np.max(sumVf_m) - 1.0) > 1e-8):
            raise ValueError('sum gamma m >0')

        Vf = (Vf_Ei, Vf_Qi, Vf_Qm, Vf_Si, Vf_Sm, Vf_Ci, Vf_Cm, Vf_Oi, Vf_Om)
        return eTot, e_i, e_m, R_gr, Vf, Vs


def calculVolumes_Infla_2D(X_nd, set_params, z, dxE_z, V_Oi_z, V_Om_z,
        Vl_z, Vlim_z,criterium=False):
    """ Computation of
        V_Oi volume of others types of arterial wall contituent in intima
        V_Om volume of others types of arterial wall contituent in media
        Vall
        citerium (bool):
            If True function return the cr = $\frac{V_a-V_{gr}}{V_l}$, else
            return values used for integrate in time

    Parameters
    ----------
    X_nd : np.ndarray shape (Neq,) or (Neq,Nx)
        The variables of the system of equations
    set_params : dict
        The dictionnary
    z : int
        Spacial index, the zth compartiment
    dxE_z : float or np.ndarray shape(Nx,)
        The length of the compartiment
    V_Oi_z : float or np.ndarray shape(Nx,)
        Volume of others species in intima
    V_Om_z : float or np.ndarray shape(Nx,)
        Volume of others species in media
    Vl_z : float or np.ndarray shape(Nx,)
        Volume of the lumen
    Vlim_z : float or np.ndarray shape (Ni_TG,)
        Volume of the lumen + intima and media
    criterium : bool
        Is the function used for criterium or not
    Returns
    -------
    if criterium==True:
        cr : float
            The criterium as (Vall - Vgr) / Vl_z
    else:
        eTot :
        e_i :
        e_m :
        R_gr :
        Vf :
        Vs :
    """
    (rho_s, rho_c, rho_E) = set_params['densities']
    # print('X_d %s' % X_d)
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    e_a = Rext - R_LEE

    if isinstance(X_nd,np.ndarray) and X_nd.ndim > 1:
        if z is not None:
            raise ValueError('z!=None and X_nd is np.ndarray not possible')
        # case of postProcessing where X_nd.shape = (Ny, Nt)
        # print("set_params['sys_dim'].shape", set_params['sys_dim'].shape)
        X_d = set_params['sys_dim']
        # print("X_d.shape", X_d.shape, "X_nd.shape", X_nd.shape)
        print('X_d.shape', X_d.shape, 'X_nd.shape', X_nd.shape)
        X = X_nd * X_d
        # X = X_nd * X_d[:,np.newaxis]
        # print('X.shape', X.shape)
        if 'P_nd' in set_params['species_nd']:
            (NOi_t, NOm_t, PDGFi_t, PDGFm_t, FGFi_t, FGFm_t, Agi_t, Agm_t,
                TGFi_t, TGFm_t, TNFi_t, TNFm_t, MMPi_t, MMPm_t,
                Qi_t, Qm_t, Si_t, Sm_t,
                Cji_t, Cjm_t, Cvi_t, Cvm_t, E_t, P_t, m_t, M_t) = X.T
        else:
            (NOi_t, NOm_t, PDGFi_t, PDGFm_t, FGFi_t, FGFm_t, Agi_t, Agm_t,
                TGFi_t, TGFm_t, TNFi_t, TNFm_t, MMPi_t, MMPm_t,
                Qi_t, Qm_t, Si_t, Sm_t,
                Cji_t, Cjm_t, Cvi_t, Cvm_t, E_t, m_t, M_t) = X.T
    else:
        # print("set_params['sys_dim'].shape", set_params['sys_dim'].shape)
        X_d = set_params['sys_dim'][z,:]
        X = X_nd * X_d
        # print('X.shape', X.shape)
        if 'P_nd' in set_params['species_nd']:
            (NOi_t, NOm_t, PDGFi_t, PDGFm_t, FGFi_t, FGFm_t, Agi_t, Agm_t,
            TGFi_t, TGFm_t, TNFi_t, TNFm_t, MMPi_t, MMPm_t,
            Qi_t, Qm_t, Si_t, Sm_t,
            Cji_t, Cjm_t, Cvi_t, Cvm_t, E_t, P_t, m_t, M_t) = X
        else:
            (NOi_t, NOm_t, PDGFi_t, PDGFm_t, FGFi_t, FGFm_t, Agi_t, Agm_t,
            TGFi_t, TGFm_t, TNFi_t, TNFm_t, MMPi_t, MMPm_t,
            Qi_t, Qm_t, Si_t, Sm_t,
            Cji_t, Cjm_t, Cvi_t, Cvm_t, E_t, m_t, M_t) = X

    # computation volume
    Vi_E = E_t / rho_E
    Vi_m = m_t / rho_s
    Vi_M = M_t / rho_s
    Vi_S = Si_t / rho_s
    Vi_Q = Qi_t / rho_s
    Vi_Cj = Cji_t / rho_c
    Vi_Cv = Cvi_t / rho_c

    Vm_S = Sm_t / rho_s
    Vm_Q = Qm_t / rho_s
    Vm_Cj = Cjm_t / rho_c
    Vm_Cv = Cvm_t / rho_c

    Vgr_i = Vi_E + Vi_S + Vi_Q + Vi_Cj + Vi_Cv + Vi_m + Vi_M
    Vgr_m = Vm_S + Vm_Q + Vm_Cj + Vm_Cv
    # print('Vgr_i %s Vgr_m %s' % (Vgr_i, Vgr_m))
    Vi = Vgr_i + V_Oi_z
    Vm = Vgr_m + V_Om_z
    Va = dxE_z * np.pi * (Rext**2 - R_LEE**2)
    # print('Va.shape', Va.shape, 'Vm.shape', Vm.shape,'Vi.shape', Vi.shape,)
    if criterium:
        Vgr = Vi + Vm
        cr = (Vlim_z - Vgr) / Vl_z
        return cr
    else:
        Vs = (Vi, Vm)
        # calcul of geometric thickness and radius
        # print('Vm[0]', Vm[0], 'R_LEE', R_LEE, 'dxE_z[0]', dxE_z[100],
                # 'sqrt(...)', np.sqrt((R_LEE)**2 - Vm[0] / (np.pi * dxE_z[0])))
        e_m = R_LEE - np.sqrt((R_LEE)**2 - Vm / (np.pi * dxE_z))
        e_i = R_LEE - e_m - np.sqrt((R_LEE - e_m)**2 - Vi / (np.pi * dxE_z))
        # print('R_LEE %s e_m %s e_i %s' % (R_LEE, e_m, e_i))
        eTot = e_m + e_i + e_a
        R_gr = Rext - eTot
        # computation volume fraction
        Vf_Ei = Vi_E / Vi
        Vf_m = Vi_m / Vi
        Vf_M = Vi_M / Vi
        Vf_Qi = Vi_Q / Vi
        Vf_Qm = Vm_Q / Vm
        Vf_Si = Vi_S / Vi
        Vf_Sm = Vm_S / Vm
        Vf_Ci = (Vi_Cj + Vi_Cv) / Vi
        Vf_Cm = (Vm_Cj + Vm_Cv) / Vm
        Vf_Oi = V_Oi_z / Vi
        Vf_Om = V_Om_z / Vm
        sumVf_i = Vf_Ei + Vf_Qi + Vf_Si + Vf_Ci + Vf_Oi + Vf_m + Vf_M
        sumVf_m = Vf_Qm + Vf_Sm + Vf_Cm + Vf_Om

        if(np.max(sumVf_i) > 1. and np.abs(np.max(sumVf_i) - 1.0) > 1e-8):
            print('sumVf_i %s' % sumVf_i)
            raise ValueError('sum gamma i >0')
        if(np.max(sumVf_m) > 1. and np.abs(np.max(sumVf_m) - 1.0) > 1e-8):
            raise ValueError('sum gamma m >0')

        Vf = (Vf_Ei, Vf_Qi, Vf_Qm, Vf_Si, Vf_Sm, Vf_Ci, Vf_Cm, Vf_Oi, Vf_Om,
                Vf_m, Vf_M)
        return eTot, e_i, e_m, R_gr, Vf, Vs


def calculVolumes_3D(X_nd, set_params, k, coord_k,
        V_Oi_k, V_Om_k, Vl_k, Vlim_k,
        criterium=False):
    """ Computation of
        V_Oi volume of others types of arterial wall contituent in intima
        V_Om volume of others types of arterial wall contituent in media
        Vall
        citerium (bool):
            If True function return the cr = $\frac{V_a-V_{gr}}{V_l}$, else
            return values used for integrate in time

    Parameters
    ----------
    X_nd : np.ndarray shape (Neq,) or (Neq,Nx)
        The variables of the system of equations
    set_params : dict
        The dictionnary
    k : int
        Spacial index, the kth compartiment
    coord_k : np.ndarray of shape (1, 8, 4) or np.ndarray shape (Ni_TG, 8, 4)
        if shape (1, 8, 4) :
            The table of coordinate of the kth compartiment
        else if shape (Np_TG, 8, 4) :
            The table of coordinate of all compartiment (tidy)
    V_Oi_k : float or np.ndarray shape (Nx,)
        Volume of others species in intima
    V_Om_k : float or np.ndarray shape (Ni_TG,)
        Volume of others species in media
    Vl_k : float or np.ndarray shape (Ni_TG,)
        Volume of the lumen
    Vlim_k : float or np.ndarray shape (Ni_TG,)
        Volume of the lumen + intima + media layer (a cst all along the
        simulation)
    criterium : bool
        Is the function used for criterium or not
    Returns
    -------
    if criterium==True:
        cr : float
            The criterium as (Vall - Vgr) / Vl_k
    else:
        eTot :
        e_i :
        e_m :
        R_gr :
        Vf :
        Vs :
    """
    (rho_s, rho_c, rho_E) = set_params['densities']
    # print('X_d %s' % X_d)
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    e_a = Rext - R_LEE

    if isinstance(X_nd,np.ndarray) and X_nd.ndim > 1:
        if k is not None:
            raise ValueError('k!=None and X_nd is np.ndarray not possible')
        # case of postProcessing where X_nd.shape = (Ny, Nt)
        # print('case of postProcessing where X_nd.shape = (Ny, Nt)')
        # print("set_params['sys_dim'].shape", set_params['sys_dim'].shape)
        X_d = set_params['sys_dim']
        # print("X_d.shape", X_d.shape, "X_nd.shape", X_nd.shape)
        X = X_nd * X_d
        # X = X_nd * X_d[:,np.newaxis]
        # print('X.shape', X.shape)
        (NOi_t, NOm_t, PDGFi_t, PDGFm_t, FGFi_t, FGFm_t, Agi_t, Agm_t,
        TGFi_t, TGFm_t, TNFi_t, TNFm_t, MMPi_t, MMPm_t,
        Qi_t, Qm_t, Si_t, Sm_t,
        Cji_t, Cjm_t, Cvi_t, Cvm_t, E_t) = X.T
    else:
        # print('case of integration')
        # print("set_params['sys_dim'].shape", set_params['sys_dim'].shape)
        X_d = set_params['sys_dim'][k,:]
        X = X_nd * X_d
        # print('X.shape', X.shape)
        (NOi_t, NOm_t, PDGFi_t, PDGFm_t, FGFi_t, FGFm_t, Agi_t, Agm_t,
        TGFi_t, TGFm_t, TNFi_t, TNFm_t, MMPi_t, MMPm_t,
        Qi_t, Qm_t, Si_t, Sm_t,
        Cji_t, Cjm_t, Cvi_t, Cvm_t, E_t) = X

    # computation volume
    Vi_E = E_t / rho_E
    Vi_S = Si_t / rho_s
    Vi_Q = Qi_t / rho_s
    Vi_Cj = Cji_t / rho_c
    Vi_Cv = Cvi_t / rho_c

    Vm_S = Sm_t / rho_s
    Vm_Q = Qm_t / rho_s
    Vm_Cj = Cjm_t / rho_c
    Vm_Cv = Cvm_t / rho_c

    Vgr_i = Vi_E + Vi_S + Vi_Q + Vi_Cj + Vi_Cv
    Vgr_m = Vm_S + Vm_Q + Vm_Cj + Vm_Cv
    # print('Vgr_i %s Vgr_m %s' % (Vgr_i, Vgr_m))
    Vi = Vgr_i + V_Oi_k
    Vm = Vgr_m + V_Om_k
    # print('Vm', Vm,'Vi', Vi, 'Vlim_k', Vlim_k, 'Vl_k', Vl_k)
    if criterium:
        Vgr = Vi + Vm
        # print('Vl', Vl, 'Vall', Vall, 'Vgr', Vgr)

        cr = (Vlim_k - Vgr) / Vl_k
        return cr
    else:
        Vs = (Vi, Vm)
        # calcul of geometric thickness and radius
        # print('Vm[0]', Vm[0], 'R_LEE', R_LEE, 'dxE_z[0]', dxE_z[100],
                # 'sqrt(...)', np.sqrt((R_LEE)**2 - Vm[0] / (np.pi * dxE_z[0])))
        # get the angle
        # if TCX_k.ndim < 2:
            # raise ValueError('Demand TCX_k.ndim == 2, '+
                    # 'here = {}'.format(TCX_k.ndim))
        # e_m = np.zeros(TCX_k.shape[0])
        # e_i = np.zeros(TCX_k.shape[0])
        # R_gr = np.zeros(TCX_k.shape[0])

        # compute dth, get the theta angle of p1 - the theta angle of p0
        # print('coord_k', coord_k)
        dth = coord_k[:,1,-1] - coord_k[:,0,-1]
        # locate none positive angle
        wrongTh0Idx = np.argwhere(dth < 0.0)[:,0]
        # for the element 0 or Np_th_TG, with angle th1<th0, need  to take the
        # principal mesure of the angle (by substrat 2 pi), to get the right dth
        dth[wrongTh0Idx] = coord_k[wrongTh0Idx,1,-1] - \
                (coord_k[wrongTh0Idx,0,-1] - 2. * np.pi)
        dxE_z = (coord_k[:,4,0] - coord_k[:,0,0])
        # print('dth', dth, 'dxE_z', dxE_z)

        if np.any(dth < 0):
            raise ValueError('A voir pk, dth < 0')
        e_m = R_LEE - np.sqrt((R_LEE)**2 - Vm / (np.sin(dth * 0.5) * dxE_z))
        # print('e_m', e_m)
        R_LEI = R_LEE - e_m
        e_i = R_LEI - np.sqrt((R_LEI)**2 - Vi / (np.sin(dth * 0.5) * dxE_z))
        # print('e_i', e_i)
        eTot = e_m + e_i + e_a
        # print('eTot', eTot, 'Rext', Rext)
        R_gr = Rext - eTot
        # e_m = R_LEE - np.sqrt((R_LEE)**2 - Vm / (np.pi * dxE_z))
        # e_i = R_LEE - e_m - np.sqrt((R_LEE - e_m)**2 - Vi / (np.pi * dxE_z))
        # print('R_LEI %s e_m %s e_i %s R_gr %s' % (R_LEI, e_m, e_i, R_gr))
        eTot = e_m + e_i + e_a
        R_gr = Rext - eTot
        # computation volume fraction
        Vf_Ei = Vi_E / Vi
        Vf_Qi = Vi_Q / Vi
        Vf_Qm = Vm_Q / Vm
        Vf_Si = Vi_S / Vi
        Vf_Sm = Vm_S / Vm
        Vf_Ci = (Vi_Cj + Vi_Cv) / Vi
        Vf_Cm = (Vm_Cj + Vm_Cv) / Vm
        Vf_Oi = V_Oi_k / Vi
        Vf_Om = V_Om_k / Vm
        sumVf_i = Vf_Ei + Vf_Qi + Vf_Si + Vf_Ci + Vf_Oi
        sumVf_m = Vf_Qm + Vf_Sm + Vf_Cm + Vf_Om
        Fi_C_S = Vf_Ci / Vf_Si
        Fm_C_S = Vf_Cm / Vf_Sm
        if(np.max(sumVf_i) > 1. and np.abs(np.max(sumVf_i) - 1.0) > 1e-8):
            print('sumVf_i %s' % sumVf_i)
            raise ValueError('sum gamma i >0')
        if(np.max(sumVf_m) > 1. and np.abs(np.max(sumVf_m) - 1.0) > 1e-8):
            raise ValueError('sum gamma m >0')

        Vf = (Vf_Ei, Vf_Qi, Vf_Qm, Vf_Si, Vf_Sm, Vf_Ci, Vf_Cm, Vf_Oi, Vf_Om)
        # print('Vf', Vf)
        if k is None:
            return eTot, e_i, e_m, R_gr, Vf, Vs
        else:
            return eTot[0], e_i[0], e_m[0], R_gr[0], Vf, Vs


def message(mess, params=None):
    """ Print to screen message with specific ``++++++`` symbols

    Parameters
    ---------
    mess : str
        The message return to user by printing it,
    params : tuple
        Tuple of values to print

    Returns
    -------

    Examples
    --------
    >>> message("The variable a is {}", (2.0))
       ++++++++++++++++++++++++++++++
       The variable a is 2.0
       ++++++++++++++++++++++++++++++
    >>> message("The {} a is {} and cond is {}", ('variable', 2.0, True))
       ++++++++++++++++++++++++++++++
       The variable a is 2.0 and cond is False
       ++++++++++++++++++++++++++++++
    """
    if params is None:
        print('++++++++++++++++++++')
        print(mess)
        print('++++++++++++++++++++')
    else:
        print('++++++++++++++++++++')
        print(mess.format(*params))
        print('++++++++++++++++++++')
    return


def sortAndCleanEvents(t_events, y_events, T_d):
    t_ev_s = np.concatenate([te for te in t_events if te.size!=0])
    y_ev_s = np.concatenate([ye for ye in y_events if ye.size!=0])
    # sort in the same way t_ev and y_ev
    idxs = np.argsort(t_ev_s)
    t_ev = t_ev_s[idxs] * T_d
    y_ev = y_ev_s[idxs]
    return t_ev, y_ev

def configFluid(model, config):
    """
    Parameters
    ----------
    model : str
        The name of the fluid rheology model
    configPars : ?
        the configuration read from a file run.ini
    Return
    ------
    The initialized fluid
    """

    if not model in AVAIL_HEMORHEO_MODELS:
        print('Available model are : %s' % AVAIL_HEMORHEO_MODELS)
        raise ValueError('The proposed fluid rheologic model %s not coded'\
                    % model)
    else:
        if model == AVAIL_HEMORHEO_MODELS[0]:
            # Newtonian
            dictF = dictN = {'model': model,
                             'rho': config.getfloat('Fluid', 'rho'),
                             'mu': config.getfloat('Fluid', 'mu')
                              }
            fluid = Newtonian(**dictF)

        elif model == AVAIL_HEMORHEO_MODELS[1]:
            #Quemada
            dictF = dictQ = {'model': model,
                             'rho': config.getfloat('Fluid', 'rho'),
                             'muF': config.getfloat('Fluid', 'muF'),
                             'kInf':config.getfloat('Fluid', 'kInf'),
                             'k0':  config.getfloat('Fluid', 'k0'),
                             'Ht':  config.getfloat('Fluid', 'Ht'),
                             'sRC': config.getfloat('Fluid', 'sRC')
                          }
            fluid = Quemada(**dictF)

        elif model == AVAIL_HEMORHEO_MODELS[2]:
            #WalburnScheck
            dictF = {'model': model,
                            'rho':config.getfloat('Fluid', 'rho'),
                            'D1': config.getfloat('Fluid', 'D1'),
                            'D2': config.getfloat('Fluid', 'D2'),
                            'D3': config.getfloat('Fluid', 'D3'),
                            'D4':   config.getfloat('Fluid', 'D4'),
                            'TPMA': config.getfloat('Fluid', 'TPMA'),
                            'Ht':   config.getfloat('Fluid', 'Ht'),
                         }
            fluid = WalburnScheck(**dictF)

        elif model == AVAIL_HEMORHEO_MODELS[3]:
            #Casson
            dictF = {'model': model,
                     'rho':   config.getfloat('Fluid', 'rho'),
                     'muInf': config.getfloat('Fluid', 'muInf'),
                     'tau0':   config.getfloat('Fluid', 'tau0')
                      }
            fluid = Casson(**dictF)
        else:
            raise ValueError('not implemented fluid model')

    return fluid

def find_last_gene(paths):
    """
    generic pattern for saved folders is : geneX_[t0_tf]

    paths (list) : list of paths got from a glob.glob('gene*')
    """
    # print('paths', paths)
    ll = []
    for i in range(len(paths)):
        # remove the characters 'gene'
        ll.append(paths[i].replace('gene',''))
        # find index where '_'
        s = ll[i].find('_')
        ll[i] = ll[i][:s]
    # print('ll', ll, '\n')
    ll_int = np.asarray([int(lli) for lli in ll])
    path_last_gene = paths[np.argmax(ll_int)]
    # print('path_last_gene', path_last_gene)
    return path_last_gene

def save_obj(obj, path, filename):
    """Save (or pickel) an object
    Parameters
    ----------
    obj : type of obj
        The object to save in file
    path : str
        Path where it will be saved
    flename : str
        Name of the file
    Returns
    ------
    """
    # print('saving the obj {} path {}'.format(filename, os.getcwd()))
    # print('\n {}/{} \n'.format(path, filename))
    with open('%s/%s' % (path, filename), 'wb') as output:  # Overwrites any existing file.
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)


def load_obj(path, filename):
    """Load object previously pickeled

    Parameters
    ----------
    filename : str
        Name of the file
    Returns
    ------
    obj : type of obj
        The object unpickeled
    """
    with open('%s/%s' % (path, filename), 'rb') as input:
        obj = pickle.load(input)
    return obj


def inflammation(Vf_I, NtypeOfGFs, P_I):
    """
        return array on inflammation values
        hypothesis Immune cells secrete PDGF (n=1), FGF(n=2),
        TNF(5), et TGF(n=4)
    """
    P_I_arr = np.zeros((NtypeOfGFs, 2))
    # the GFs influenced by immune cells
    for i in [1,2,4,5]:
        # only in intima layer
        P_I_arr[i,0] = P_I * Vf_I
    return P_I_arr

# def PHI_d(X, X0):
    # """ modulatory function with  excess. Use of Macaulay function
        # X ():

        # X0 ():
            # threshold value
    # """
    # return (np.abs(-(X - X0)) - (X - X0)) * 0.5

# def PHI_d(X, X0):
    # """ modulatory function with  excess. Use of Macaulay function
        # X ():

        # X0 ():
            # threshold value
    # """
    # return (np.abs(-(X - X0)) - (X - X0)) * 0.5

def PHI_d(X, X0, FACT=5e-2):
    """ modulatory function with  deficit. Use of Macaulay function + cubic poly
        function smoother

        .. math::
            \\phi_e = \\begin{cases}
                        poly                       ,& \text{if } x \\in
                                            [x_0-\\epsilon, x_0+\\epsilon]\\\\
                        \\frac{|-(x-x_0)| - (x-x_0)}{2} ,& \\text{otherwise}
                        \\end{cases}

        Parameters
        ----------
        X : float of np.ndarray
            x values
        X0 : float
            threshold value
        FACT : factor to define EPS = X0 * FACT. limit value to ensure
        smoothness of the function
        (continuity of derivative in interval X in [X0 - EPS, X0 + EPS])

        Returns
        -------
        res : float of np.ndarray
            The resulting function

    """
    def poly(X, alpha, beta, gamma):
        """ The polynom which assure smoothness, i.e. continuity of derivative
        Also seen for sympy construction of the poly :
        /home/jjansen/Bureau/These/recherche/EDOcroissance/jansen2019/regularisationPhiFun/cubicFunDefParams.py
        """
        return (-beta + X)*(alpha**2*beta - alpha**2*X + 2*alpha*beta**2 -
                3*alpha*beta + alpha*X**2 + 2*beta**2*gamma - 2*beta**2*X +
                2*beta*gamma*X - 3*beta*gamma - beta*X**2 + 3*beta*X +
                2*gamma*X**2 - 3*gamma*X)/((alpha - beta)*(2*alpha**2 +
                    2*alpha*beta - 3*alpha + 2*beta**2 - 3*beta))

    # def of EPS value
    EPS = FACT * X0

    alpha = X0 - EPS
    beta = X0 + EPS
    gamma = EPS
    if isinstance(X, np.ndarray):
        if np.any(X < beta) or np.any(X > alpha):
            # there are values in array that need to be eval by poly
            # decomposition of 2 arrays
            res = np.zeros(X.shape)
            argsPoly = np.flatnonzero(np.logical_and(X > alpha, X < beta))
            argsMacau = np.flatnonzero(np.logical_or(X <= alpha, X >= beta))
            res[argsPoly] = poly(X[argsPoly], alpha, beta, gamma)
            res[argsMacau] = (np.abs(-(X[argsMacau]-X0)) - (X[argsMacau]-X0)) * 0.5
        else:
            # direct eval of macaulay function
            res = (np.abs(-(X - X0)) - (X - X0)) * 0.5
    else:
        if X < beta and X > alpha:
            # within the interval
            res = poly(X, alpha, beta, gamma)
        else:
            res = (np.abs(-(X - X0)) - (X - X0)) * 0.5
    return res

# def PHI_e(X, X0):
    # """ modulatory function with  excess. Use of Macaulay function
        # Parameters
        # ----------
        # X : float of np.ndarray
            # x values
        # X0 : float
            # threshold value
        # Returns
        # -------
    # """
    # return (np.abs(X - X0) + X - X0) * 0.5

def PHI_e(X, X0, FACT=5e-2):
    """ modulatory function with excess. Use of Macaulay function + a cubic poly
        function smoother

        .. math::
            \\phi_e = \\begin{cases}
                        poly                       ,& \text{if } x \\in
                                            [x_0-\\epsilon, x_0+\\epsilon]\\\\
                        \\frac{|x-x_0| + x-x_0}{2} ,& \\text{otherwise}
                        \\end{cases}

        Parameters
        ----------
        X : float of np.ndarray
            x values
        X0 : float
            threshold value
        FACT : float
            factor to define EPS = X0 * FACT. limit value to ensure smoothness
            of the function (continuity of derivative in interval X in
            [X0 - EPS, X0 + EPS])

        Returns
        -------
        res : float of np.ndarray
            The resulting function

    """
    def poly(X, alpha, beta, gamma):
        """ The polynom which assure smoothness, i.e. continuity of derivative
        """
        return -(-alpha + X) * (-2 * alpha**2 * beta + 2 * alpha**2 * gamma+\
           2 * alpha**2*X - alpha*beta**2 + 3*alpha*beta + \
           2 * alpha * gamma * X - 3 * alpha * gamma + alpha * X**2 -\
           3 * alpha * X + beta**2 * X - beta * X**2 + 2 * gamma * X**2 -\
           3 * gamma * X) / ((alpha - beta) * (2 * alpha**2 + 2 * alpha * beta -\
           3 * alpha + 2 * beta**2 - 3 * beta))
    # def of EPS value
    EPS = FACT * X0

    alpha = X0 - EPS
    beta = X0 + EPS
    gamma = EPS
    if isinstance(X, np.ndarray):
        if np.any(X < beta) or np.any(X > alpha):
            # there are values in array that need to be eval by poly
            # decomposition of 2 arrays
            res = np.zeros(X.shape)
            argsPoly = np.flatnonzero(np.logical_and(X > alpha, X < beta))
            argsMacau = np.flatnonzero(np.logical_or(X <= alpha, X >= beta))
            res[argsPoly] = poly(X[argsPoly], alpha, beta, gamma)
            res[argsMacau] = (np.abs(X[argsMacau]-X0) + X[argsMacau]-X0) * 0.5
        else:
            # direct eval of macaulay function
            res = (np.abs(X - X0) + X - X0) * 0.5
    else:
        if X < beta and X > alpha:
            # within the interval
            res = poly(X, alpha, beta, gamma)
        else:
            res = (np.abs(X - X0) + X - X0) * 0.5
    return res

def switchingPheno(X, X_tauc, dictSwitching):
    """
    """
    corresp = {'NO': 0, 'PDGF': 2, 'FGF': 4, 'Ag': 6, 'TGF': 8,
               'TNF': 10, 'MMP': 12}
    chgtPh = np.zeros(2)
    keyWords = ['int.p.+th.e.', 'int.p.+th.d.', '-']
    s = 0
    for (GF, v) in dictSwitching.items():
        if v in keyWords:
            if(v == keyWords[0]):
                s += 1
                idx = corresp[GF]
            if(v == keyWords[1]):
                s += 1
                idx = corresp[GF]
                # print('GF', GF)
                # print('X[idx]', X[idx])
                # print('X[idx+1]', X[idx+1])
                # print('X_tauc[idx]', X_tauc[idx])
                # print('X_tauc[idx+1]', X_tauc[idx+1])
                # print('intima', PHI_d(X[idx], 1.0) - PHI_d(X_tauc[idx], 1.0) )
                # print('media', PHI_d(X[idx + 1], 1.0) - PHI_d(X_tauc[idx + 1], 1.0) )
        else:
            print('GF', GF)
            raise ValueError("Keywork not handled for GF : {}.".format(GF) +
                    "keywork accepted 'int.p.+th.e' / given {}".format(v))
    chgtPh = chgtPh / s
    return chgtPh

def colAging(X, X_tauC, dCi_eq, dCi_eq_tau, dCm_eq, dCm_eq_tau, FACT_PHI=5e-2):
    """ Collagen aging rate calculation.
    X unknow vector a time t=t, where :
                X[18] = Cy_i(t)
                X[19] = Cy_m(t)
    X_tauC unknow vector a time t=t-tauC, where :
                X_tauC[18] = Cy_i(t-tauC)
                X_tauC[19] = Cy_m(t-tauC)

    Parameters
    ----------
    X : np.ndarray of shape (Neq,)
        State vector
    X_tauC : np.ndarray of shape (Neq,)
        Delay state vector
    dCi_eq : float
        None dimensional amount of collagen in intima at equilibrium
    dCi_eq_tau : float
        None dimensional amount of collagen in intima at equilibrium at t=t-tau
    dCm_eq : float
        None dimensional amount of collagen in media at equilibrium
    dCm_eq_tau : float
        None dimensional amount of collagen in media at equilibrium at t=t-tau
    FACT_PHI : float
        None dimensional tolerance around threshold values

    Returns
    -------
    phiE_Cj : np.ndarray of shape (2,)
        Result of collagen aging rate

    """
    tol = 1e-8 # tol to check is value are equal at tol
    phiE_Cj = np.zeros(2)
    i_t = X[18] > dCi_eq and not np.isclose(X[18],dCi_eq,tol)
    i_t_tau = X_tauC[18] > dCi_eq_tau and not np.isclose(X_tauC[18],dCi_eq_tau,tol)
    if i_t and i_t_tau:
        phiE_Cj[0] = X[18] - dCi_eq
    else :
        phiE_Cj[0] = 0.0

    m_t = X[19] > dCm_eq and not np.isclose(X[19],dCm_eq,tol)
    m_t_tau = X_tauC[19] > dCm_eq_tau and not np.isclose(X_tauC[19],dCm_eq_tau,tol)
    if m_t and m_t_tau:
        phiE_Cj[1] = X[19] - dCm_eq
    else:
        phiE_Cj[1] = 0.0
    return phiE_Cj

    # phiE_Cj = np.zeros(2)

    # # boolean if  > x_t + FACT_PHI
    # i_t_delta = (X[18] >= (dCi_eq + FACT_PHI))
    # # boolean if  in the intervall 
    # i_t_inter = ((dCi_eq - FACT_PHI)<= X[18]) and (X[18] < (dCi_eq + FACT_PHI))
    # # same a t = t-tau
    # i_t_tau_delta = (X_tauC[18] >= (dCi_eq_tau + FACT_PHI))
    # i_t_tau_inter = ((dCi_eq_tau - FACT_PHI)<= X_tauC[18]) and \
            # (X_tauC[18] < (dCi_eq_tau + FACT_PHI))

    # # print('          X[18]', X[18], 'dCi_eq', dCi_eq)
    # # print('          X[18] > dCi_eq', X[18] > dCi_eq,
            # # 'np.isclose(X[18],dCi_eq,,tol)', np.isclose(X[18],dCi_eq,tol))
    # # print('          X_tauC[18]', X_tauC[18], 'dCi_eq_tau', dCi_eq_tau)
    # # print('          X_tauC[18] > dCi_eq_tau', X_tauC[18] > dCi_eq_tau,
            # # 'np.isclose(X_tauC[18], dCi_eq_tau, tol)', np.isclose(X_tauC[18], dCi_eq_tau, tol))
    # # print('i_t', i_t,'i_t_tau', i_t_tau)

    # if i_t_delta and i_t_tau_delta:
        # # print('         YES : X[18] - dCi_eq', X[18] - dCi_eq)
        # phiE_Cj[0] = X[18] - dCi_eq
    # elif i_t_inter and i_t_tau_inter:
        # phiE_Cj[0] = (X[18] - (dCi_eq - FACT_PHI))**2 / (4 * FACT_PHI)
    # else :
        # phiE_Cj[0] = 0.0

    # # boolean if  > x_t + FACT_PHI
    # m_t_delta = (X[19] >= (dCm_eq + FACT_PHI))
    # # boolean if  in the intervall 
    # m_t_inter = ((dCm_eq - FACT_PHI)<= X[19]) and (X[19] < (dCm_eq + FACT_PHI))
    # # same a t = t-tau
    # m_t_tau_delta = (X_tauC[19] >= (dCm_eq_tau + FACT_PHI))
    # m_t_tau_inter = ((dCm_eq_tau - FACT_PHI)<= X_tauC[19]) and \
            # (X_tauC[19] < (dCm_eq_tau + FACT_PHI))

    # # print('          X[19]', X[19], 'dCm_eq', dCm_eq)
    # # print('          X_tauC[19]', X_tauC[19], 'dCm_eq_tau', dCm_eq_tau)
    # # print('          X[19] > dCm_eq',  X[19]>dCm_eq, 'X_tauC[19]>dCm_eq_tau', X_tauC[19]>dCm_eq_tau)
    # # print('m_t', m_t,'m_t_tau', m_t_tau)
    # if m_t_delta and m_t_tau_delta:
        # # print('         YES : X[18] - dCi_eq', X[18] - dCi_eq)
        # phiE_Cj[1] = X[19] - dCm_eq
    # elif m_t_inter and m_t_tau_inter:
        # phiE_Cj[1] = (X[10] - (dCm_eq - FACT_PHI))**2 / (4 * FACT_PHI)
    # else :
        # phiE_Cj[1] = 0.0

    # return phiE_Cj


def paramsRelationsGFs(X_nd, Z, dictParam, T, csts, FACT=0.1):
    """
        For non delay dependante parameters i.e. parameter execpt
        switching  phenotype $c_{i,m}$
        return params with is a array of len = nbr of parameter
        calcuated within dictParam. params[k] = value of params dictParam[j]
        where j = ['p', 'a', 'm'.... the order of dictParams]

    Parameters
    ----------
    X_nd : ndarray, shape (Neq,) or shape (Neq, Nt) or (Neq, Nt, Nx)
        state vector y(t)
    Z : ndarray, shape (Neq,) or shape (Nzq, Nt) or (Neq, Nt, Nx)
        state vector y(t-tau)
    dictParam : dict,
        dictionnary describing the relations between GFs and weighted average
    T : ndarray, shape (nCst,nGFs)
        thresholds values for each GF and each functional prop
    csts : ndarray, shape (Cst,)
        constant values for species functional properties
    FACT : float
        factor to define EPS = X0 * FACT. limit value to ensure smoothness of the
        function (continuity of derivative in interval X in [X0 - EPS, X0 + EPS])

    Returns
    -------
    params : list of list of shape 6
        A ``bad format``, as params = [[p_i, p_m], [a_i, a_m], [m_i,m], [l_i,l_m],
        [chi_i, chi_m], [c_i, c_m]], where p_i,m , a_i,m ... are float or
        np.ndarray of shape or (Nt), (Nt, Nx).

    """
    keyWords = ['p.', 'i.p.',
                'p.+th.e.', 'p.+th.d.',
                'i.p.+th.e.', 'i.p.+th.d.',
                'int.p.+th.e.', 'int.p.+th.d.', '-']
    corresp = {'NO': 0, 'PDGF': 2, 'FGF': 4, 'Ag': 6, 'TGF': 8,
               'TNF': 10, 'MMP': 12}
    if(isinstance(X_nd, np.ndarray) and len(X_nd.shape)==1):
        # condition is X_nd is the vector array of differential resolution
        w_a = np.zeros((len(dictParam),2))
        m = 0
        for k in dictParam.keys():
            noRelated = all([v == '-' for v in dictParam[k].values()])
            if noRelated:
                # the functional parameter have not dependency on GFs
                w_a[m,:] = 1.0
            else:
                s = 0
                for (GF, v) in dictParam[k].items():
                    if v in keyWords:
                        if v in keyWords[:-1]:
                            s += 1
                            idx = corresp[GF]
                            # print('GF', GF)
                            # print('relation', v)
                            # print('idx', idx, '\n')
                            if(v == keyWords[0]):
                                w_a[m,0] += X_nd[idx]
                                w_a[m,1] += X_nd[idx + 1]
                            elif(v == keyWords[1]):
                                w_a[m,0] += (X_nd[idx])**-1
                                w_a[m,1] += (X_nd[idx + 1])**-1
                            elif(v in keyWords[2:] and 0.0 in T[m,:]):
                                print('we demand a threshold dependency for param\
                                      %s, but the threshold value given is 0' % k)
                                raise ValueError('if threshold demanded, threshold \
                                                 values must do different of 0.0')
                            elif(v == keyWords[2]):
                                # print('in paramsRelation Threshold PHI_e')
                                # print('GF', GF)
                                # print('relation', v)
                                # print('idx', idx, '\n')
                                # print('X_nd[idx]', X_nd[idx], T[m,idx])
                                # print('PHI_e(X_nd[idx], T[m,idx])',
                                        # PHI_e(X_nd[idx], T[m,idx], FACT))
                                w_a[m,0] += PHI_e(X_nd[idx], T[m,idx], FACT)
                                w_a[m,1] += PHI_e(X_nd[idx + 1], T[m, idx + 1], FACT)
                            elif(v == keyWords[3]):
                                w_a[m,0] += PHI_d(X_nd[idx], T[m, idx], FACT)
                                w_a[m,1] += PHI_d(X_nd[idx + 1], T[m, idx + 1], FACT)
                            elif(v == keyWords[4]):
                                w_a[m,0] += PHI_e(X_nd[idx], T[m,idx], FACT)**-1
                                w_a[m,1] += PHI_e(X_nd[idx + 1], T[m, idx + 1], FACT)**-1
                            elif(v == keyWords[5]):
                                w_a[m,0] += PHI_d(X_nd[idx], T[m,idx], FACT)**-1
                                w_a[m,1] += PHI_d(X_nd[idx + 1], T[m, idx + 1], FACT)**-1
                            elif(v == keyWords[6]):
                                # print('X_nd[idx]', X_nd[idx],'Z[idx]', Z[idx],
                                        # 'T[m,idx]', T[m,idx])
                                w_a[m,0] += PHI_e(X_nd[idx], T[m,idx], FACT) - \
                                          PHI_e(Z[idx], T[m,idx], FACT)
                                w_a[m,1] += PHI_e(X_nd[idx + 1], T[m, idx + 1], FACT) - \
                                          PHI_e(Z[idx + 1], T[m, idx + 1], FACT)
                            elif(v == keyWords[7]):
                                w_a[m,0] += PHI_d(X_nd[idx], T[m,idx], FACT) - \
                                          PHI_d(Z[idx], T[m,idx], FACT)
                                w_a[m,1] += PHI_d(X_nd[idx + 1], T[m,idx+1], FACT) -\
                                          PHI_d(Z[idx + 1], T[m,idx+1], FACT)
                    else:
                        print('\n===================================')
                        print('list of keyWords accepted = {}\n'.format(keyWords)+
                                'key word = {}'.format(v))
                        print('===================================\n')
                        raise NameError('ERROR WRONG key word to define weighted average means')
                w_a[m] = w_a[m]/s
            m += 1
        # print('w_a',w_a)
        params = np.zeros(w_a.shape)
        for k in range(len(csts)):
            params[k,:] = w_a[k,:] * csts[k]
    elif(isinstance(X_nd, np.ndarray) and len(X_nd.shape) in (2,3) or isinstance(X_nd,list)):
        # X_nd is a 2D or 3D array as a result of DDEs/ODEs integration
        # between t0 and tf
        listOfGFs = [[[], []] for _ in range(len(dictParam))]
        params = [[[], []] for _ in range(len(dictParam))]
        m = 0
        for k in dictParam.keys():
            # print('k', k)
            noRelated = all([v == '-' for v in dictParam[k].values()])
            # print('noRelated', noRelated)
            if noRelated:
                # the functional parameter have not dependency on GFs
                # initialization of a arrya of ones of correct shape X_nd[0],
                # X_nd[1] ... whatever you want
                listOfGFs[m][0].append(np.ones(X_nd[0].shape))
                listOfGFs[m][1].append(np.ones(X_nd[0].shape))
            else:
                for (GF, v) in dictParam[k].items():
                    if v in keyWords:
                        # print('GF', GF, 'v',v)
                        if v in keyWords[:-1]:
                            idx = corresp[GF]
                            if(v == keyWords[0]):
                                listOfGFs[m][0].append(X_nd[idx])
                                listOfGFs[m][1].append(X_nd[idx + 1])
                            elif(v == keyWords[1]):
                                listOfGFs[m][0].append(X_nd[idx]**-1)
                                listOfGFs[m][1].append(X_nd[idx + 1]**-1)
                            elif(v == keyWords[2]):
                                listOfGFs[m][0].append(
                                                       np.clip(X_nd[idx] - T[m,idx],
                                                       0, 1e10)
                                                       )
                                listOfGFs[m][1].append(
                                                       np.clip(X_nd[idx+1] - T[m,idx+1],
                                                       0, 1e10)
                                                       )
                            elif(v == keyWords[3]):
                                listOfGFs[m][0].append(
                                                       np.clip(-(X_nd[idx] - T[m,idx]),
                                                       0, 1e10)
                                                       )
                                listOfGFs[m][1].append(
                                                       np.clip(-(X_nd[idx+1] - T[m,idx+1]),
                                                       0, 1e10)
                                                       )
                            elif(v == keyWords[4]):
                                listOfGFs[m][0].append(
                                                       np.clip(X_nd[idx] - T[m,idx],
                                                       0, 1e10)**-1
                                                       )
                                listOfGFs[m][1].append(
                                                       np.clip(X_nd[idx+1] - T[m,idx+1],
                                                       0, 1e10)**-1
                                                       )
                            elif(v == keyWords[5]):
                                listOfGFs[m][0].append(
                                                       np.clip(-(X_nd[idx] - T[m,idx]),
                                                       0, 1e10)**-1
                                                       )
                                listOfGFs[m][1].append(
                                                       np.clip(-(X_nd[idx+1] - T[m,idx+1]),
                                                       0, 1e10)**-1
                                                       )
                            elif(v == keyWords[6] or v == keyWords[7]):
                                print('list are empty v condition ')
                    else:
                        print('list of key words accepted = %s \n key word = %s' % (v,keyWords))
                        raise NameError('ERROR WRONG key word to define weighted average means')
                #print('csts[m]', csts[m])
            # print('type(listOfGFs[m][0])', type(listOfGFs[m][0]))
            # print('listOfGFs[m][0', listOfGFs[m][0])
            # print('listOfGFs[m][1', listOfGFs[m][1])
            if listOfGFs[m][0]  and listOfGFs[m][1] :
                params[m][0] = np.mean(listOfGFs[m][0], axis=0) * csts[m]
                params[m][1] = np.mean(listOfGFs[m][1], axis=0) * csts[m]
            # print('listOfGFs[m][1]', listOfGFs[m][1])
            # print('params[m][1]', params[m][1])
            m += 1
    else:
        raise NameError('ERROR, wrong type for X_nd')
    return params


def findIndex_t0tf(t_d, t_gene, t0, tf):
    """
    find good index to be between t0 and tf
    """
    if (tf == None and t0 == None):
        tf = t_d[-1]
        t0 = t_d[0]
        idx_t0 = 0
        idx_tf = None
        idxG_t0 = 0
        idxG_tf = None
    else:
        if(t0 < t_d[0] or tf > t_d[-1]):
            print('t0 = %s t_d[0] = %s tf = %s t_d[-1] = %s' % (t0, t_d[0], tf, t_d[-1]))
            raise ValueError("postProcess time not in intervall")
        # idxG_t0 = np.searchsorted(t_gene, t0, 'left')
        idxG_t0 = len(t_gene) - len(t_gene[t_gene >= t0])
        # idxG_tf = np.searchsorted(t_gene, tf, 'left')
        idxG_tf = len(t_gene[t_gene <= tf])
        # t_gene = t_gene[idxG_t0:idxG_tf]
        # idx_t0 = np.searchsorted(t_d,[t0, tf],'left')
        # print('in findIndex t0 %s tf %s t_d %s' % (t0, tf, t_d))
        idx_t0 = abs(t_d - t0).argmin()
        idx_tf = abs(t_d - tf).argmin()
        # idx_tf = np.searchsorted(t_d,tf,'left')
        # print('t_d[idx_tf] = %s' % t_d[idx_tf])

    return idx_t0, idx_tf, idxG_t0, idxG_tf

def diffusionBC_EC(BC, Rf, Rext, e_i, e_m, L, kappa):
    """ BC boundary condition at ECs cells

    Parameters
    ----------
    BC : float or 1D array or 2D array
       If BC is
        * 1D array -> 1D simulation
        * 2D array -> 1D simulation
    Rf :
        Radius of the segment of artery considered
    Rext : float
        External radius of the segment of artery considered (always constant)
    e_i :
        Intimal thickness
    e_m :
        Medial thickness
    L :
        Length of the segment of artery considered
    kappa : float
        Ratio bewtenn consumption rate of milieu k_w over the diffusion
        coefficiant of the milieu D_w kappa = k_w/D_w
    Returns
    -------
    intFlux :
        if BC is 1D array (in a 1D simulation):
            specific managment
            return array ()
        elif BC is 2D array (in a 2D simulation):
            another one
            return array.shape = (NtypeOfGFs, 2, Nx)

    """
    N = 400 # number of integration points
    # print('\nBC', BC, 'Rf', Rf, '\n')
    if(isinstance(BC, np.ndarray) and BC.ndim==2 and
       isinstance(Rf, np.ndarray) and Rf.ndim == 1 and
       isinstance(L, np.ndarray) and L.ndim == 1):
        # when 2D simulation and Rf array
        # print("when 2D simulation and Rf array and L array")
        NtypeOfGFs, Nx = BC.shape
        intFlux = np.zeros((NtypeOfGFs, 2, Nx))
        for y in range(NtypeOfGFs):
            for z in range(Nx):
                denom = k1(kappa * Rf[z]) * i1(kappa * Rext) - \
                        k1(kappa * Rext) * i1(kappa * Rf[z])
                # print('denom', denom)
                RLEI = Rf[z] + e_i[z]
                RLEE = RLEI + e_m[z]
                r_i = np.linspace(Rf[z], RLEI, N)
                r_m = np.linspace(RLEI, RLEE, N)
                fluxi = 2. * np.pi * r_i * L[z] * BC[y, z] *\
                        (k1(kappa * r_i) *\
                        i1(kappa * Rext) -\
                        k1(kappa * Rext)*\
                        i1(kappa * r_i)  ) / denom
                fluxm = 2. * np.pi * r_m * L[z] * BC[y, z] *\
                        (k1(kappa * r_m) *\
                        i1(kappa * Rext) -\
                        k1(kappa * Rext) * \
                        i1(kappa * r_m) ) / denom
                intFlux[y, 0, z] = 1./e_i[z] * np.trapz(fluxi, r_i)
                intFlux[y, 1, z] = 1./e_m[z] * np.trapz(fluxm, r_m)
    elif(isinstance(BC, np.ndarray) and BC.ndim == 2 and
        isinstance(Rf, np.ndarray) and Rf.ndim == 1):
        # case of postProcessing after 1D simultation
        print("case of postProcessing after 1D simultation")
        N_Rf = len(Rf)
        NtypeOfGFs = len(BC)
        intFlux = np.zeros((NtypeOfGFs, 2, N_Rf))
        for y in range(NtypeOfGFs):
            for z in range(N_Rf):
                denom = k1(kappa * Rf[z]) * i1(kappa * Rext) - \
                        k1(kappa * Rext) * i1(kappa * Rf[z])
                # print('denom', denom)
                RLEI = Rf[z] + e_i[z]
                RLEE = RLEI + e_m[z]
                r_i = np.linspace(Rf[z], RLEI, N)
                r_m = np.linspace(RLEI, RLEE, N)
                fluxi = 2. * np.pi * r_i * L * BC[y,z] *\
                        (k1(kappa * r_i) * i1(kappa * Rext) -\
                        k1(kappa * Rext) * i1(kappa * r_i)  ) / denom # L[z]
                fluxm = 2. * np.pi * r_m * L * BC[y,z] *\
                        (k1(kappa * r_m) * i1(kappa * Rext) -\
                        k1(kappa * Rext) * i1(kappa * r_m) ) / denom # L[z]
                intFlux[y, 0, z] = 1. / e_i[z] * np.trapz(fluxi, r_i)
                intFlux[y, 1, z] = 1. / e_m[z] * np.trapz(fluxm, r_m)
    elif(isinstance(BC, np.ndarray) and BC.ndim == 1 and isinstance(Rf, np.float)):
        # cas simu 1D durant simu
        # print("cas simu 1D durant simu")
        NtypeOfGFs = len(BC)
        intFlux = np.zeros((NtypeOfGFs, 2))
        for z in range(NtypeOfGFs):
            denom = k1(kappa * Rf) * i1(kappa * Rext) - \
                    k1(kappa * Rext) * i1(kappa * Rf)
            # print('kappa, Rf', kappa, Rf)
            # print('k1(kappa * Rf)', k1(kappa * Rf))
            # print('i1(kappa * Rext)', i1(kappa * Rext))
            # print('k1(kappa * Rext)', k1(kappa * Rext))
            # print('i1(kappa * Rf)', i1(kappa * Rf))
            # print('denom', denom)
            RLEI = Rf + e_i
            RLEE = RLEI + e_m
            r_i = np.linspace(Rf, RLEI, N)
            r_m = np.linspace(RLEI, RLEE, N)
            # print('coef', 2. * np.pi * L * BC[z] / denom)
            coef = 2. * np.pi * L * BC[z] / denom
            fluxi = coef * r_i *(
                    k1(kappa * r_i) * i1(kappa * Rext) -
                    i1(kappa * r_i) * k1(kappa * Rext))
            fluxm = coef * r_m * (
                    k1(kappa * r_m) * i1(kappa * Rext) -
                    i1(kappa * r_m) * k1(kappa * Rext))
            # print('e_i', e_i, 'e_m', e_m)
            # print('Rf', Rf, "R_LEI", RLEI, "RLEE", RLEE)
            # print('max fluxi', np.max(fluxi))
            # print('min fluxi', np.min(fluxi))
            # print('max fluxm', np.max(fluxm))
            # print("trapz fluxi", np.trapz(fluxi, r_i))
            # print("trapz fluxm", np.trapz(fluxm, r_m))
            intFlux[z,0] = 1./e_i * np.trapz(fluxi, r_i)
            intFlux[z,1] = 1./e_m * np.trapz(fluxm, r_m)
    elif(isinstance(BC, np.ndarray) and BC.ndim == 3 and
         isinstance(Rf, np.ndarray) and Rf.ndim == 2 and
         isinstance(L, np.ndarray) and L.ndim == 1):
        print("when 2D simulation and postprocess")
        NtypeOfGFs, Nx, Nt = BC.shape
        intFlux = np.zeros((NtypeOfGFs, 2, Nx, Nt))
        for y in range(NtypeOfGFs):
            for z in range(Nx):
                for i in range(Nt):
                    denom = k1(kappa * Rf[i, z]) * i1(kappa * Rext) - \
                            k1(kappa * Rext) * i1(kappa * Rf[i, z])
                    # print('denom', denom)
                    RLEI = Rf[i, z] + e_i[i, z]
                    RLEE = RLEI + e_m[i, z]
                    r_i = np.linspace(Rf[i, z], RLEI, N)
                    r_m = np.linspace(RLEI, RLEE, N)
                    fluxi = 2. * np.pi * r_i * L[z] * BC[y, z, i] *\
                            (k1(kappa * r_i) *\
                            i1(kappa * Rext) -\
                            k1(kappa * Rext)*\
                            i1(kappa * r_i)  ) / denom
                    fluxm = 2. * np.pi * r_m * L[z] * BC[y, z, i] *\
                            (k1(kappa * r_m) *\
                            i1(kappa * Rext) -\
                            k1(kappa * Rext) * \
                            i1(kappa * r_m) ) / denom
                    intFlux[y, 0, z, i] = 1./e_i[i, z] * np.trapz(fluxi, r_i)
                    intFlux[y, 1, z, i] = 1./e_m[i, z] * np.trapz(fluxm, r_m)
    else:
        print("in diffusion ")
        print('BC.shape = %s,\n Rf.shape=%s,\n Rext.shape=%s, e_i.shape=%s,\
               \ne_m.shape=%s,\n L.shape=%s,\n kappa=%s' %\
               (BC.shape, Rf.shape, Rext.shape, e_i.shape,
                   e_m.shape, L.shape, kappa))
        raise TypeError("utilisation de diffusion dans un cadre non implemente")
    return intFlux



def domainCut(iCPU, NCPU, Nx):
    """ The domain's cut between CPUs
    Parameters
    ----------
    iCPU : int
        The ieme CPU
    NCPU : int
        Totale number of CPU
    Nx : int
        Number total number of element in longitudinal direction

    Returns
    -------
    k_iCPU : int
        Starting element of the ieme CPU
    Nx_iCPU : int
        Number of CPU of the ieme CPU after the domain's cut
    """
    Nx_iCPU = Nx//NCPU
    rest = Nx % NCPU
    if(iCPU<rest):
        Nx_iCPU = Nx_iCPU + 1
        k_iCPU = Nx_iCPU*iCPU
    else:
        k_iCPU = (Nx_iCPU+1)*(rest) + Nx_iCPU * (iCPU-rest)
    return k_iCPU, Nx_iCPU

def testEquilibrium(t0, y0, h, Z0, fun, args, eps):
    """
    Parameters
    t0 : float
        intial time
    y0 : np.ndarray
        Inital values
    h : np.ndarray
        history values
    Z0 : np.ndarray
        delayed state values
    fun : callable
        The function to call
    args : tuple
        Tuple of arguments to pass to fun
    Returns
    -------
    eq : bool
        Is there a equilibrium ?
    """
    dydt = fun(t0, y0, Z0, *args)
    norm = np.linalg.norm(dydt)
    print('norm', norm)
    return norm < eps

def mesure(phi):
    """ Give the principal mesure of an angle
    """
    x = phi
    if phi > 0:
        while x > np.pi:
            x -= 2*np.pi
    else:
        while x <= -np.pi:
            x += 2*np.pi
    return x

def computeInflaV_gr(species, set_params):
    """
    Parameters
    ----------
    species :
    """
    (rho_s, rho_c, rho_E) = set_params['densities']
    if 'P_nd' in set_params['species_nd']:
        (Qi_t, Qm_t, Si_t, Sm_t, Cji_t, Cjm_t,
                Cvi_t, Cvm_t, E_t, m_t, M_t, P_t) = species
    else:
        (Qi_t, Qm_t, Si_t, Sm_t, Cji_t, Cjm_t,
                Cvi_t, Cvm_t, E_t, m_t, M_t) = species

    # computation volume

    Vi_E = E_t / rho_E
    Vi_m = m_t / rho_s
    Vi_M = M_t / rho_s
    Vi_S = Si_t / rho_s
    Vi_Q = Qi_t / rho_s
    Vi_Cj = Cji_t / rho_c
    Vi_Cv = Cvi_t / rho_c

    Vm_S = Sm_t / rho_s
    Vm_Q = Qm_t / rho_s
    Vm_Cj = Cjm_t / rho_c
    Vm_Cv = Cvm_t / rho_c

    Vgr_i = Vi_E + Vi_S + Vi_Q + Vi_Cj + Vi_Cv + Vi_m + Vi_M
    Vgr_m = Vm_S + Vm_Q + Vm_Cj + Vm_Cv

    return Vgr_i, Vgr_m, Vi_E, Vi_m, Vi_M, Vi_S, Vi_Q, \
            Vi_Cj,Vi_Cv,Vm_S, Vm_Q, Vm_Cj, Vm_Cv


def computeV_gr(species, set_params):
    """
    Parameters
    ----------
    species :
    """
    (rho_s, rho_c, rho_E) = set_params['densities']
    if 'P_nd' in set_params['species_nd']:
        (Qi_t, Qm_t, Si_t, Sm_t, Cji_t, Cjm_t, Cvi_t, Cvm_t, E_t, P_t) = species
    else:
        (Qi_t, Qm_t, Si_t, Sm_t, Cji_t, Cjm_t, Cvi_t, Cvm_t, E_t) = species

    # computation volume
    Vi_E = E_t / rho_E
    Vi_S = Si_t / rho_s
    Vi_Q = Qi_t / rho_s
    Vi_Cj = Cji_t / rho_c
    Vi_Cv = Cvi_t / rho_c

    Vm_S = Sm_t / rho_s
    Vm_Q = Qm_t / rho_s
    Vm_Cj = Cjm_t / rho_c
    Vm_Cv = Cvm_t / rho_c

    Vgr_i = Vi_E + Vi_S + Vi_Q + Vi_Cj + Vi_Cv
    Vgr_m = Vm_S + Vm_Q + Vm_Cj + Vm_Cv

    return Vgr_i, Vgr_m, Vi_E, Vi_S, Vi_Q, \
            Vi_Cj,Vi_Cv,Vm_S, Vm_Q, Vm_Cj, Vm_Cv


def findNtMax(X):
    """
    """
    N = []
    for i in range(len(X)):
        N.append(len(X[i].t))
    el_Nt_max = np.argmax(N)
    print('element Nt_max = ', el_Nt_max)
    return el_Nt_max, max(N)

def calcul_var1D_2D(sol_nd, set_params, dxE, T_d, V_Oi, V_Om, Vl, t_eval_nd):
    """

    Parameters
    ----------
    sol_nd : list
        List of DdeResults
    set_params:  dict
    dxE : np.ndarray of shape (Ni_TG,)
        Length of each TG compartiments
    T_d : float
        Dimensional time
    V_Oi : np.ndarray of shape (Ni_TG,)
        Volume of others species in the intima
    V_Om : np.ndarray of shape (Ni_TG,)
        Volume of others species in the media
    V_l : np.ndarray of shape (Ni_TG,)
        Luminal volume
    t_eval_nd : float
        Time where returns arrays are evaluated

    Returns
    -------
    eTot : np.ndarray of shape (Ni_TG,)
        Total thickness of the arterial wall at t=t_eval_nd
    e_i : np.ndarray of shape (Ni_TG,) 
        Intima thickness of the arterial wall at t=t_eval_nd
    e_m : np.ndarray of shape (Ni_TG,)
        Media thickness of the arterial wall at t=t_eval_nd
    R_gr : np.ndarray of shape (Ni_TG,)
        Luminal radius due to tissue growth at t=t_eval_nd

    """
    (rho_s, rho_c, rho_E) = set_params['densities']
    # print('calcul_var1D_2D t_eval_nd = ', t_eval_nd)
    Neq = sol_nd[0].y.shape[0]
    # print('sol_nd[0].y.shape', sol_nd[0].y.shape)
    # print('t_eval_nd', t_eval_nd)
    Ni_TG = len(sol_nd)

    Y_tf_nd = np.zeros((Neq, Ni_TG))
    # print('Y_tf_nd.shape', Y_tf_nd.shape)
    for j in range(Ni_TG):
        if sol_nd[j].__class__.__name__ == 'Inline':
            # here the sol.h_b and sol.y0 is the same init array
            # print('Inline')
            Y_tf_nd[:,j] = sol_nd[j].y0_nd
        else:
            # print('DDEresult t_eval_nd = ', t_eval_nd)
            # print('sol_nd[j].sol.tmax %s tmin %s' % (sol_nd[j].sol.t_max, sol_nd[j].sol.t_min))
            Y_tf_nd[:,j] = sol_nd[j].sol(t_eval_nd)
    # calculation of derived variables
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    e_a = Rext - R_LEE
    NGFs = len(set_params['GFs_nd'])
    # print('NGFs', NGFs)
    species_nd = Y_tf_nd[NGFs:,:]
    # print('Y_tf_nd', Y_tf_nd[NGFs:,:])
    # print("et_params['species_dim'].T.shape", set_params['species_dim'].T.shape)
    species = species_nd * set_params['species_dim'].T
    if set_params['model'].count('Infla') > 0:
        if 'P_nd' in set_params['species_nd']:
            (Qi_t, Qm_t, Si_t, Sm_t, Cji_t, Cjm_t,
                    Cvi_t, Cvm_t, E_t, P_t, m_t, M_t) = species
        else:
            (Qi_t, Qm_t, Si_t, Sm_t, Cji_t, Cjm_t,    
                    Cvi_t, Cvm_t, E_t, m_t, M_t) = species
        # computation volume
        Vi_E = E_t / rho_E
        Vi_m = m_t / rho_s
        Vi_M = M_t / rho_s
        Vi_S = Si_t / rho_s
        Vi_Q = Qi_t / rho_s
        Vi_Cj = Cji_t / rho_c
        Vi_Cv = Cvi_t / rho_c

        Vm_S = Sm_t / rho_s
        Vm_Q = Qm_t / rho_s
        Vm_Cj = Cjm_t / rho_c
        Vm_Cv = Cvm_t / rho_c
    
        Vgr_i = Vi_E + Vi_S + Vi_Q + Vi_Cj + Vi_Cv + Vi_m + Vi_M
        Vgr_m = Vm_S + Vm_Q + Vm_Cj + Vm_Cv

    else:
        if 'P_nd' in set_params['species_nd']:
            (Qi_t, Qm_t, Si_t, Sm_t, Cji_t, Cjm_t,
                    Cvi_t, Cvm_t, E_t, P_t) = species
        else:
            (Qi_t, Qm_t, Si_t, Sm_t, Cji_t, Cjm_t,    
                    Cvi_t, Cvm_t, E_t) = species
        # computation volume
        Vi_E = E_t / rho_E
        Vi_S = Si_t / rho_s
        Vi_Q = Qi_t / rho_s
        Vi_Cj = Cji_t / rho_c
        Vi_Cv = Cvi_t / rho_c
        Vm_S = Sm_t / rho_s
        Vm_Q = Qm_t / rho_s
        Vm_Cj = Cjm_t / rho_c
        Vm_Cv = Cvm_t / rho_c
    
        Vgr_i = Vi_E + Vi_S + Vi_Q + Vi_Cj + Vi_Cv
        Vgr_m = Vm_S + Vm_Q + Vm_Cj + Vm_Cv

    Vi = Vgr_i + V_Oi
    Vm = Vgr_m + V_Om
    # print('Vi[0,0]', Vi[0,0], 'Vm[0,0]', Vm[0,0])

    # calcul of geometric thickness and radius
    # print('dxE.shape', dxE.shape)
    e_m = R_LEE - np.sqrt((R_LEE)**2 - Vm / (np.pi * dxE))
    # print('e_m[0,0]', e_m[0,0], 'dxE', dxE[0])
    e_i = R_LEE - e_m - np.sqrt((R_LEE - e_m)**2 - Vi / (np.pi * dxE))
    # print('R_LEE %s e_m %s e_i %s' % (R_LEE, e_m, e_i))
    eTot = e_m + e_i + e_a
    R_gr = Rext - eTot

    return eTot, e_i, e_m, R_gr

def calcul_var1D_3D(sol_nd, set_params, TCO_order,
        T_d, V_Oi, V_Om, Vl, t_eval_nd):
    """

    Parameters
    ----------
    sol_nd : list
        List of DdeResults
    set_params:  dict
    TCO_order : np.ndarray of shape (Np_TG, 8, 4)
        Tidy table of coordinate of TG compartiments
    T_d : float
        Dimensional time
    V_Oi : np.ndarray of shape (Ni_TG,)
        Volume of others species in the intima
    V_Om : np.ndarray of shape (Ni_TG,)
        Volume of others species in the media
    V_l : np.ndarray of shape (Ni_TG,)
        Luminal volume
    t_eval_nd : float
        Time where returns arrays are evaluated

    Returns
    -------
    eTot : np.ndarray of shape (Ni_TG,)
        Total thickness of the arterial wall at t=t_eval_nd
    e_i : np.ndarray of shape (Ni_TG,) 
        Intima thickness of the arterial wall at t=t_eval_nd
    e_m : np.ndarray of shape (Ni_TG,)
        Media thickness of the arterial wall at t=t_eval_nd
    R_gr : np.ndarray of shape (Ni_TG,)
        Luminal radius due to tissue growth at t=t_eval_nd

    """
    (rho_s, rho_c, rho_E) = set_params['densities']
    print('calcul_var1D_2D t_eval_nd = ', t_eval_nd)
    Neq = sol_nd[0].y.shape[0]
    Ni_TG = len(sol_nd)

    Y_tf_nd = np.zeros((Neq, Ni_TG))
    for j in range(Ni_TG):
        if sol_nd[j].__class__.__name__ == 'Inline':
            # here the sol.h_b and sol.y0 is the same init array
            print('Inline')
            Y_tf_nd[:,j] = sol_nd[j].y0_nd
        else:
            # print('DDEresult t_eval_nd = ', t_eval_nd)
            # print('sol_nd[j].sol.tmax %s tmin %s' % (sol_nd[j].sol.t_max,
                # sol_nd[j].sol.t_min))
            Y_tf_nd[:,j] = sol_nd[j].sol(t_eval_nd)
    # calculation of derived variables
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    e_a = Rext - R_LEE
    NGFs = len(set_params['GFs_dim'][0,:])
    # print('NGFs', NGFs)
    species_nd = Y_tf_nd[NGFs:,:]
    species = species_nd * set_params['species_dim'].T
    if set_params['model'].count('Infla') > 0:
        (Qi_t, Qm_t, Si_t, Sm_t, Cji_t, Cjm_t,    
                Cvi_t, Cvm_t, E_t, m_t, M_t) = species
        # computation volume
        Vi_E = E_t / rho_E
        Vi_m = m_t / rho_s
        Vi_M = M_t / rho_s
        Vi_S = Si_t / rho_s
        Vi_Q = Qi_t / rho_s
        Vi_Cj = Cji_t / rho_c
        Vi_Cv = Cvi_t / rho_c

        Vm_S = Sm_t / rho_s
        Vm_Q = Qm_t / rho_s
        Vm_Cj = Cjm_t / rho_c
        Vm_Cv = Cvm_t / rho_c
    
        Vgr_i = Vi_E + Vi_S + Vi_Q + Vi_Cj + Vi_Cv + Vi_m + Vi_M
        Vgr_m = Vm_S + Vm_Q + Vm_Cj + Vm_Cv

    else:
        (Qi_t, Qm_t, Si_t, Sm_t, Cji_t, Cjm_t,    
                Cvi_t, Cvm_t, E_t) = species
        # computation volume
        Vi_E = E_t / rho_E
        Vi_S = Si_t / rho_s
        Vi_Q = Qi_t / rho_s
        Vi_Cj = Cji_t / rho_c
        Vi_Cv = Cvi_t / rho_c
        Vm_S = Sm_t / rho_s
        Vm_Q = Qm_t / rho_s
        Vm_Cj = Cjm_t / rho_c
        Vm_Cv = Cvm_t / rho_c
    
        Vgr_i = Vi_E + Vi_S + Vi_Q + Vi_Cj + Vi_Cv
        Vgr_m = Vm_S + Vm_Q + Vm_Cj + Vm_Cv

    Vi = Vgr_i + V_Oi
    Vm = Vgr_m + V_Om
    # print('Vi[0,0]', Vi[0,0], 'Vm[0,0]', Vm[0,0])

    dth = TCO_order[:,1,-1] - TCO_order[:,0,-1]
    wrongTh0Idx = np.argwhere(dth < 0.0)[:,0]
    dth[wrongTh0Idx] = TCO_order[wrongTh0Idx,1,-1] - \
            (TCO_order[wrongTh0Idx,0,-1] - 2. * np.pi)
    dxE = (TCO_order[:,4,0] - TCO_order[:,0,0])
    if np.any(dth < 0):
        raise ValueError('A voir pk, dth < 0')
    # calcul of geometric thickness and radius
    # print('dxE.shape', dxE.shape)
    e_m = R_LEE - np.sqrt((R_LEE)**2 - Vm / (np.sin(dth * 0.5) * dxE))
    # print('e_m[0,0]', e_m[0,0], 'dxE', dxE[0])
    e_i = R_LEE - e_m - np.sqrt((R_LEE - e_m)**2 - Vi / \
            (np.sin(dth * 0.5) * dxE))
    # print('R_LEE %s e_m %s e_i %s' % (R_LEE, e_m, e_i))
    eTot = e_m + e_i + e_a
    R_gr = Rext - eTot

    return eTot, e_i, e_m, R_gr

    
def pp2D(sol_nd, set_params, dxE, V_Oi, V_Om, t_data_nd):
    """Post process a 2D cas to return data on the same time(s)
    for all compartiments (using continuous extension).

    Parameters
    ----------
    sol_nd : list of len Ni_TG
        List of all object DdeSolver for each compartiments
    set_params : dict
        The set_params dictionnay of the object Jansen.
    dxE : np.ndarray shape (Ni_TG,)
        Length of compartiments
    T_d : float
        Dimensional time
    V_Oi : np.ndarray shape (Ni_TG,)
    V_Om : np.ndarray shape (Ni_TG,)
    t_data_nd : tuple or np.ndarray
        If t_data_nd if tuple : (t_gene_nd, tf_nd) = t_data_nd where t_gene
        list of generation time and tf_nd the final time. If t_data_nd if
        np.ndarray, it is the time array demanded for interpolation,
        t_data_nd = tInterp_nd

    Returns
    -------
    Y_interp : np.ndarray shape (NtInterp, Ni_TG)

    tArrNtMax : np.ndarray shape (Neq, NtInterp, Ni_TG)
        a linspace of NtMax+NtEv from t0 to tf. This array have states where
        events occur.
    var_1D : np.ndarray of shape (4, NtInterp, Ni_TG)
       As a function of time and space. (eTot, e_i, e_m, R_gr) = var_1D
    Vf_stack : np.ndarray of shape (N_Vf, NtInterp, Ni_TG)
        As a function of time and space. (VfEi, VfEm, VfQi, ....,
        VfCi, VfCm, Vf_Oi, Vf_Om) = Vf_stack
    V_stack : np.ndarray of shape (2, NtInterp, Ni_TG)
        As a function of time and space. (Vi, Vm) = Vf_stack
    Y_node : np.ndarray shape (Neq, NtMax, Ni_TG)
        state + nan values
    dYdt_node : np.ndarray (Neq, NtMax, Ni_TG)
        derivative state + nan values
    t_node : np.ndarray shape (NtMax, Ni_TG)
        integration time + nan values

    """
    T_d = set_params['T_d']
    (rho_s, rho_c, rho_E) = set_params['densities']
    print('\nstart pp2D, interpolation values from calc')
    # print('t_gene', t_gene)
    # print('z_tEvMin', z_tEvMin)
    if isinstance(t_data_nd, tuple):
        # print('t_data is tuple and  = ', t_data_nd)
        t_gene_nd, tf_nd = t_data_nd
        z_NtMax, NtMax = findNtMax(sol_nd)
        # Nt_interp = NtMax
        # print('z_NtMax %s, NtMax %s' % (z_NtMax, NtMax))
        t0_nd = sol_nd[0].t[0]
        # print('t0_nd', t0_nd)
        tInterp_nd = np.linspace(t0_nd, tf_nd, NtMax)
        # print('t_interp_nd', t_interp_nd)
        # we need to have in the interpolating time array t_interp_nd all event values
        # stored in t_gene
        # so we check if there is some duplicate times values to remove
        idxToRm = []
        for i in range(len(t_gene_nd)):
            test = np.isclose(tInterp_nd, t_gene_nd[i]) == True
            if np.any(test):
                # print('some time value to to locate and rm at t_gene %s' % (t_gene_nd[i]))
                if len(tInterp_nd[test]) > 1:
                    raise ValueError("More than 2 time values equal to t_gene_nd_i")
                idx = np.argwhere(test)[:,0]
                idxToRm.append(idx)
        # print("idxToRm", idxToRm)
        # remove index at in t_interp_nd
        # print('t_interp_nd', t_interp_nd)
        tInterp_nd = np.delete(tInterp_nd, idxToRm)
        # print('t_interp_nd', t_interp_nd)
        tInterp_nd = np.sort(np.append(tInterp_nd, np.asarray(t_gene_nd)))
    elif isinstance(t_data_nd, np.ndarray):
        tInterp_nd = t_data_nd
    else:
        raise TypeError("t_data_nd not tuple either np.ndarray !")
    # update of Nt_interp shape
    NtInterp = len(tInterp_nd)
    Neq = sol_nd[0].y.shape[0]
    Ni_TG = len(sol_nd)
    # init arrays
    Y_interp_nd = np.zeros((Neq, NtInterp, Ni_TG))
    if isinstance(t_data_nd, tuple):
        t_node_nd = np.ones((NtMax, Ni_TG)) * np.nan
        Y_node_nd = np.ones((Neq, NtMax, Ni_TG)) * np.nan
        dYdt_node_nd = np.ones((Neq, NtMax, Ni_TG)) * np.nan
    for i in range(Neq):
        for j in range(Ni_TG):
            # print('z=%s' % j)
            if sol_nd[j].__class__.__name__ == 'Inline':
                # here the sol.h_b and sol.y0 is the same init array
                # print('sol_nd[j].y', sol_nd[j].y)
                # print('len(sol_nd[j].y) %s Neq %s' % (len(sol_nd[j].y), Neq) )
                # print('sol_nd[j].y %s ' % (sol_nd[j].h_b))
                # print('np.full((NtMax, Neq), NtMax), sol_nd[j].y).shape ',  (np.full((NtMax, Neq), sol_nd[j].h_b).shape))
                # error when I try np.full((Neq, NtMax), sol_nd[j].y)
                # so I do np.full((NtMax, Neq), sol_nd[j].y).T
                # contruct a array with shape Neq, NtMax full of sol_nd[j].y
                Y_interp_nd[i, :, j] = np.full((NtInterp, Neq), sol_nd[j].y0_nd).T[i,:]
            else:
                # print('\t - has not sol class Inline')
                # if np.any(np.diff(ts) < 1e-8):
                    # # case of a discont at initial time
                    # # not saving the first value which is the history val
                    # t_interp_nd = np.append(np.array([T0]), t_interp_nd)
                    # Y_interp[i, :, j] = sol_nd[j].sol(t_interp_nd)[i,1:]
                # else:
                    # Y_interp[i, :, j] = sol_nd[j].sol(t_interp_nd)[i,:]
                Y_interp_nd[i, :, j] = sol_nd[j].sol(tInterp_nd)[i,:]
            # filling array with none interp values and time
            if isinstance(t_data_nd, tuple):
                N_t_j = len(sol_nd[j].t)
                t_node_nd[:N_t_j, j] = sol_nd[j].t
                Y_node_nd[:, :N_t_j, j] = sol_nd[j].y
                dYdt_node_nd[:, :N_t_j, j] = sol_nd[j].yp

    # calculation of derived variables
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    e_a = Rext - R_LEE
    NGFs = len(set_params['GFs_nd'])
    # print('NGFs', NGFs)
    species_nd = Y_interp_nd[NGFs:,:,:]
    # print('pop_nd.shape', pop_nd.shape, 'species_dim.T.shape', set_params['species_dim'].T.shape)
    species = species_nd * set_params['species_dim'].T[:,np.newaxis,:]

    if set_params['model'].count('Infla') > 0:
        (Vgr_i, Vgr_m,
            Vi_E, Vi_m, Vi_M, Vi_S, Vi_Q,
            Vi_Cj, Vi_Cv, Vm_S, Vm_Q, Vm_Cj, Vm_Cv) = computeInflaV_gr(species,
                    set_params)
    else:
        (Vgr_i, Vgr_m,
            Vi_E, Vi_S, Vi_Q, Vi_Cj,
            Vi_Cv,Vm_S, Vm_Q, Vm_Cj, Vm_Cv) = computeV_gr(species, set_params)

    Vi = Vgr_i + V_Oi
    Vm = Vgr_m + V_Om

    V_stack = np.vstack((Vi[np.newaxis,:,:], Vm[np.newaxis,:,:]))

    # calcul of geometric thickness and radius
    # print('dxE.shape', dxE.shape)
    e_m = R_LEE - np.sqrt((R_LEE)**2 - Vm / (np.pi * dxE))
    e_i = R_LEE - e_m - np.sqrt((R_LEE - e_m)**2 - Vi / (np.pi * dxE))
    eTot = e_m + e_i + e_a
    R_gr = Rext - eTot
    # variable for "1D" thickness raduis variations
    var_1D = np.vstack((eTot[np.newaxis,:,:],
                        e_i[np.newaxis,:,:], e_m[np.newaxis,:,:],
                        R_gr[np.newaxis,:,:]))

    # computation volume fraction
    Vf_Ei = Vi_E / Vi
    Vf_Qi = Vi_Q / Vi
    Vf_Qm = Vm_Q / Vm
    Vf_Si = Vi_S / Vi
    Vf_Sm = Vm_S / Vm
    Vf_Ci = (Vi_Cj + Vi_Cv) / Vi
    Vf_Cm = (Vm_Cj + Vm_Cv) / Vm
    Vf_Oi = V_Oi / Vi
    Vf_Om = V_Om / Vm
    Vf_stack = np.vstack((Vf_Ei[np.newaxis,:,:],
                          Vf_Qi[np.newaxis,:,:],Vf_Qm[np.newaxis,:,:],
                          Vf_Si[np.newaxis,:,:],Vf_Sm[np.newaxis,:,:],
                          Vf_Ci[np.newaxis,:,:],Vf_Cm[np.newaxis,:,:],
                          Vf_Oi[np.newaxis,:,:],Vf_Om[np.newaxis,:,:]))

    sumVf_i = Vf_Ei + Vf_Qi + Vf_Si + Vf_Ci + Vf_Oi
    sumVf_m = Vf_Qm + Vf_Sm + Vf_Cm + Vf_Om
    if np.any(np.max(sumVf_i) > 1.) and np.any(np.abs(np.max(sumVf_i) - 1.0) > 1e-8):
        print('sumVf_i %s' % sumVf_i)
        raise ValueError('sum gamma i >0')
    if np.any(np.max(sumVf_m) > 1.) and np.any(np.abs(np.max(sumVf_m) - 1.0) > 1e-8):
        raise ValueError('sum gamma m >0')

    # give back dimensional values for time
    tInterp = tInterp_nd * T_d
    if isinstance(t_data_nd, tuple):
        t_node = t_node_nd * T_d
        print('go out of pp2D')
        return Y_interp_nd, tInterp, var_1D, Vf_stack, V_stack,\
                Y_node_nd, dYdt_node_nd, t_node
    elif isinstance(t_data_nd, np.ndarray):
        return Y_interp_nd, tInterp, var_1D, Vf_stack, V_stack
    else:
        TypeError('Oops')
    
def pp3D(sol_nd, set_params, TCO_order, V_Oi, V_Om, t_data_nd):
    """Post process a 3D cas

    Parameters
    ----------
    sol_nd : list of len Ni_TG
        List of all object DdeSolver for each compartiments
    set_params : dict
        The set_params dictionnay of the object Jansen.
    TCO_order : np.ndarray of shape (Np_TG, 8, 4)
        Tidy table of coordinate of TG compartiments
    T_d : float
        Dimensional time
    V_Oi : np.ndarray shape (Ni_TG,)
    V_Om : np.ndarray shape (Ni_TG,)
    rho_E : float
        density of ECs
    rho_c : float
        density of collagen
    rho_s : float
        density of cells
    t_data_nd : tuple or np.ndarray
        If t_data_nd if tuple : (t_gene_nd, tf_nd) = t_data_nd where t_gene
        list of generation time and tf_nd the final time. If t_data_nd if
        np.ndarray, it is the time array demanded for interpolation,
        t_data_nd = tInterp_nd

    Returns
    -------
    Y_interp : np.ndarray shape (NtInterp, Ni_TG)

    tArrNtMax : np.ndarray shape (Neq, NtInterp, Ni_TG)
        a linspace of NtMax+NtEv from t0 to tf. This array have states where
        events occur.
    var_1D : np.ndarray of shape (4, NtInterp, Ni_TG)
       As a function of time and space. (eTot, e_i, e_m, R_gr) = var_1D
    Vf_stack : np.ndarray of shape (N_Vf, NtInterp, Ni_TG)
        As a function of time and space. (VfEi, VfEm, VfQi, ....,
        VfCi, VfCm, Vf_Oi, Vf_Om) = Vf_stack
    V_stack : np.ndarray of shape (2, NtInterp, Ni_TG)
        As a function of time and space. (Vi, Vm) = Vf_stack
    Y_node : np.ndarray shape (Neq, NtMax, Ni_TG)
        state + nan values
    dYdt_node : np.ndarray (Neq, NtMax, Ni_TG)
        derivative state + nan values
    t_node : np.ndarray shape (NtMax, Ni_TG)
        integration time + nan values

    """
    T_d = set_params['T_d']
    (rho_s, rho_c, rho_E) = set_params['densities']
    print('\nstart pp3D\n interpolation values from calc')
    # print('t_gene', t_gene)
    # print('z_tEvMin', z_tEvMin)
    if isinstance(t_data_nd, tuple):
        # print('t_data is tuple and  = ', t_data_nd)
        t_gene_nd, tf_nd = t_data_nd
        z_NtMax, NtMax = findNtMax(sol_nd)

        # print('z_NtMax %s, NtMax %s' % (z_NtMax, NtMax))
        t0_nd = sol_nd[0].t[0]
        # print('t0_nd', t0_nd)
        tInterp_nd = np.linspace(t0_nd, tf_nd, NtMax)
        # print('tinterp_nd', tInterp_nd)
        # we need to have in the interpolating time array tInterp_nd all event values
        # stored in t_gene
        # so we check if there is some duplicate times values to remove
        idxToRm = []
        for i in range(len(t_gene_nd)):
            test = np.isclose(tInterp_nd, t_gene_nd[i]) == True
            if np.any(test):
                # print('some time value to to locate and rm at t_gene %s' % (t_gene_nd[i]))
                if len(tInterp_nd[test]) > 1:
                    raise ValueError("More than 2 time values equal to t_gene_nd_i")
                idx = np.argwhere(test)[:,0]
                idxToRm.append(idx)
        # print("idxToRm", idxToRm)
        # remove index at in tInterp_nd
        # print('tInterp_nd', tInterp_nd)
        tInterp_nd = np.delete(tInterp_nd, idxToRm)
        # print('tInterp_nd', tInterp_nd)
        tInterp_nd = np.sort(np.append(tInterp_nd, np.asarray(t_gene_nd)))
    elif isinstance(t_data_nd, np.ndarray):
        tInterp_nd = t_data_nd
    else:
        raise TypeError("t_data_nd not tuple either np.ndarray !")
    NtInterp = len(tInterp_nd)
    Neq = sol_nd[0].y.shape[0]
    Ni_TG = len(sol_nd)
    # init arrays
    Y_interp_nd = np.zeros((Neq, NtInterp, Ni_TG))
    if isinstance(t_data_nd, tuple):
        t_node_nd = np.ones((NtMax, Ni_TG)) * np.nan
        Y_node_nd = np.ones((Neq, NtMax, Ni_TG)) * np.nan
        dYdt_node_nd = np.ones((Neq, NtMax, Ni_TG)) * np.nan
    for i in range(Neq):
        for j in range(Ni_TG):
            # print('z=%s' % j)
            if sol_nd[j].__class__.__name__ == 'Inline':
                # here the sol.h_b and sol.y0 is the same init array
                # print('sol_nd[j].y', sol_nd[j].y)
                # print('len(sol_nd[j].y) %s Neq %s' % (len(sol_nd[j].y), Neq) )
                # print('sol_nd[j].y %s ' % (sol_nd[j].h_b))
                # print('np.full((NtMax, Neq), NtMax), sol_nd[j].y).shape ',
                    #(np.full((NtMax, Neq), sol_nd[j].h_b).shape))
                # error when I try np.full((Neq, NtMax), sol_nd[j].y)
                # so I do np.full((NtMax, Neq), sol_nd[j].y).T
                # contruct a array with shape Neq, NtMax full of sol_nd[j].y
                Y_interp_nd[i, :, j] = np.full((NtMax, Neq), sol_nd[j].y0_nd).T[i,:]
            else:
                # print('\t - has not sol class Inline')
                # if np.any(np.diff(ts) < 1e-8):
                    # # case of a discont at initial time
                    # # not saving the first value which is the history val
                    # tInterp_nd = np.append(np.array([T0]), tInterp_nd)
                    # Y_interp[i, :, j] = sol_nd[j].sol(tInterp_nd)[i,1:]
                # else:
                    # Y_interp[i, :, j] = sol_nd[j].sol(tInterp_nd)[i,:]
                Y_interp_nd[i, :, j] = sol_nd[j].sol(tInterp_nd)[i,:]
            # filling array with none interp values and time
            if isinstance(t_data_nd, tuple):
                N_t_j = len(sol_nd[j].t)
                t_node_nd[:N_t_j, j] = sol_nd[j].t
                Y_node_nd[:, :N_t_j, j] = sol_nd[j].y
                dYdt_node_nd[:, :N_t_j, j] = sol_nd[j].yp

    # calculation of derived variables
    (Rext, R_LEE, R0, L) = set_params['geom_dim']
    e_a = Rext - R_LEE
    NGFs = len(set_params['GFs_dim'][0,:])
    # print('NGFs', NGFs)
    species_nd = Y_interp_nd[NGFs:,:,:]
    # print('pop_nd.shape', pop_nd.shape, 'species_dim.T.shape', set_params['species_dim'].T.shape)
    species = species_nd * set_params['species_dim'].T[:,np.newaxis,:]

    if set_params['model'].count('Infla') > 0:
        (Vgr_i, Vgr_m,
            Vi_E, Vi_m, Vi_M, Vi_S, Vi_Q,
            Vi_Cj, Vi_Cv, Vm_S, Vm_Q, Vm_Cj, Vm_Cv) = computeInflaV_gr(species,
                    set_params)
    else:
        (Vgr_i, Vgr_m,
            Vi_E, Vi_S, Vi_Q, Vi_Cj,
            Vi_Cv,Vm_S, Vm_Q, Vm_Cj, Vm_Cv) = computeV_gr(species, set_params)

    Vi = Vgr_i + V_Oi
    Vm = Vgr_m + V_Om
    V_stack = np.vstack((Vi[np.newaxis,:,:], Vm[np.newaxis,:,:]))

    dth = TCO_order[:,1,-1] - TCO_order[:,0,-1]
    wrongTh0Idx = np.argwhere(dth < 0.0)[:,0]
    dth[wrongTh0Idx] = TCO_order[wrongTh0Idx,1,-1] - \
            (TCO_order[wrongTh0Idx,0,-1] - 2. * np.pi)
    dxE = (TCO_order[:,4,0] - TCO_order[:,0,0])
    if np.any(dth < 0):
        raise ValueError('A voir pk, dth < 0')
    # calcul of geometric thickness and radius
    # print('dxE.shape', dxE.shape)
    e_m = R_LEE - np.sqrt((R_LEE)**2 - Vm / (np.sin(dth * 0.5) * dxE))
    # print('e_m[0,0]', e_m[0,0], 'dxE', dxE[0])
    e_i = R_LEE - e_m - np.sqrt((R_LEE - e_m)**2 - Vi / \
            (np.sin(dth * 0.5) * dxE))
    # print('R_LEE %s e_m %s e_i %s' % (R_LEE, e_m, e_i))
    eTot = e_m + e_i + e_a
    R_gr = Rext - eTot
    # variable for "1D" thickness raduis variations
    var_1D = np.vstack((eTot[np.newaxis,:,:],
                        e_i[np.newaxis,:,:], e_m[np.newaxis,:,:],
                        R_gr[np.newaxis,:,:]))

    # computation volume fraction
    Vf_Ei = Vi_E / Vi
    Vf_Qi = Vi_Q / Vi
    Vf_Qm = Vm_Q / Vm
    Vf_Si = Vi_S / Vi
    Vf_Sm = Vm_S / Vm
    Vf_Ci = (Vi_Cj + Vi_Cv) / Vi
    Vf_Cm = (Vm_Cj + Vm_Cv) / Vm
    Vf_Oi = V_Oi / Vi
    Vf_Om = V_Om / Vm
    Vf_stack = np.vstack((Vf_Ei[np.newaxis,:,:],
                          Vf_Qi[np.newaxis,:,:],Vf_Qm[np.newaxis,:,:],
                          Vf_Si[np.newaxis,:,:],Vf_Sm[np.newaxis,:,:],
                          Vf_Ci[np.newaxis,:,:],Vf_Cm[np.newaxis,:,:],
                          Vf_Oi[np.newaxis,:,:],Vf_Om[np.newaxis,:,:]))

    sumVf_i = Vf_Ei + Vf_Qi + Vf_Si + Vf_Ci + Vf_Oi
    sumVf_m = Vf_Qm + Vf_Sm + Vf_Cm + Vf_Om
    if np.any(np.max(sumVf_i) > 1.) and np.any(
            np.abs(np.max(sumVf_i) - 1.0) > 1e-8):
        print('sumVf_i %s' % sumVf_i)
        raise ValueError('sum gamma i >0')
    if np.any(np.max(sumVf_m) > 1.) and np.any(
            np.abs(np.max(sumVf_m) - 1.0) > 1e-8):
        raise ValueError('sum gamma m >0')

    # give back dimensional values for time
    tInterp = tInterp_nd * T_d
    if isinstance(t_data_nd, tuple):
        t_node = t_node_nd * T_d
        print('go out of pp3D')
        return Y_interp_nd, tInterp, var_1D, Vf_stack, V_stack,\
                Y_node_nd, dYdt_node_nd, t_node
    elif isinstance(t_data_nd, np.ndarray):
        return Y_interp_nd, tInterp, var_1D, Vf_stack, V_stack
    else:
        raise TypeError('Oops')


def interp1d_nearest(xOldCenter,xNewCenter,data):
    """
     on repartie propotionnellement les quantites sur un nouveau
     maillage (de tete, jene me souviens pas trop cela fait longtems)
    """
    #detecte les points qui sont en dehors
    dataInterp=np.zeros(len(xNewCenter))

    bo=True
    o=-1
    while(bo):
        o=o+1
        if(xNewCenter[o]>xOldCenter[0]):
            bo=False

    m=len(xNewCenter)
    bo=True
    while(bo):
        m=m-1
        if(xNewCenter[m]<xOldCenter[-1]):
            bo=False

    f = interpolate.interp1d(xOldCenter, data, kind='nearest')

    xNinter=xNewCenter[o:m+1]
    dataInterp[o:m+1]=f(xNinter)
    dataInterp[0:o]=data[0]
    dataInterp[m+1:]=data[-1]

    return dataInterp

def writeMainEnSight(t_gene, path_abs, path_EnSight, nameCase, dictFields):
    """ecriture du fichier main du format EnSight
        pour fabrication du dossier de post-traitement

    Parameters                                                              
    ----------                                                              
    t_gene : list                                                           
        List of time associated with generation                             
    path_abs : str                                                          
        Absolute path of the case                                           
    nameCase : str                                                          
        Name of the case which will be the name of the postPorcess directory
    dictFields : dict

    Returns
    -------
    path_EnSight : directory
        The all case in EnSight format, open path_EnSight/nameCase.case in
        ParaView for visualisation of hemodynamic during each geometrical update

    """
    iteMax = len(t_gene) - 1  # car on a t=0 compris dans t_gene
    # path_search = glob.glob(path_abs + '/gene0_*')
    # # print('path_List', path_List)
    # if len(path_search) > 1 or path_search == []:
        # raise ValueError('Error, More than 1 path')
    # else:
        # path_g0 = path_search[0]
    # get the type of root file for EnSight format
    path_g0 = '{}/gene-1_/main.case'.format(path_abs)

    newName = '%s/%s.case' % (path_EnSight, nameCase)
    shutil.copy2(path_g0, newName)
    fMain = open(newName)
    fMain_lines = fMain.readlines()
    l_var, l_mod = None, None
    for i, li in enumerate(fMain_lines):
        if li.strip().startswith('VARIABLE'):
            l_var = i + 1
        if li.strip().startswith('model'):
            l_mod = i
    fMain.close()
    fMain_lines[l_mod] = 'model:        1     %s.****.mesh\n' % nameCase
    # write all the fields
    for key, val in dictFields.items():
        if key not in  ('scalar', 'vector'):
            raise ValueError('keys fields accepted are scalar and vector')
        for field in val:
            fMain_lines.insert(l_var,
                    "{} per element:\t1\t{}\t{}.****.{}\n".format(key,
                        field, nameCase,field))
            l_var += 1
    # write modification
    f = open(newName, 'w')
    f.write(''.join(fMain_lines))
    f.close()
    # open again for adding the time values
    fMain = open(newName)
    fMain_lines = fMain.readlines()
    l_t, l_step = None, None
    for i, li in enumerate(fMain_lines):
        # print('li', li.split(' '))
        if li.split(' ')[0] =='time' and li.split(' ')[1] =='values:\n':
            # print('youpi, time values', li)
            l_t = i + 1
        if li.split(' ')[0] =='number' and li.split(' ')[1] =='of':
            # print('youpi, step', li)
            l_step = i
    if l_t is None or l_step is None: raise ValueError('l_t or l_step is None')
    fMain.close()

    fMain_lines[l_step] = 'number of steps:\t{}\n'.format(iteMax)
    for i in range (iteMax):
        try:
            fMain_lines[l_t] = "{:.6e}\n".format(t_gene[i])
        except IndexError:
            fMain_lines.append("{:.6e}\n".format(t_gene[i]))
        l_t += 1

    # write ones again modification
    f = open(newName, 'w')
    f.write(''.join(fMain_lines))
    f.close()
    return


def caseDefinitionEnd(t_gene, path_abs, nameCase, unsteady,
     dictFields={'scalar': ['p', 'nu'], 'vector': ['U']}):
    """ Creation of directory gathering all CFD simulation for each morphological
    updates in EnSight format. User can choose specific fields to save.

    Parameters
    ----------
    t_gene : list
        List of time associated with generation
    path_abs : str
        Absolute path of the case
    nameCase : str
        Name of the case which will be the name of the directory
    unsteady : bool
        CFD is unsteady ?
    dictFields : dict

    Returns
    -------
    path_abs/nameCase : directory
        Write a directory in Ensight format for visualisation of the simulation
        in 3D viewver as Paraview.
    """
    message("Build a EnSight case gathering all arterial generations")
    # standard dict
    dictFieldsSTD = {'scalar': ['p', 'nu'], 'vector': ['U']}
    if dictFields == dictFieldsSTD:
        # adding wall shear stress fields
        if unsteady:
            dictFields['scalar'] += ['wallShearStressMagMean',
                    'wallShearStressMeanMag']
        else:
            dictFields['scalar'] += ['wallShearStressMag']
        message('Default dictFields : {}', (dictFields,))
    else:
        message('Custom dictFields demanded : {}', (dictFields,))

    # path for my EnSight remodeling folder with all generation
    path_EnSight = path_abs + '/' + nameCase
    # print('self.path_EnSight', self.path_EnSight)

    # if case already here, rm it to build the new
    if os.path.exists(path_EnSight) :
        os.system('rm -rf {}'.format(path_EnSight))
    os.makedirs(path_EnSight)

    # at last remod we do not compute the cfd so -1
    iteMax = len(t_gene) - 1
    # settings in ***.case the main file readed by paraview
    # no dif for steady or unsteady file
    writeMainEnSight(t_gene, path_abs, path_EnSight, nameCase, dictFields)
    # print('t_gene', t_gene)
    dataToGet = ['mesh']
    for key, val in dictFields.items():
        dataToGet += val
    # print('dataToGet', dataToGet)
    # loop for each CFD simu, i.e. hemodynamic configuration
    for i in range(iteMax):
        # finding the right name genei_*****
        path_List = glob.glob(path_abs + '/gene{}_*'.format(i))
        # print('path_List', path_List)
        if len(path_List) > 1 or path_List == []:
            warn('Error, More than 1 path or no gene{}_*'.format(i))
            break
        else:
            path_i = path_List[0]
        path_CFD = path_i + '/EnSight/'
        for name in  os.listdir(path_CFD):
            path_name = path_CFD + name
            # print('path_name', path_name)
            if (i<10):
                for key in dataToGet:
                    # print('name', name)
                    # print('key', key)
                    if name.endswith(key):
                        # print('yes the key is ok')
                        newN = '{}/{}.000{}.{}'.format(path_EnSight, nameCase,
                                i, key)
                        shutil.copy2(path_name, newN)
            elif (i<100):
                for key in dataToGet:
                    # print('name', name)
                    # print('key', key)
                    if name.endswith(key):
                        # print('yes the key is ok')
                        newN = '{}/{}.00{}.{}'.format(path_EnSight, nameCase,
                                i, key)
                        shutil.copy2(path_name, newN)
                                                     # self.nameCase, i, key))
            elif (i<1000):
                for key in dataToGet:
                    # print('name', name)
                    # print('key', key)
                    if name.endswith(key):
                        # print('yes the key is ok')
                        newN = '{}/{}.0{}.{}'.format(path_EnSight, nameCase,
                                i, key)
                        shutil.copy2(path_name, newN)
    return


def domainCutOff(iCPU, NCPU, Nx):
    """ Compute the intervall handled by a given CPU ``iCPU``.
    
    Parameters
    ----------
    iCPU : int
        the current cpu number
    NCPU : int
        Total number of CPU used
    Nx : int
        Number of intervall
    Returns
    -------
    Nx_iCPU : int
        Number of intervall for the ieme cpu
    k_iCPU : int
        The index where start the domain of the ieme cpu
    """
    Nx_iCPU = Nx//NCPU
    reste = Nx % NCPU
    if(iCPU<reste):
        Nx_iCPU = Nx_iCPU + 1
        k_iCPU = Nx_iCPU*iCPU
    else:
        k_iCPU = (Nx_iCPU+1)*(reste) + Nx_iCPU * (iCPU-reste)

    return Nx_iCPU, k_iCPU



def runSA(myTuple):
    """ Run SA in parallel with in house
            parallelization using multiprocessing

    Parameters
    ----------
    myTuple : tuple
        Tuple of the ieme cpu, iCPU, the starting index ,k_iCPU, the lenght ,
        Nx_iCPU, the samples ,samples, and the sample name ,problem_name.

    Returns
    -------
    output : np.ndarray of shape (Nx_iCPU, Nout + 1)
        array storing idx of sample run (first column of array), and output at
        t=tf
        Nout nbr of output saved
            as 1 (idx of sample) + 23 (variable) + 1 (radius) = 25
    """
    Neq = 23 # nbr of variable in the system of ODEs, DDEs
    Nout = Neq + 1 # plus the radius
    (iCPU, idxToRun, samples, problem_name,
            problem, Nsave, Nout, integr) = myTuple
    tf, Nt = integr
    if Nt == 0:
        output = np.zeros((len(idxToRun), Nout))
    else:
        time = np.linspace(0, tf, Nt)
        output = np.zeros((len(idxToRun), Nout, Nt))
    p = 0
    print('integr', integr, 'len(idxToRun)', len(idxToRun), idxToRun)
    for idx in idxToRun:
        # bool var to rm the sample dir if everything goes well
        ok = True
        # print('*************\niCPU {} idx {} sample_{}'.format(iCPU, idx, idx))
        sample_i = samples[idx]
        os.system('cp -r Template sample_{}'.format(idx))
        with open('sample_{}/run.ini'.format(idx), 'r') as f:
            f_lines = f.readlines()

            # # balayer les ligne a la recherche du mot cle teste
            for j, lj in enumerate(f_lines):
                if lj.strip().startswith('tf'):
                    f_lines[j] = "tf = {}\n".format(tf)
                    # print('f_lines[j]', f_lines[j])
                if len(lj.strip()) > 1:
                    test_lj = [lj.split()[0] == key_i
                            for key_i in problem['names']]
                    if any(test_lj):
                        theValue = sample_i[test_lj]
                        key = np.asarray(problem['names'])[test_lj]
                        if len(sample_i[test_lj]) > 1:
                            print('lj', lj)
                            raise ValueError('len > 1', sample_i[test_lj])
                        if key =="pEq":
                            f_lines[j] = "{} = {}\n".format(key[0],
                                    theValue[0])
                            f_lines[j+1] = "aEq = {}\n".format(theValue[0])

                        else:
                            f_lines[j] = "{} = {}\n".format(key[0],
                                    theValue[0])

        f = open('sample_{}/run.ini'.format(idx), 'w')
        f.write(''.join(f_lines))
        f.close()

        os.chdir('sample_{}/'.format(idx))
        os.system('./AllRun')
        # get the desired data
        try:
            if Nt == 0:
                y_nd_i = np.load("allGenes/tissueGrowth/Pop/y_nd.npy")
                R_nd_i = np.load("allGenes/tissueGrowth/Pop/R_gr.npy")
                output[p,:-1] = y_nd_i[:,-1]
                output[p,-1] = R_nd_i[-1]
            else:
                t_num = np.load("allGenes/tissueGrowth/global/t_d.npy")
                names = glob.glob('*.pkl')
                if len(names) > 1: raise ValueError("len(names) > 1")
                obj_cp = load_obj('.', names[0])
                sol = load_obj('allGenes/tissueGrowth/', 'sol.pkl')

                if sol.t_events[-1].size > 0:
                    # print('Occlusion !!!!!!!!!')
                    # raise ValueError('Occlusion !!!!!!!!!')
                    t_occ_nd = np.min(sol.t_events[-1])
                    time_nd = time / obj_cp.myTG.T_d
                    t_idx = np.argwhere(time_nd < t_occ_nd)[:,0]
                    t_idx_occ = np.argwhere(time_nd > t_occ_nd)[:,0]
                    # normal filling 
                    output[p,:-1,t_idx] = sol.sol(time_nd[t_idx]).T
                    # as occlusion, fill with last value at t = t_occ which is
                    output[p,:-1,t_idx_occ] = sol.sol(t_occ_nd).T
                else:
                    output[p,:-1,:] = sol.sol(time / obj_cp.myTG.T_d)
                # calcul radius
                crit = False
                (ePlus, e_i, e_m,
                    R_gr, Vf, Vs) = calculVolumes_1D(output[p,:-1],
                                            obj_cp.myTG.set_params,
                                            obj_cp.myTG.V_Oi, obj_cp.myTG.V_Om,
                                            None, None, crit)
                output[p,-1,:] = R_gr

        except (ValueError, FileNotFoundError) as err:
            ok = False
            print("ERROR DETECTED IN PARALLEL JOB")
            print(err)
            print("sample_{}".format(idx))
            print('\nPRINT THE LAST 40 LINES OF /log.jansen*'.format(idx))
            print(os.system("tail -n40 log.jansen*".format(idx)))
            raise ValueError("ERROR DETECTED IN PARALLEL JOB")

        # test if error
        os.chdir('../')
        if ok:
            os.system('rm -rf sample_{}'.format(idx))
        # print('p =', p, Nsave)
        if p % Nsave == 0 and p > 0:
            np.save('inRun_cpu{}_{}_output'.format(
                iCPU, problem_name), output)
        p += 1
    # print("output.shape", output.shape,
            # "idxToRun[:,np.newaxis].shape", idxToRun[:,np.newaxis].shape)
    # return np.concatenate((idxToRun[:,np.newaxis], output
    return output

